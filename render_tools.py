# -*- coding: utf-8 -*-

"""Настройки рендера, рендер, заливка рендера в версию. """

import os
import shutil
import logging

import bpy


try:
    import server_connect as srvconn
    import path_utils
    import settings as global_settings
except:
    from . import server_connect as srvconn
    from . import path_utils
    from . import settings as global_settings


logger=logging.getLogger(__name__)

LIGHGTROUPSEPARATOR="_lightgroup_"


def _get_lightgroup_name(light_ob):
    """Возвращает имя Light группы для данного светильника.
     
    Parameters
    ----------
    light_ob : bpy.types.Object
        светильник

    Returns
    ---------
    str
        имя лайт группы или ""
    """
    name=''
    if LIGHGTROUPSEPARATOR in light_ob.name:
        try:
            name=light_ob.name.split(LIGHGTROUPSEPARATOR)[1]
        except:
            pass
    return name


def _create_light_groups(context):
    """Создаёт лайтгруппы для светильников и раскладывает их по ним.
    
    .. note: имя светильника [уникальная часть имени][:attr:`LIGHGTROUPSEPARATOR`][имя лайт группы]
    """
    for ob in bpy.data.objects:
        #(get ob type)
        if ob.type!="LIGHT":
            continue
        #(get lg name)
        light_group_name=_get_lightgroup_name(ob)
        if not light_group_name:
            continue
        #(greate lightgroup)
        for l in context.scene.view_layers:
            if not light_group_name in l.lightgroups:
                l.lightgroups.add(name=light_group_name)
        #(set lightgroup)
        ob.lightgroup=light_group_name


def _create_aovs(context):
    """Создаёт AOV каналы для всех ``OUTPUT_AOV`` нод в сцене. Во всех рендер слоях."""
    #(get aov names)
    aov_names=set()
    for m in bpy.data.materials:
        if m.node_tree:
            for n in m.node_tree.nodes:
                if n.type=="OUTPUT_AOV":
                    if n.name:
                        aov_names.add(n.name)

    #(create aovs)
    for l in context.scene.view_layers:
        for name in aov_names:
            if not name in l.aovs:
                aov=l.aovs.add()
                aov.name=name



def _set_render_folder(task, create=True):
    """Создание при отсутствии директории для рендеров в папке задачи. """
    # b,task_folder=path_utils.get_task_folder_path(task)
    # if b:
    #     render_folder=os.path.join(task_folder, path_utils.RENDER_FOLDER)
    #     if create:
    #         return path_utils._create_path(render_folder, create)
    #     else:
    #         return(b,render_folder)
    # else:
    #     return(b,task_folder)
    return path_utils.get_internal_task_folder_path(task, path_utils.RENDER_FOLDER, create=create)


def clean_render_folder(task):
    """Очистка директории с рендерами. """
    b,path=_set_render_folder(task, create=True)
    if b:
        shutil.rmtree(path)
    return path_utils._create_path(path, True)


def _set_global_render_settings(context):
    """Применени настроек рендера из параметров проекта.

    ``width``, ``heith``, ``fps``, ``output_path``.
    """
    bpy.context.scene.render.resolution_x=int(float(os.environ["CEREBRO_B3D_CURRENT_PROJECT_WIDTH"]))
    bpy.context.scene.render.resolution_y=int(float(os.environ["CEREBRO_B3D_CURRENT_PROJECT_HEIGHT"]))
    bpy.context.scene.render.fps=int(float(os.environ["CEREBRO_B3D_CURRENT_PROJECT_FPS"]))
    bpy.context.scene.render.fps_base=1
    bpy.context.scene.render.filepath=f'//{path_utils.RENDER_FOLDER}/'

    if context.scene.camera:
        if os.environ.get("CEREBRO_B3D_CLIP_STARTS"):
            context.scene.camera.data.clip_start=float(os.environ.get("CEREBRO_B3D_CLIP_STARTS"))
        if os.environ.get("CEREBRO_B3D_CLIP_END"):
            context.scene.camera.data.clip_end=float(os.environ.get("CEREBRO_B3D_CLIP_END"))


def _set_file_output_path(context, task):
    """Формирование путей в нодах ``OUTPUT_FILE``

    Сохранение текукщих состояний в словарь {имя ноды : путь}
    """
    # srvconn._get_future_render_commit_num(task)
    srvconn._get_future_commit_num(task)

    if context.scene.node_tree and context.scene.node_tree.nodes:
        for n in context.scene.node_tree.nodes:
            if n.type=="OUTPUT_FILE":
                if n.format.file_format=='OPEN_EXR_MULTILAYER':
                    # n.base_path=f"//{path_utils.RENDER_FOLDER}/{os.environ['CEREBRO_B3D_CURRENT_ASSET_NAME']}_v{G.future_render_commit_num}_{(n.label.replace(' ', '_')+'_') if n.label else ''}"
                    n.base_path=f"//{path_utils.RENDER_FOLDER}/v{G.future_commit_num}/{os.environ['CEREBRO_B3D_CURRENT_ASSET_NAME']}_v{G.future_commit_num}_{(n.label.replace(' ', '_')+'_') if n.label else ''}#####"
                else:
                    n.base_path=f"//{path_utils.RENDER_FOLDER}/v{G.future_commit_num}/"
                    try:
                        n.file_slots[0].path=f"{os.environ['CEREBRO_B3D_CURRENT_ASSET_NAME']}_v{G.future_commit_num}_{(n.label.replace(' ', '_')+'_') if n.label else ''}#####"
                    except:
                        pass


def _set_scene_timing(context):
    """Выставление тайминга сцены по параметрам шота """
    if os.environ.get('CEREBRO_B3D_CURRENT_PROJECT_FSTART'):
        bpy.context.scene.frame_start=int(float(os.environ.get('CEREBRO_B3D_CURRENT_PROJECT_FSTART')))
    if os.environ.get('CEREBRO_B3D_SCENE_FEND'):
        bpy.context.scene.frame_end=int(float(os.environ.get('CEREBRO_B3D_SCENE_FEND')))


def _set_simplify_to_default(context):
    """Simplify до настроект по умолчанию. """
    context.scene.render.simplify_subdivision = 0
    context.scene.render.simplify_child_particles = 1
    context.scene.render.simplify_volumes = 1
    context.scene.render.simplify_subdivision_render = 6
    context.scene.render.simplify_child_particles_render = 1
    try:
        # context.scene.cycles.texture_limit = 'OFF'
        context.scene.cycles.texture_limit = '1024'
        context.scene.cycles.texture_limit_render = 'OFF'
    except:
        pass


def _lock_camera_to_view(context):
    for area in context.screen.areas:
        if area.type == 'VIEW_3D':
            for space in area.spaces:
                if space.type == 'VIEW_3D':
                    space.lock_camera = False


def settings(context, task):
    """
    Выставление настроек + тайминг.

    Parameters
    ----------
    context : bpy.types.context
        context
    task : list
        task

    Returns
    -------
    None
    """
    _set_render_folder(task)
    _set_global_render_settings(context)
    _set_simplify_to_default(context)
    _set_file_output_path(context, task)
    _set_scene_timing(context)
    _create_light_groups(context)
    _create_aovs(context)
    context.scene.tool_settings.use_keyframe_insert_auto = False
    _lock_camera_to_view(context)


def render_evee(context, task, test=True, render=True):
    """Запуск рендера

    Parameters
    ----------
    context : bpy.types.context
        context
    task : list
        task
    test : bool optional default=True
        Если True - тайминг сцены выставляться не будет.

    Returns
    -------
    None
    """
    _set_render_folder(task)
    _set_global_render_settings(context)
    _set_file_output_path(context, task)

    if not test:
        _set_scene_timing(context)

    #(hide нерендерибельное)
    context.space_data.overlay.show_overlays = False

    #(render)
    if render:
        bpy.ops.render.opengl(animation=True, write_still=True)


def _get_parent_of_collections():
    """возвращает словарь, где ключ это коллекция, а значение - родительская для неё коллекция. """
    parent = dict()
    for c in bpy.data.collections:
        parent[c] = None
    for c in bpy.data.collections:
        for ch in c.children:
            parent[ch] = c
    return parent


def _is_rig_collection(c):
    """True - если коллекция рига """
    if c.name.split('.')[0].endswith('_rig'):
        return True


def _get_rig_collection(rig, parent=None):
    """возвращает коллекцию рига. """
    if not parent:
        parent=_get_parent_of_collections()
    #()
    rig_collection=None
    #()
    if rig.users_collection:
        for c in rig.users_collection:
            if _is_rig_collection(c):
                rig_collection=c
                break
            elif _is_rig_collection(parent[c]):
                rig_collection=parent[c]
                break
    return rig_collection


def objects_to_render_collection(context):
    """
    Собирают коллекции выбранных ригов в коллекцию :attr:`settings.TO_RENDER_COLLECTION`
    """
    #(get TO_RENDER_COLLECTION)
    if global_settings.TO_RENDER_COLLECTION in bpy.data.collections:
        to_render_collection=bpy.data.collections[global_settings.TO_RENDER_COLLECTION]
    else:
        to_render_collection=bpy.data.collections.new(global_settings.TO_RENDER_COLLECTION)
        context.scene.collection.children.link(to_render_collection)

    parent=_get_parent_of_collections()
    #
    for ob in context.selected_objects:
        if ob.type=="ARMATURE":
            collection=_get_rig_collection(ob, parent=parent)
            logger.debug(f"{ob.name} - {collection.name}")
            to_render_collection.children.link(collection)

    return (True, "Ok!")


class CEREBRO_objects_to_render_collection(bpy.types.Operator):
    """
    команда запуска: bpy.ops.cerebro.objects_to_render_collection()
    """
    bl_idname = "cerebro.objects_to_render_collection"
    bl_label = "Objects to render collection"

    def execute(self, context):
        # (1)
        b,r=objects_to_render_collection(context)
        if not b:
            self.report({'WARNING'}, r)
            return{'FINISHED'}
        # (end)
        self.report({'INFO'}, "Ok!")
        return{'FINISHED'}