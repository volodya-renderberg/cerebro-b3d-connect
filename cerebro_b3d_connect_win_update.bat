@echo off
echo Start of Ftrack_b3d_connect update ...

set /p bversion=Input version of blender[2.93]:

if "%bversion%" == "" (
set bversion=2.93
)

echo version - %bversion%

set addons_dir=%appdata%\Blender Foundation\Blender\%bversion%\scripts\addons\cerebro-b3d-connect

cd "%addons_dir%"

git pull origin main

echo update is complete.

pause