# -*- coding: utf-8 -*-

"""Локальная организация работы с ассетами. Ведение журналов. Рабочие и бекап версии. """

import os
import json
import logging
import traceback
import datetime
import webbrowser
import time
import bpy
import threading
import tempfile
import subprocess
import platform
import shutil
from zipfile import ZipFile
import ssl
from urllib.request import urlretrieve
import csv

from pycerebro import dbtypes

try:
    import aliases
    import path_utils
    import settings
    import db
    import hooks
    import server_connect as srvconn
except:
    from . import aliases
    from . import path_utils
    from . import settings
    from . import db
    from . import hooks
    from . import server_connect as srvconn


logger=logging.getLogger(__name__)


FOLDERS={
    "common":("versions", ".backup", "meta"),
    "AssetModel":("versions", ".backup", path_utils.TEXTURES_FOLDER, "meta"),
    "Location":("versions", ".backup", path_utils.TEXTURES_FOLDER, "meta"),
}
"""Состав директорий в папке задачи, по типам ассетов (parent.object_type.name). """


EXCLUDED_DIRECTORIES=[".backup", "versions"]
"""Имена директорий, которые не загружаются на Ftrack. """

SUPPORTED_VIDEO_EXTENSIONS=[
        ".mp4",
        ".mov"
    ]
"""Форматы видеофайлов поддерживаемые для загрузки аниматика. """

ORDER_OF_ENTITIES=("Episode", "Sequence", "Shot")


def _deselect_all():
    for ob in bpy.data.objects:
        try:
            ob.select_set(False)
        except:
            pass

def _clean_unuse_data_blocks():
    for block in bpy.data.meshes:
        if block.users == 0 or block.name.endswith("_deleted"):
            bpy.data.meshes.remove(block)

    for block in bpy.data.materials:
        if block.users == 0 or block.name.endswith("_deleted"):
            bpy.data.materials.remove(block)

    for block in bpy.data.textures:
        if block.users == 0 or block.name.endswith("_deleted"):
            bpy.data.textures.remove(block)

    for block in bpy.data.images:
        if block.users == 0 or block.name.endswith("_deleted"):
            bpy.data.images.remove(block)
            
    for block in bpy.data.curves:
        if block.users == 0 or block.name.endswith("_deleted"):
            bpy.data.curves.remove(block)


def _remove_collection(collection_name):
    """Полное удаление коллекции со всем содержимым. При наличии. При отсутствии ничего. """
    if collection_name in bpy.data.collections:
        #
        c=bpy.data.collections[collection_name]
        #
        for ob in c.all_objects:
            if ob.animation_data:
                action=ob.animation_data.action
                action.name=f"{action.name}_deleted"
            bpy.data.objects.remove(ob)
        for ch in c.children_recursive:
            bpy.data.collections.remove(ch)
        bpy.data.collections.remove(c)
        _clean_unuse_data_blocks()


def __test_is_video(filepath) -> bool:
    """Является ли файл видеофайлом. """
    if os.path.splitext(filepath)[1].lower() in SUPPORTED_VIDEO_EXTENSIONS:
        return True
    else:
        print(os.path.splitext(filepath)[1].lower())
        return False


def _create_shot_camera(context):
    """Создаёт камеру и назначает текущей ``Shot_camera``

    Returns
    -------
    camera
    """
    camera_data=bpy.data.cameras.new('Shot_camera')
    camera=bpy.data.objects.new("Shot_camera", camera_data)
    context.scene.collection.objects.link(camera)
    bpy.context.scene.camera=camera

    return camera


def _valid_hide_visible(ob):
    """Возвращает True если объект выделяем и виден. """
    # if any((ob.hide_select, ob.hide_viewport)):
    #     # logger.info(ob.name)
    #     return False
    # if ob.parent:
    #     return(_valid_hide_visible(ob.parent))
    # else:
    #     logger.info(f"TRUE - {ob.name}")
    #     return True
    if not ob in bpy.context.selectable_objects:
        return False
    if not ob in bpy.context.visible_objects:
        return False
    return True


def _get_sequencer(context):
    """Делает проверку на наличие и возвращает секвенсор.

    Parameters
    ----------
    context : bpy.context
        КОнтекст блендера

    Returns
    -------
    tuple
        (True, context) или (False, comment)
    """
    area=None
    for area in context.window.workspace.screens[0].areas:
        if area.type == "SEQUENCE_EDITOR":
            break
    if not area:
        return(False, "No found SEQUENCE_EDITOR!")
    sequencer = context.scene.sequence_editor
    return(True, sequencer)


def _get_scene_sequence(sequencer, channel=1):
    """Возвращает при наличии секвенцию 1 канала, ессли её тип 'SCENE', или None

    Parameters
    ----------
    sequencer : bpy.types.SequenceEditor
        SequenceEditor
    channel : int optional
        номер канала
    """
    for seq in sequencer.sequences:
        if seq.channel==channel and seq.type=='SCENE':
            return seq


def _get_sorted_timeline_markers(context):
    """Возвращает отсортированный по фреймам список маркеров сцены. 
    
    Returns
    -------
    list
        Список маркеров.
    """
    markers=context.scene.timeline_markers
    return sorted(markers, key=lambda m: m.frame)


def _get_selected_shots(context):
    """
    Возвращает имена выделенных секвенций шотов в секвенсоре.
    * Игнорирует аниматик,
    * Преобразует ``.sound`` и ``.review`` в шот,
    * Удаляет совпадения имён.
    """
    sequences=list()
    for seq in bpy.context.selected_sequences:
        if not seq.name.startswith("Shot"):
            continue
        else:
            # sequences.append(seq.name.replace('.sound', ''))
            sequences.append(seq.name.split('.')[0])
    set_seq=set(sequences)
    return sorted(list(set_seq))


def _get_all_shots(context):
    """
    Возвращает имена всех шотов в секвенсоре.
    * Игнорирует аниматик,
    * Преобразует ``.sound`` и ``.review`` в шот,
    * Удаляет совпадения имён.
    """
    sequences=list()
    for seq in bpy.context.sequences:
        if not seq.name.startswith("Shot"):
            continue
        else:
            # sequences.append(seq.name.replace('.sound', ''))
            sequences.append(seq.name.split('.')[0])
    set_seq=set(sequences)
    return sorted(list(set_seq))


def get_selected_shot(context):
    """Возвращает выделенный в секвенсоре шот (что выделять звук или видео не важно).

    Returns
    -------
    tuple
        (True, [shot]) или (False, comment)
    """
    selected_shots=_get_selected_shots(context)
    if not selected_shots:
        return(False, "Шот не выбран!")
    if len(selected_shots)>1:
        return(False, "Выбрано больше одного шота!")
    shot_name=selected_shots[0]
    logger.info(shot_name)

    #(SESS)
    b,sess=srvconn.get_session()
    if not b:
        return(b,sess)

    try:
        b,shot=srvconn.get_asset_by_name(shot_name, sess=sess)
        # logger.info(shot)
        if not b:
            raise Exception(str(shot))
        elif not shot:
            raise Exception(f"Шот по имени \"{shot_name}\" не обнаружен на сервере!")
        else:
            return(True, shot)
    except Exception as e:
        print(f'{traceback.format_exc()}')
        return(False, e)


def _get_all_shots(context):
    """
    Возвращает имена всех секвенций шотов в секвенсоре.
    * Игнорирует аниматик,
    * Преобразует ``.sound`` и ``.review`` в шот,
    * Удаляет совпадения имён.
    """
    sequences=list()
    for seq in bpy.context.scene.sequence_editor.sequences_all:
        if not seq.name.startswith("Shot"):
            continue
        else:
            # sequences.append(seq.name.replace('.sound', ''))
            sequences.append(seq.name.split('.')[0])
    set_seq=set(sequences)
    return sorted(list(set_seq))


def _get_shot_sequences(context, shot_name):
    """Возвращает кортеж секвенций связанных с данным шотом.

    Parameters
    ----------
    shot_name : str
        Название шота.

    Returns
    -------
    tuple
        (sequence1, sequence2, ....).
    """
    data=list()
    for postfix in ('', '.review', '.sound'):
        if f"{shot_name}{postfix}" in context.scene.sequence_editor.sequences_all:
            data.append(context.scene.sequence_editor.sequences_all[f"{shot_name}{postfix}"])
    return(data)


def _download_last_assetversion_move(sess, shot_name, startswith="Review", shot_id=False):
    """Загрузка видеофайла последней версии превью или аниматика в tmp директорию.

    Parameters
    ----------
    sess : sess
        Сессия cerebro
    shot_name : str
        наименование шота, берётся из названия секвенции.
    startswith : str, optional
        Префикс имена ассета версии, Если не передавать - "Review\_".
    shot_id : int, optional
        ``shot_id`` проверяется прежде ``shot_name``, если его передавать то вместо ``shot_name`` можно передать любое значение.

    Returns
    -------
    tuple
        (download_path - путь загрузки мувки, version - {"message":message, "attachment":attachment})
    """

    #(shot_id)
    if not shot_id:
        b,shot=srvconn.get_asset_by_name(shot_name, sess=sess)
        if not b:
            raise Exception(str(shot))
        shot_id=shot[dbtypes.TASK_DATA_ID]

    #(animatic_ft)
    b,version=srvconn.get_reviews_list_of_asset(shot_id, last=True, startswith=startswith, sess=sess, close_session=False)
    if not b:
        raise Exception(str(version))
    #(-- download move file)
    b,download_path = srvconn.download_attachment(version["attachment"])
    if not b:
        raise Exception(str(download_path))

    return (download_path, version)


def _rig_collection_name(asset_name):
    """Возвращает имя коллекции рига, оно же и имя арматуры, по имени ассета. """
    return(f"{asset_name}_rig")


def check_file_structure(task) -> tuple:
    """Проверка наличия необходимой файловой структуры, в зависимости от типа ассета. """
    b,r = path_utils.get_task_folder_path(task)
    if not b:
        return(False,r)
    print(r)

    #() if not task['parent']['object_type']['name'] in FOLDERS:
    object_type=srvconn.get_asset_type_of_task(task)
    
    #()
    if not object_type in FOLDERS:
        asset_type = "common"
    else:
        # asset_type = task['parent']['object_type']['name']
        asset_type = object_type

    for folder in FOLDERS[asset_type]:
        path = os.path.join(r, folder)
        if not os.path.exists(path):
            try:
                os.makedirs(path)
            except Exception as e:
                print(f'exception from check_file_structure() - {e}')
                return (False, f"{e}")
    return(True, "Ok!")


def save_current_scene_as_top_version(task) -> tuple:
    """Сохранение текущей сцены в топ версию, с изменением статуса в ``in_progress``

    * дополнительно выполняет ``collect_textures``
    """

    b,r = path_utils.get_top_version_path_of_workfile(task)
    if not b:
        return(False,r)
    if bpy.data.filepath != r:
        try:
            bpy.ops.wm.save_as_mainfile(filepath = r, check_existing = True)
        except Exception as e:
            return(False, f'{e}')
        #(collect textures)
        b,r=collect_textures(task)
        if not b:
            return(b,r)
    # return(True, "Ok!")
    return hooks.post_open(task, False)


def open_scene_from_incoming_task(task, incoming_task):
    """Взятие в работу файла входящей задачи.

    * Копирование файла в топ версию.
    * Изменением статуса в ``in_progress``.
    * дополнительно выполняет ``collect_textures``

    Parameters
    ----------
    task : ftrack.Task
        Текущая задача.
    incoming_task : ftrack.Task
        Входящая задача, чей файл забираем.

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    b,path = path_utils.get_top_version_path_of_workfile(incoming_task)
    if not b:
        return(False,path)
    if not os.path.exists(path):
        return(False, f'{path} - File is missing!')
    else:
        # bpy.ops.wm.open_mainfile(filepath = path)
        try:
            bpy.ops.wm.open_mainfile(filepath = path)
        except Exception as e:
            print(f"{('WARNING!'*10,)}")
            print(f"working().open_scene_from_incoming_task()")
            print(f"{('-'*100,)}")
            print(f'{traceback.format_exc()}')
            # return(False, e)

    # return (True, f"Opened from {incoming_task['id']}")
    b,r=check_file_structure(task)
    if not b:
        return(b,r)
    return save_current_scene_as_top_version(task)


def _build_append_commit_data(context, commit_data, source_file_path, ignor_linked=False, clean_exists_action=False):
    """Применение анимации, и прочего из commit_data 
    
    Parameters
    ----------
    context : bpy.data.context
        context
    commit_data : dict
        Словарь из meta/comit_data.json
    source_file_path : str
        путь до бленд файла - источника анимации.
    ignor_linked : bool
        При ``True`` если в сцене есть экшн с аналогичным названием и имеющим ``library`` - его загрузка будет игнорирована.
    clean_exists_action : bool
        При ``True`` : Если имя экшена объекта совпадает с именем загружаемого экшена - существующий будет удалятся.
    """
    for name,d  in commit_data.items():
        if name in ["overide_doubles"]:
            continue
        if not name in context.scene.objects:
            logger.info(f"Object \"{name}\" no found!")
            continue
        ob=context.scene.objects[name]
        ob.rotation_mode=d[0]
        if d[0]=="QUATERNION":
            ob.rotation_quaternion=d[2]
        else:
            ob.rotation_euler=d[2]
        ob.location=d[1]
        ob.scale=d[3]
        if d[4]:
            exists=False
            library=False
            if d[4] in bpy.data.actions:
                a=bpy.data.actions[d[4]]
                exists=True
                if a.library:
                    library=True
            if exists and ignor_linked and library:
                pass
            else:
                if ob.animation_data and ob.animation_data.action and ob.animation_data.action.name==d[4]:
                    if clean_exists_action and not library:
                        bpy.data.actions.remove(a)
                b,r=append_action(context, source_file_path, d[4])
                if d[4] in bpy.data.actions:
                    action=bpy.data.actions[d[4]]
                    if not ob.animation_data:
                        ob.animation_data_create()
                    ob.animation_data.action=action
                else:
                    logger.warning(f"No action - {d[4]}")

        if len(d)>5 and ob.type=="ARMATURE":
            for b in ob.pose.bones:
                if b.name in d[5]:
                    if b.rotation_mode != d[5][b.name]:
                        b.rotation_mode=d[5][b.name]
        if len(d)>6 and ob.type=="ARMATURE":
            for b_name in d[6].keys():
                if b_name in ob.pose.bones:
                    for c_name in d[6][b_name]:
                        #(delete exists child of)
                        if clean_exists_action:
                            for con in ob.pose.bones[b_name].constraints:
                                if con.type=="CHILD_OF" and con.name==c_name:
                                    ob.pose.bones[b_name].constraints.remove(con)
                        #(new child of)
                        c=ob.pose.bones[b_name].constraints.new("CHILD_OF")
                        c.name=c_name
                        for at in d[6][b_name][c_name].keys():
                            if at=="target":
                                if d[6][b_name][c_name][at] in bpy.data.objects:
                                    c.target=bpy.data.objects[d[6][b_name][c_name][at]]
                            else:
                                setattr(c, at, d[6][b_name][c_name][at])
        if len(d)>7 and ob.type=="ARMATURE":
            for b_name in d[7].keys():
                if b_name in ob.pose.bones:
                    for c_name in d[7][b_name]:
                        #(delete exists follow path)
                        if clean_exists_action:
                            for con in ob.pose.bones[b_name].constraints:
                                if con.type=="FOLLOW_PATH" and con.name==c_name:
                                    ob.pose.bones[b_name].constraints.remove(con)
                        #(new follow path)
                        c=ob.pose.bones[b_name].constraints.new("FOLLOW_PATH")
                        c.name=c_name
                        for at in d[7][b_name][c_name].keys():
                            if at=="target":
                                if d[7][b_name][c_name][at] in bpy.data.objects:
                                    c.target=bpy.data.objects[d[7][b_name][c_name][at]]
                            else:
                                setattr(c, at, d[7][b_name][c_name][at])


        if ob.name=="Shot_camera":
            ob.data.lens=d[5]
            ob.data.lens_unit=d[6]


def build_scene(context, task, link, source_path=False, from_incoming=False):
    """Сборка сцены из компонент по входящим связям.

    Parameters
    ----------
    context : bpy.context
        context
    task : list
        task
    source_path : str
        путь к файлу из которого импортятся экшены и прочая дата. По умолчанию это топ версия.
    from_incoming : list optional
        Входящая задача, из которой идёт билд.

    Returns
    -------
    tuple
        (True, "ok") или (False, comment)
    """
    b,r=srvconn.get_downloadable_incoming_links()
    if not b:
        return(b,r)

    if not source_path:
        if from_incoming:
            b,source_file_path=path_utils.get_top_version_path_of_workfile(from_incoming)
            if not b:
                return(b,source_file_path)
        else:
            b,source_file_path=path_utils.get_top_version_path_of_workfile(task)
            if not b:
                return(b,source_file_path)
    else:
        source_file_path=source_path

    start_collections=list()
    for c in context.scene.collection.children:
        start_collections.append(c.name)

    #(ANIMATORS_COLLECTION)
    if not settings.ANIMATORS_COLLECTION in start_collections:
        try:
            with bpy.data.libraries.load(source_file_path, link=False) as (data_from, data_to):
                if settings.ANIMATORS_COLLECTION in data_from.collections:
                    data_to.collections.append(settings.ANIMATORS_COLLECTION)
            context.scene.collection.children.link(bpy.data.collections[settings.ANIMATORS_COLLECTION])
            start_collections.append(settings.ANIMATORS_COLLECTION)
        except:
            logger.error(f'{traceback.format_exc()}')
        #()
        if not settings.ANIMATORS_COLLECTION in bpy.data.collections:
            hooks.tools.make_excipients_collection(context, change_view_layer=False)
            start_collections.append(settings.ANIMATORS_COLLECTION)

    #(create camera)
    _create_shot_camera(context)

    #(link objects from incoming links)
    t_exists=list() # чтобы исключить повторное создание.
    parents=dict()
    for num,i in enumerate(r[0]):
        t=i[0]
        p=srvconn.get_parent_task(t)
        parents[t[dbtypes.TASK_DATA_ID]]=p
        logger.info(p[dbtypes.TASK_DATA_ACTIVITY_NAME])
        if p[dbtypes.TASK_DATA_ACTIVITY_NAME]=="Location":
            logger.info(f"LOCATION: {p[dbtypes.TASK_DATA_NAME]}")
            input_tasks=srvconn.get_direct_incoming_tasks(p)
            for key,item in input_tasks.items():
                logger.info(f"{key} - {item[dbtypes.TASK_DATA_NAME]}")
                in_task=srvconn.get_task_of_asset(item)
                if in_task:
                    t_exists.append(in_task[dbtypes.TASK_DATA_ID])
            # t_exists=t_exists+list(input_tasks.keys())
    logger.info(f"EXISTS: {t_exists}")

    for num,i in enumerate(r[0]):
        t=i[0]
        #
        if t[dbtypes.TASK_DATA_ID] in t_exists:
            continue
        else:
            t_exists.append(t[dbtypes.TASK_DATA_ID])
        #
        # p=srvconn.get_parent_task(t)
        p=parents[t[dbtypes.TASK_DATA_ID]]
        logger.info(f"{p[dbtypes.TASK_DATA_ACTIVITY_NAME]}-{p[dbtypes.TASK_DATA_NAME]}")
        if p[dbtypes.TASK_DATA_ACTIVITY_NAME] in ("Shot",):
            logger.info("continue")
            continue
        b,filepath=path_utils.get_top_version_path_of_workfile(t)
        if not b:
            return(b,filepath)
        
        #(link collection)
        print(filepath)
        collection=_rig_collection_name(srvconn.parent_name(t))
        # if collection=="Childrens_room_rig":
        #     continue
        b,c=link_collection(context, filepath, collection, link=link)
        if not b:
            logger.warning(c)
        else:
            if link:
                #(override)
                # rig=None
                # for ob in c.all_objects:
                #     if not ob.override_library and _valid_hide_visible(ob):
                #         rig=ob
                # try:
                #     context.view_layer.objects.active=rig
                #     rig.select_set(True)
                #     bpy.ops.object.make_override_library(collection=c.name)
                #     rig.select_set(False)
                # except:
                #     # logger.info(rig)
                #     print(f'{traceback.format_exc()}')
                b,r=owerride_collection(context, c)
                if not b:
                    return(b,r)

        #(WORLD)
        if p[dbtypes.TASK_DATA_ACTIVITY_NAME] in ("Location",):
            link_world(context, filepath, link=link)
            for w in bpy.data.worlds:
                w.use_fake_user = True

        # if num<2:
        #     break

    #(unlink no override objects)
    if link:
        for c in context.scene.collection.children:
            if c.name in start_collections:
                continue
            if not c.override_library:
                context.scene.collection.children.unlink(c)

    #(apply position and actions)

    #(-- read commit data)
    if from_incoming:
        b,meta=path_utils.get_internal_task_folder_path(from_incoming, "meta")
        if not b:
            raise Exception(meta)
    else:
        b,meta=path_utils.get_internal_task_folder_path(task, "meta")
        if not b:
            raise Exception(meta)
    file_path=os.path.join(meta, settings.COMMIT_DATA_FILE)
    commit_data=settings._read_setting_data(key=False, file_path=file_path)

    #(-- make duplicates of overrides)
    if "overide_doubles" in commit_data:
        for dupli_name in commit_data["overide_doubles"]:
            if not dupli_name in context.scene.objects:
                orig_name=dupli_name.split('.')[0]
                for ob in bpy.data.objects:
                    if ob.name==orig_name and ob.library:
                        logger.info(f"ORIG EXISTS {dupli_name}")
                        for c in ob.users_collection:
                            if c.library:
                                try:
                                    context.scene.collection.children.link(c)
                                    owerride_collection(context, c, renaming=False, method=2)
                                except Exception as e:
                                    logger.warning(e)
                                    # return(True, "Ok!")

    # return(True, "Ok!")

    #(-- positions/actions)
    _build_append_commit_data(context, commit_data, source_file_path)

    # for name,d  in commit_data.items():
    #     if name in ["overide_doubles"]:
    #         continue
    #     if not name in context.scene.objects:
    #         logger.info(f"Object \"{name}\" no found!")
    #         continue
    #     ob=context.scene.objects[name]
    #     ob.rotation_mode=d[0]
    #     if d[0]=="QUATERNION":
    #         ob.rotation_quaternion=d[2]
    #     else:
    #         ob.rotation_euler=d[2]
    #     ob.location=d[1]
    #     ob.scale=d[3]
    #     if d[4]:
    #         append_action(context, source_file_path, d[4])
    #         if d[4] in bpy.data.actions:
    #             action=bpy.data.actions[d[4]]
    #             if not ob.animation_data:
    #                 ob.animation_data_create()
    #             ob.animation_data.action=action
    #         else:
    #             logger.warning(f"No action - {d[4]}")
    #     if len(d)>5 and ob.type=="ARMATURE":
    #         for b in ob.pose.bones:
    #             if b.name in d[5]:
    #                 if b.rotation_mode != d[5][b.name]:
    #                     b.rotation_mode=d[5][b.name]
    #     if len(d)>6 and ob.type=="ARMATURE":
    #         for b_name in d[6].keys():
    #             if b_name in ob.pose.bones:
    #                 for c_name in d[6][b_name]:
    #                     c=ob.pose.bones[b_name].constraints.new("CHILD_OF")
    #                     c.name=c_name
    #                     for at in d[6][b_name][c_name].keys():
    #                         if at=="target":
    #                             if d[6][b_name][c_name][at] in bpy.data.objects:
    #                                 c.target=bpy.data.objects[d[6][b_name][c_name][at]]
    #                         else:
    #                             setattr(c, at, d[6][b_name][c_name][at])

    #     if ob.name=="Shot_camera":
    #         ob.data.lens=d[5]
    #         ob.data.lens_unit=d[6]

    #(rename library)
    for l in bpy.data.libraries:
        if os.environ["CEREBRO_B3D_CURRENT_TASK_ID"] in l.filepath:
            l.filepath="//temp_librari.blend"

    # print(commit_data)

    return (True, "ok")


def open_task(task, look=False) -> tuple:
    """Открытие top work версии. 

    .. note:: Если look=True то статус задачи меняться не будет.
    """
    b,path = path_utils.get_top_version_path_of_workfile(task)
    if not b:
        return(False,path)
    if not os.path.exists(path):
        return(False, 'File is missing!')
    else:
        bpy.ops.wm.open_mainfile(filepath = path)
        return hooks.post_open(task, look)


def open_version(task, version, look=False) -> tuple:
    """Открытие work версии по номеру.

    Parameters
    ----------
    task : list
        task
    version : int
        номер локальной версии
    look : bool
        Если look=True то статус задачи меняться не будет.

    Returns
    -------
    tuple
        (True, Ok) или (False, comment)
    """
    b,tpath= path_utils.get_top_version_path_of_workfile(task)
    if not b:
        return(False,tpath)

    b,vpath = path_utils.get_version_path_of_workfile(task, version)
    if not b:
        return(False,vpath)
    if not os.path.exists(vpath):
        return(False, 'File is missing!')
    else:
        #(old)
        # bpy.ops.wm.open_mainfile(filepath = vpath)
        # bpy.ops.wm.save_as_mainfile(filepath = tpath, check_existing = True)
        #(new)
        shutil.copyfile(vpath, tpath)
        bpy.ops.wm.open_mainfile(filepath = tpath)
        return hooks.post_open(task, look)


def lag_test_of_version(task) -> tuple:
    """
    Тест отставания локальной версии (выбор последней по дате между созданной и загруженной) от последней версии на сервере.

    Parameters
    ----------
    task : task
        Объект задачи.
    
    Returns
    -------
    tuple
        (True, num: int - количество опережающих версий, 0 - значит версия свежая)  или (False, comment)
    """
    b1,r1=srvconn.get_versions_list(task_id=task[dbtypes.TASK_DATA_ID], startswith="Commit")
    b2,r2=db.get_latest_work_version(task)
    b3,r3=db.get_latest_loaded_version(task)

    if not all((b1,b2,b3)):
        return(False, (r1,r2,r3))

    r1=list(r1[0])
    if not r1:
        lag=0
    if not any((r2, r3)):
        lag=len(r1)
    else:
        if not r2 and r3:
            version_id=dict(r3)["version_id"]
        elif not r3 and r2:
            version_id=dict(r2)["version_id"]
        else:
            d2=dict(r2)
            d3=dict(r3)
            if d2["version_id"] != d3["version_id"]:
                if d2["created"]>d3["created"]:
                    version_id=d2["version_id"]
                else:
                    version_id=d3["version_id"]
            else:
                version_id=d2["version_id"]

        r1.reverse()

        lag=0
        for item in r1:
            # if item['id']==version_id:
            if item['message'][dbtypes.MESSAGE_DATA_ID]==version_id:
                break
            else:
                lag+=1

    return(True, lag)


def push_render(context, task, description):
    """
    Выгрузка на сервер версии рендера.

    Пакуется директория задачи :attr:`path_utils.RENDER_FOLDER`
    
    Parameters
    ----------
    context : bpy.context
        context
    task : task
        Текущая задача.
    description : str
        Коментарий к версии.

    Returns
    -------
    tuple
        (True, ok) или (False, comment)
    """

    b,folder=path_utils.get_internal_task_folder_path(task, path_utils.RENDER_FOLDER, create=False)
    if not b:
        return(b,folder)
    b,zip_path=path_utils._pack_folder_to_zip(settings.RENDER_ZIP_NAME, folder, new_name=f"{os.environ['CEREBRO_B3D_CURRENT_ASSET_NAME']}_v{G.future_render_commit_num}_render_sequence")
    if not b:
        return(b,zip_path)

    return (True, zip_path)



def commit(context, task, description, status_name, push=True) -> tuple:
    """Создание рабочей версии локально и фоновая выгрузка версии на Фтрек.

    Parameters
    ----------
    context : bpy.context
        context
    task : task
        Текущая задача.
    description : str
        Коментарий к версии.
    status_name : str
        Статус назначаемый задаче.
    push : bool
        Выгружать или нет версию на сервер.

    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)
    """
    if not description:
        return(False, 'Description is a required parameter!')

    #(pre_commit)
    hooks.pre_commit(context, task, description, status_name)

    # SAVE FILE
    # -- определить номер новой локальной версии
    b,version = db.get_new_work_version(task)
    if not b:
        return(False, version)
    # -- path version
    b,path = path_utils.get_version_path_of_workfile(task, version)
    if not b:
        return(False, path)
    else:
        if not os.path.exists(os.path.dirname(path)):
            path_utils._create_path(os.path.dirname(path), True)
    # -- save file
    bpy.ops.wm.save_as_mainfile()
    try:
        # bpy.ops.wm.save_as_mainfile(filepath = path, check_existing = True, copy=True)
        shutil.copyfile(bpy.data.filepath, path)
    except Exception as e:
        return(False, f'{e}')
    # WRITE TO DB
    b,v = db.add_work_version(task, description)
    if not b:
        return(False, v)

    # ФОНОВАЯ ВЫГРУЗКА
    if push:
        t = threading.Thread(target=_load_commit_to_ftrack, args=(task, v, description, status_name), kwargs={})
        t.setDaemon(True)
        t.start()

    return(True, v)


def _load_commit_to_ftrack(task, version, description, status_name) -> tuple:
    """Выгрузка локальной версии на Ftrack. """
    print(f"_load_commit_to_ftrack({version})")

    # (0) get paths
    zip_dir = tempfile.mkdtemp()

    # (1) copy work file to zip_dir
    b,vpath = path_utils.get_version_path_of_workfile(task, version)
    if not b:
        print(False,vpath)
    ext=os.path.splitext(vpath)[1]
    # file_name = os.path.basename(vpath).split('_v')[0]
    # tpath = os.path.join(zip_dir, f"{file_name}{ext}")
    tpath = os.path.join(zip_dir, f"{path_utils._parent_name(task)}{ext}")
    shutil.copyfile(vpath, tpath)

    # (2) copy directories to zip_dir
    b,task_dir = path_utils.get_task_folder_path(task, create=False)
    if not b:
        print(False,task_dir)
    if not os.environ["CEREBRO_B3D_CURRENT_ASSET_TYPE"] in FOLDERS:
        asset_type = "common"
    else:
        asset_type = os.environ["CEREBRO_B3D_CURRENT_ASSET_TYPE"]

    for folder in FOLDERS[asset_type]:
        if folder in EXCLUDED_DIRECTORIES:
            continue
        from_path = os.path.join(task_dir, folder)
        to_path = os.path.join(zip_dir, folder)
        if os.path.exists(from_path):
            shutil.copytree(from_path, to_path)

    # (3) copy other files to zip_dir
    other_paths=[]
    # for f in os.listdir(task_dir):
    #     fpath=os.path.join(task_dir, f)
    #     if os.path.isfile(fpath) and __test_is_video(fpath) and f.startswith("Animatic_"):
    #         video_zip_path=os.path.join(zip_dir, f)
    #         shutil.copyfile(fpath, video_zip_path)
    #         other_paths.append(video_zip_path)
        
    # (1) make ZIP
    zip_name = 'task_data.zip'
    zip_path = os.path.join(zip_dir, zip_name)
    # (2)
    os.chdir(zip_dir)
    try:
        with ZipFile(zip_name, "w") as zf:
            for path in (tpath,*other_paths):
                if os.path.exists(path):
                    zf.write(os.path.basename(path))
                    # zf.write(path)
            for folder in FOLDERS[asset_type]:
                if os.path.exists(folder):
                    # pack_zip(folder, '')
                    for root, dirs, files in os.walk(folder):
                        for file in files:
                            if os.path.samefile(root, folder):
                                f_path=os.path.join(os.path.basename(folder), file)
                            else:
                                f_path=os.path.join(os.path.basename(folder), os.path.relpath(root, start=folder), file)
                            print(f'file: {f_path}')
                            zf.write(f_path)
                        for fld in dirs:
                            if os.path.samefile(root, folder):
                                d_path=os.path.join(os.path.basename(folder), fld)
                            else:
                                d_path=os.path.join(os.path.basename(folder), os.path.relpath(root, start=folder), fld)
                            print(f'folder: {d_path}')
                            zf.write(d_path)
    except Exception as e:
        print(e)

    # (3) Push to ftrack
    srvconn.push_version_to_cerebro(task, (zip_path,), version, description, status_name=status_name)


def playblast_to_review(task, description, path, status_name):
    # ФОНОВАЯ ВЫГРУЗКА
    # t = threading.Thread(target=_load_video_to_ftrack, args=(task, path, description, status_name), kwargs={})
    # t.setDaemon(True)
    # t.start()
    # return(True, "Ok!")
    if not description:
        return(False, 'Description is a required parameter!')
    return _load_video_to_ftrack(task, path, description, status_name)


def _load_video_to_ftrack(task, path, description, status_name, prefix="Review"):
    # return srvconn.push_video_to_ftrack(task, path, description, status_name=status_name, prefix=prefix)
    return srvconn.push_video_to_cerebro(task, path, description, prefix=prefix)


def download_version(task, version) -> tuple:
    """Загрузка версии с cerebro и создание локальной.

    Parameters
    ----------
    task : list
        Текущая задача.
    version : dict
        Словарь версии {"message": message, "attachment": attachment}

    Returns
    -------
    tuple
        (True, comment) или (False, comment)
    """
    # (1)
    b,r=check_file_structure(task)
    if not b:
        return(False,r)
    
    # (2) Пути
    # -- НОМЕР НОВОЙ ЛОКАЛЬНОЙ ВЕРСИИ
    b,vers = db.get_new_work_version(task)
    if not b:
        return(False, vers)
    # -- TASK FOLDER
    b,task_folder=path_utils.get_task_folder_path(task)
    if not b:
        return(False, task_folder)
    # -- VERSIONS PATH
    b,version_path=path_utils.get_version_path_of_workfile(task, vers)
    if not b:
        return(False, version_path)
    b,top_version_path=path_utils.get_top_version_path_of_workfile(task)
    if not b:
        return(False, top_version_path)
    else:
        top_version_name=os.path.basename(top_version_path)

    print(f"{vers}\n{task_folder}\n{version_path}\n{top_version_path}")
    # return (True, "Ok!")

    # (3) DOWNLOAD
    b,dowload_path=_download_task_data_component(version_id=version["message"][dbtypes.MESSAGE_DATA_ID])
    if not b:
        return(False, dowload_path)
    
    # (4) EXTRACK
    # -- to task_folder
    download_dir=os.path.dirname(dowload_path)
    with ZipFile(dowload_path) as myzip:
        members=myzip.namelist()
        # print(members)
        members.remove(top_version_name)
        # print(members)
        myzip.extractall(path=task_folder, members=members)
        myzip.extractall(path=download_dir, members=[top_version_name])
    # -- to version
    from_path = os.path.join(download_dir, top_version_name)
    shutil.copyfile(from_path, version_path)

    # (5) DB
    # -- work_version
    b,r=db.add_work_version(task, f"[{version['message'][dbtypes.MESSAGE_DATA_CREATOR_NAME ]}]: {version['message'][dbtypes.MESSAGE_DATA_TEXT]}")
    if not b:
        return(False, r)
    b,r = db.update_work_version(task, r, version['message'][dbtypes.MESSAGE_DATA_ID])
    if not b:
        return(False, r)
    # -- loaded_version
    b,r = db.exists_loaded_version(version)
    if not b:
        return(False, r)
    if r[0]=="missing":
        b,r=db.add_loaded_version(version)
        if not b:
            return(False, r)
    else:
        b,r=db.update_loaded_version(version)
        if not b:
            return(False, r)
    return(True, f'Download to "{vers}" version')


def download_incoming_task_data_component(task, version, url=False, sess=False) -> tuple:
    """Загрузка в топ версию компонента task_data, внесение записи в журнал загруженных компонент.

    Parameters
    ----------
    task : list, str, int
        Задача или ``id`` задачи.
    version : dict
        {"message":message, "attachment": attachment}.
    url : str optional
        Путь до компонента. Проверяется до проверки ``version_id``.
    
    Returns
    -------
    tuple
        (True, 'Ok') или (False, comment)
    """
    # (get task)
    timing = time.time()
    if isinstance(task, str) or isinstance(task, int):
        b,task=srvconn.get_task_by_id(int(task))
        if not b:
            return(b, task)
    # (0)
    b,r=check_file_structure(task)
    if not b:
        return(False,r)
    # (1) DOWNLOAD
    b,dowload_path=_download_task_data_component(url=url, version_id=version["message"][dbtypes.MESSAGE_DATA_ID], sess=sess)
    if not b:
        return(b,dowload_path)
    # (2) TASK_FOLDER
    b,task_folder=path_utils.get_task_folder_path(task)
    if not b:
        return(False, task_folder)
    # (3) EXTRAKT
    download_dir=os.path.dirname(dowload_path)
    with ZipFile(dowload_path) as myzip:
        myzip.extractall(path=task_folder)
    # (4) LOADED_VERSION
    b,r= db.update_loaded_version(version)
    if not b:
        return(b,r)
    # return(True, f"Loaded version({version["message"][dbtypes.MESSAGE_DATA_ID]}) of the: {task[dbtypes.TASK_DATA_PARENT_URL].split('/')[-2:][0]} - {task[dbtypes.TASK_DATA_NAME]} ({time.time() - timing} sec.)")
    return(True, "Ok!")


def _download_file_from_url(url, file_name):
    """
    Скачивание файла из *url* в темп с указанным именем файла.

    Parameters
    ----------
    url : str
        Путь для скачивания.
    file_name : str
        Имя для скачиваемого файла.

    Returns
    -------
    tuple
        (True, download_path) или (False, comment)
    """
    download_dir = tempfile.mkdtemp()
    path=os.path.join(download_dir, file_name)
    try:
        ssl._create_default_https_context = ssl._create_unverified_context
        urlretrieve(url, path)
    except Exception as e:
        return (False, f'download error: {str(e)}')

    return(True, path)


def _download_task_data_component(url=False, version_id=False, sess=False):
    """Скачивание компонента task_data версии задачи.
    
    Parameters
    ----------
    url : str optional
        Путь до компонента. Проверяется до проверки ``version_id``.
    version_id : str optional
        Загружаемая версия. Если не передавать - будет скачена последняя версия. Не имеет смысла если передан ``url``.
    sess : ftrack_api.Session optional

    Returns
    -------
    tuple
        (True, "path to task_data.zip") или (False, comment)
    """
    if url:
        return _download_file_from_url(url, f'{srvconn.TASK_DATA_COMPONENT}{srvconn.TASK_DATA_EXT}')
    else:
        return srvconn.download_attachment(G.ftrack_versions_dict[str(version_id)]["attachment"])


def collect_textures(task) -> tuple:
    """Сборка текстур в директорию :attr:`path_utils.TEXTURES_FOLDER`

    Parameters
    ----------
    task : ftrack.Task
        Текущая задача.
    
    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    # if os.environ['CEREBRO_B3D_CURRENT_ASSET_TYPE'] in ("Shot", "Location"):
    #     return (False, "Для данного типа ассета сбор текстур не производится.")
    b,textures=path_utils.get_folder_of_textures_path(task)
    if not b:
        return(b,textures)
    for image in bpy.data.images:
        # logger.debug(image.name)
        # if not image.has_data:
        #     logger.debug("not has_data")
        #     continue
        if image.library:
            logger.debug(f"{image.name} - exists library")
            continue
        origin_path=image.filepath_from_user()
        if os.path.dirname(origin_path) != textures:
            try:
                image.pack()
                image.unpack(method='WRITE_LOCAL')
                logger.debug(f"{image.name} - Collected image: {os.path.basename(origin_path)}")
            except Exception as e:
                print(f"Exception when collecting image: {origin_path}")
                print(e)

    return (True, "Ok!")


def get_collections_of_source(task) -> tuple:
    """Получение списка коллекций на загрузку. """
    b,filepath=path_utils.get_top_version_path_of_workfile(task)
    if not b:
        return(b,filepath)
    collections=[]
    if os.path.exists(filepath):
        with bpy.data.libraries.load(filepath, link=False) as (data_from, data_to):
            for name in data_from.collections:
                print(f"collection - {name}")
                collections.append(name)
    return(True, (collections, filepath))


def link_world(context, filepath: str, link=True):
    """Линкование World в сцену. 

    Parameters
    ----------
    context : bpy.context
        context
    filepath : str
        путь до бленд файла.

    Returns
    -------
    None
    """
    try:
        if os.path.exists(filepath):
            with bpy.data.libraries.load(filepath, link=link) as (data_from, data_to):
                for w in data_from.worlds:
                    data_to.worlds.append(w)
    except Exception as e:
        logger.warning(f'{traceback.format_exc()}')


def link_collection(context, filepath: str, collection: str, link=True, task_id=None) -> tuple:
    """Линкование коллекции в сцену. 

    Parameters
    ----------
    context : bpy.context
        context
    filepath : str
        путь до бленд файла.
    collection : str
        имя скачиваемой коллекции
    link : bool
        link or not link
    task_id : str
        ``id`` задачи, для определения родителя, для выявления доп действий: загрузка ``Word`` из локации.

    Returns
    -------
    tuple
        (True, bpy.types.Collection) или (False, comment)
    """
    #(with parent)
    if task_id and task_id in G.task_dict:
        #(--get parent)
        p=srvconn.get_parent_task(G.task_dict[task_id])
        #(--link world)
        if p[dbtypes.TASK_DATA_ACTIVITY_NAME] in ("Location",):
            link_world(context, filepath, link=link)
        #(-- -- fake users of world)
        for w in bpy.data.worlds:
            w.use_fake_user = True
    
    # bpy.ops.file.make_paths_absolute()
    if bpy.app.version_string.startswith("3."):
        try:
            try:
                bpy.ops.object.mode_set(mode='OBJECT')
            except:
                pass
            # logger.info(filepath)
            directory=f"{filepath}/Collection/"
            # logger.info(directory)
            filename=f"{collection}"
            # logger.info(filename)
            if link:
                bpy.ops.wm.link(filepath=filepath, directory=directory, filename=filename)
                for ob in bpy.data.objects:
                    if ob.name==collection and ob.type=="EMPTY":
                        return(True, ob)
            else:
                bpy.ops.wm.append(filepath=filepath, directory=directory, filename=filename)
                if collection in bpy.data.collections:
                    return(True, bpy.data.collections[collection])
            return(False, f"{collection} not found!")
        except Exception as e:
            print(f'{traceback.format_exc()}')
            return(False, str(e))
    else:
        try:
            with bpy.data.libraries.load(filepath, link=link) as (data_from, data_to):
                if collection in data_from.collections:
                    data_to.collections.append(collection)
            if collection in bpy.data.collections:
                context.scene.collection.children.link(bpy.data.collections[collection])
        except Exception as e:
            logger.warning(f'{traceback.format_exc()}')
            return(False, str(e))
        # bpy.ops.file.make_paths_relative()
        return(True, bpy.data.collections[collection]) 


def _unlink_no_owerride_collection(context, collection):
    """Unlink коллекции, если она не *Owerride*.

    Parameters
    -----------
    context : bpy.types.Context
        Context
    collection : str
        имя коллекции

    Returns
    --------
    None
    """
    for c in context.scene.collection.children:
        if c.name == collection:
            if not c.override_library:
                context.scene.collection.children.unlink(c)


def owerride_collection(context, collection, renaming=True, method=1):
    """Создание оверрайда из коллекции.

    Parameters
    ----------
    context : bpy.types.Context
        Context
    collection : bpy.types.Collection
        Коллекция (или EMPTY начиная с 3.3) бленд объект.
    renaming : bool optional
        Если ``True`` то имена созданных оверрайдов переименовываются с удалением автонумерации. (справедливо для блендера начиная с 3 серии.)

    Returns
    -------
    tuple
        (True, Ok!) или (False, comment)
    """
    _deselect_all()
    if bpy.app.version_string.startswith("3."):
        if method==1:
            name=collection.name
            collection.select_set(True)
            context.view_layer.objects.active=collection
            bpy.ops.object.make_override_library(collection=0)
            if renaming:
                for ob in bpy.data.objects:
                    if ob.name==f"{name}.001":
                        ob.name=name
                        break
        elif method==2:
            ov=collection.override_hierarchy_create(context.scene, context.view_layer)
            # for ob in ov.objects:
            #     if ob.type=="ARMATURE":
            #         context.view_layer.objects.active=ob
            #         ob.select_set(True)
            #         # bpy.ops.object.make_override_library(collection=0)
            #         bpy.ops.outliner.liboverride_troubleshoot_operation(type='OVERRIDE_LIBRARY_RESYNC_HIERARCHY', selection_set='SELECTED')
            context.scene.collection.children.unlink(collection)

        """
        collection.override_hierarchy_create(context.scene, context.view_layer)
        """
    else:
        rig=None
        for ob in collection.all_objects:
            if not ob.override_library and _valid_hide_visible(ob):
                rig=ob
        try:
            context.view_layer.objects.active=rig
            rig.select_set(True)
            bpy.ops.object.make_override_library(collection=collection.name)
            rig.select_set(False)
        except Exception as e:
            # logger.info(rig)
            print(f'{traceback.format_exc()}')
            return (False, f"{e}")

        _unlink_no_owerride_collection(context, collection.name)

    return (True, "Ok!")


def append_action(context, path, action, ignor_linked=False) -> tuple:
    """Линкование анимационных экшенов в сцену. 

    Parameters
    ----------
    context : bpy.context
        context
    path : str
        путь до бленд файла.
    action : str
        имя экшена.

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    if ignor_linked and action in bpy.data.actions:
        a=bpy.data.actions[action]
        if a.library:
            return(True, "IGNOR_LINKED")
    try:
        if path and os.path.exists(path):
            with bpy.data.libraries.load(path, link=False) as (data_from, data_to):
                if action in data_from.actions:
                    data_to.actions.append(action)
    except:
        logger.warning(f'{traceback.format_exc()}')

    # # filepath=f"{path}/Action/{action}"
    # filepath=os.path.join(path, "Action", action)
    # directory=f"{os.path.join(path, 'Action')}/"

    # bpy.ops.wm.append(
    #     filepath=filepath,
    #     filename=action,
    #     directory=directory)

    # logger.info(f"Append - {action}")

    return(True, "Ok!")


def open_images(application, filepath):
    if not os.path.exists(filepath):
        return(False, f"path not found - {filepath}")
    try:
        cmd = [application, filepath]
        print(f"*** {cmd}")
        subprocess.Popen(cmd)
    except Exception as e:
        print(e)
        return(False, str(e))
    
    return(True, 'Ok!')


def open_task_folder_in_filebrowser(task) -> tuple:
    """Открытие папки задачи в проводнике. """
    b,r=path_utils.get_task_folder_path(task)
    if not b:
        return(b,r)
    osname=platform.system()
    try:
        if osname == 'Windows':
            subprocess.Popen(['explorer', r])
        elif osname == "Linux":
            subprocess.Popen(['nautilus', r])
        elif osname == "Darwin":
            try:
                subprocess.Popen(['nautilus', r])
            except:
                subprocess.Popen(['open', r])
    except Exception as e:
        return(False, str(e))
    return(True, f"Is open: {r}")


def append_selected_link(context, ob):
    """Замена линкованного объекта, с удалением, на append. """
    if not ob.proxy:
        return(False, "No proxy")
    #(position)
    locations=ob.location[:]
    rotations=ob.rotation_euler[:]
    scales=ob.scale[:]
    ob_name=ob.proxy.name
    #(library)
    lib=None
    for l in bpy.data.libraries:
        if ob.proxy in l.users_id:
            lib=l
    if not lib:
        return(False, "No lib")
    path=lib.filepath
    #(get root collection of library)
    
    return(True, path)


def _get_entity_name_type(context) -> tuple:
    """Возвращает имя и тип сущности из (Episode, Sequence, Shot) по выбранной дорожке в **Sequence_editor**

    .. note:: Выбирать надо одну дорожку.

    Returns
    -------
    tuple
        (True, {"entity_name": entity_name, "entity_type": entity_type}) или (False, comment)
    """
    if not context.selected_sequences or len(context.selected_sequences) >1:
        return(False, "You have to choose ONE track")
    name=context.selected_sequences[0].name
    if name.endswith(".sound"):
        entity_name=name[:-6]
    else:
        entity_name=name
    if name.lower().startswith("episode"):
        entity_type="Episode"
    elif name.lower().startswith("sequence"):
        entity_type="Sequence"
    elif name.lower().startswith("shot"):
        entity_type="Shot"
    else:
        return(False, f'Undefined type of entity: "{name}"')
    
    return(True, {"entity_name": entity_name, "entity_type": entity_type})


def download_animatic(context, task, filepath):
    """
    * Загрузка видео файла аниматика из указанного пути в директорию задачи и размещение в ``sequence_editor`` на первых двух дорожках:
        * дорожка 1 - мувик, 
        * дорожка 2 - sound.
    * Установка тайминга сцены по диапазону аниматика.
    """
    #(TEST FILE)
    if not __test_is_video(filepath):
        return(False, f"The format of the selected file is not supported! Enum in: {str(SUPPORTED_VIDEO_EXTENSIONS)}")

    #(SCENE HIERARCHY)
    if context.scene.name==os.environ["CEREBRO_B3D_CURRENT_ASSET_NAME"]:
        pass
    elif len(bpy.data.scenes)==1 and context.scene.name != os.environ["CEREBRO_B3D_CURRENT_ASSET_NAME"]:
        context.scene.name=os.environ["CEREBRO_B3D_CURRENT_ASSET_NAME"]
    elif len(bpy.data.scenes)>1 and not os.environ["CEREBRO_B3D_CURRENT_ASSET_NAME"] in bpy.data.scenes:
        film_scene=bpy.data.scenes.new(os.environ["CEREBRO_B3D_CURRENT_ASSET_NAME"])
        context.window.scene = film_scene
    if not context.scene.sequence_editor:
        context.scene.sequence_editor_create()

    #(GET SEQUENCER)
    #( -- get sequencer)
    if 'Video Editing' in bpy.data.workspaces:
        bpy.context.window.workspace = bpy.data.workspaces['Video Editing']
    area=None
    for area in bpy.context.window.workspace.screens[0].areas:
        if area.type == "SEQUENCE_EDITOR":
            break
    if not area:
        return(False, "No found SEQUENCE_EDITOR!")
    sequencer = context.scene.sequence_editor

    #(COPY FILE)
    b,r=path_utils.get_task_folder_path(task)
    if not b:
        return(False, r)
    #(-- new path)
    animatic_path=os.path.join(r, f'{path_utils.PREFIX_ANIMATIC}{os.environ["CEREBRO_B3D_CURRENT_ASSET_NAME"]}{os.path.splitext(filepath)[1]}')
    #(-- copy)
    shutil.copyfile(filepath, animatic_path)

    #(ANIMATIC TO SEQUENCER)
    #(-- clear)
    for seq in sequencer.sequences:
        if seq.channel in (1,2):
            if seq.type=="MOVIE":
                sequencer.sequences.remove(seq)
            elif seq.type=="SOUND":
                sequencer.sequences.remove(seq)
            else:
                sequencer.sequences.remove(seq)
    #(-- insert)
    sequence=sequencer.sequences.new_movie(os.environ["CEREBRO_B3D_CURRENT_ASSET_NAME"], animatic_path, 1, 0, fit_method='ORIGINAL')
    sound=sequencer.sequences.new_sound(f'{os.environ["CEREBRO_B3D_CURRENT_ASSET_NAME"]}.sound', animatic_path, 2, 0)
    #(-- duration)
    context.scene.frame_start=0
    context.scene.frame_end=int(sequence.frame_duration)
    #(-- settings)
    hooks._set_render_setting_for_playblast()

    return(True, "Animatic loaded!")


def _make_bg_images(context, path):
    """
    Создание Background Image c мувкой аниматика для камеры сцены, при отсутствии камеры создаст её.

    Parameters
    ----------
    context : bpy.context
        Контекст блендера
    path : str
        Путь до мувки.

    Returns
    -------
    None
    """
    #(camera)
    if not bpy.context.scene.camera:
        camera=None
        for ob in bpy.data.objects:
            if ob.type=="CAMERA":
                camera=bpy.context.scene.camera=ob
        if not camera:
            camera=_create_shot_camera(context)
    else:
        camera=bpy.context.scene.camera
        
    #(clip)
    clip=None
    for c in bpy.data.movieclips:
        if bpy.path.abspath(c.filepath).replace('/..','')==path: # ?
            clip=c
    if not clip:
        clip=bpy.data.movieclips.load(path, check_existing=True)
    print(f"INFO:{clip.filepath}")
    clip.frame_start=int(float(os.environ['CEREBRO_B3D_CURRENT_PROJECT_FSTART']))
        
    #(background images)
    camera.data.show_background_images=True
    if camera.data.background_images:
        bg=camera.data.background_images[0]
    else:
        bg=camera.data.background_images.new()
    #(bg settings)
    bg.clip=clip
    bg.display_depth="FRONT"
    bg.source="MOVIE_CLIP"
    bg.alpha=0.5
    bg.scale=0.5
    bg.offset=(0.25,0.25)


def download_animatic_to_shot(context, task, use_prefix=True):
    """
    * Загрузка последней версии аниматика из фтрек в задачу шота(animatic_asset_name.mp4)
    * Создание, или обновление, с ним секвенции.
    * Установка параметров сцены.
    * Создание или обновление Background Image камеры.

    Parameters
    ----------
    context : bpy.Context
        Блендер контекст
    task : list
        Объект задачи
    use_prefix : bool optional
        Использовать или нет префикс **Animatic_** в названии скачиваемого видео.

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    #(DOWNLOAD FILE)
    b,sess=srvconn.get_session()
    if not b:
        return(b,sess)
    
    try:
        download_path, version = _download_last_assetversion_move(sess, None, startswith="Animatic", shot_id=task[dbtypes.TASK_DATA_PARENT_ID])
    except Exception as e:
        return(False, str(e))

    #(COPY FILE)
    b,task_folder_path=path_utils.get_task_folder_path(task)
    if not b:
        return(b,task_folder_path)
    #(--)
    if use_prefix:
        prefix=path_utils.PREFIX_ANIMATIC
    else:
        prefix=''
    #(--)
    animatic_name=f"{prefix}{srvconn.parent_name(task)}"
    file_name=f"{animatic_name}.mp4"
    animatic_path=os.path.join(task_folder_path, file_name)
    shutil.copyfile(download_path, animatic_path)

    #(SEQUENCER)
    b,sequencer=_get_sequencer(context)
    if not b:
        return(b,sequencer)
    #(--)
    move_name=animatic_name
    sound_name=f"{move_name}.sound"
    #(-- clear sequences)
    for seq in sequencer.sequences:
        if seq.channel in (1,2) or seq.name in (move_name, sound_name):
            sequencer.sequences.remove(seq)
    #(-- insert sequences)
    fstart=float(os.environ["CEREBRO_B3D_CURRENT_PROJECT_FSTART"])
    move=sequencer.sequences.new_movie(move_name, animatic_path, 1, int(fstart), fit_method='ORIGINAL')
    sound=sequencer.sequences.new_sound(sound_name, animatic_path, 2, int(fstart))
    #(-- settings)
    hooks._set_render_setting_for_playblast(fstart='ENV', fend='ENV')

    try:
        _make_bg_images(context, animatic_path)
    except Exception as e:
        print(f"{traceback.format_exc()}")
        return(False, str(e))

    return(True, "Animatic loaded")


def rename_sequences_markers(context, method=1) -> tuple:
    """Переименование маркеров на таймлайне аниматика под имена секвенций.

    Returns
    -------
    tuple
        (True, Ok) или (False, comment)
    """
    #(get markers sorted by frame)
    IGNORE_NAMES=("shot",)
    sorted_markers=list()
    markers=bpy.context.scene.timeline_markers

    #
    if method==1:
        for mr in sorted(markers, key=lambda m: m.frame):
            if mr.name in IGNORE_NAMES:
                continue
            elif mr.frame<context.scene.frame_start or mr.frame>context.scene.frame_end:
                continue
            else:
                sorted_markers.append(mr)
    else:
        #(method=2)
        for mr in sorted(markers, key=lambda m: m.frame):
            if mr.name.lower()=="s" or mr.frame==context.scene.frame_start:
                sorted_markers.append(mr)
            else:
                continue

    #(get episode num)
    ep_name=os.environ["CEREBRO_B3D_CURRENT_ASSET_NAME"]
    ep_type=os.environ["CEREBRO_B3D_CURRENT_ASSET_TYPE"]
    ep_mun=ep_name.replace(ep_type, '')
    
    d=1
    if not sorted_markers or sorted_markers[0].frame != context.scene.frame_start:
        d=2
        m_name=f"Sequence{ep_mun}_{1:03}"
        print(m_name)
        context.scene.timeline_markers.new(m_name, frame=context.scene.frame_start)
    for i, m in enumerate(sorted_markers):
        num=i+d
        m_name=f"Sequence{ep_mun}_{num:03}"
        m.name=m_name
        print(m_name)
        
    
    return(True, "Ok!")


def rename_shots_markers(context) -> tuple:
    """
    Переименование маркеров на таймлайне аниматика под имена шотов.
    
    .. note:: Требуется наличие маркеров секвенций, создаваемых в в :func:`rename_sequences_markers`
    
    Returns
    -------
    tuple
        (True, Ok) или (False, comment)
    """
    SHOTS_STEP=10
    sequence_markers=list()
    all_markers=bpy.context.scene.timeline_markers
    for mr in sorted(all_markers, key=lambda m: m.frame):
        if mr.name.startswith("Sequence"):
            sequence_markers.append(mr)
            
    if not sequence_markers:
        return(False, "No found Sequence markers!")
            
    for I,seq_m in enumerate(sequence_markers):
        num=seq_m.name.replace("Sequence", '')
        shot_name=f'Shot{num}_{10:04}'
        print(f"{shot_name} - {seq_m.frame}")
        seq_m.name=shot_name
        
        all_sorted_ms=sorted(all_markers, key=lambda m: m.frame)
        i=2
        for m in all_sorted_ms:
            if m.name.startswith("Sequence"):
                continue
            elif m.frame<=seq_m.frame or (I<len(sequence_markers)-1 and m.frame>=sequence_markers[I+1].frame):
                continue
            else:
                shot_name=f'Shot{num}_{i*SHOTS_STEP:04}'
                m.name=shot_name
                i+=1
                print(f"{shot_name} - {m.frame}")
                continue
    
    return(True, "Ok!")


def create_shots_from_markers(context, step=False, template="shot_3d", method=1) -> tuple:
    """
    Создание шотов по маркерам созданным в :func:`rename_shots_markers`
    
    Parameters
    ----------
    context : bpy.context
        context
    step : bool, int, optional
        Если False то для всех маркеров, если число - то создаётся количество равное числу, начиная с начала.
    template : str
        Именования набора задач, который будет использоваться при создании шота.
    method : int optional
        номер метода 1 - из видео аниматика, 2 - из сцены с использованием сущестующей анимации.
        
    Returns
    -------
    tuple
        (True, Ok) или (False, comment)
    """
    frame_start=bpy.context.scene.frame_start
    frame_end=bpy.context.scene.frame_end

    if step%2:
        step+=1

    all_markers=sorted(bpy.context.scene.timeline_markers, key=lambda m: m.frame)
    markers=list()
    for m in all_markers:
        if m.name.startswith("Shot"):
            markers.append(m)
    if not markers:
        return(False, "No found Shot markers!")
    if step and step<len(markers):
        pass
    else:
        step=len(markers)
    
    #(sess)
    b,sess=srvconn.get_session()
    if not b:
        return(b,sess)

    #(Shots folder)
    b,shots_id=srvconn.get_shots_folder_of_episode(sess=sess)
    if not b:
        return(b,shots_id)

    m_to_remove=list()
    return_data=(True, "Ok")
    for i in range(step):
        try:
            m=markers[i]
            #(m_data)
            m_data=dict()
            if i%2:
                m_data["video_channel"]=3
                m_data["sound_channel"]=4
            else:
                m_data["video_channel"]=5
                m_data["sound_channel"]=6
            m_data["fstart"]=m.frame
            if i<len(markers)-1:
                m_data["fend"]=markers[i+1].frame - 1
            else:
                m_data["fend"]=context.scene.frame_end
            m_data["duration"]=m_data["fend"]-m_data["fstart"]
            m_data["clip_start"]=0.1
            m_data["clip_end"]=10.0

            #(shot)
            b,shot=srvconn.get_asset_by_name(m.name, sess=sess)
            if not b:
                raise Exception(str(shot))
            if not shot:
                b,shot=srvconn.create_shot(m.name, template, m_data, shots_id, sess=sess)
                if not b:
                    raise Exception(str(shot_id))
                shot_id=shot[dbtypes.TASK_DATA_ID]
            else:
                shot_id=shot[dbtypes.TASK_DATA_ID]
            #(set shot parameters)
            for tag_name in ["fstart", "fend", "clip_start", "clip_end"]:
                srvconn.set_task_tags(shot_id, tag_name, m_data[tag_name])
            #
            if shot:
                print(f'{m.name} - {m_data} - {shot_id}')
            else:
                print(f'{m.name} - {m_data} - None')
            
            #(animatic to version)
            hooks._set_render_setting_for_playblast(fstart=m.frame, fend=m.frame+m_data["duration"])
            bpy.ops.render.opengl(animation=True, sequencer=True)
            path=bpy.path.abspath(context.scene.render.filepath)
            index=1000
            
            print("INFO: push video to task START")
            # b,children=srvconn.get_task_children(shot_id)
            # if not b:
            #     Exception(str(children))
            # for task in children:
            #     tech_task=task
            #     if task[dbtypes.TASK_DATA_ACTIVITY_NAME]=="animatic":
            #         break
            #     # if task['type']['name'] in srvconn.TASK_TYPE_PRIORITY:
            #     #     if srvconn.TASK_TYPE_PRIORITY.index(task['type']['name'])<index:
            #     #         index=srvconn.TASK_TYPE_PRIORITY.index(task['type']['name'])
            #     #         tech_task=task
            #     # else:
            #     #     tech_task=task
            #     # print(f"{task['name']} - tech: {tech_task}")
            #     # b,r=srvconn.push_video_to_ftrack(task, path, "animatic", asset_type_name="Video", prefix="Animatic", sess=sess, close_session=False)

            tech_task=srvconn.get_task_of_shot_for_loading_animatic(shot_id)
            b,r=srvconn.push_video_to_cerebro(tech_task, path, "create shot", status_name="готова к работе", prefix="Animatic")
            if not b:
                raise Exception(str(r))
                
            print("INFO: push video to task END")

            #(animatic to sequencer)
            #(--video to task folder)
            print(f"INFO: tech_task is \"{tech_task[dbtypes.TASK_DATA_NAME]}\"")
            b,task_folder=path_utils.get_task_folder_path(tech_task)
            if not b:
                raise Exception(str(task_folder))
            animatic_path=os.path.join(task_folder, f"{shot[dbtypes.TASK_DATA_NAME]}.mp4")
            shutil.copyfile(path, animatic_path)
            print(f"INFO: File is copied to {animatic_path}")
            #(-- to sequencer)
            sequencer = context.scene.sequence_editor
            sequence=sequencer.sequences.new_movie(shot[dbtypes.TASK_DATA_NAME], animatic_path, m_data["video_channel"], m.frame, fit_method='ORIGINAL')
            sound=sequencer.sequences.new_sound(f'{shot[dbtypes.TASK_DATA_NAME]}.sound', animatic_path, m_data["sound_channel"], m.frame)
            sound.mute=True
            print("INFO: Sequence is created")
            m_to_remove.append(m)
        except Exception as e:
            print(f'{traceback.format_exc()}')
            return_data=(False, f'{e}')

    #(fin)
    bpy.context.scene.frame_start=int(frame_start)
    bpy.context.scene.frame_end=int(frame_end)

    for m in m_to_remove:
        bpy.context.scene.timeline_markers.remove(m)

    bpy.ops.wm.save_as_mainfile()
        
    return return_data


def re_create_selected_shots(context):
    """
    Пересоздание выделенных шотов по аниматику \
    (для случаев, когда подгружен отредактированный аниматик).

    Parameters
    ----------
    context : bpy.context
        Контекст блендера.

    Returns
    -------
    tuple
        (True, (tuple names of shots,)) или (False, comment)

    """
    print("FTRACK INFO: re_create_selected_shots()")
    frame_start=bpy.context.scene.frame_start
    frame_end=bpy.context.scene.frame_end
    #
    shots=_get_selected_shots(context)
    #(SESS)
    b,sess=srvconn.get_session()
    if not b:
        return(b,sess)

    sequencer = context.scene.sequence_editor
    return_data=(True, shots)
    for shot_name in shots:
        try:
            #(GET SEQUENCES of shot)
            #(--picture)
            if not shot_name in context.scene.sequence_editor.sequences_all:
                print(f"FTRACK INFO: Shot by that name \"{shot_name}\" does not exist!")
                continue
            shot=context.scene.sequence_editor.sequences_all[shot_name]
            #(--sound)
            sound_name=f"{shot_name}.sound"
            if not sound_name in context.scene.sequence_editor.sequences_all:
                print(f"FTRACK INFO: Sound by that name \"{sound_name}\" does not exist!")
                shot_sound=None
            else:
                shot_sound=context.scene.sequence_editor.sequences_all[sound_name]
            #(--hide sequences)
            shot.mute=True
            if shot_sound:
                shot_sound.mute=True

            #(M_DATA)
            m_data=dict()
            m_data["video_channel"]=shot.channel
            if shot_sound:
                m_data["sound_channel"]=shot_sound.channel
            else:
                m_data["sound_channel"]=m_data["video_channel"]+1
            m_data["fstart"]=int(shot.frame_start)
            m_data["fend"]=int(shot.frame_final_end-1)
            m_data["duration"]=shot.frame_duration
            # print(m_data)
            # continue

            #(HIDE SEQUENCES)
            shot_sound.mute=True
            shot.mute=True

            #(SHOT)
            b,shot_ft=srvconn.get_asset_by_name(shot_name, sess=sess)
            if not b:
                raise Exception(str(shot_ft))
            if not shot_ft:
                raise Exception(f"Shot named \"{shot_name}\" does not exist on the server!")
            else:
                # shot_ft["custom_attributes"]["fstart"]= m_data["fstart"]
                # shot_ft["custom_attributes"]["fend"]= m_data["fend"]
                # sess.commit()
                srvconn.set_task_tags(shot_ft[dbtypes.TASK_DATA_ID], "fstart", int(m_data["fstart"]))
                srvconn.set_task_tags(shot_ft[dbtypes.TASK_DATA_ID], "fend", int(m_data["fend"]))

            #(ANIMATIC TO VERSION)
            print(m_data)
            # hooks._set_render_setting_for_playblast(fstart=shot.frame_start, fend=m_data["fend"])
            hooks._set_render_setting_for_playblast()
            bpy.context.scene.frame_start=int(m_data["fstart"])
            bpy.context.scene.frame_end=int(m_data["fend"])
            bpy.ops.render.opengl(animation=True, sequencer=True)
            path=bpy.path.abspath(context.scene.render.filepath)
            index=1000
            # tech_task=None
            # for task in shot_ft["children"]:
            #     if task['type']['name'] in srvconn.TASK_TYPE_PRIORITY:
            #         if srvconn.TASK_TYPE_PRIORITY.index(task['type']['name'])<index:
            #             index=srvconn.TASK_TYPE_PRIORITY.index(task['type']['name'])
            #             tech_task=task
            #     # (--push video)
            #     b,r=srvconn.push_video_to_ftrack(task, path, "animatic", asset_type_name="Video", prefix="Animatic", sess=sess, close_session=False)
            #     if not b:
            #         raise Exception(str(r))

            # b,children=srvconn.get_task_children(shot_ft[dbtypes.TASK_DATA_ID], sess=sess)
            # if not b:
            #     raise Exception(srt(children))
            # tech_task=task=children[0]

            tech_task=task=srvconn.get_task_of_shot_for_loading_animatic(shot_ft[dbtypes.TASK_DATA_ID])

            b,r=srvconn.push_video_to_cerebro(task, path, "create shot", status_name="готова к работе", prefix="Animatic")
            if not b:
                raise Exception(str(r))
            
            #(ANIMATIC TO SEQUENCER)
            #(--video to task folder)
            b,task_folder=path_utils.get_task_folder_path(tech_task)
            if not b:
                raise Exception(str(task_folder))
            animatic_path=os.path.join(task_folder, f"{shot_name}.mp4")
            shutil.copyfile(path, animatic_path)
            #(-- CLEAR SEQUENCES)
            sequencer.sequences.remove(shot)
            sequencer.sequences.remove(shot_sound)
            #(--to sequencer)
            sequence=sequencer.sequences.new_movie(shot_name, animatic_path, m_data["video_channel"], m_data["fstart"], fit_method='ORIGINAL')
            sound=sequencer.sequences.new_sound(f'{shot_name}.sound', animatic_path, m_data["sound_channel"], m_data["fstart"])
            sound.mute=True
        except Exception as e:
            print(f'{traceback.format_exc()}')
            srvconn.closing_session(sess)
            return_data=(False, f'in re_create_selected_shots: "{e}" by shot "{shot_name}"')

    #(fin)
    bpy.context.scene.frame_start=int(frame_start)
    bpy.context.scene.frame_end=int(frame_end)
    return return_data


def download_review(context) -> tuple:
    """Загрузка в секвенсор последней версии плейбласта ревью.

    Parameters
    ----------
    context : bpy.context
        Контекст блендера
    
    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)

    """
    #(last Review)
    shots=_get_selected_shots(context)
    #(SESS)
    b,sess=srvconn.get_session()
    if not b:
        return(b,sess)

    try:
        b,sequencer=_get_sequencer(context)
        if not b:
            raise Exception(str(sequencer))

        for shot_name in shots:
            try:
                #(shot)
                if not shot_name in sequencer.sequences_all:
                    print(f"FTRACK INFO: Shot by that name \"{shot_name}\" does not exist!")
                    continue
                shot=sequencer.sequences_all[shot_name]
                download_path, review_ft =_download_last_assetversion_move(sess, shot_name)
                            
                #(Copy file to task folder)
                #(-- task)
                b,task=srvconn.get_task_by_id(review_ft["message"][dbtypes.MESSAGE_DATA_TID], sess=sess)
                if not b:
                    raise Exception(str(task))
                #(-- task_folder)
                b,task_folder=path_utils.get_task_folder_path(task)
                if not b:
                    raise Exception(str(f"{task_folder}"))
                #(-- copy file)
                review_path=os.path.join(task_folder, f"Review_{shot_name}.mp4")
                shutil.copyfile(download_path, review_path)

                #(review)
                review_name=f"{shot_name}.review"
                #(--clear)
                if review_name in sequencer.sequences_all:
                    sequencer.sequences.remove(sequencer.sequences_all[review_name])
                sequence=sequencer.sequences.new_movie(
                    review_name,
                    review_path,
                    shot.channel+5,
                    shot.frame_start,
                    fit_method='ORIGINAL')
                print(f"INFO: download review to {shot_name}")
            except Exception as e:
                print(f'{traceback.format_exc()}')
                print(f"WARNING: in {shot_name} {e}")
                continue
    except Exception as e:
        print(f'{traceback.format_exc()}')
        return(False, str(e))

    return(True, "Ok!")


def download_shot_animatic_to_episode(context):
    """Загрузка в секвенсор последней версии аниматика шота.

    * загружает для выделенных секвенций аниматиков.
        * при отсутствии на компьютере видеофайлов, мувки остаются пустые, но их можно выделить.
    * нужно для случая когда аниматики нарезались на другом компьютере.

    Parameters
    ----------
    context : bpy.context
        Контекст блендера
    
    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)
    """
    #(last Review)
    shots=_get_selected_shots(context)
    #(SESS)
    b,sess=srvconn.get_session()
    if not b:
        return(b,sess)

    try:
        b,sequencer=_get_sequencer(context)
        if not b:
            raise Exception(str(sequencer))

        for shot_name in shots:
            #(shot)
            if not shot_name in sequencer.sequences_all:
                print(f"FTRACK INFO: Shot by that name \"{shot_name}\" does not exist!")
                continue
            shot=sequencer.sequences_all[shot_name]
            #(shot ob)
            b,shot_ob=srvconn.get_asset_by_name(shot_name, sess=sess)
            if not b:
                raise Exception(str(shot_ob))
            #(download animatic)
            download_path, animatic_ft =_download_last_assetversion_move(sess, shot_name, startswith="Animatic", shot_id=shot_ob[dbtypes.TASK_DATA_ID])
                        
            #(Copy file to task folder)
            save_path=os.path.join(
                os.environ['CEREBRO_B3D_PROJECTS_DIR'],
                os.environ['CEREBRO_B3D_CURRENT_PROJECT_ID'],
                os.path.basename(os.path.dirname(shot.filepath)),
                os.path.basename(shot.filepath))
            b,r=path_utils._create_path(os.path.dirname(save_path), True)
            if not b:
                raise Exception(str(r))
            shutil.copyfile(download_path, save_path)
            #(update)
            # shot.update()
            bpy.ops.sequencer.refresh_all()
            sound_name=f'{shot_name}.sound'
            if sound_name in sequencer.sequences_all:
                sequencer.sequences.remove(sequencer.sequences_all[sound_name])
            sound=sequencer.sequences.new_sound(sound_name, save_path, shot.channel+1, int(shot.frame_start))
            sound.mute=True
            #(log)
            print(f"INFO: download animatic to {shot_name}")

    except Exception as e:
        print(f'{traceback.format_exc()}')
        srvconn.closing_session(sess)
        return(False, str(e))

    srvconn.closing_session(sess)
    return(True, "Ok!")


def select_shot_sequences_by_task_status(context, status_name: str):
    """
    Выделяет все секвенции шотов в **Sequence Editor**, статусы задач последних версий ревью которых соответствуют ``status_name``.
    """
    #(SHOTS)
    shots=_get_all_shots(context)
    #(SESS)
    b,sess=srvconn.get_session()
    if not b:
        return(b,sess)

    #(DESELECT)
    for sec in context.scene.sequence_editor.sequences_all:
        sec.select=False

    cash=os.environ.get('CEREBRO_B3D_SHOTS_STATUSES')
    from_cash=False
    if cash:
        cash=json.loads(cash)
        delta=datetime.datetime.now() - datetime.datetime.fromisoformat(cash['time'])
        if delta<=datetime.timedelta(minutes=1):
            from_cash=True

    if from_cash:
        for shot_name in shots:
            if shot_name in cash['data'] and cash['data'][shot_name] == status_name:
                for ob in _get_shot_sequences(context, shot_name):
                    ob.select=True

    else:
        statuses=dict()
        for shot_name in shots:
            try:
                b,shot_ft=srvconn.get_asset_by_name(shot_name, sess=sess)
                if not b:
                    raise Exception(str(shot_ft))
                if not shot_ft:
                    raise Exception(str(f"Shot named \"{shot_name}\" does not exist on the server!"))

                b,status=srvconn.get_status_name_of_last_review(shot_ft[dbtypes.TASK_DATA_ID], sess=sess, close_session=False, f=True)
                if not b:
                    raise Exception(str(status))

                print(f"INFO: {shot_name} - {status}")
                statuses[shot_name]=status

                if status == status_name:
                    for ob in _get_shot_sequences(context, shot_name):
                        ob.select=True

            except Exception as e:
                print(f'{traceback.format_exc()}')
                continue
        os.environ['CEREBRO_B3D_SHOTS_STATUSES']=json.dumps({
            "time": datetime.datetime.now().isoformat(),
            "data": statuses
            })

    srvconn.closing_session(sess)
    return(True, "Ok!")


def open_last_version_by_webbrowser(task=None, asset_name=False):
    """
    Запускает страницу обзора последнего ревью или коммита задачи в браузере.

    Parameters
    ----------
    task : list str
        Задача или её ``ID``, если пердавать, то будет открыто для неё, всё остальное проигнорируется.
    asset_name : str optional
        Имя ассета (пока только **Shot**), если не передавать будет использован id ассета из :ref:`CEREBRO_B3D_CURRENT_TASK_ID`
    

    Returns
    -------
    None
    """
    #path=https://propls.ftrackapp.com/#itemId=overview&objectType=show&slideEntityId=6790a903-e7ed-4168-9488-a6a9bbf1b6a7&slideEntityType=assetversion
    #(SESS)
    b,sess=srvconn.get_session()
    if not b:
        return(b,sess)

    #get_asset_by_name(name, type_name, sess=False, close_session=True, f=False)
    if task:
        if isinstance(task, list):
            asset_id=task[dbtypes.TASK_DATA_ID]
        elif isinstance(task, str):
            asset_id=int(task)
    elif asset_name:
        b,asset=srvconn.get_asset_by_name(asset_name, sess=sess)
        if not b:
            return(b, asset)
        asset_id=asset[dbtypes.TASK_DATA_ID]
    else:
        asset_id=os.environ['CEREBRO_B3D_CURRENT_TASK_ID']

    #(SEVER URL)
    # server_url=settings.read_auth_data()['server_url']
    # b,asset=srvconn.get_reviews_list_of_asset(asset_id, last=True, sess=sess, close_session=False, startswith=prefix)
    # if not b:
    #     return(b,asset)
    # webbrowser.open_new_tab(f"{server_url}/#itemId=overview&objectType=show&slideEntityId={asset['id']}&slideEntityType=assetversion")

    # rdata=asset['id']
    # sess.close()
    url=f"https://apps.cerebrohq.com/app/browse/{asset_id}-104?tid={asset_id}-104"
    webbrowser.open_new_tab(url)

    return(True, f"Opened webbrowser of task")


def change_status_of_selected_shot(shot_name, status_name) -> tuple:
    """
    Меняет статус последнего ревью и статус задачи этого ревью для выделенного в секвенсоре шота.
    

    Parameters
    ----------
    shot_name : str
        Имя шота для которого меняется статус.
    status_name : str
        Имя статуса

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    #(SESS)
    b,sess=srvconn.get_session()
    if not b:
        return(b,sess)

    r_data=(True, "Ok!")
    try:
        #(ASSET)
        b,asset=srvconn.get_asset_by_name(shot_name, sess=sess)
        if not b:
            raise Exception(str(asset))
        #(REVIEW)
        b,review=srvconn.get_reviews_list_of_asset(asset[dbtypes.TASK_DATA_ID], last=True, sess=sess, close_session=False, startswith="Review")
        if not b:
            raise Exception(str(review))

        #(TASK OF REVIEW)
        tid=review["message"][dbtypes.MESSAGE_DATA_TID]
        b,task=srvconn.get_task_by_id(tid, sess=sess)
        if not b:
            raise Exception(str(task))

        #(CHANGE STATUS)
        b,r=srvconn.task_status_to(task, status_name, sess=sess)
        if not b:
            raise Exception(str(r))

    except Exception as e:
        print(f'{traceback.format_exc()}')
        r_data=(False, str(e))

    return r_data


def timing_from_selected_shots(context) -> tuple:
    """Выставляет границы сцены по пределам выделенных секвенций. 


    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    fstart=None
    fend=None
    try:
        for seq in context.selected_sequences:
            if fstart is None or seq.frame_start<fstart:
                fstart=seq.frame_start
            if fend is None or fend<seq.frame_final_end:
                fend=seq.frame_final_end
        context.scene.frame_start=int(fstart)
        context.scene.frame_end=int(fend)
    except Exception as e:
        print(f'{traceback.format_exc()}')
        return(False, f"Problem in working.timing_from_selected_shots(): {e}")

    return(True, "Ok!")


def _get_activity_id_by_name(name):
    for item in G.activites.items():
        if name==item[1]:
            return int(item[0])
    return 0


def recovery(path, limit):
    """
    bpy.ops.cerebro.recovery(path='/tmp/backup/Test3', limit=3)

    """
    #(timer start)
    timing = time.time()

    #(path)
    if not os.path.exists(path):
        logging.error(f"Path {path} not exist!")
        return(False, f"Path {path} not exist!")

    #(init recover_data/maked_tasks)
    recover_data=path_utils._backup_get_folder_data(path, file_name=path_utils.BACKUP_RECOVER_DATA)
    maked_tasks=path_utils._backup_get_folder_data(path, file_name=path_utils.BACKUP_MAKED_TASKS)

    
    #(checking)
    srvconn.recovery_check_activities()
    srvconn.recovery_check_custom_attributes() # + для рут директории проекта

    #(init tags)
    tags=dict()
    for tg in G.sess.project_tags(int(os.environ["CEREBRO_B3D_CURRENT_PROJECT_ID"])):
        tags[tg[dbtypes.TAG_DATA_NAME]]=(tg[dbtypes.TAG_DATA_ID], tg[dbtypes.TAG_DATA_TYPE])
    
    #(get saved tasks)
    project_data=path_utils._backup_get_folder_data(path)
    tasks=project_data.get('tasks')

    #(tasks)
    try:
        for i,tid in enumerate(tasks):
            if limit and i>=limit:
                break
            t=tasks[tid]
            logging.info(f"[{i}] start task {t['name']}{tid} ")

            #(hierarchy)
            entity_path=path
            parent_ob=G.projects_dict[os.environ["CEREBRO_B3D_CURRENT_PROJECT_ID"]]
            for item in t['link'][1:]:
                #(get entity data)
                entity_path=os.path.join(entity_path, item['name'])
                entity_data=path_utils._backup_get_folder_data(entity_path)
                # logging.info(entity_data)
                #()
                if item['id'] in recover_data:
                    parent_ob=G.sess.task(recover_data[item['id']])
                    continue
                #(create task)
                if entity_data['object_type']!="Task":
                    activity_id=_get_activity_id_by_name(entity_data['object_type'].replace(' ', ''))
                else:
                    activity_id=_get_activity_id_by_name(entity_data['type'].lower())
                #(--)
                if not entity_data["id"] in maked_tasks:
                    new_task_id=G.sess.add_task(parent_ob[dbtypes.TASK_DATA_ID], entity_data['name'], activity_id=activity_id)
                    maked_tasks[entity_data['id']]=new_task_id
                else:
                    new_task_id=maked_tasks[entity_data["id"]]
                #(-- set status)
                if "status" in entity_data and entity_data['object_type']!="Task":
                    try:
                        G.sess.task_set_status(new_task_id, srvconn._get_status_id_by_name(entity_data['status']))
                    except:
                        pass
                #(-- custom attributes)
                for attr,value in entity_data["custom_attributes"].items():
                    if attr in tags:
                        if tags[attr][1]==0: # int
                            G.sess.task_set_tag_int(new_task_id, tags[attr][0], value)
                        elif tags[attr][1]==2: # float
                            G.sess.task_set_tag_float(new_task_id, tags[attr][0], value)
                        elif tags[attr][1]==3: # string
                            G.sess.task_set_tag_string(new_task_id, tags[attr][0], value)
                #(--)
                new_task=G.sess.task(new_task_id)
                old_parent_ob=parent_ob
                parent_ob=new_task

                #(uploads)
                if entity_data['object_type']=="Task":
                    #(-- set status)
                    G.sess.task_set_status(new_task_id, srvconn._get_status_id_by_name(t['status']))

                    #(-- push animatic)
                    animatic_path=os.path.join(entity_path, f"Animatic_{old_parent_ob[dbtypes.TASK_DATA_NAME]}.mp4")
                    if os.path.exists(animatic_path):
                        b,r=srvconn.push_video_to_cerebro(new_task, animatic_path, "Animatic", prefix="Animatic")
                        if not b:
                            logging.error(r)
                        else:
                            logging.info("Upload Animatic_")
                    else:
                        logging.warning(f"No found path to animatic: {animatic_path}")

                    #(-- push task_data)
                    versions_path=os.path.join(entity_path, "versions")
                    versions_list=path_utils._backup_get_folder_data(versions_path)
                    for v in versions_list:
                        task_data_path=os.path.join(versions_path, v["id"], f"{srvconn.TASK_DATA_COMPONENT}{srvconn.TASK_DATA_EXT}")
                        if os.path.exists(task_data_path):
                            try:
                                srvconn.push_task_data_to_cerebro(new_task, task_data_path, v["comment"])
                            except Exception as e:
                                print(f'{traceback.format_exc()}')
                                logging.error(f'{traceback.format_exc()}')
                                raise Exception(e)
                        else:
                            logging.warning(f"Commit path: \"{task_data_path}\" not found!")

                    #(-- push review)
                    review_path=os.path.join(entity_path, f"Review_{old_parent_ob[dbtypes.TASK_DATA_NAME]}.mp4")
                    if os.path.exists(review_path):
                        b,r=srvconn.push_video_to_cerebro(new_task, review_path, "Review", prefix="Review")
                        if not b:
                            logging.error(r)
                        else:
                            logging.info("Upload Review_")
                    else:
                        logging.warning(f"No found path to animatic: {review_path}")

                #(-- fin)
                recover_data[entity_data['id']]=new_task_id                    
    except:
        print(f'{traceback.format_exc()}')
        logging.error(f'{traceback.format_exc()}')

    #(saving: recover_data/maked_tasks)
    path_utils._backup_update_folder_data(path, recover_data, file_name=path_utils.BACKUP_RECOVER_DATA)
    path_utils._backup_update_folder_data(path, maked_tasks, file_name=path_utils.BACKUP_MAKED_TASKS)
    #(timer fin)
    delta=time.time() - timing
    logging.info(F'All time: {delta/60} min')

    #(end)
    return(True, f"Project {os.environ['CEREBRO_B3D_CURRENT_PROJECT_NAME']} recovered!")


def recovery_assign(path=None, tasks=None):
    """Назначения исполнителей на задачи

    если передавать tasks то path не нужен.

    bpy.ops.cerebro.recovery_assign(path='/tmp/backup/Test3')

    """
    timing=time.time()
    logging.info("Start Assigned:")
    #(get tasks)
    if not tasks:
        if not os.path.exists(path):
            return(False, f"path \"{path}\" not found!")
        #(--)
        project_data=path_utils._backup_get_folder_data(path)
        tasks=project_data.get('tasks')

    #(get recover_data)
    recover_data=path_utils._backup_get_folder_data(path, file_name=path_utils.BACKUP_RECOVER_DATA)

    #(get users)
    users=dict()
    for u in G.sess.users():
        users[u[dbtypes.USER_DATA_LOGIN]]=u[dbtypes.USER_DATA_ID]

    #(assign)
    for num,tid in enumerate(tasks):
        t=tasks[tid]
        logging.info(f"[{num}]")
        if not t["id"] in recover_data:
            continue
        #(get task)
        task=G.sess.task(recover_data[t["id"]])
        for username in t['assignments']:
            logging.info(f"assigned {username} to {t['id']}")
            if username in users:
                logging.info(users[username])
                try:
                    G.sess.task_set_allocated(recover_data[tid], users[username])
                except Exception as e:
                    print(f'{traceback.format_exc()}')

    #(timer fin)
    delta=time.time() - timing
    logging.info(F'All time: {delta/60} min')
    return(True, "Ok!")


def recovery_links(path):
    """
    Создание взаимосвязей между задачами.

    bpy.ops.cerebro.recovery_links(path='/tmp/backup/Test3')

    """
    if not os.path.exists(path):
        return(False, f"path \"{path}\" not found!")

    timing=time.time()
    logging.info("Start Links:")

    #(get links)
    project_data=path_utils._backup_get_folder_data(path)
    links=project_data.get('links')

    #(get recover_data)
    recover_data=path_utils._backup_get_folder_data(path, file_name=path_utils.BACKUP_RECOVER_DATA)

    for from_id,to_id in links:
        if not from_id in recover_data or not to_id in recover_data:
            continue
        try:
            G.sess.set_link_tasks(recover_data[from_id], recover_data[to_id])
            logging.info(f"created link from {from_id} to {to_id}")
        except Exception as e:
            print(f'{traceback.format_exc()}')
            logging.warning(f"Exception in created link from {from_id} to {to_id}: {e}")

    #(timer fin)
    delta=time.time() - timing
    logging.info(F'All time: {delta/60} min')
    return(True, "Ok!")


def recover_paths(path):
    """восстановление путей из файла ``recover.json``"""
    def _change_path(filepath, data):
        for old,new in data.items():
            if old in filepath:
                print(f"{old}-{new}")
                filepath=filepath.replace(old,str(new))
        return filepath
    #
    if os.path.exists(path):
        data=path_utils._backup_get_folder_data(os.path.dirname(path), os.path.basename(path))
    else:
        return(False, f"Not found {path}")
    for l in bpy.data.libraries:
        new_filepath=_change_path(l.filepath[:], data)
        print(f"*****{new_filepath}")
        l.filepath=new_filepath
    return(True, "Ok!")


def assets_migrate(source, target):
    """Переезд из одной директории в другую (между проектами) с последними версиями task_data.

    * Создание файла recovery.json {old_id: new_id, ...} 

    Parameters
    ----------
    source : int, str
        ``id`` директории из которой идёт копирование
    target : int, str
        ``id`` директории в которую идёт копирование

    Returns
    -------
    tuple
        (bool, data)
    """
    ch=G.sess.task_children(int(source))
    recovery_data=settings._read_setting_data(file_name="recovery.json")
    if not recovery_data:
        recovery_data=dict()

    #(target exists)
    target_exists=list()
    for a in G.sess.task_children(int(target)):
        target_exists.append(a[dbtypes.TASK_DATA_NAME])
    logger.info(target_exists)

    for asset in ch:
        try:
            logger.info(asset[dbtypes.TASK_DATA_NAME])

            #(test exists)
            if asset[dbtypes.TASK_DATA_NAME] in target_exists:
                logger.info(f"exists - {asset[dbtypes.TASK_DATA_NAME]}")
                continue

            # continue

            #(COPY ASSET)
            assets_list=[(asset[dbtypes.TASK_DATA_ID], asset[dbtypes.TASK_DATA_NAME]),]
            new_asset_id=tuple(G.sess.copy_tasks(int(target), assets_list, flags=dbtypes.COPY_TASKS_INTERNAL_LINKS|dbtypes.COPY_TASKS_TAGS))[0]
            
            recovery_data[str(asset[dbtypes.TASK_DATA_ID])]=new_asset_id
            logger.info(f"-- new asset {new_asset_id}")
            
            #(COPY TASKS)
            tasks=G.sess.task_children(asset[dbtypes.TASK_DATA_ID])
            for t in tasks:
                logger.info(f"-- {t[dbtypes.TASK_DATA_NAME]}")
                
                tasks_list=[(t[dbtypes.TASK_DATA_ID], t[dbtypes.TASK_DATA_NAME]),]
                new_task_id=tuple(G.sess.copy_tasks(new_asset_id, tasks_list, flags=dbtypes.COPY_TASKS_INTERNAL_LINKS|dbtypes.COPY_TASKS_TAGS))[0]
                
                recovery_data[str(t[dbtypes.TASK_DATA_ID])]=new_task_id
                logger.info(f"-- new task {new_task_id}")

                #(copy task_data)
                b,v=srvconn.get_latest_version_of_task(task_id=t[dbtypes.TASK_DATA_ID], sess=G.sess, close_session=False, f=True, startswith="Commit")
                if b:
                    b,path=srvconn.download_attachment(v["attachment"])
                    if b:
                        logger.info(f"-- -- path {path}")
                        #(upload task_data to new task)
                        b,r=srvconn.push_task_data_to_cerebro([0, new_task_id], path, v["message"][dbtypes.MESSAGE_DATA_TEXT])
                        if not b:
                            logger.warning(f"upload task data - {r}")
                    else:
                        logger.warning(f"download attachment - {path}")
                else:
                    logger.warning(f"get latest version - {v}")
            # break
        except Exception as e:
            print(f'{traceback.format_exc()}')
            return_data=(False, f'{e}')

    recovery_data["file_name"]="recovery.json"
    settings._write_setting_data(**recovery_data)
    return(True, "Ok!")


def mass_links(output_task_id, input_folder_id):
    """
    Связывает задачу или ассет со всеми подзадачами какой-либо директории. Нпример связать локацию со всеми шотами директории "Shots" эпизода.

    Parameters
    ----------
    output_task_id : int
        ``id`` исходящей задачи или ассета.
    input_folder_id : int
        ``id`` директории назначения.

    Returns
    -------
    tuple
        (True, ok) или (False, comment)
    """
    b,children=srvconn.get_task_children(input_folder_id)
    if not b:
        return(b,children)

    bug=None

    for t in children:
        try:
            srvconn.set_link_tasks(output_task_id, t[dbtypes.TASK_DATA_ID])
        except:
            print(f'{traceback.format_exc()}')
            bug=True
            print(f"*** bug with connect: {output_task_id}->{t[dbtypes.TASK_DATA_ID]}")

    if bug:
        return(True, "Не все задачи связаны. См. терминал.")
    else:
        return (True, "Ok")


def remove_mass_links(folder_id):
    """Удаляет все входящие связи из задач верхнего уровня указанной директории. """
    b,children=srvconn.get_task_children(folder_id)
    if not b:
        return(b,children)

    for t in children:
        links=srvconn.get_incoming_links(t[dbtypes.TASK_DATA_ID])
        for l in links:
            srvconn.drop_link_tasks(l[dbtypes.TASK_LINK_ID])

    return(True, "Ok!")


def copy_incoming_links(source_task_id, target_task_id):
    """Копирование входящих связей из одной задачи на другую.

    Parameters
    ----------
    source_task_id : int
        ``id`` задача чьи связи копируются.
    target_task_id : int
        ``id`` задача на которую копируются связи.

    Returns
    -------
    tuple
        (True, "ok") или (False, comment)
    """
    try:
        incoming_links=srvconn.get_incoming_links(source_task_id)
        for l in incoming_links:
            srvconn.set_link_tasks(l[dbtypes.TASK_LINK_SRC], target_task_id)
            print(f"connected: {l[dbtypes.TASK_LINK_SRC]} - {target_task_id}")
    except Exception as e:
        print(f'{traceback.format_exc()}')
        return(False, e)


    return (True, "Ok!")


def download_scene_to_animatic(context, source_scene):
    """Animatic Tools

    Загружает в секвенсор указанную сцену, копирует маркеры из неё.

    Parameters
    ----------
    context : bpy.types.Context
        context
    source_scene : bpy.types.Scene
        загружаемая сцена

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    #(markers)
    context.scene.timeline_markers.clear()
    for m in source_scene.timeline_markers:
        context.scene.timeline_markers.new(m.name, frame=m.frame)
    
    #(SCENE TO SEQUENCER)
    #(-- clear)
    sequencer=context.scene.sequence_editor
    for seq in sequencer.sequences:
        if seq.channel in (1,2):
            sequencer.sequences.remove(seq)
    #(new sequencer)
    context.scene.frame_start=source_scene.frame_start
    context.scene.frame_end=source_scene.frame_end
    sequencer.sequences.new_scene(source_scene.name, source_scene, 1, source_scene.frame_start)

    return (True, "Ok!")


def set_shot_timing(context):
    """Устанавливает в серебре значения тегов ("fstart", "fend") шота активной задачи. """
    shot_id=int(os.environ["CEREBRO_B3D_CURRENT_ASSET_ID"])
    srvconn.set_task_tags(shot_id, "fstart", context.scene.frame_start)
    srvconn.set_task_tags(shot_id, "fend", context.scene.frame_end)
    os.environ["CEREBRO_B3D_SCENE_FEND"]=str(float(os.environ["CEREBRO_B3D_CURRENT_PROJECT_FSTART"]) + context.scene.frame_end - context.scene.frame_start)


def set_shot_clipping(context):
    """Устанавливает в серебре значения тегов ("clip_start", "clip_end") шота активной задачи. """
    shot_id=int(os.environ["CEREBRO_B3D_CURRENT_ASSET_ID"])
    srvconn.set_task_tags(shot_id, "clip_start", context.scene.camera.data.clip_start)
    os.environ["CEREBRO_B3D_CLIP_START"]=str(context.scene.camera.data.clip_start)
    srvconn.set_task_tags(shot_id, "clip_end", context.scene.camera.data.clip_end)
    os.environ["CEREBRO_B3D_CLIP_END"]=str(context.scene.camera.data.clip_end)


def animatic_export_shots_data_to_csv(context, task, timeformat="frame"):
    """Экспорт тайминга шотов аниматика в таблицу .csv из задачи аниматика эпизода. """
    #(--)
    shots_data=list()
    for name in _get_all_shots(context):
        if not name in context.scene.sequence_editor.sequences_all:
            continue
        shot=context.scene.sequence_editor.sequences_all[name]
        shots_data.append(dict(name=name, start=shot.frame_final_start, end=shot.frame_final_end))
    #(--)
    sorted_shots_data = sorted(shots_data, key=lambda d: d['start'])
    if timeformat=="time":
        fps=int(os.environ["CEREBRO_B3D_CURRENT_PROJECT_FPS"])
        for item in sorted_shots_data:
            # print(item)
            # -- start
            ms=datetime.datetime.utcfromtimestamp(item["start"]/fps).strftime('%f')[:2]
            # print(f"""{item['start']/fps} - {datetime.datetime.utcfromtimestamp(item['start']/fps).strftime('%f')} - start ms-{ms}""")
            item["start"]=f"""{datetime.datetime.utcfromtimestamp(item["start"]/fps).strftime('%H:%M:%S')}:{ms}"""
            # -- end
            ms=datetime.datetime.utcfromtimestamp(item["end"]/fps).strftime('%f')[:2]
            # print(f"""{item["end"]/fps} - end ms-{ms}""")
            item["end"]=f"""{datetime.datetime.utcfromtimestamp(item["end"]/fps).strftime('%H:%M:%S')}:{ms}"""
            # -- fin
            # print(item)
    # print(json.dumps(sorted_shots_data, sort_keys=True, indent=4))
    #(--)
    b,meta=path_utils.get_internal_task_folder_path(task, "meta")
    if not b:
        return(False, b)
    path=os.path.join(meta, aliases.format("%asset_name%_shots_timing.csv"))
    logger.info(path)

    with open(path, 'w', newline='') as csvfile:
        fieldnames = ['name', 'start', 'end']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()
        for item in sorted_shots_data:
            writer.writerow(item)

    return (True, "Ok!")


def link_worlds_from_location(context, location):
    """Линкует все ворды из локации. 
    
    Parameters
    -----------
    location : list
        Задача-ассета локации.

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    task=srvconn.get_task_of_asset(location)
    b,filepath=path_utils.get_top_version_path_of_workfile(task)
    if not b:
        return(b,filepath)
    link_world(context, filepath, link=True)
    for w in bpy.data.worlds:
        w.use_fake_user = True
    return (True, "Ok!")


def animation_from_incoming(context, task, incoming_task):
    """Загрузка анимации из входящей задачи.

    * Подмена экшенов.
    * Подмена коллекции **Excipients**.
    * Создание чилдофов.
    
    Parameters
    ----------
    task : ftrack.Task
        Текущая задача.
    incoming_task : ftrack.Task
        Входящая задача, из которой забираем анимацию.

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    #(get source_file_path)
    b,source_file_path=path_utils.get_top_version_path_of_workfile(incoming_task)
    if not b:
        return(b,source_file_path)

    #(ANIMATORS_COLLECTION)
    #(-- clean exists collection)
    _remove_collection(settings.ANIMATORS_COLLECTION)

    #(-- append collection)
    try:
        with bpy.data.libraries.load(source_file_path, link=False) as (data_from, data_to):
            if settings.ANIMATORS_COLLECTION in data_from.collections:
                data_to.collections.append(settings.ANIMATORS_COLLECTION)
        context.scene.collection.children.link(bpy.data.collections[settings.ANIMATORS_COLLECTION])
    except:
        logger.error(f'{traceback.format_exc()}')

    #(get commit data)
    b,meta=path_utils.get_internal_task_folder_path(incoming_task, "meta")
    if not b:
        return (b,meta)
    
    file_path=os.path.join(meta, settings.COMMIT_DATA_FILE)
    commit_data=settings._read_setting_data(key=False, file_path=file_path)

    _build_append_commit_data(context, commit_data, source_file_path, ignor_linked=True, clean_exists_action=True)

    return (True, "Ok!")