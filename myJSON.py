# -*- coding: utf-8 -*-

"""Сериализатор в ``json`` формат с устарнением неполадок с некоторыми типами.

.. attention:: Пока принимает только списки

Преобразуемые типы:

* ``datetime.datetime``
"""

import json
import datetime
import logging

logger=logging.getLogger(__name__)

class myJSON(object):
    def json_item_encode(self, item):
        """Преобразует элементы в формат подходящий для сериализации ``json``.
        
        Parameters
        -----------
        item : optional
            элемент который приводится к требуему виду.

        Returns
        -------
        edit_item
            Преобразованный при необходимости элемент.
        """
        if isinstance(item, datetime.datetime):
            return item.isoformat()
        else:
            return item


    def json_list_dumps(self, input_list):
        """Обёртка на ``json.dumps`` для списков. 
        
        Parameters
        -----------
        input_list : list
            Список на преоббразование.

        Returns
        -------
        str
            Преобразованный в строку список.
        """
        w=list()
        for item in input_list:
            w.append(self.json_item_encode(item))
        return json.dumps(w)

    def dumps(self, item):
        """Сериализатор """
        if isinstance(item, list):
            return self.json_list_dumps(item)
        else:
            raise Exception('Не определённый формат')