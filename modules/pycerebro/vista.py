# -*- coding: utf-8 -*-
"""
Manipulate Mirada playlists

.. rubric:: Classes

* :py:class:`py_cerebro.vista.Vista`

"""



import os
import time
import tempfile
#import shutil
#import datetime
import sqlite3

# ===============================================
# ============== PLAYLIST HELPERS ===============
# ===============================================

class ListaUser:
	
	def __init__(self):
		self.id = 0
		self.flags = 0
		self.fname = "Anonymous"
		self.lname = ""
		self.email = ""
		self.avatar = ""
	
	def from_cerebro_user(self, cerero_user):
		from cerebro.aclasses import Users as CUsers
		
		self.id = cerero_user[CUsers.DATA_ID]
		self.flags = cerero_user[CUsers.DATA_FLAGS]
		self.fname = cerero_user[CUsers.DATA_FIRST_NAME]
		self.lname = cerero_user[CUsers.DATA_LAST_NAME]
		self.email = cerero_user[CUsers.DATA_EMAIL]
		
		return self

class ListaVersion:
	
	def __init__(self):
		# Attach IDs
		self.id = 0
		self.event_id = None
		self.group_id = None
		self.flags = 0
		# Current msec since epoch
		self.created_utc = int(time.time() * 1000)
		self.name = ""
		self.duration = None
		self.fps = None
		self.path = None
		self.download_url = None
		self.download_path = None
	
	def is_null(self):
		return not self.path and not self.download_url

# ===============================================
# ============ PLAYLIST MANIPULATOR =============
# ===============================================

class Vista:
	
	LISTA_EXT = "lista"
	VERSION = 2
	
	def __init__(self, temp_dir = ""):
		self.__temp_dir = os.path.join(tempfile.gettempdir(), "tempMirada") if not temp_dir else temp_dir
		self.__temp_file = os.path.join(self.__temp_dir, "~vistade_{}.{}".format(int(time.time()), Vista.LISTA_EXT))
		self.__db = None
		
		if not os.path.exists(self.__temp_dir):
			os.makedirs(self.__temp_dir)
	
	def __del__(self):
		self.close_lista()
	
	def create_lista(self, lista_user = ListaUser(), lista_path = ""):
		if lista_path:
			pass
		else:
			self.__temp_file = os.path.join(self.__temp_dir, "~vistade_{}.{}".format(int(time.time()), Vista.LISTA_EXT))
			self.__open_lista(self.__temp_file)
			self.__init_lista(lista_user)
	
	def close_lista(self):
		if self.__db:
			self.__db.commit()
			self.__db.close()
			self.__db = None
		
		return self.__temp_file
	
	def add_users(self, lista_users):
		pass
	
	def add_file_version(self, lista_version, file_id):
		# check file id, insert file and order if new, check version id, set ab
		pass
	
	def set_lista(self, lista_path):
		pass
	
	def add_lista(self, lista_path):
		pass
	
	def import_lista(self, lista_path, file_id):
		if self.__db:
			dbFrom = sqlite3.connect(lista_path)
			dbFrom.execute("PRAGMA journal_mode = OFF;")
			
			self.__db.execute("ATTACH DATABASE \"{0}\" AS copydb".format(lista_path))
			
			t_comments = ListaComments(Vista.VERSION)
			t_bitmaps = ListaBitmaps(Vista.VERSION)
			t_answers = ListaAnswers(Vista.VERSION)
			t_comments_w = ListaCommentsWatched(Vista.VERSION)
			t_users = ListaUsers(Vista.VERSION)
			
			self.__copy_by_id(t_comments, self.__db, dbFrom, "file_id", file_id)
			self.__copy_by_id(t_bitmaps, self.__db, dbFrom, "file_id", file_id)
			self.__copy_by_id(t_answers, self.__db, dbFrom, "file_id", file_id)
			self.__copy_by_id(t_comments_w, self.__db, dbFrom, "file_id", file_id)
			
			self.__copy(t_users, self.__db, dbFrom)
			
			# Commit to unlock copydb
			self.__db.commit()
			self.__db.execute("DETACH DATABASE copydb")
			
			dbFrom.close()
	
	
	
	
	# TEST
	def insert_file(self, lista_file):
		if self.__db:
			trans = self.__db.cursor()
			
			t_file_order = ListaFileOrder(Vista.VERSION)
			t_files = ListaFiles(Vista.VERSION)
			
			trans.execute(t_files.insert(), lista_file)
			trans.execute(t_file_order.insert(), (lista_file[0],))
			
			#self.__db.commit()
	
	def insert_version(self, lista_version):
		if self.__db:
			trans = self.__db.cursor()
			
			t_versions = ListaVersions(Vista.VERSION)
			
			trans.execute(t_versions.insert(), lista_version)
			
			#self.__db.commit()
	
	def insert_ab(self, lista_ab):
		if self.__db:
			trans = self.__db.cursor()
			
			t_file_ab = ListaFileAb(Vista.VERSION)
			
			trans.execute(t_file_ab.insert(), lista_ab)
			
			#self.__db.commit()
	
	def insert_file_status(self, file_id, status_id):
		if self.__db:
			trans = self.__db.cursor()
			
			t_file_status = ListaFileStatus(Vista.VERSION)
			
			trans.execute(t_file_status.insert(), (file_id, status_id))
	
	def insert_status(self, lista_status):
		if self.__db:
			trans = self.__db.cursor()
			
			t_statuses = ListaStatuses(Vista.VERSION)
			
			trans.execute(t_statuses.insert(), lista_status)
	
	def insert_user(self, lista_user):
		if self.__db:
			trans = self.__db.cursor()
			
			t_users = ListaUsers(Vista.VERSION)
			
			trans.execute(t_users.insert(), lista_user)
	
	
	
	
	
	
	# Private Vista
	def __open_lista(self, lista_path):
		self.close_lista()
		self.__db = sqlite3.connect(lista_path)
		self.__db.execute("PRAGMA journal_mode = WAL;")
		self.__create_tables(self.__db, Vista.VERSION)
	
	def __init_lista(self, lista_user):
		self.__init_tables(self.__db, Vista.VERSION, lista_user)
	
	# Private Lista
	def __copy_by_id(self, lista_table, db, db_from, column, id):
		trans = db.cursor()
		trans_exists = db_from.cursor()
		
		trans_exists.execute(lista_table.exists())
		exists = len(trans_exists.fetchall()) == 1
		
		if exists:
			insert = "copydb.{0}.{1}".format(lista_table.name(), column)
			trans.execute(lista_table.copy().replace(insert, str(id)))
	
	def __copy(self, lista_table, db, db_from):
		trans = db.cursor()
		trans_exists = db_from.cursor()
		
		trans_exists.execute(lista_table.exists())
		exists = len(trans_exists.fetchall()) == 1
		
		if exists:
			trans.execute(lista_table.copy())
	
	def __create_table(self, lista_table, trans):
		trans.execute(lista_table.exists())
		exists = len(trans.fetchall()) == 1
		
		if not exists:
			trans.execute(lista_table.create())
		
		return not exists
	
	def __create_tables(self, db, version):
		trans = db.cursor()
		
		trans.execute("PRAGMA foreign_keys = ON;")
		
		t_format = ListaFormat(version)
		if self.__create_table(t_format, trans):
			trans.execute(t_format.insert())
		
		self.__create_table(ListaApi(version), trans)
		self.__create_table(ListaStatuses(version), trans)
		self.__create_table(ListaFiles(version), trans)
		self.__create_table(ListaFileStatus(version), trans)
		self.__create_table(ListaVersions(version), trans)
		self.__create_table(ListaFileAb(version), trans)
		self.__create_table(ListaFileOrder(version), trans)
		self.__create_table(ListaCurUser(version), trans)
		self.__create_table(ListaUsers(version), trans)
		self.__create_table(ListaFileChanges(version), trans)
		self.__create_table(ListaCurState(version), trans)
		self.__create_table(ListaTails(version), trans)
		self.__create_table(ListaAspects(version), trans)
		self.__create_table(ListaMark(version), trans)
		self.__create_table(ListaLoop(version), trans)
		self.__create_table(ListaOnionSkin(version), trans)
		self.__create_table(ListaViewport(version), trans)
		self.__create_table(ListaTools(version), trans)
		self.__create_table(ListaRender(version), trans)
		self.__create_table(ListaComments(version), trans)
		self.__create_table(ListaBitmaps(version), trans)
		self.__create_table(ListaAnswers(version), trans)
		self.__create_table(ListaCommentsWatched(version), trans)
		self.__create_table(ListaFileGenerated(version), trans)
		self.__create_table(ListaSupport(version), trans)
		self.__create_table(ListaFileExported(version), trans)
		
		#db.commit()
	
	def __init_tables(self, db, version, lista_user):
		trans = db.cursor()
		
		# Set current user data
		t_cur_user = ListaCurUser(version)
		t_users = ListaUsers(version)
		
		trans.execute(t_cur_user.select())
		if len(trans.fetchall()) == 0:
			trans.execute(t_cur_user.insert(), (
					lista_user.id
					, lista_user.flags
					, lista_user.fname
					, lista_user.lname
					, lista_user.email
					, lista_user.avatar
				)
			)
		
		trans.execute(t_cur_user.select())
		cur_user = trans.fetchall()[0]
		trans.execute(t_users.insert(), cur_user)
		
		# Ensure file order is set
		self.__insert_order(trans, version)
		
		# Enable Cerebro API by default
		t_api = ListaApi(version)
		
		trans.execute(t_api.select())
		if len(trans.fetchall()) == 0:
			trans.execute(t_api.insert(), (
					1
					, None
					, None
					, None
					, 0
				)
			)
		
		#db.commit()
	
	def __insert_order(self, trans, version):
		t_file_order = ListaFileOrder(version)
		t_files = ListaFiles(version)
		
		trans.execute("INSERT OR IGNORE INTO {0}(file_id) SELECT {1}.id FROM {1}".format(
			t_file_order.name(), t_files.name()))

# ===============================================
# =========== LISTA TABLE BASE CLASS ============
# ===============================================

class ListaTable:
	
	STATEMENT_SELECT_ORDER = 0
	STATEMENT_INSERT = 1
	STATEMENT_COPY = 2
	
	DEFAULT_SELECT_ORDER = None
	DEFAULT_INSERT = "INSERT OR REPLACE"
	DEFAULT_COPY = "INSERT OR IGNORE"
	
	def __init__(self, name, version, columns = None):
		self.__name = name
		self.__version = version
		self.__version_columns = []
		self.__version_attributes = []
		self.__version_statements = {}
		
		if columns:
			self.set_version_columns(self.__version, columns)
	
	def set_version_columns(self, version, columns):
		version = int(version)
		if version > 0:
			defined_versions = len(self.__version_columns)
			col_types = [ i.strip().split(maxsplit=1) for i in columns ]
			col_names = [ i[0] for i in col_types if not i[0].upper() in ["PRIMARY", "FOREIGN"] ]
			col_attributes = [ " ".join(i) for i in col_types ]
			for i in range(defined_versions, version):
				self.__version_columns.append(col_names)
				self.__version_attributes.append(col_attributes)
			if defined_versions >= version:
				self.__version_columns[version - 1] = col_names
				self.__version_attributes[version - 1] = col_attributes
	
	def set_version_statement(self, version, statement, value):
		self.__version_statements[(version, statement)] = value
	
	def __check_columns(self, version):
		if version > len(self.__version_columns):
			raise Exception("columns")
	
	def __columns(self, version_from, version_to):
		self.__check_columns(version_from)
		self.__check_columns(version_to)
		
		columns = []
		if version_from != version_to:
			columns = [ i for i in self.__version_columns[version_from - 1] if i in self.__version_columns[version_to - 1] ]
		else:
			columns = self.__version_columns[version_from - 1]
		
		return columns
	
	def name(self):
		return self.__name
	
	def version(self):
		return self.__version
	
	def copy(self, version_from = -1, version_to = -1):
		if version_from < 1: version_from = self.__version
		if version_to < 1: version_to = self.__version
		
		columns = self.__columns(version_from, version_to)
		if len(columns) == 0: return None
		
		copy_statement = self.__version_statements.get((version_to, ListaTable.STATEMENT_COPY), ListaTable.DEFAULT_COPY)
		
		return "{0} INTO {1}({2}) SELECT {3} FROM copydb.{1}".format(
			copy_statement
			, self.__name
			, ", ".join(columns)
			, ", ".join([ "copydb.{0}.{1}".format(self.__name, n) for n in columns ])
		)
	
	def insert(self, version = -1):
		if version < 1: version = self.__version
		
		columns = self.__version_columns[version - 1]
		if len(columns) == 0: return None
		
		ins_statement = self.__version_statements.get((version, ListaTable.STATEMENT_INSERT), ListaTable.DEFAULT_INSERT)
		
		return "{0} INTO {1}({2}) VALUES({3})".format(
			ins_statement
			, self.__name
			, ", ".join(columns)
			, ", ".join([ "?" for i in range(len(columns)) ])
		)
	
	def create(self, version = -1):
		if version < 1: version = self.__version
		
		self.__check_columns(version)
		
		attributes = self.__version_attributes[version - 1]
		if len(attributes) == 0: return None
		
		return "CREATE TABLE {0}({1})".format(
			self.__name
			, ", ".join(attributes)
		)
	
	def exists(self, version = -1):
		return "SELECT 1 from sqlite_master where name='{0}'".format(self.__name)
	
	def select(self, version = -1):
		if version < 1: version = self.__version
		
		order_by = self.__version_statements.get((version, ListaTable.STATEMENT_SELECT_ORDER), ListaTable.DEFAULT_SELECT_ORDER)
		order_clause = " ORDER BY {0}".format(order_by) if order_by else ""
		
		return "SELECT * from {0}{1}".format(self.__name, order_clause)

# ===============================================
# ================ LISTA TABLES =================
# ===============================================

class ListaFormat(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "format", version, (
				"version INTEGER NOT NULL DEFAULT({0})".format(version),
			)
		)
	
	def copy(self, version_from = -1, version_to = -1):
		return None
	
	def insert(self, version = -1):
		return "INSERT OR REPLACE INTO {0}(version) VALUES({1})".format(self.name(), self.version())

class ListaApi(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "api", version, (
				"sending_type INTEGER NOT NULL DEFAULT(0)"
				, "sending_url TEXT"
				, "session_key TEXT"
				, "session_key_name TEXT"
				, "flags INTEGER NOT NULL DEFAULT(0)"
			)
		)
		
		for i in range(0, version):
			self.set_version_statement(i + 1, ListaTable.STATEMENT_INSERT, "INSERT OR IGNORE")

class ListaStatuses(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "statuses", version)
		
		self.set_version_columns(1, (
				"id INTEGER PRIMARY KEY NOT NULL"
				, "flags INTEGER NOT NULL DEFAULT(0)"
				, "name TEXT NOT NULL"
				, "color TEXT"
				, "icon BLOB"
			)
		)
		self.set_version_columns(2, (
				"id INTEGER PRIMARY KEY NOT NULL"
				, "flags INTEGER NOT NULL DEFAULT(0)"
				, "name TEXT NOT NULL"
				, "color TEXT"
				, "icon BLOB"
				, "order_no INTEGER NOT NULL"
			)
		)
		self.set_version_statement(2, ListaTable.STATEMENT_SELECT_ORDER, "order_no, id")

class ListaFiles(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "files", version, (
				"id INTEGER PRIMARY KEY NOT NULL "
				, "flags INTEGER NOT NULL DEFAULT(0)"
				, "task_id INTEGER"
				, "task_name TEXT"
				, "url TEXT"
				, "name TEXT"
				, "duration INTEGER"
				, "fps INTEGER"
			)
		)

class ListaFileStatus(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "file_status", version, (
				"file_id INTEGER PRIMARY KEY NOT NULL"
				, "status_id INTEGER"
				, "FOREIGN KEY(file_id) REFERENCES files(id)"
				, "FOREIGN KEY(status_id) REFERENCES statuses(id)"
			)
		)

class ListaVersions(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "versions", version, (
				"id INTEGER PRIMARY KEY NOT NULL"
				, "flags INTEGER NOT NULL DEFAULT(0)"
				, "file_id INTEGER NOT NULL"
				, "created_utc INTEGER NOT NULL"
				, "name TEXT NOT NULL"
				, "event_id INTEGER"
				, "group_id INTEGER"
				, "duration INTEGER"
				, "fps INTEGER"
				, "path TEXT"
				, "download_url TEXT"
				, "download_path TEXT"
				, "FOREIGN KEY(file_id) REFERENCES files(id)"
			)
		)

class ListaFileAb(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "file_ab", version, (
				"file_id INTEGER PRIMARY KEY NOT NULL"
				, "version_a INTEGER NOT NULL"
				, "version_b INTEGER"
				, "FOREIGN KEY(file_id) REFERENCES files(id)"
				, "FOREIGN KEY(version_a) REFERENCES versions (id)"
				, "FOREIGN KEY(version_b) REFERENCES versions (id)"
			)
		)

class ListaFileOrder(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "file_order", version, (
				"number INTEGER PRIMARY KEY AUTOINCREMENT"
				, "file_id INTEGER NOT NULL UNIQUE"
				, "FOREIGN KEY(file_id) REFERENCES files(id)"
			)
		)
	
	def copy(self, version_from = -1, version_to = -1):
		return "INSERT OR IGNORE INTO {0}(file_id) SELECT copydb.{0}.file_id FROM copydb.{0} ORDER BY copydb.{0}.number".format(
			self.name()
		)
	
	def insert(self, version = -1):
		return "INSERT OR IGNORE INTO {0}(file_id) VALUES (?)".format(self.name())

class ListaCurUser(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "cur_user", version, (
				"id INTEGER NOT NULL"
				, "flags INTEGER NOT NULL DEFAULT(0)"
				, "firstname TEXT NOT NULL"
				, "lastname TEXT"
				, "email TEXT"
				, "avatar BLOB"
			)
		)

class ListaUsers(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "users", version, (
				"id INTEGER PRIMARY KEY NOT NULL"
				, "flags INTEGER NOT NULL DEFAULT(0)"
				, "firstname TEXT NOT NULL"
				, "lastname TEXT"
				, "email TEXT"
				, "avatar BLOB"
			)
		)

class ListaFileChanges(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "file_changes", version, (
				"file_id INTEGER PRIMARY KEY NOT NULL"
				, "flags INTEGER NOT NULL DEFAULT(0)"
				, "FOREIGN KEY(file_id) REFERENCES files(id)"
			)
		)

class ListaCurState(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "cur_state", version, (
				"file_id INTEGER NOT NULL"
				, "frame INTEGER"
				, "comment_id  INTEGER"
				, "FOREIGN KEY(file_id) REFERENCES files(id)"
			)
		)
		
		for i in range(0, version):
			self.set_version_statement(i + 1, ListaTable.STATEMENT_SELECT_ORDER, "file_id")

class ListaAspects(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "aspects", version, (
				"file_id INTEGER PRIMARY KEY"
				, "flags INTEGER DEFAULT(0)"
				, "aspect REAL NOT NULL"
				, "FOREIGN KEY(file_id) REFERENCES files(id)"
			)
		)
		
		for i in range(0, version):
			self.set_version_statement(i + 1, ListaTable.STATEMENT_COPY, "INSERT OR REPLACE")

class ListaTails(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "tails", version, (
				"file_id INTEGER PRIMARY KEY"
				, "flags INTEGER DEFAULT(0)"
				, "at_begin INTEGER NOT NULL"
				, "at_end INTEGER NOT NULL"
				, "FOREIGN KEY(file_id) REFERENCES files(id)"
			)
		)
		
		for i in range(0, version):
			self.set_version_statement(i + 1, ListaTable.STATEMENT_COPY, "INSERT OR REPLACE")

class ListaMark(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "mark", version, (
				"file_id INTEGER PRIMARY KEY"
				, "flags INTEGER DEFAULT(0)"
				, "frame INTEGER NOT NULL"
				, "FOREIGN KEY(file_id) REFERENCES files(id)"
			)
		)

class ListaLoop(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "playback_loop", version, (
				"file_id INTEGER PRIMARY KEY NOT NULL"
				, "flags INTEGER DEFAULT(0)"
				, "frame NOT NULL"
				, "duration INTEGER NOT NULL"
				, "FOREIGN KEY(file_id) REFERENCES files(id)"
			)
		)

class ListaOnionSkin(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "onion_skin", version, (
				"flags INTEGER NOT NULL DEFAULT(0)"
				, "left INTEGER NOT NULL DEFAULT(1)"
				, "right INTEGER NOT NULL DEFAULT(1)"
			)
		)

class ListaViewport(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "viewport", version, (
				"flags INTEGER NOT NULL  DEFAULT(0)"
				, "compare_mode INTEGER NOT NULL DEFAULT(0)"
				, "pan_x INTEGER NOT NULL DEFAULT(0)"
				, "pan_y INTEGER NOT NULL DEFAULT(0)"
				, "zoom INTEGER NOT NULL DEFAULT(100)"
			)
		)
	
	def insert(self, version = -1):
		return "INSERT OR REPLACE INTO {0}(flags) VALUES (0)".format(self.name())

class ListaTools(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "tools", version)
		
		self.set_version_columns(1, (
				"flags INTEGER NOT NULL DEFAULT(0)"
				, "tool INTEGER NOT NULL DEFAULT(1)"
				, "size INTEGER NOT NULL DEFAULT(8)"
				, "color TEXT NOT NULL DEFAULT('#FF4072') "
				, "transparency INTEGER NOT NULL DEFAULT(0) "
			)
		)
		self.set_version_columns(2, (
				"flags INTEGER NOT NULL DEFAULT(0)"
				, "tool INTEGER NOT NULL DEFAULT(1)"
				, "size INTEGER NOT NULL DEFAULT(8)"
				, "size_eraser INTEGER NOT NULL DEFAULT(8)"
				, "color TEXT NOT NULL DEFAULT('#FF4072') "
				, "transparency INTEGER NOT NULL DEFAULT(0) "
			)
		)
	
	def insert(self, version = -1):
		return "INSERT OR REPLACE INTO {0}(tool) VALUES (0)".format(self.name())

class ListaRender(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "render", version)
		
		self.set_version_columns(1, (
				"flags INTEGER NOT NULL DEFAULT(0)"
				, "metadata TEXT"
				, "lut TEXT"
				, "aspect REAL NOT NULL DEFAULT(1.77777777777778)"
				, "letterboxing REAL NOT NULL DEFAULT(1.77777777777778)"
				, "letterboxing_transparency INTEGER NOT NULL DEFAULT(15)"
				, "watermark TEXT"
			)
		)
		self.set_version_columns(2, (
				"flags INTEGER NOT NULL DEFAULT(0)"
				, "metadata TEXT"
				, "lut TEXT"
				, "aspect REAL NOT NULL DEFAULT(1.77777777777778)"
				, "letterboxing REAL  NOT NULL DEFAULT(1.77777777777778)"
				, "letterboxing_transparency INTEGER NOT NULL DEFAULT(15)"
				, "watermark TEXT"
				, "sequence_quality INTEGER DEFAULT(-1)"
			)
		)
	
	def insert(self, version = -1):
		return "INSERT OR REPLACE INTO {0}(flags) VALUES (0)".format(self.name())

class ListaComments(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "comments", version, (
				"file_id INTEGER NOT NULL"
				, "comment_id INTEGER NOT NULL"
				, "number_comment INTEGER NOT NULL"
				, "flags INTEGER NOT NULL DEFAULT(0)"
				, "create_utc INTEGER"
				, "frame INTEGER NOT NULL"
				, "duration INTEGER NOT NULL"
				, "version_id INTEGER NOT NULL"
				, "user_id INTEGER NOT NULL"
				, "mark_x REAL NOT NULL DEFAULT(0)"
				, "mark_y REAL NOT NULL DEFAULT(0)"
				, "comment TEXT"
				, "PRIMARY KEY(file_id, comment_id)"
				, "FOREIGN KEY(user_id) REFERENCES users(id)"
				, "FOREIGN KEY(file_id) REFERENCES files(id)"
			)
		)
		
		for i in range(0, version):
			self.set_version_statement(i + 1, ListaTable.STATEMENT_SELECT_ORDER, "file_id, comment_id")

class ListaBitmaps(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "bitmaps", version)
		
		self.set_version_columns(1, (
				"file_id INTEGER NOT NULL"
				, "comment_id INTEGER NOT NULL"
				, "number_bitmap INTEGER NOT NULL"
				, "flags INTEGER NOT NULL DEFAULT(0)"
				, "type INTEGER NOT NULL"
				, "frame INTEGER NOT NULL"
				, "version_id INTEGER NOT NULL"
				, "pos_x REAL NOT NULL DEFAULT(0)"
				, "pos_y REAL NOT NULL DEFAULT(0)"
				, "pos_z REAL NOT NULL DEFAULT(0)"
				, "scale REAL NOT NULL DEFAULT(1)"
				, "width INTEGER NOT NULL DEFAULT(0)"
				, "height INTEGER NOT NULL DEFAULT(0)"
				, "data BLOB NOT NULL"
				, "PRIMARY KEY(file_id, comment_id, number_bitmap)"
				, "FOREIGN KEY(file_id, comment_id) REFERENCES comments(file_id, comment_id)"
			)
		)
		
		self.set_version_columns(2, (
				"file_id INTEGER NOT NULL"
				, "comment_id INTEGER NOT NULL"
				, "number_bitmap INTEGER NOT NULL"
				, "flags INTEGER NOT NULL DEFAULT(0)"
				, "type INTEGER NOT NULL"
				, "frame INTEGER NOT NULL"
				, "duration INTEGER NOT NULL DEFAULT(0)"
				, "version_id INTEGER NOT NULL"
				, "pos_x REAL NOT NULL DEFAULT(0)"
				, "pos_y REAL NOT NULL DEFAULT(0)"
				, "pos_z REAL NOT NULL DEFAULT(0)"
				, "scale REAL NOT NULL DEFAULT(1)"
				, "width INTEGER NOT NULL DEFAULT(0)"
				, "height INTEGER NOT NULL DEFAULT(0)"
				, "data BLOB NOT NULL"
				, "PRIMARY KEY(file_id, comment_id, number_bitmap)"
				, "FOREIGN KEY(file_id, comment_id) REFERENCES comments(file_id, comment_id)"
			)
		)
		
		for i in range(0, version):
			self.set_version_statement(i + 1, ListaTable.STATEMENT_COPY, "INSERT OR REPLACE")

class ListaAnswers(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "answers", version, (
				"file_id INTEGER NOT NULL"
				, "comment_id INTEGER NOT NULL"
				, "number_reply INTEGER NOT NULL"
				, "user_id INTEGER NOT NULL"
				, "create_utc INTEGER NOT NULL"
				, "comment TEXT NOT NULL"
				, "PRIMARY KEY(file_id, comment_id, number_reply)"
				, "FOREIGN KEY(user_id) REFERENCES users(id)"
				, "FOREIGN KEY(file_id) REFERENCES files(id)"
			)
		)
		
		for i in range(0, version):
			self.set_version_statement(i + 1, ListaTable.STATEMENT_COPY, "INSERT OR REPLACE")

class ListaCommentsWatched(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "comments_watched", version, (
				"file_id INTEGER NOT NULL"
				, "comment_id INTEGER NOT NULL"
				, "user_id INTEGER NOT NULL"
				, "watch_utc INTEGER"
				, "PRIMARY KEY (file_id, comment_id, user_id)"
				, "FOREIGN KEY(user_id) REFERENCES users(id)"
				, "FOREIGN KEY(file_id) REFERENCES files(id)"
			)
		)
		
		for i in range(0, version):
			self.set_version_statement(i + 1, ListaTable.STATEMENT_COPY, "INSERT OR REPLACE")

class ListaFileGenerated(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "file_generated", version, (
				"file_id INTEGER PRIMARY KEY NOT NULL"
				, "thumbs TEXT"
				, "pdf TEXT"
				, "mov TEXT"
				, "FOREIGN KEY(file_id) REFERENCES files(id)"
			)
		)

class ListaSupport(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "support", version, (
				"file_id INTEGER PRIMARY KEY NOT NULL"
				, "tiff_comments TEXT"
				, "FOREIGN KEY(file_id) REFERENCES files(id)"
			)
		)

class ListaFileExported(ListaTable):
	def __init__(self, version):
		ListaTable.__init__(self, "file_exported", version, (
				"file_id INTEGER NOT NULL"
				, "export_id INTEGER NOT NULL"
				, "flags INTEGER NOT NULL DEFAULT(0)"
				, "export_file TEXT"
				, "export_thumbs TEXT"
				, "PRIMARY KEY(file_id, export_id)"
				, "FOREIGN KEY(file_id) REFERENCES files(id)"
			)
		)
