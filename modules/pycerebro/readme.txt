************************************
pycerebro is a software interface written in Python language for access to Memoria database and Cargador file management system
************************************

Uses Python 2.x and 3.x libraries

pycerebro consists of 4 modules:

database
dbtypes
cargador
cclib


* database module
-------------------
Provides functions for access to Memoria database

* dbtypes module
-------------------
Contains a describes fields and flags database

* cargador module
-------------------
Provides functions for access to Cargador

* cclib module
-------------------
Provides other functions

