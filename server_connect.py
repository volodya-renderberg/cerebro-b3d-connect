# -*- coding: utf-8 -*-

"""Взаимодействие с сервером. Загрузка-выгрузка компонент. 

Требуемые кастомные атрибуты у проектов:

* fps
* width
* height

Требуемые типы AssetType:

* Vodeo
"""

import os
import traceback
import datetime
import json
import logging
import time
import tempfile
import ssl
from urllib.request import urlretrieve

import bpy

from pycerebro import database, dbtypes, cargador, cclib

try:
    import settings
    import db
except:
    from . import settings
    from . import db

# # logging
# logging.basicConfig()
# plugin_logger = logging.getLogger('com.example.example-plugin')
# plugin_logger.setLevel(logging.DEBUG)
logger = logging.getLogger(__name__)

TASK_DATA_COMPONENT='task_data'
"""str: Имя компонента содержащего архив с данными для загрузки версии. """

REVIEW_COMPONENT="ftrackreview-mp4"
"""str: Имя компонента ревью, содержащего видео в формате .mp4"""

TASK_DATA_EXT='.zip'
"""str: Расширение архива версии. """

TASK_TYPE_PRIORITY=("modeling", "shading", "rigging", "layout")
"""tuple: Список типов задач с возрастающим приоритетом для загрузки в качестве входящей задачи. """

TASK_TYPE_LINKS=(
    ("modeling","shading"),
    ("shading","rigging"),
    ("rigging","layout"),
    ("layout", "animation"),
    ("animation","fx"),
    ("animation","compositing"),
    ("animation","rendering"),
    ("fx","compositing"),
    ("fx","rendering"),
    ("compositing","rendering"),
)
"""tuple: кортеж с парами типов задач, по установлению связей. 0 - тип задачи исходящей, 1 - тип задачи входящей. 

Если один тип может в разных шаблонах иметь различные выходы, то порядок следования от ближайших возможных путей к дальним.
"""

CHEKING_STATUSES=("на утверждение",)
"""tuple: Список имён статусов, соответствующих состоянию на проверке. """

SHOTS_FOLDER="Shots"
"""str: имя папки для размещения шотов. """


def _get_future_render_commit_num(task):
    """Получение и запись номера будущего коммита рендера в ``G.future_render_commit_num``.

    Parameters
    ----------
    task : list
        task

    Returns
    -------
    None
    """
    if G.future_render_commit_num is not None:
        return

    b,r=get_versions_list(task_id=task[dbtypes.TASK_DATA_ID], startswith='Render')

    G.future_render_commit_num=len(r[0])+1


def _get_future_commit_num(task):
    """Получение и запись номера будущего коммита задачи в ``G.future_commit_num``. 

    Parameters
    ----------
    task : list
        task

    Returns
    -------
    None
    """
    if G.future_commit_num is not None:
        return

    b,r=get_versions_list(task_id=task[dbtypes.TASK_DATA_ID], startswith='Commit')

    G.future_commit_num=len(r[0])+1


def _create_activity(name, color):
    """Создание активити по имени и цвету. для вселенной из :attr:`settings.UNIVERSEID` """
    #(create activity)
    com1='''select "newActivity"('%s'::character varying, %s::integer)
    '''% (name, settings.UNIVERSEID)
    new_activity_id=G.sess.execute(com1)
    #(set color)
    com2='''select "activitySetColor"(%s::bigint, %s::integer)
    '''% (new_activity_id[0][0], color)
    G.sess.execute(com2)


def _get_url_of_attachment(attachment):
    """Получение ссылки для скачивания файла аттачмента.

    Returns
    -------
    str
        url для скачивания файла.
    """
    dns = settings.get_storage()[3]
    port = settings.get_storage()[9]
    return f'https://{dns}:{port}/file?hash={attachment[dbtypes.ATTACHMENT_DATA_HASH]}'


def _get_status_id_by_name(name):
    for s_id,s_name in G.statuses.items():
        if settings.FROM_FTRACK_STATUSES[name]==s_name:
            return s_id
    return None


def parent_name(task):
    """Возвращает имя родительской задачи, из параметра ``url`` без запросов на сервер.

    Parameters
    ----------
    task : list
        Задача.

    Returns
    -------
    str
        Имя родительской задачи.
    """

    return task[dbtypes.TASK_DATA_PARENT_URL].split('/')[-2:][0]


def get_parent_task(task):
    """Возвращает родительскую задачу. Заполняет G.task_parent_dict.

    Последовательный поиск по G.task_parent_dict, G.tasks_cache и в случае остутствия делает запрос.

    Parameters
    ----------
    task : list
        Задача.

    Returns
    -------
    list
        Задача.
    """
    if str(task[dbtypes.TASK_DATA_ID]) in G.task_parent_dict:
        parent_task=G.task_parent_dict[str(task[dbtypes.TASK_DATA_ID])]
    elif str(task[dbtypes.TASK_DATA_PARENT_ID]) in G.tasks_cache:
        parent_task=G.tasks_cache[str(task[dbtypes.TASK_DATA_PARENT_ID])]
        G.task_parent_dict[str(task[dbtypes.TASK_DATA_ID])]=parent_task
    else:
        b,parent_task=get_task_by_id(task[dbtypes.TASK_DATA_PARENT_ID])
        if not b:
            raise Exception(parent_task)
        G.task_parent_dict[str(task[dbtypes.TASK_DATA_ID])]=parent_task
    return parent_task


def get_asset_type_of_task(task) -> tuple:
    """Возвращает тип родительской задачи.
    
    Родительская задача берётся из :func:`get_parent_task`.

    Parameters
    ----------
    task : list
        Задача.

    Returns
    -------
    str
        activity_name
    """
    parent_task=get_parent_task(task)
    activity=parent_task[dbtypes.TASK_DATA_ACTIVITY_NAME]
    return activity


def download_attachment(attachment):
    """Скачивание файла attachment в tmp директорию.

    Parameters
    ----------
    attachment : list
        attachment

    Returns
    -------
    tuple
        (True, download_path) или (False, comment)
    """
    #(sess)
    if not G.sess.connected:
        b,r=get_session()
        if not b:
            return(b,r)
    #(url)
    url=_get_url_of_attachment(attachment)
    #(download)
    download_dir = tempfile.mkdtemp()
    dowload_path=os.path.join(download_dir, f'{attachment[dbtypes.ATTACHMENT_DATA_FILE_NAME]}')
    try:
        ssl._create_default_https_context = ssl._create_unverified_context
        urlretrieve(url, dowload_path)
    except Exception as e:
        print(f'{traceback.format_exc()}')
        return (False, f'download error: {str(e)}')
    return(True, dowload_path)


def recovery_check_activities():
    """
    проверяет и создаёт при отсутствии нужные активити, список активити из :attr:`settings.OBJECT_TYPES`.
    """
    activities=G.sess.activities()
    for (k,v) in settings.OBJECT_TYPES.items():
        ex=False
        for a in activities:
            if k==a[dbtypes.ACTIVITY_DATA_NAME]:
                ex=True
        print(f"{k} - {ex}")
        logging.info(f"{k} - {ex}")
        if not ex:
            _create_activity(k, v)
    G.activites=_get_activites(G.sess)


def recovery_check_custom_attributes():
    """Проверяет наличие из из :attr:`settings.CUSTOM_ATTRIBUTES` и в случае необходимости создаёт кастомные атрибуты
    """
    tags=dict()
    for tag in  G.sess.execute('''select * from "tagSchemaList"(false)'''):
        tags[tag[3]]=tag[1]
    logging.info(tags)

    prj_tags=list()
    for tag in  G.sess.project_tags(int(os.environ["CEREBRO_B3D_CURRENT_PROJECT_ID"])):
        prj_tags.append(tag[dbtypes.TAG_DATA_NAME])
    logging.info(prj_tags)

    for tag_name in settings.CUSTOM_ATTRIBUTES:
        logging.info(tag_name)
        if not tag_name in tags:
            tag_id=G.sess.execute(f'''select "tagSchemaNew"(
                '{tag_name}'::character varying,
                {settings.CUSTOM_ATTRIBUTES[tag_name]}::integer,
                {settings.UNIVERSEID}::integer)''')[0][0]
        else:
            tag_id=tags[tag_name]
        if not tag_name in prj_tags:
            G.sess.execute(f'''select "tagSchemaBind"(
                {tag_id}::bigint,
                {int(os.environ["CEREBRO_B3D_CURRENT_PROJECT_ID"])}::bigint,
                {True}::boolean)''')


def _make_tasks_link(tasks, sess):
    """Создание взаимосвязей между задачами на основе :attr:`TASK_TYPE_LINKS`

    Parameters
    ----------
    tasks : dict
        Словарь задач, где ключ - имя типа задачи, а значение объект задачи.

    Returns
    -------
    None
    """
    maked=list()
    for out_type, in_type in TASK_TYPE_LINKS:
        if out_type in tasks and not out_type in maked:
            for t in tasks.keys():
                if t==in_type:
                    sess.create('TypedContextLink', {
                        'from': tasks[out_type],
                        'to': tasks[in_type]
                    })
                    maked.append(out_type)
    sess.commit()


def set_link_tasks(output_task_id, input_task_id) -> tuple:
    """Создание связи между двумя задачами. 

    Parameters
    ----------
    output_task_id : int
        ``id`` исходящей задачи
    input_task_id : int
        ``id`` входящей задачи

    Returns
    -------
    int
        ``id`` связи
    """
    #(sess)
    if not G.sess.connected:
        b,r=get_session()
        if not b:
            raise Exception(r)
    return G.sess.set_link_tasks(output_task_id, input_task_id)


def _freshness_downloaded_version(task_id, sess, startswith=False):
    """
    Проверка свежести загруженной версии задачи.

    Parameters
    ----------
    task_id : int
        id задачи.
    sess : database.Database()
        сессия ftrack
    startswith : str, optional
        Префикс имени ассета.

    Returns
    -------
    str
        **status** - значение из ['missing', 'old', 'latest']
    """
    b, vers = get_latest_version_of_task(task_id=task_id, sess=sess, close_session=False, f=True, startswith=startswith)
    status="missing"
    if b:
        b,r=db.exists_loaded_version(vers)
        if b:
            status=r[0]
    return status


def get_session() -> tuple:
    """Создаёт и возвращает сессию, или сообщение об ошибке.

    .. note:: Также заполняет G.sess


    Returns
    -------
    tuple
        (True, session) или (False, comment)
    """
    try:
        if G.sess.connected:
            return (True, G.sess)
        else:
            sess=database.Database()
            if not sess.connected and sess.connect_from_cerebro_client() != 0:
                sess.connect(os.environ["CEREBRO_API_USER"], os.environ["CEREBRO_API_KEY"])
                G.sess=sess
                return (True, sess)
            else:
                return (True, G.sess)
    except Exception as e:
        print(f'{traceback.format_exc()}')
        return(False, str(e))


def closing_session(sess):
    if sess.connected:
        sess.disconnect()


def auth(user: str, password: str) -> tuple:
    """Аутентификация.

    Returns
    -------
    tuple
        (True, sess) или (False, comment)
    """
    sess = database.Database()
    try:
        if not sess.connected and sess.connect_from_cerebro_client() != 0: # пробуем установить соединение с помощью запущенного клиента Cerebro. 
            # Если не выходит, устанавливаем с помощью логина и пароля
            sess.connect(user, password)
    except Exception as e:
        print('*** Cerebro authorization error ***')
        print(f'{traceback.format_exc()}')
        return(False, 'Auth problem! Look the terminal.')
    settings.write_auth_data(user, password)
    # vnvs
    # os.environ["CEREBRO_SERVER"]=url
    os.environ["CEREBRO_API_USER"]=user
    os.environ["CEREBRO_API_KEY"]=password
    #
    return (True, sess)


def _get_activites(sess):
    """Возвращает словарь имён activites по id. """
    activites=dict()
    all_activities=sess.activities()
    for a in all_activities:
        activites[str(a[dbtypes.ACTIVITY_DATA_ID])]=a[dbtypes.ACTIVITY_DATA_NAME]
    return activites


def _get_activity_id_by_name(sess, name):
    """Возвращает id активити по его имени или 0. """
    all_activities=sess.activities()
    for a in all_activities:
        if a[dbtypes.ACTIVITY_DATA_NAME]==name:
            return a[dbtypes.ACTIVITY_DATA_ID]
    return 0


def _get_project_tag_id_by_name(sess, name):
    """Возвращает ``id`` тега текущего проекта по его имени, или 0 при отсутствии тега. """
    for tag in sess.project_tags(int(os.environ["CEREBRO_B3D_CURRENT_PROJECT_ID"])):
        if tag[dbtypes.TAG_DATA_NAME]==name:
            return tag[dbtypes.TAG_DATA_ID]
    return 0


def get_active_projects_list(sess=False, close_session=False) -> tuple:
    """Получение списка активных проектов.

    .. note:: Читаются поля: ``name``, ``full_name``, ``id``, ``custom_attributes``

    Returns
    -------
    tuple
        (True, (p_enums, p_dict(рут задачи проектов), p_tags_dict, statuses, activites)) или (False, comment) где p_enums - [(project_id, project_name, '')...]
    """
    try:
        if not sess or not sess.connected:
            b,sess = get_session()
            if not b:
                raise Exception(sess)

        projects=sess.root_tasks()
    
        p_dict=dict()
        p_tags_dict=dict()
        p_enums=list()
        for p in projects:
            p_dict[str(p[dbtypes.TASK_DATA_PROJECT_ID])]=p # используем id не верхней задачи, а проэкта этой задачи.
            p_enums.append((str(p[dbtypes.TASK_DATA_PROJECT_ID]), p[dbtypes.TASK_DATA_NAME], ""))

            tags=sess.task_tags(p[dbtypes.TASK_DATA_ID])
            tags_dict=dict()
            for tg in tags:
                tags_dict[tg[dbtypes.TASK_TAG_DATA_NAME]]=tg[dbtypes.TASK_TAG_DATA_VALUE]
            p_tags_dict[str(p[dbtypes.TASK_DATA_PROJECT_ID])]=tags_dict

        #(statuses)
        statuses=dict()
        all_statuses=sess.statuses()
        for st in all_statuses:
            statuses[str(st[dbtypes.STATUS_DATA_ID])]=st[dbtypes.STATUS_DATA_NAME]

        #(Activites)
        activites=_get_activites(sess)

        rdata=(True, (p_enums, p_dict, p_tags_dict, statuses, activites))

    except Exception as e:
        print(f'{traceback.format_exc()}')
        rdata=(False, 'The problem in get_active_projects_list()! Look the terminal.')


    if close_session:
        closing_session(sess)
    return rdata


def get_asset_by_name(name, sess=False) -> tuple:
    """Возвращает ассет по имени. ``id проекта`` из :ref:`CEREBRO_B3D_CURRENT_PROJECT_ID`

    Parameters
    ----------
    name : str
        имя ассета
    sess : database.Database() optional
        Session
    
    Returns
    -------
    tuple
        (True, asset или None-если не найден) или (False, comment-если ошибка)
    """
    if not sess or not sess.connected:
        b, sess=get_session()
        if not b:
            return(False, sess)

    #(get asset)
    com="""select null::timestamp with time zone as mtm, * from "search_Tasks"(
    '{%s}'::bigint[]
    , ''::text
    , 't.flags & 0000001 = 0
    and 
    (
    (lower(t.name) LIKE lower(''%s''))
    )'::text
    , 10000
    );
    """ % (os.environ["CEREBRO_B3D_CURRENT_PROJECT_ID"], name)
    r=sess.execute(com)
        
    if r:
        asset=sess.task(r[0][1])
    else:
        asset=None

    #(fin)
    return(True, asset)


def get_shots_folder_of_episode(sess=False):
    """Возвращает ``id`` папки ``Shots`` для эпизода, при отсутствии создаёт. """
    if not sess or not sess.connected:
        b, sess=get_session()
        if not b:
            return(False, sess)

    project_id=os.environ["CEREBRO_B3D_CURRENT_PROJECT_ID"]
    episode_id=int(os.environ["CEREBRO_B3D_CURRENT_ASSET_ID"])

    #(Shots фолдер)
    episode=sess.task(episode_id)
    episode_parent_url=episode[dbtypes.TASK_DATA_PARENT_URL]
    com=f"""select null::timestamp with time zone as mtm, * from "search_Tasks"(
    '{{{project_id}}}'::bigint[]
    , ''::text
    , 't.flags & 0000001 = 0
    and 
    (
    (lower(t.name) LIKE lower(''{SHOTS_FOLDER}''))
    )
    and 
    (
    lower(t.cc_url) LIKE lower(''{episode_parent_url}'')
    )'::text
    , 10000
    );"""
    query=sess.execute(com)
    if query:
        shots_id=query[0][1]
    else:
        episode_root_id=sess.task_by_url(episode_parent_url[:-1])[0]
        activity_id=_get_activity_id_by_name(sess, "Folder")
        shots_id=sess.add_task(episode_root_id, SHOTS_FOLDER, activity_id=activity_id)

    # return(False, shots_id)
    return(True, shots_id)


def create_shot(name, template, data, shots_id, sess=False):
    """
    Создание шота в папке Shots - на одном уровне с эпизодом.

    Parameters
    ----------
    name : str
        имя создаваемого шота
    template : str
        имя темплейта для создания задач шота.
    data : dict
        параметры дополнительных атрибутов шота.
    shots_id : int
        id папки Shots текущего эпизода.

    Returns
    -------
    tuple
        (True, shot) или (False, comment)
    """
    if not sess or not sess.connected:
        b, sess=get_session()
        if not b:
            return(False, sess)

    #(Shot)
    try:
        # tasks=dict()
        # template=sess.query(f"TaskTemplate where name='{template}'")[0]
        # shot=sess.create("Shot", {"name":name, "project":project, "parent":shots})
        # # sess.commit()
        # shot["custom_attributes"]["fstart"]= data["fstart"]
        # shot["custom_attributes"]["fend"]= data["fend"]
        # #(tasks)
        # for item in template["items"]:
        #     task=sess.create("Task", {"name":item["task_type"]["name"], "type":item["task_type"], "parent":shot})
        #     tasks[item["task_type"]["name"]]=task
        # sess.commit()
        # _make_tasks_link(tasks, sess)
        #(template)
        for item in G.task_templates:
            if item['name']==template:
                template_id=item['id']
        #(create Shot)
        shot_id=sess.add_task(shots_id, name, activity_id=_get_activity_id_by_name(sess, "Shot"))
        b,shot=get_task_by_id(shot_id, sess=sess)
        if not b:
            raise Exception(shot)
        #(-- set tags of Shot)
        for tag_name in ["fstart", "fend"]:
            set_task_tags(shot_id, tag_name, data[tag_name])
        #(-- create tasks of Shot)
        tmp_tasks=sess.task_children(template_id)
        tasks_list=list()
        for t in tmp_tasks:
            tasks_list.append((t[dbtypes.TASK_DATA_ID], t[dbtypes.TASK_DATA_NAME]))
        sess.copy_tasks(shot_id, tasks_list, flags=dbtypes.COPY_TASKS_INTERNAL_LINKS|dbtypes.COPY_TASKS_TAGS)
        
    except Exception as e:
        print(f'{traceback.format_exc()}')
        return(False, f"{e}")

    #(fin)
    return(True, shot)


def add_asset_models_of_project() -> tuple:
    """Получение списка задач с видом деятельности ``AssetModel`` текущего проекта. """
    b,sess = get_session()
    if not b:
        return(False, sess)

    project_id=os.environ["CEREBRO_B3D_CURRENT_PROJECT_ID"]
    activity_ids=(2927339757819473657, 2927339757819523861)
    
    tasks_ids=list()
    for activity_id in activity_ids:
        com=f"""select null::timestamp with time zone as mtm, * from "search_Tasks"(
        '{{{project_id}}}'::bigint[]
        , ''::text
        , 't.flags & 0000001 = 0
        and 
        (
        t.activityid = {activity_id}
        )'::text
        , 10000
        );
        """
        r=sess.execute(com)
        # print(r)

        for b,id in r:
            tasks_ids.append(id)
        # print(tasks_ids)

    tasks=sess.tasks(tasks_ids)
    # print(tasks)

    return(True, tasks)


def get_tasks_list(sess=False, close_session=True, check_list=False) -> tuple:
    """Получение списка задач проекта для данного пользователя.

    .. note:: Читаются поля: :attr:`construct_entity_type.TASKS_READABLE_FIELDS`

    Parameters
    ----------
    sess : database.Database() optional
        Сессия ftrack, если не передавать будет создана.
    close_session : bool
        Если *True*, то сессия будет закрыта по завершению запроса.
    check_list : bool optional
        Если *True* то  будет возвращён список задач для проверки, где данный юзер в качестве менеджера.

    Returns
    -------
    tuple
        (True, ([tasks], {tasks_dict}) или (False, comment)
    """
    project_id=os.environ["CEREBRO_B3D_CURRENT_PROJECT_ID"]

    if not sess or not sess.connected:
        b,sess = get_session()
        if not b:
            return(False, sess)
    try:
        if check_list:
            com="""select null::timestamp with time zone as mtm, * from "search_Tasks"(
            '{%s}'::bigint[]
            , ''::text
            , 't.flags & 0000001 = 0
            and 
            (
            t.cc_status = 2927339757819282374
            )'::text
            , 10000
            );
            """ % (os.environ["CEREBRO_B3D_CURRENT_PROJECT_ID"],)
            r=sess.execute(com)
            all_tasks=list()
            tasks=list()
            tasks_ids=list()
            for item in r:
                tasks_ids.append(item[1])
            all_tasks=sess.tasks(set(tasks_ids))
            for task in all_tasks:
                if cclib.has_flag(task[dbtypes.TASK_DATA_FLAGS], dbtypes.TASK_FLAG_INTEREST):
                    tasks.append(task)
        else:
            com="""select null::timestamp with time zone as mtm, * from "search_Tasks"(
            '{%s}'::bigint[]
            , '
            join users_tasks as ut on t.uid = ut.taskid'::text
            , 't.flags & 0000001 = 0
            and t.cc_status != 2927339757819282376
            and t.cc_status != 2927339757819282375
            and 
            (
            (ut.userid = %s and ut.assigned_perc is not null)
            )'::text
            , 10000
            );
            """ % (os.environ["CEREBRO_B3D_CURRENT_PROJECT_ID"], sess.current_user_id())
            r=sess.execute(com)
            tasks=list()
            tasks_ids=list()
            for item in r:
                tasks_ids.append(item[1])
            if tasks_ids:
                tasks=sess.tasks(set(tasks_ids))
            logger.info(f"tasks_ids: {tasks_ids}")
        #
        tasks_dict = {}
        tasks_parent_dict = {}
        #
        for task in tasks:
            tasks_dict[str(task[dbtypes.TASK_DATA_ID])]=task
            G.tasks_cache[str(task[dbtypes.TASK_DATA_ID])]=task
            # if task[dbtypes.TASK_DATA_PARENT_ID]:
            #     p=sess.tasks({task[dbtypes.TASK_DATA_PARENT_ID]})
            #     p=p[0]
            #     G.tasks_cache[str(p[dbtypes.TASK_DATA_ID])]=p
            # else:
            #     p=None
            # tasks_parent_dict[str(task[dbtypes.TASK_DATA_ID])]=p
        #
        rdata=(True, (tasks, tasks_dict, tasks_parent_dict))
    except Exception as e:
        print('***')
        print(f'{traceback.format_exc()}')
        rdata=(False, 'The problem of getting a list of tasks! Look the terminal.')

    
    if close_session:
        closing_session(sess)
    return rdata

def get_tasks_list_by_status(status_id, sess=False, close_session=True):
    """
    Список задач пользователя в указанном статусе, в текущем проекте.

    """
    project_id=os.environ["CEREBRO_B3D_CURRENT_PROJECT_ID"]

    if not sess or not sess.connected:
        b,sess = get_session()
        if not b:
            return(False, sess)
    try:
        com="""
        select null::timestamp with time zone as mtm, * from "search_Tasks"(
        '{%s}'::bigint[]
        , '
        join users_tasks as ut on t.uid = ut.taskid'::text
        , 't.flags & 0000001 = 0
        and 
        (
        (ut.userid = %s and ut.assigned_perc is not null)
        )
        and 
        (
        t.cc_status = %s
        )'::text
        , 10000
        );
        """% (os.environ["CEREBRO_B3D_CURRENT_PROJECT_ID"], sess.current_user_id(), str(status_id))
        r=sess.execute(com)
        tasks=list()
        tasks_ids=list()
        for item in r:
            tasks_ids.append(item[1])
        tasks=sess.tasks(set(tasks_ids))
        if tasks is None:
            tasks=list()
        rdata=(True, tasks)
    except Exception as e:
        print('***')
        print(f'{traceback.format_exc()}')
        rdata=(False, 'The problem of getting a list of tasks! Look the terminal.')

    
    if close_session:
        closing_session(sess)
    return rdata

def get_tasks_by_id_list():
    """Возвращает список задач по списку id задач.

    Empty
    """
    pass


def get_task_tags(task_id: int):
    """Возвращает словарь значений тегов задачи по именам. """
    tags=G.sess.task_tags(task_id)
    tags_dict=dict()
    for tag in tags:
        tags_dict[tag[dbtypes.TASK_TAG_DATA_NAME]]=tag[dbtypes.TASK_TAG_DATA_VALUE ]
    return tags_dict


def set_task_tags(task_id, tag_name, value):
    """Устанавливает значения тега задачи. 

    Parameters
    ----------
    task_id : int
        id 
    tag_name : str
        name
    value : int, float, str
        setting value

    Returns
    -------
    None
    """
    tag_id=_get_project_tag_id_by_name(G.sess, tag_name)
    logger.info(f"{type(task_id)} - {task_id}")
    logger.info(f"{type(tag_id)} - {tag_id} - {tag_name}")
    logger.info(f"{type(value)} - {int(value)}")

    if tag_id:
        if isinstance(value, int):
            G.sess.task_set_tag_int(task_id, tag_id, value)
        elif isinstance(value, float):
            G.sess.task_set_tag_float(task_id, tag_id, value)
        elif isinstance(value, str):
            G.sess.task_set_tag_string(task_id, tag_id, value)
        else:
            raise Exception("Unsupported \"value\" type, only: int, float, str")


def get_task_by_id(task_id: int, sess=False, close_session=True) -> tuple:
    """Возвращает задачу по id, заполняет кеш G.tasks_cache.

    Returns
    -------
    tuple
        (bool, <task or "error mesage">)
    """
    #(sess)
    if not sess or not sess.connected:
        b,sess=get_session()
        if not b:
            return(False, sess)
    #(get task)
    if str(task_id) in G.tasks_cache:
        return(True, G.tasks_cache[str(task_id)])
    else:
        task=sess.task(task_id)
        G.tasks_cache[str(task_id)]=task
        return(True, task)


def get_task_children(task_id: int, sess=False):
    """Возвращает дочерние задачи, заполняет кеш G.tasks_cache.

    Returns
    -------
    tuple
        (bool, <[tasks] or "error mesage">)
    """
    #(sess)
    if not sess or not sess.connected:
        b,sess=get_session()
        if not b:
            return(False, sess)

    children=sess.task_children(task_id)
    for t in children:
        G.tasks_cache[str(t[dbtypes.TASK_DATA_ID])]=t

    return (True, children)


def get_task_of_shot_for_loading_animatic(shot_id):
    """Возвращает задачу шота для выгрузки аниматика, в приоритете задача с типом ``anomatic`` """
    b,children=get_task_children(shot_id)
    if not b:
        Exception(str(children))
    task=None
    for task in children:
        if task[dbtypes.TASK_DATA_ACTIVITY_NAME]=="animatic":
            break
    return task


def get_versions_list(task_id=False, sess=False, startswith="Commit", parent=False):
    """Получение списка версий задачи.

    .. note:: Версия это словарь {"message":message, "attachment":attachment}

    Parameters
    ----------
    task_id : str optional
        ``id`` задачи, если не передавать - будет взято из :ref:`CEREBRO_B3D_CURRENT_TASK_ID`.
    sess : database.Database() optional
        Сессия.
    startswith : str, optional
        фильтр по началу названия ассета версии.
    parent : bool, optional
        если True - то будут возвращены версии задач, для которых ``task_id`` является родительской.

    Returns
    -------
    tuple
        (True, ([versions], {versions_dict})) или (False, comment) 
    """
    if not task_id:
        task_id = os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]
    if not sess or not sess.connected:
        b,sess = get_session()
        if not b:
            return(False, sess)

    TYPE_MESSAGE={
    "Animatic":0,
    "Review":2,
    "Commit":2,
    "Render":2,
    }

    version_list = []
    versions_dict=dict()
    
    #(task_id)
    if not task_id:
        task_id=os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]
    
    # берем только неудаленные файлы из неудаленных сообщений
    query_text = "t.flags & 0000001 = 0 and e.del = 0 and a.del = 0"
    query_text += ' '
    if not parent:
        # берем файлы из конкретной задачи
        query_text += f"and t.uid = {task_id}"
    else:
        # берем файлы для дочерних задач
        query_text += f"and t.lnk_front_parent = {task_id}"
    query_text += ' '
    # берем только записи о файле. В церебро, файл может иметь несколько записей: запись о файле, записи о его табнейлах, записи с рецензией и тп
    query_text += "and a.tag = 0"
    query_text += ' '
    # берем файлы только из сообщений типа отчет
    query_text += f"and e.tag = {TYPE_MESSAGE[startswith]}"
    query_text += ' '
    if startswith:
        # если есть startswith добавляем фильтр по началу коммента
        query_text += f"and a.description like '{startswith}%'"# a.description
        query_text += ' '
    
    prj_id=int(os.environ["CEREBRO_B3D_CURRENT_PROJECT_ID"])
    com=f"select * from \"search_Attachments\"([{prj_id},]::bigint[], ''::text, {query_text}::text, 1000)"
    sattachs = G.sess.execute("select * from \"search_Attachments\"(%s::bigint[], ''::text, %s::text, 1000)", [prj_id,], query_text)
    
    if len(sattachs) == 0:
        print('*'*100)
        print(com)
        return (True, (list(), dict()))
    
    attachids = []
    attachids = list([atch[0] for atch in sattachs])
    attachs = G.sess.execute('select * from "attachQuery_01"(%s::bigint[]) where tag=0;', attachids)
    # получаем сообщения (отчеты) для файлов
    message_ids = set(atch[dbtypes.ATTACHMENT_DATA_EVENT_ID] for atch in attachs)
    #print(message_ids)
    messages = sess.messages(message_ids)
    #print(messages)
    
    for i, m in enumerate(messages):
        # print(f"{m[dbtypes.MESSAGE_DATA_ID ]} {attachs[i][dbtypes.ATTACHMENT_DATA_EVENT_ID]}")
        version={"message":m, "attachment":attachs[i]}
        version_list.append(version)
        versions_dict[str(m[dbtypes.MESSAGE_DATA_ID])]=version
    
    return(True, (version_list, versions_dict))

def get_latest_version_of_task(task_id=False, sess=False, close_session=True, f=False, startswith=False) -> tuple:
    """Получение последней версии задачи. 

    .. note:: Читаются поля: :attr:`construct_entity_type.VERSIONS_READABLE_FIELDS`

    Parameters
    ----------
    task_id : str optional
        ID задачи, если не передавать будет взят из :ref:`CEREBRO_B3D_CURRENT_TASK_ID`
    sess : database.Database() optional
        Сессия ftrack, если не передавать будет создана.
    close_session : bool
        рудимент.
    f : bool
        рудимент.
    startswith : str, optional
        фильтр по началу названия ассета версии.

    Returns
    -------
    tuple
        (True, version) или (False, comment)
    """
    if not sess or not sess.connected:
        b,sess = get_session()
        if not b:
            return(False, sess)
    
    b,r=get_versions_list(task_id=task_id, startswith=startswith)
    if not b:
        return(b,r)
    versions=r[0]

    if not versions:
        return(False, f"No versions found for task id = {task_id}")
    else:
        return(True, versions[-1:][0])


def get_reviews_list_of_asset(asset_id, last=False, startswith="Review", sess=False, close_session=True, f=False) -> tuple:
    """Возвращает список версий Review ассета, или последнюю версию если Last=``True``

    .. note:: версия это словарь {"message": message, "attachment": attachment}

    Parameters
    ----------
    asset_id : int
        ID ассета
    last : bool, optional
        Если True - вернётся последняя версия, иначе - список версий, в порядке возрастания.(!!!)
    startswith : str, optional
        фильтр по началу названия ассета версии. По умолчанию "Review"
    sess : database.Database() optional
        Сессия, если не передавать будет создана.
    close_session : bool
        рудимент.
    f : bool
        рудимент.

    Returns
    -------
    tuple
        (True, version или [versions]) или (False, comment)
    """
    if not sess or not sess.connected:
        b,sess = get_session()
        if not b:
            return(False, sess)
    
    b,r=get_versions_list(task_id=asset_id, sess=sess, startswith=startswith, parent=True)
    if not b:
        return(b,r)

    versions=r[0]

    if last:
        if versions:
            return(True, versions[-1:][0])
        else:
            return(False, f"No found {startswith} of asset \"{asset_id}\"")
    else:
        return(True, versions)


def get_status_name_of_last_review(asset_id, sess=False, close_session=True, f=False):
    """
    Возвращает имя статуса задачи последнего ревью шота.

    Returns
    -------
    str
        Имя статуса задачи последнего ревью шота.
    """
    if not sess or not sess.connected:
        b,sess = get_session()
        if not b:
            return(False, sess)
    try:
        startswith="Review"
        # versions=sess.query(f"select task.status.name from AssetVersion where task.parent.id = {asset_id} and asset.name like \"{startswith}%\" order by date desc limit 1")
        # if not versions:
        #     raise Exception(f"No versions found for asset_id = {asset_id}")
        # else:
        #     status_name=versions[0]['task']['status']['name']
        b,version=get_reviews_list_of_asset(asset_id, last=True, startswith=startswith, sess=sess)
        if not b:
            raise Exception(str(version))
        print(version['message'][dbtypes.MESSAGE_DATA_TID])
        b,task=get_task_by_id(version['message'][dbtypes.MESSAGE_DATA_TID], sess=sess)
        if not b:
            raise Exception(task)
        status_name=G.statuses[str(task[dbtypes.TASK_DATA_CC_STATUS])]
    except Exception as e:
        print(f'{traceback.format_exc()}')
        return(False, f'Problem in get_status_name_of_last_review({asset_id}): {e}')

    #(END)
    return(True, status_name)


def get_incoming_links(task_id):
    """Возвращает входящие линки данной задачи. """
    b, sess=get_session()
    incoming_links=list()
    links=sess.task_links(task_id)
    # print(links)
    for link in links:
        if link[dbtypes.TASK_LINK_DST]==task_id and not link[dbtypes.TASK_LINK_DEL]:
            incoming_links.append(link)
    return incoming_links


def get_direct_incoming_tasks(task, r=False, incoming_tasks=dict()):
    """Возвращает прямой список входящих задач данной задачи (словарь по ``id`` самих связей), без учёта статуса свежести.

    Список именно прямой, потому что не обрабатывается по видам деятельности, по тому ``id`` который числется в связи.

    При выполнении идёт заполнение кеша **G.tasks_cache**.

    Parameters
    -----------
    task : list
        задача
    r : bool
        Если True - будет рекурсия по входящим локации.
    """
    # incoming_tasks=dict()

    incoming_links=get_incoming_links(task[dbtypes.TASK_DATA_ID])
    
    for l in incoming_links:
        b,t=get_task_by_id(l[dbtypes.TASK_LINK_SRC])
        if b:
            incoming_tasks[l[dbtypes.TASK_LINK_ID]]=t
            if r and t[dbtypes.TASK_DATA_ACTIVITY_NAME]=="Location":
                incoming_tasks=get_direct_incoming_tasks(t, incoming_tasks=incoming_tasks)
        else:
            logger.warning(t)
    return incoming_tasks


def get_incoming_tasks(task, this_parent=True, sess=False, close_session=True, f=False) -> tuple:
    """
    Возвращает словарь по входящим задачам, для локального поиска загруженных данных из входящих задач.
    
    * ключи - ``id`` задач; 
    * значения - словари: {``task``: входящая задача, ``status``: свежесть загруженной версии}.

    .. note:: Пока что отработано только для входящих связей на прямые задачи, по связям из ассетов не будет делаться сбор.

    Parameters
    ----------
    task : list
        Объект задачи
    this_parent : bool, optional
        Если True - будут рассматриваться только задачи данного ассета.
    sess : database.Database() optional
        Сессия cerebro, если не передавать будет создана.
    close_session : bool
        рудимент.
    f : bool
        рудимент.

    Returns
    -------
    tuple
        (True, **incoming_data**) или (False, conmment) где **incoming_data** - {task_id: {"task": входящая задача, "status": свежесть загруженной версии}, ...}
    """
    if not sess or not sess.connected:
        b,sess = get_session()
        if not b:
            return(False, sess)
    incoming_data=dict()
    
    # #(get Task)
    # if not isinstance(task, sess.types["Task"]):
    #     return(False, f"in server_connect.get_incoming_tasks(): Wrong type of \"task\"")

    #(get Data)
    try:
        # if this_parent:
        #     query=f" and parent.id = \"{task['parent']['id']}\""
        # incoming_tasks=sess.query(f"Task where outgoing_links any (to.id = {task['id']}){query}")
        incoming_tasks=list()
        incoming_links=get_incoming_links(task[dbtypes.TASK_DATA_ID])
        for link in incoming_links:
            # print(f"***{link}")
            input_task_id=link[dbtypes.TASK_LINK_SRC]
            # print(f"***{input_task_id}")
            b,input_task=get_task_by_id(input_task_id)
            if this_parent:
                if input_task[dbtypes.TASK_DATA_PARENT_ID]==task[dbtypes.TASK_DATA_PARENT_ID]:
                    incoming_tasks.append(input_task)

    except Exception as e:
        print(f'{traceback.format_exc()}')
        return(False, f"Exception in get_incoming_tasks() #2: {e}")

    #(make output data)
    for in_task in incoming_tasks:
        status = _freshness_downloaded_version(in_task[dbtypes.TASK_DATA_ID], sess, startswith="Commit")
        # status="opa!"
        incoming_data[str(in_task[dbtypes.TASK_DATA_ID])]={"task":in_task, "status":status}

    #(end)
    return(True, incoming_data)


def get_task_of_asset(asset):
    """Возвращает задачу из ассета, для загрузки в шот или локацию. 
    
    Parameters
    ----------
    asset : list
        ассет, задача серебры

    Returns
    -------
    list
        задача серебры
    """
    # -- TASK OF ASSET
    priority=0
    in_task=None
    children=G.sess.task_children(asset[dbtypes.TASK_DATA_ID])
    for task in children:
        if task[dbtypes.TASK_DATA_ACTIVITY_NAME] in TASK_TYPE_PRIORITY:
            index= TASK_TYPE_PRIORITY.index(task[dbtypes.TASK_DATA_ACTIVITY_NAME])
            if index >= priority:
                in_task=task
                priority=index
    return in_task


def _get_tasks_conponents(tasks, sess=False):
    """Возвращает список кортежей [(task, vers, size, url, status), ...] для списка задач.
    
    Parameters
    -----------
    tasks : list
        Список задач

    Returns
    --------
    list
    """
    if not sess or not sess.connected:
        b,sess = get_session()
        if not b:
            return(False, sess)
    
    components=[]
    volume=0
    for task in tasks:
        G.task_dict[str(task[dbtypes.TASK_DATA_ID])]=task
        size=0
        url=None
        b, vers = get_latest_version_of_task(task_id=task[dbtypes.TASK_DATA_ID], sess=sess, close_session=False, f=True, startswith="Commit")
        status="missing"
        if b:
            b,r=db.exists_loaded_version(vers)
            if b:
                status=r[0]
                if r[0] in ("missing", "old"):
                    attachment=vers["attachment"]
                    size=attachment[dbtypes.ATTACHMENT_DATA_FILE_SIZE]
                    url=_get_url_of_attachment(attachment)
        else:
            vers=None
        components.append((task, vers, size, url, status))
        volume+=size
        if vers:
            vers_id=vers["attachment"][dbtypes.ATTACHMENT_DATA_EVENT_ID]
        else:
            vers_id="None"
        print(f"INFO: append to incoming links: [{parent_name(task)}] {task[dbtypes.TASK_DATA_NAME]} - vers: {vers_id} - {status}")

    return components, volume


def get_downloadable_incoming_links(task_id=False, sess=False, close_session=True, for_download=False, get_components=True, only_direct_links=False) -> tuple:
    """Возвращает список данных компонент последних версий для всех входящих задач (рекурсивно) и общий объём загружаеммых файлов.

    Parameters
    ----------
    for_download : bool
        **True** означает, что предполагается загрузка без отображения списка на загрузку в ГУИ. поэтому будут загружаться так же и входящие локаций.
    get_components : bool optional
        True по умолчанию. Если True - то собирает всю инфу, если False - то *components* будет содержать кортежи (Task, None, None, None, None)
    only_direct_links : bool optional
        False по умолчанию. Если True - то будет собирать входящие и для родительской задачи (ассета). если False - то только по прямым ссылкам.

    Returns
    -------
    tuple
        (True, (**components**, byte_all))  or (False, comment) где **components** - список кртежей: (Task, AsseVersion, byte, url, status)
    """
    timing = time.time()
    def _parent_is_location(task):
        """True - если родитель задачи локация. """
        b,parent_task=get_task_by_id(task[dbtypes.TASK_DATA_PARENT_ID])
        return parent_task[dbtypes.TASK_DATA_ACTIVITY_NAME]=="Location"
        # return task["parent"]["object_type"]["name"]=="Asset Model" and task['parent']['type']['name'] in ("Environment", "Location")
    
    def _parent_is_shot(task):
        """True - если родитель задачи Shot. """
        b,parent_task=get_task_by_id(task[dbtypes.TASK_DATA_PARENT_ID])
        return parent_task[dbtypes.TASK_DATA_ACTIVITY_NAME]=="Shot"
    
    def _get_downloadable_incoming_links(incoming_links, tasks=[]):
        """Возвращает список id всех входящих задач (рекурсивно) """
        # incoming_tasks_ids=[link[dbtypes.TASK_LINK_SRC] for link in incoming_links]
        # incoming_tasks=sess.tasks(incoming_tasks_ids)

        for link in incoming_links:
            link_from_id=link[dbtypes.TASK_LINK_SRC]
            b,link_from=get_task_by_id(link_from_id)
            logger.debug(f"{link_from_id} - {b}")
            if not b:
                continue            
            if link_from and link_from[dbtypes.TASK_DATA_ACTIVITY_NAME]:
                object_type = link_from[dbtypes.TASK_DATA_ACTIVITY_NAME]
            else:
                continue
            # if object_type=="Task":
            if object_type not in settings.CONTAINER_TYPES:
                # print(f"{link_from['parent']['name']} - {link_from['name']}")
                # tasks.append((link_from['parent']['name'], link_from['name'], link_from['id']))
                tasks.append(link_from)
            # elif object_type=="AssetModel":
            elif object_type in settings.ASSET_TYPES:
                # -- TASK OF ASSET
                # priority=0
                # in_task=None
                # children=G.sess.task_children(link_from[dbtypes.TASK_DATA_ID])
                # for task in children:
                #     if task[dbtypes.TASK_DATA_ACTIVITY_NAME] in TASK_TYPE_PRIORITY:
                #         index= TASK_TYPE_PRIORITY.index(task[dbtypes.TASK_DATA_ACTIVITY_NAME])
                #         if index >= priority:
                #             in_task=task
                #             priority=index
                in_task=get_task_of_asset(link_from)
                # print(f"{in_task['parent']['name']} - {in_task['name']}")
                if in_task:
                    # tasks.append((in_task['parent']['name'], in_task['name'], in_task['id']))
                    tasks.append(in_task)
                else:
                    logger.info(f"No found tasks of asset: {link_from.dbtypes.TAG_DATA_NAME}")
                # -- INCOMING TASKS OF LOCATION
                if for_download:
                    if link_from[dbtypes.TASK_DATA_ACTIVITY_NAME] in settings.COMPOSITE_ASSETS:
                        # print(f"{link_from[dbtypes.TASK_DATA_NAME]}-{link_from[dbtypes.TASK_DATA_ACTIVITY_NAME]}")
                        incoming_links=get_incoming_links(link_from[dbtypes.TASK_DATA_ID])
                        _get_downloadable_incoming_links(incoming_links, tasks=tasks)
        return tasks
    
    # (1)
    if not sess or not sess.connected:
        b,sess = get_session()
        if not b:
            return(False, sess)
    # (2)
    if not task_id:
        task_id=os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]
    tasks=[]
    # (3)
    incoming_links=get_incoming_links(int(task_id))
    # (4) INNER LINKS
    # if not _parent_is_shot(task):
    #     _get_downloadable_incoming_links(task['incoming_links'], tasks=tasks)
    _get_downloadable_incoming_links(incoming_links, tasks=tasks)

    if not only_direct_links:
        # (5) OUTER LINKS
        b,task=get_task_by_id(task_id)
        if not b:
            return(b,task)
        parent_incoming_links=get_incoming_links(task[dbtypes.TASK_DATA_PARENT_ID])
        _get_downloadable_incoming_links(parent_incoming_links, tasks=tasks)

    if not get_components:
        return (True, ([(task, None, None, None, None) for task in tasks], 0))

    # (6)
    components, volume=_get_tasks_conponents(tasks, sess=sess)
    # components=[]
    # volume=0
    # for task in tasks:
    #     G.task_dict[str(task[dbtypes.TASK_DATA_ID])]=task
    #     size=0
    #     url=None
    #     b, vers = get_latest_version_of_task(task_id=task[dbtypes.TASK_DATA_ID], sess=sess, close_session=False, f=True, startswith="Commit")
    #     status="missing"
    #     if b:
    #         b,r=db.exists_loaded_version(vers)
    #         if b:
    #             status=r[0]
    #             if r[0] in ("missing", "old"):
    #                 attachment=vers["attachment"]
    #                 size=attachment[dbtypes.ATTACHMENT_DATA_FILE_SIZE]
    #                 url=_get_url_of_attachment(attachment)
    #     else:
    #         vers=None
    #     components.append((task, vers, size, url, status))
    #     volume+=size
    #     if vers:
    #         vers_id=vers["attachment"][dbtypes.ATTACHMENT_DATA_EVENT_ID]
    #     else:
    #         vers_id="None"
    #     print(f"INFO: append to incoming links: [{parent_name(task)}] {task[dbtypes.TASK_DATA_NAME]} - vers: {vers_id} - {status}")

    # print(components)
    print(f"INFO: time: {time.time() - timing} sec.")
    return(True, (components, volume))

def get_task_data_component(version_id, sess=False, close_session=True, component_name=TASK_DATA_COMPONENT) -> tuple:
    """Получение компонента версии, по умолчанию - :attr:`TASK_DATA_COMPONENT`, но можно передать любое имя компонента.

    Parameters
    ----------
    version_id : str
        Id версии
    sess : database.Database() optional
        Сессия ftrack, если не передавать будет создана.
    close_session : bool
        Если True, то сессия будет закрыта по завершению запроса.
    component_name : str optional
        Имя компонента, по умолчанию :attr:`TASK_DATA_COMPONENT`

    Returns
    -------
    tuple
        (True, component) или (False, comment)
    """
    if not sess or not sess.connected:
        b,sess = get_session()
        if not b:
            return(False, sess)
    try:
        components=sess.query(f"Component where version_id = {version_id} and name='{component_name}'")
        if not components:
            return (False, "There is no download data in this version!")
        if close_session:
            sess.close()
    except Exception as e:
        print('***')
        print(f'{traceback.format_exc()}')
        if not sess.closed and close_session:
            sess.close()
        return(False, 'The problem of getting a list of components! Look the terminal.')
    
    return(True, components[0])

def get_url_of_task_data_component(version_id, sess=False, close_session=True):
    """Получение url компонента ``task_data``.

    .. note:: Локация не выбирается, используется по умолчанию ftrack.server

    Returns
    -------
    tuple
        (True, path) или (False, comment)
    """
    b,r = get_task_data_component(version_id, sess=sess, close_session=False)
    if not b:
        return(b,r)

    path=r['component_locations'][0]['url']['value']
    
    return(True, path)


def get_url_of_review_component(version_id, sess=False, close_session=True):
    """Получение url ревью компонента.

    .. note:: Локация не выбирается, используется по умолчанию ftrack.server

    Returns
    -------
    tuple
        (True, path) или (False, comment)
    """
    b,r = get_task_data_component(version_id, sess=sess, close_session=False, component_name=REVIEW_COMPONENT)
    if not b:
        return(b,r)

    path=r['component_locations'][0]['url']['value']
    
    return(True, path)


def task_status_to_in_progress(task, sess=False, close_session=True, hold=False):
    """Меняет статус задачи на ``In progress``, если у данного пользователя есть другие задачи в этом статусе, то их
    статус меняется на ``On Hold``.

    .. note:: Так же запускает таймер задачи.
    
    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    if G.statuses[str(task[dbtypes.TASK_DATA_CC_STATUS])]=='в работе':
        timer_start()
        return (True, "Ok!")

    if not sess or not sess.connected:
        b,sess = get_session()
        if not b:
            return(False, sess)

    # rdata=(True, "Ok!")
    # try:
    project_id=os.environ["CEREBRO_B3D_CURRENT_PROJECT_ID"]

    progress_status = G.get_status_id('в работе')
    hold_status = G.get_status_id('на паузе')

    #(to HOLD)
    if hold:
        b,tasks = get_tasks_list_by_status(progress_status, sess=sess, close_session=False)
        for tsk in tasks:
            try:
                sess.task_set_status(tsk[dbtypes.TASK_DATA_ID], hold_status)
            except Exception as e:
                print(f'{traceback.format_exc()}')
                return(True, str(e))

    #(to WORK)
    try:
        sess.task_set_status(task[dbtypes.TASK_DATA_ID], progress_status)
    except Exception as e:
        print(f'{traceback.format_exc()}')     
        return(True, str(e))

    # (start timer)
    timer_start(task=task, sess=sess)
    # except Exception as e:
    #     # print(f"Exception in task_status_to_in_progress(): {e}")
    #     rdata=(False, f"Exception in task_status_to_in_progress(): {e}")

    rdata=(True, "Ok!")
    return rdata

def task_status_to(task, status_name: str, sess=False, close_session=True) -> tuple:
    """Замена статуса текущей задачи.

    .. note:: Статус ``In progress`` будет перенаправлен в :func:`task_status_to_in_progress`.

    Returns
    -------
    tuple
        (True, path) или (False, comment)
    """
    #(1)
    if status_name=="в работе":
        return task_status_to_in_progress(task, sess=sess, close_session=close_session)
    #(2)
    if not sess or not sess.connected:
        b,sess = get_session()
        if not b:
            return(False, sess)
    #(3)
    status_id = G.get_status_id(status_name)
    try:
        sess.task_set_status(task[dbtypes.TASK_DATA_ID], status_id)
    except Exception as e:
        print(f'{traceback.format_exc()}')
        return(True, str(e))
    
    #(4)
    return (True, "Ok!")


def _timer_read():
    """Чтение словарика по текущей задачи из файла :attr:`settings.TIMER_DATA_FILE` """
    if os.environ.get("CEREBRO_B3D_CURRENT_TASK_ID"):
        return settings._read_setting_data(
            key=os.environ.get("CEREBRO_B3D_CURRENT_TASK_ID"),
            file_name=settings.TIMER_DATA_FILE
            )


def _timer_write(time_data):
    """Запись ``time_data`` в файл :attr:`settings.TIMER_DATA_FILE` """
    w_data={
        os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]: time_data,
        "file_name": settings.TIMER_DATA_FILE
        }
    settings._write_setting_data(**w_data)


def timer_start(**kwargs):
    """Запуск таймера задачи. 
    
    * delta: 0 или использование текущего.
    * time: установка текущего времени.
    """
    # task=os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]
    # file_name=settings.TIMER_DATA_FILE
    logger.info("TIMER START")

    time_data=_timer_read()
    logger.info(time_data)

    if time_data:
        time_data["time"]=datetime.datetime.now().isoformat()
    else:
        time_data=dict(
            delta=0,
            time=datetime.datetime.now().isoformat(),
            )
    #
    _timer_write(time_data)
    return(True, "Ok!")

def timer_stop(sess=False):
    """Остановка таймера задачи.
    
    * delta: минуты, расчёт затраченное + прошлая запись.
    * time: null

    Returns
    -------
    tuple
        (True, затраченное время в минутах) или (False, comment)
    """
    logger.info("TIMER STOP")
    # return(True, "Ok")

    delta=0
    
    time_data=_timer_read()
    logger.info(time_data)

    if time_data and not time_data["time"] is None:
        now=datetime.datetime.now()
        start=datetime.datetime.fromisoformat(time_data["time"])
        time_data["delta"]+=(now-start).total_seconds()/60
        time_data["time"]=None
        logger.info(time_data)

        delta=time_data["delta"]

        _timer_write(time_data)

    return(True, delta)
    # t = threading.Thread(target=_timer_stop, args=(sess,), kwargs={})
    # t.setDaemon(True)
    # t.start()


def timer_reset(**kwargs):
    """Сброс таймера задачи. 
    
    * delta: 0
    * time: null
    """
    logger.info("TIMER RESET")

    time_data=_timer_read()
    logger.info(time_data)

    if time_data:
        time_data["time"]=None
        time_data["delta"]=0

        _timer_write(time_data)

    return(True, "Ok")


def push_video_to_cerebro(task, path, description, status_name=None, prefix="Review", storage=None):
    """Загрузка плейбласта или аниматика на Cerebro.

    Parameters
    ----------
    task : Task
        текущая задача
    path : str
        путь до видео файла.
    description : str
        комментарий к данной версии.
    status_name : str optional
        Имя статуса, по умолчанию **None**.
    prefix : str
        префикс имени ассета
    storage : storage, optional
        хранилище.

    Returns
    -------
    tuple
        (True, "ok") или (False, comment)
    """
    #(storage)
    if not storage:
        storage=settings.get_storage()
    #()
    if prefix=="Animatic":
        new_message_id=G.sess.add_definition(task[dbtypes.TASK_DATA_ID], description)
        logging.info(f"make message: {description} {new_message_id}")
    else: 
        work_time = 0
        new_message_id=G.sess.add_report(task[dbtypes.TASK_DATA_ID], None, prefix, work_time) # (!!!) надо различать дескрипшены сообщения и аттачмента.

    carga = cargador.Cargador(storage[3], 4040,  storage[9])
    G.sess.add_attachment(new_message_id, carga, path, [],  prefix,  False)
    logging.info(f"uploaded file: {path}")

    return(True, "Ok!")


def push_task_data_to_cerebro(task, path, description, message_id=None, work_time=0, storage=None):
    """Загрузка архива task_data.zip на Cerebro.

    Parameters
    ----------
    task : Task
        текущая задача
    path : str
        путь до видео файла.
    description : str
        комментарий к данной версии.
    message_id : int
        id сообщения для которого загружается файл, если не передавать будет создан новый.
    work_time : int
        затраченное время в минутах. Нужет только если не передавать message_id.
    storage : storage, optional
        хранилище.

    Returns
    -------
    tuple
        (True, (message_id, attachment_id)) или (False, comment)
    """
    #(storage)
    if not storage:
        storage=settings.get_storage()

    #(message)
    if not message_id:
        if not description:
            description=f'Commit'
        message_id=G.sess.add_report(task[dbtypes.TASK_DATA_ID], None, description, work_time)

    #(attachment)
    carga = cargador.Cargador(storage[3], 4040,  storage[9])
    #(-- commit num)
    if G.future_commit_num:
        at_name=f"Commit_v{G.future_commit_num}"
    else:
        at_name="Commit"
    #(--)
    attachment_id=G.sess.add_attachment(message_id, carga, path, [],  at_name,  False)
    logging.info(f"uploaded file: {path}")

    G.future_commit_num=None

    return(True, (message_id, attachment_id))


def push_video_to_ftrack(task, path, description, status_name="в работе", prefix="Review", sess=False, close_session=True, location="ftrack.server"):
    """Загрузка плейбласта на Ftrack. 

    Parameters
    ----------
    task : Task
        текущая задача
    path : str
        путь до видео файла.
    description : str
        комментарий к данной версии.
    status_name : str optional
        Имя статуса, по умолчанию *In progress*.
    asset_type_name : str
        имя типа ассета, по умолчанию *Video*
    prefix : str
        префикс имени ассета
    sess : Session
        сессия, если не передавать, то будет создана.
    close_session : bool
        Если True, то сессия будет закрыта по завершению запроса.
    location : str
        имя локации ftrack.

    Returns
    -------
    tuple
        (True, "ok") или (False, comment)
    """
    if not sess or not sess.connected:
        b,sess = get_session()
        if not b:
            return(False, sess)
    try:
        #(1)
        type_parent=task["parent"]["object_type"]["name"].replace(' ', '')
        print(f"{'*'*5} {type_parent}")

        #(2)
        asset_parent = sess.get(type_parent, task["parent"]["id"])
        print(f"{'*'*5} {asset_parent['name']}")

        #(3)
        asset_type = sess.query(f'AssetType where name is "{asset_type_name}"').one()
        print(f"{'*'*5} {asset_type['name']}")

        #(4)
        asset_name = f'{prefix}_{task["parent"]["name"]}_{task["name"].replace(" ", "_")}'
        assets = sess.query(f"Asset where name is {asset_name} and context_id is {asset_parent['id']}")
        print(f"{'*'*5} assets1")
        if assets:
            asset = assets[0]
        else:
            asset = sess.create('Asset', {
                'name': asset_name,
                'type': asset_type,
                'parent': asset_parent
            })
        print(f"{'*'*5} assets2")

        #(5)
        asset_version = sess.create('AssetVersion', {
            'asset': asset,
            'task': task,
            'comment': description
        })
        try:
            status = sess.query(f"Status where name = '{status_name}'").one()
            asset_version["status"]=status
        except Exception as e:
            print(f'{traceback.format_exc()}')
            return(False, f'{e}')
        sess.commit()
        print(f"{'*'*5} assets3")

        #(6)
        loc = sess.query(f'Location where name="{location}"').one()
        print(f"{'*'*5} {loc['name']}")
        
        component = asset_version.create_component(
            path,
            data={
                'name': 'ftrackreview-mp4-1080'
            },
            location=loc
        )
        component['name']=REVIEW_COMPONENT
        component['metadata']['ftr_meta'] = json.dumps({
            'frameIn': 1,
            'frameOut': bpy.context.scene.frame_end - bpy.context.scene.frame_start,
            'frameRate': int(float(os.environ["CEREBRO_B3D_CURRENT_PROJECT_FPS"])),
            'height': int(float(os.environ["CEREBRO_B3D_CURRENT_PROJECT_HEIGHT"])),
            'width': int(float(os.environ["CEREBRO_B3D_CURRENT_PROJECT_WIDTH"]))
        })
        sess.commit()

        return(True, "Ok!")
    except Exception as e:
        print(f'{traceback.format_exc()}')
        return(False, f'{e}')


def push_version_to_cerebro(task, paths, version, description, sess=False, close_session=True, location="ftrack.server", status_name="в работе") -> tuple:
    """Загрузка версии на Ftrack. 

    .. note:: Тип актива определяется автоматом от типа задачи.

    Parameters
    ----------
    task : Task
        текущая задача
    paths : list
        список путей до файлов, которые будут загружены с версией.
    version : int
        номер локальной версии.
    description : str
        комментарий к данной версии.
    sess : Session
        сессия, если не передавать, то будет создана.
    close_session : bool
        Если True, то сессия будет закрыта по завершению запроса.
    location : str
        имя локации ftrack.
    status_name : str optional
        Имя статуса, по умолчанию *In progress*.
    """
    if not sess or not sess.connected:
        b,sess = get_session()
        if not b:
            return(False, sess)

    try:
        # type_parent=os.environ["CEREBRO_B3D_CURRENT_ASSET_TYPE"].replace(' ', '')
        # print(type_parent)

        # asset_parent = sess.get(type_parent, task["parent"]["id"])
        # # GET ASSET_TYPE
        # if task["type"]["name"] in ("Modeling", "Lookdev", "Shading"):
        #     asset_type = sess.query('AssetType where name is "Geometry"').one()
        # elif task["type"]["name"]=="Rigging":
        #     asset_type = sess.query('AssetType where name is "Rig"').one()
        # elif task["type"]["name"] in ("Animation", "Layout"):
        #     asset_type = sess.query('AssetType where name is "Animation"').one()
        # else:
        #     asset_type = sess.query('AssetType where name is "Upload"').one()
        # # GET ASSET
        # asset_name = f'Commit_{task["name"]}'
        # assets = sess.query(f"Asset where name is {asset_name} and context_id is {asset_parent['id']}")
        # if assets:
        #     asset = assets[0]
        # else:
        #     asset = sess.create('Asset', {
        #         'name': asset_name,
        #         'type': asset_type,
        #         'parent': asset_parent
        #     })
        # # CREATE VERSION
        # asset_version = sess.create('AssetVersion', {
        #     'asset': asset,
        #     'task': task,
        #     'comment': description
        # })
        # try:
        #     status = sess.query(f"Status where name = '{status_name}'").one()
        #     asset_version["status"]=status
        # except Exception as e:
        #     print(e)
        # sess.commit()

        # loc = sess.query(f'Location where name="{location}"').one()
        # print(loc)
        # for path in paths:
        #     component = asset_version.create_component(
        #         path, location=loc
        #     )
        #     component['name']=os.path.splitext(os.path.basename(path))[0]
        # sess.commit()

        # DOWNLOAD task data to cerebro (!!!) в один месседж и коммит и плейбласт.
        delta=round(timer_stop()[1])
        timer_reset()
        b,created_data=push_task_data_to_cerebro(task, paths[0], description, work_time=delta)
        if not b:
            raise Exception(created_data)
        asset_version={"message":sess.message(created_data[0]), "attachment":sess.message_attachments(created_data[0])[0]}

        # CHANGE task status
        b,r=task_status_to(task, status_name)
        if not b:
            raise Exception(r)

        # ID OF VERSION TO LOCAL BD
        # -- WORK VERSION
        b,r=db.update_work_version(task, version, created_data[0])
        if not b:
            print(r)
        # -- LOADED VERSION
        b,r=db.exists_loaded_version(asset_version)
        if not b:
            print(r)
        elif r[0]=="missing":
            b,r=db.add_loaded_version(asset_version)
            if not b:
                print(r)
        elif r[0]=="old":
            b,r=db.update_loaded_version(asset_version)
            if not b:
                print(r)
        # END
        # timer_stop(sess=sess)
        timer_start(sess=sess)
        return(True, "Ok!")
    except Exception as e:
        print(f'{traceback.format_exc()}')
        return(False, e)

def get_task_templates(sess=False):
    """Возвращает список TaskTemplate объектов (словари с ключами ``name``, ``id``). """
    if not sess or not sess.connected:
        b, sess=get_session()
        if not b:
            return(False, sess)

    templates=list()
    try:
        for t in sess.root_tasks():
            if t[dbtypes.TASK_DATA_PROJECT_ID]==settings.TEMPLATE_PROJECT_ID:
                children=sess.task_children(t[dbtypes.TASK_DATA_ID])
                for ch in children:
                    templates.append({"name": ch[dbtypes.TASK_DATA_NAME], "id":ch[dbtypes.TASK_DATA_ID]})

    except Exception as e:
        print(f'{traceback.format_exc()}')
        return(False, e)

    #(fin)
    return(True, templates)


def drop_link_tasks(lid):
    """Удаление связи по её ``id`` """
    if not G.sess or not G.sess.connected:
        b, sess=get_session()
        if not b:
            return(False, sess)

    G.sess.drop_link_tasks(lid)

    return (True, "Ok!")


#### Utilites

def clean_final_slash(path):
    while path.endswith('/'):
        path=path[:-1]
    return path