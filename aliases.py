# -*- coding: utf-8 -*-

import os
import logging

logger=logging.getLogger(__name__)

ALIASES={
    'asset_name': "CEREBRO_B3D_CURRENT_ASSET_NAME",
    'asset_type': "CEREBRO_B3D_CURRENT_ASSET_TYPE",
    'project_name': "CEREBRO_B3D_CURRENT_PROJECT_NAME",
    'project_full_name': "CEREBRO_B3D_CURRENT_PROJECT_FULLNAME",
}
"""dict : Словарь соответсвия псевдонимов и переменных окружения на замену. """

SEPARATOR='%'

def _get_alias(alias):
    """Возвращает замену псевдониму. """
    if alias.lower() in ALIASES.keys():
        return os.environ.get(ALIASES[alias], '')
    else:
        return ''

def format(template):
    """Замена строковых шаблонов на значения из переменных окружения из :attr:`ALIASES`.
    
    Parameters
    -----------
    template : str
        Строка содержащая шаблоны на замену. ``%alias%``

    Returns
    --------
    str
        Форматированная строка.
    """
    output_string=''
    status='main'
    for i in template:
        if i!=SEPARATOR and status=='main':
            output_string=output_string+i
            # logger.info(f"{status} - {output_string}")
            continue
        elif i==SEPARATOR and status=='main':
            status='alias'
            alias=''
            # logger.info(f"{status} - {output_string}")
            continue
        elif i!=SEPARATOR and status=='alias':
            alias=alias+i
            # logger.info(f"{status} - {output_string}")
            continue
        elif i==SEPARATOR and status=='alias':
            output_string=output_string+_get_alias(alias)
            status='main'
            # logger.info(f"{status} - {output_string}")
            continue
    return output_string