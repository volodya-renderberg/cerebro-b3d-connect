# -*- coding: utf-8 -*-

"""Действия не связанные с **Cerebro**"""

import json
import tempfile
import os

import bpy

try:
    import settings
except:
    from . import settings


def search(search: str, target: str) -> bool:
    """Поиск  ``search`` в ``target`` """
    return all([s.lower() in target.lower() for s in search.split()])


def make_excipients_collection(context, change_view_layer=True):
    """Проверяет наличие и создаёт при отсутствии коллекцию :ref:`settings.ANIMATORS_COLLECTION` """
    if os.environ.get("CEREBRO_B3D_CURRENT_ASSET_TYPE") and os.environ.get("CEREBRO_B3D_CURRENT_ASSET_TYPE")=="Shot":
        name=settings.ANIMATORS_COLLECTION
        exists=False
        for c in context.scene.collection.children_recursive:
            if c.name==name:
                exists=True
        if not exists:
            if change_view_layer:
                context.view_layer.active_layer_collection=context.view_layer.layer_collection
            c=bpy.data.collections.new(name=name)
            context.collection.children.link(c)


def _get_sequencer(context):
    """Делает проверку на наличие и возвращает секвенсор.

    Parameters
    ----------
    context : bpy.context
        КОнтекст блендера

    Returns
    -------
    tuple
        (True, context) или (False, comment)
    """
    area=None
    for a in context.window.workspace.screens[0].areas:
        if a.type == "SEQUENCE_EDITOR":
            area=a
            break
    if not area:
        return(False, "No found SEQUENCE_EDITOR!")
    sequencer = context.scene.sequence_editor
    return(True, sequencer)


def refresh_proxy(context):
    """
    Пересоздаёт выделенный прокси,
    с сохранением положения и анимационного экшена.
    Копирует ``child_off`` констрейн на **root** контрол и на сам прокси объект(если есть).
    """
    #(get data)
    ob_data={}
    ob_child_ofs={}
    root_child_ofs={}
    objects=[]
    for ob in context.selected_objects:
        objects.append(ob.name)
    for name in objects:
        ob=bpy.data.objects[name]
        #(ob_data)
        ob_data[ob.name]={}
        if ob.proxy:
            ob_data[ob.name]["original"]=ob.proxy
        else:
            ob_data[ob.name]["original"]=None
        ob_data[ob.name]["loc"]=ob.location[:]
        ob_data[ob.name]["rot"]=ob.rotation_euler[:]
        ob_data[ob.name]["scl"]=ob.scale[:]
        if ob.animation_data:
            ob_data[ob.name]["action"]=ob.animation_data.action
        else:
            ob_data[ob.name]["action"]=None
        #(object CHILD_OF)
        ob_child_ofs[ob.name]=[]
        for constr in ob.constraints:
            if constr.type=="CHILD_OF":
                ob_child_ofs[ob.name].append({
                    "name":constr.name,
                    "target":constr.target.name,
                    "subtarget":constr.subtarget,
                    })
                ob.constraints.remove(constr)
        #(root bone CHILD_OF)
        ob.select_set(True)
        context.view_layer.objects.active = ob
        bpy.ops.object.mode_set(mode='POSE')
        pose_root=None
        if "root" in ob.pose.bones:
            pose_root=ob.pose.bones["root"]
        if pose_root:
            root_child_ofs[ob.name]=[]
            for constr in pose_root.constraints:
                if constr.type=="CHILD_OF":
                    root_child_ofs[ob.name].append({
                        "name":constr.name,
                        "target":constr.target.name,
                        "subtarget":constr.subtarget,
                        })
                    pose_root.constraints.remove(constr)
    #(remove)
    for name in objects:
        ob=bpy.data.objects[name]
        if ob_data[name]["original"]:
            ob.select_set(True)
            context.view_layer.objects.active = ob
            bpy.ops.object.mode_set(mode='OBJECT')
            bpy.data.objects.remove(ob, do_unlink=True)

    tmpdir=tempfile.gettempdir()
    with open(os.path.join(tmpdir,'ob_data.json'), 'w') as f:
        json.dump(str(ob_data), f, sort_keys=True, indent=4)
    with open(os.path.join(tmpdir,'ob_child_ofs.json'), 'w') as f:
        json.dump(str(ob_child_ofs), f, sort_keys=True, indent=4)
    with open(os.path.join(tmpdir,'root_child_ofs.json'), 'w') as f:
        json.dump(str(root_child_ofs), f, sort_keys=True, indent=4)
    with open(os.path.join(tmpdir,'objects.json'), 'w') as f:
        json.dump(str(objects), f, sort_keys=True, indent=4)
        
    #(create proxy)
    for name in objects:
        if "original" in ob_data[name]:
            original=ob_data[name]["original"]
            original.select_set(True)
            context.view_layer.objects.active = original
            #(create proxy)
            bpy.ops.object.proxy_make(object='DEFAULT')
            proxy=bpy.data.objects[f"{original.name}_proxy"]
            #(-- hide original)
            original.select_set(False)
            original.hide_set(True)
            #(-- activate proxy)
            proxy.select_set(True)
            context.view_layer.objects.active = proxy
            #(-- proxy position)
            proxy.location=ob_data[name]["loc"]
            proxy.rotation_euler=ob_data[name]["rot"]
            proxy.scale=ob_data[name]["scl"]
            #(-- action)
            if ob_data[name]["action"]:
                proxy.animation_data_create()
                proxy.animation_data.action=ob_data[name]["action"]

    #(create constraints)
    for name in objects:
        ob=bpy.data.objects[name]
        ob.select_set(True)
        context.view_layer.objects.active = ob
        #(-- pose constraint)
        bpy.ops.object.mode_set(mode='POSE')
        if "root" in ob.pose.bones:
            pose_root=ob.pose.bones["root"]
            if name in root_child_ofs:
                for item in root_child_ofs[name]:
                    constr=pose_root.constraints.new("CHILD_OF")
                    constr.name=item["name"]
                    # try:
                    # constr.target=item["target"]
                    constr.target=bpy.data.objects[item["target"]]
                    constr.subtarget=item["subtarget"]
                    constr.influence=0
                    # except Exception as e:
                    #     print(e)
                    #     print(f'{"*"*50} {item["target"]} {item["subtarget"]}')
        #(-- ob constraint)
        bpy.ops.object.mode_set(mode='OBJECT')
        if name in ob_child_ofs:
            for item in ob_child_ofs[name]:
                constr=ob.constraints.new("CHILD_OF")
                constr.name=item["name"]
                # try:
                # constr.target=item["target"]
                constr.target=bpy.data.objects[item["target"]]
                constr.subtarget=item["subtarget"]
                constr.influence=0
                # except Exception as e:
                #     print(e)
                #     print(f'{"*"*50} {item["target"]} {item["subtarget"]}')
        
        return(True, "Ok!")


def _bonekeyframe_ON_keys(context, bone, child_of):
    """Пара ключей, в текущем кадре визуал, в следующем - обычные. """
    start_frame=context.scene.frame_current-1
    end_frame=context.scene.frame_current

    #(end rame)
    bone.keyframe_insert("location", frame=end_frame, options={'INSERTKEY_VISUAL'})
    if bone.rotation_mode=='QUATERNION':
        bone.keyframe_insert("rotation_quaternion", frame=end_frame, options={'INSERTKEY_VISUAL'})
    else:
        bone.keyframe_insert("rotation_euler", frame=end_frame, options={'INSERTKEY_VISUAL'})
    bone.keyframe_insert("scale", frame=end_frame)

    #(C)
    child_of.influence=1
    child_of.keyframe_insert("influence", frame=end_frame)

    #(start frame)
    bone.keyframe_insert("location", frame=start_frame, options={'INSERTKEY_VISUAL'})
    if bone.rotation_mode=='QUATERNION':
        bone.keyframe_insert("rotation_quaternion", frame=start_frame, options={'INSERTKEY_VISUAL'})
    else:
        bone.keyframe_insert("rotation_euler", frame=start_frame, options={'INSERTKEY_VISUAL'})
    bone.keyframe_insert("scale", frame=start_frame, options={'INSERTKEY_VISUAL'})
    #(C)
    child_of.influence=0
    child_of.keyframe_insert("influence", frame=start_frame)

    context.scene.frame_set(context.scene.frame_current - 1)
    context.scene.frame_set(context.scene.frame_current + 1)
    bpy.ops.constraint.childof_set_inverse(constraint=child_of.name, owner='BONE')
    
    
def _bonekeyframe_OFF_keys(context, bone, child_of):
    """Пара ключей, в текущем кадре визуал, в следующем - обычные. """
    start_frame=context.scene.frame_current-1
    end_frame=context.scene.frame_current
    #(no visual)
    # bone.keyframe_insert("location", frame=start_frame, options={'INSERTKEY_NEEDED'})
    bone.keyframe_insert("location", frame=start_frame)
    if bone.rotation_mode=='QUATERNION':
        # bone.keyframe_insert("rotation_quaternion", frame=start_frame, options={'INSERTKEY_NEEDED'})
        bone.keyframe_insert("rotation_quaternion", frame=start_frame)
    else:
        # bone.keyframe_insert("rotation_euler", frame=start_frame, options={'INSERTKEY_NEEDED'})
        bone.keyframe_insert("rotation_euler", frame=start_frame)
    # bone.keyframe_insert("scale", frame=start_frame, options={'INSERTKEY_NEEDED'})
    bone.keyframe_insert("scale", frame=start_frame)

    #(C)
    child_of.influence=1
    child_of.keyframe_insert("influence", frame=start_frame)

    #(visual)
    # bone.keyframe_insert("location", frame=end_frame, options={'INSERTKEY_VISUAL', 'INSERTKEY_NEEDED'})
    bone.keyframe_insert("location", frame=end_frame, options={'INSERTKEY_VISUAL'})
    if bone.rotation_mode=='QUATERNION':
        # bone.keyframe_insert("rotation_quaternion", frame=end_frame, options={'INSERTKEY_VISUAL', 'INSERTKEY_NEEDED'})
        bone.keyframe_insert("rotation_quaternion", frame=end_frame, options={'INSERTKEY_VISUAL'})
    else:
        # bone.keyframe_insert("rotation_euler", frame=end_frame, options={'INSERTKEY_VISUAL', 'INSERTKEY_NEEDED'})
        bone.keyframe_insert("rotation_euler", frame=end_frame, options={'INSERTKEY_VISUAL'})
    # bone.keyframe_insert("scale", frame=end_frame, options={'INSERTKEY_VISUAL', 'INSERTKEY_NEEDED'})
    bone.keyframe_insert("scale", frame=end_frame, options={'INSERTKEY_VISUAL'})

    #(C)
    child_of.influence=0
    child_of.keyframe_insert("influence", frame=end_frame)

    context.scene.frame_set(context.scene.frame_current - 1)
    context.scene.frame_set(context.scene.frame_current + 1)


def _bone_keyframe_insert(context, bone, frame=None, interpolation="BEZIER"):
    """Ключи t,r,s на кость.
    
    interpolation in: CONSTANT, BEZIER
    """
    if not frame:
        frame=context.scene.frame_current
    bone.keyframe_insert("location", frame=frame)
    if bone.rotation_mode=='QUATERNION':
        bone.keyframe_insert("rotation_quaternion", frame=frame)
    else:
        bone.keyframe_insert("rotation_euler", frame=frame)
    bone.keyframe_insert("scale", frame=frame)

    rig=context.object
    fcurves=list()
    #(location)
    for i in range(3):
        f=rig.animation_data.action.fcurves.find(f'pose.bones["{bone.name}"].location', index=i)
        fcurves.append(f)
    #(scale)
    for i in range(3):
        f=rig.animation_data.action.fcurves.find(f'pose.bones["{bone.name}"].scale', index=i)
        fcurves.append(f)
    #(rotation)
    if bone.rotation_mode=='QUATERNION':
        end=4
        path="rotation_quaternion"
    else:
        end=3
        path="rotation_euler"
    for i in range(end):
        f=rig.animation_data.action.fcurves.find(f'pose.bones["{bone.name}"].{path}', index=i)
        fcurves.append(f)

    for f in fcurves:
        for p in f.keyframe_points:
            if p.co[0]==frame:
                p.interpolation=interpolation
    
    
def _child_of_keyframe_insert(context, child_of, on=True):
    """
    Parameters
    -----------
    context : bpy.data.Context
        context
    child_of : bpy.data.BoneConstraint
    """
    child_of.influence=int(not on)
    child_of.keyframe_insert("influence", frame=context.scene.frame_current-1)
    child_of.influence=int(on)
    child_of.keyframe_insert("influence")
    

def child_of_on(context):
    """
    Создаёт чилд офы только от простых объектов, не арматур.

    * Чтобы создать чилдоф надо выделить объект-таргет и затем риг, потом в POSE_MODE выделить кость для которой создаётся чилдоф, спозиционировать её и выполнить эту процедуру.
    * Чтобы обозначить включение существующего чилдофа кости, надо просто выделить эту кость и запустить процедуру.
    """
    #(get_bone)
    if not context.selected_pose_bones:
        return (False, "Не выделена ни одна кость!")
    bone=context.active_pose_bone
    #(get child_of)
    child_of=None
    for c in bone.constraints:
        if c.type=="CHILD_OF":
            child_of=c
    if not child_of:
        #(-- get object)
        ob=None
        if len(context.selected_objects)>2:
            return (False, "Выделено слишком много объектов!")
        elif len(context.selected_objects)==1:
            return (False, "Не выбрано объекта привязки!")
        else:
            for obj in context.selected_objects:
                if obj.type!="ARMATURE":
                    ob=obj
        if not ob:
            (False, "Объект привязки не правильного типа!")
        #(keyframes)
        #m=bone.matrix.copy()
        # _bone_keyframe_insert(context, bone, interpolation="CONSTANT")
        # _bone_keyframe_insert(context, bone, frame=context.scene.frame_current-1)
        #(child_off)
        child_of=bone.constraints.new("CHILD_OF")
        child_of.target=ob
        # _child_of_keyframe_insert(context, child_of)
        _bonekeyframe_ON_keys(context, bone, child_of)
    else:
        # m=bone.matrix.copy()                      
        # child_of.influence=0
        # bone.matrix=m
        # _bone_keyframe_insert(context, bone, interpolation="CONSTANT")
        
        # _bone_keyframe_insert(context, bone, frame=context.scene.frame_current-1)
        # _child_of_keyframe_insert(context, child_of)
        _bonekeyframe_ON_keys(context, bone, child_of)

    return (True, "Ok!")


def child_of_off(context):
    """Отключение чилдофа, выделенной кости. """
    #(get_bone)
    if not context.selected_pose_bones:
        return (False, "Не выделена ни одна кость!")
    bone=context.active_pose_bone
    #(get child_of)
    child_of=None
    for c in bone.constraints:
        if c.type=="CHILD_OF":
            child_of=c
    if not child_of:
        return (False, "У данной кости нет CHILD_OF констрейна!")
    else:
        # m=bone.matrix.copy()                      
        # child_of.influence=1
        _bonekeyframe_OFF_keys(context, bone, child_of)
        # _child_of_keyframe_insert(context, child_of, on=False)
        # bone.matrix=m
        # _bone_keyframe_insert(context, bone, frame=context.scene.frame_current)


class B3DTOOLS_child_of_on_off(bpy.types.Operator):
    bl_idname = "b3dtools.child_of_on_off"
    bl_label = "Text"

    action: bpy.props.StringProperty(default='ON')

    def execute(self, context):
        if self.action=="ON":
            child_of_on(context)
        else:
            child_of_off(context)

        self.report({'INFO'}, f"Child_of {self.action}")
        return{'FINISHED'}