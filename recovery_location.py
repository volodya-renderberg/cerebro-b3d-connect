# -*- coding: utf-8 -*-

import os

import bpy

try:
    import path_utils
    import settings
except:
    from . import path_utils
    from . import settings

ADDED_NAMES=["ballon_rope",
    "Bed_for boy",
    "Bad_for_girls",
    "Ballon",
    # "Bams_body",
    # "Bams_facial",
    # "Gudy_body",
    # "Gudy_facial",
    # "Kafa_body",
    # "Kafa_facial",
    # "Nyn_body",
    # "Nyn_facial",
    ]


CHARACTERS=[
    "Bams_body",
    "Bams_facial",
    "Gudy_body",
    "Gudy_facial",
    "Kafa_body",
    "Kafa_facial",
    "Nyn_body",
    "Nyn_facial",
]


def _get_rig_data(ob):
    """возвращает словарь параметров объекта по ключам: location, rotation, scale, action."""
    ob_data=dict()
    ob_data['location']=(ob.location[0], ob.location[1], ob.location[2])
    ob_data['rotation']=(ob.rotation_euler[0], ob.rotation_euler[1], ob.rotation_euler[2])
    ob_data['scale']=(ob.scale[0], ob.scale[1], ob.scale[2])
    if ob.animation_data and ob.animation_data.action:
        ob_data['action']=(ob.animation_data.action.name)
    else:
        ob_data['action']=None
    #(CHILD_OF)
    try:
        bpy.context.view_layer.objects.active = ob
        bpy.ops.object.mode_set(mode='POSE')
        pose_root=None
        if "root" in ob.pose.bones:
            pose_root=ob.pose.bones["root"]
        #(pose CHILD_OF)
        ob_data['child_offs']=dict()
        if pose_root:
            for constr in pose_root.constraints:
                if constr.type=="CHILD_OF":
                    ob_data['child_offs'][constr.name]=dict()
                    ob_data['child_offs'][constr.name]['target']=constr.target.name
                    ob_data['child_offs'][constr.name]['subtarget']=constr.subtarget
        bpy.ops.object.mode_set(mode='OBJECT')
    except:
        pass
    #(end)
    return ob_data


def _set_rig_data(ob, data):
    ob.location[0]=data['location'][0]
    ob.location[1]=data['location'][1]
    ob.location[2]=data['location'][2]
    ob.rotation_euler[0]=data['rotation'][0]
    ob.rotation_euler[1]=data['rotation'][1]
    ob.rotation_euler[2]=data['rotation'][2]
    ob.scale[0]=data['scale'][0]
    ob.scale[1]=data['scale'][1]
    ob.scale[2]=data['scale'][2]
    if data['action']:
        if not ob.animation_data:
            ob.animation_data_create()
        ob.animation_data.action=bpy.data.actions[data['action']]


def _get_empty_data():
    data=dict()
    for n in ADDED_NAMES:
        data[n]=list()
    #(--)
    for l in bpy.data.libraries:
        name=os.path.basename(l.filepath).replace('.blend','')
        data[name]=list()

    return data


def _get_meta_folder():
    meta_folder=data_path=os.path.join(os.path.dirname(bpy.data.filepath), "meta")
    path_utils._create_path(meta_folder, True)

    return meta_folder


def save_proxy_data(context):
    """Сохраняет данные персонажей. """
    #(folder path)
    meta_folder=_get_meta_folder()

    data=dict()

    for name in CHARACTERS:
        data[name]=list()
        for ob in bpy.data.objects:
            if f"{name}_rig_proxy" in ob.name and ob.type=="ARMATURE":
                data[name].append(_get_rig_data(ob))

    #(--)
    path_utils._backup_update_folder_data(meta_folder, data, file_name="recovery_proxy.json")


def save_data(context):
    """Сохранение позиций и экшенов ригов из списка библиотек.  """
    #(folder path)
    meta_folder=_get_meta_folder()
    
    #(save data)
    data=_get_empty_data()
    
    #(--)  
    for name in data.keys():
        for ob in bpy.data.objects:
            if f"{name}_rig" in ob.name and ob.type=="ARMATURE":
                data[name].append(_get_rig_data(ob))
    #(--)
    path_utils._backup_update_folder_data(meta_folder, data, file_name=settings.RECOVERY_DATA)

    #(fake user)
    for action in bpy.data.actions:
        action.use_fake_user=True

    #(proxy)
    save_proxy_data(context)

    return(True, "Ok!")


def set_proxy_data(context):
    meta_folder=_get_meta_folder()

    saved_data=path_utils._backup_get_folder_data(meta_folder, file_name="recovery_proxy.json")

    data=dict()

    for name in saved_data.keys():
        data[name]=list()
        for ob in bpy.data.objects:
            if f"{name}_rig_proxy" in ob.name and ob.type=="ARMATURE":
                if ob in data[name]:
                    continue
                data[name].append(ob)
        # try:
        #     data[name]=tuple(set(data[name]))
        # except:
        #     pass

    print(saved_data.keys())
    print(data.keys())

    for k in data.keys():
        for i,ob in enumerate(data[k]):
            try:
                _set_rig_data(ob, saved_data[k][i])
            except Exception as e:
                print('#'*100)
                print(e)
                print(k)
                print(data[k])
                print(saved_data[k])


def set_data(context):
    #(folder path)
    meta_folder=_get_meta_folder()

    saved_data=path_utils._backup_get_folder_data(meta_folder, file_name=settings.RECOVERY_DATA)

    data=_get_empty_data()

    for name in saved_data.keys():
        for ob in bpy.data.objects:
            if f"{name}_rig" in ob.name and ob.type=="ARMATURE":
                try:
                    data[name].append(ob)
                except:
                    pass
        try:
            data[name]=tuple(set(data[name]))
        except:
            pass

    for k in data.keys():
        for i,ob in enumerate(data[k]):
            try:
                _set_rig_data(ob, saved_data[k][i])
            except Exception as e:
                print('#'*100)
                print(e)
                print(k)
                print(data[k])
                print(saved_data[k])


    set_proxy_data(context)

    return(True, "Ok!")
