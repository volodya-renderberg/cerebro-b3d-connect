# -*- coding: utf-8 -*-

"""
Процедуры бекапа.
"""

import logging
import os
import datetime
import shutil

from .. import (
    path_utils,
    db,
    server_connect as srvconn
)
from ..myJSON import myJSON
from pycerebro import dbtypes


logger=logging.getLogger(__name__)


ATTACHMENT_SAVED_TAGS=(0,4,5,6)
"""tuple: теги аттачментов, которые сохраняются, остальное в игнор. """


TABLES={
    "Statuses":[
        'id INTEGER PRIMARY KEY',
        'name TEXT',
        'description TEXT',
        'color INTEGER',
    ],
    "Activities":[
        'id INTEGER PRIMARY KEY',
        'name TEXT',
        'color INTEGER',
    ],
    "Users":[
        'id INTEGER PRIMARY KEY',
        'login TEXT',
        'email TEXT',
        'first_name TEXT',
        'last_name TEXT',
        'full_name TEXT',
        'phone TEXT',
    ],
    "Tasks":[
        'id INTEGER PRIMARY KEY',
        'activity_id INTEGER', #Идентификатор вида деятельности.
        'activity_name TEXT', #Имя вида деятельности.
        'allocated TEXT', #Назначенные пользователи (исполнители) на задачу. Тип string. Разделитель „;“. USER_DATA_FULL_NAME
        'budget FLOAT', #Бюджет задачи с её подзадачами.
        'cc_status INTEGER', #Рассчитанный статус задачи. Рассчитанный статус может отличатся от собственного, поскольку он может быть унаследован от контейнера. Рассчетный статус является актуальным статусом задачи.
        'created TIMESTAMP', #Время создания задачи.
        'creator_id INTEGER', #Идентификатор автора задачи.
        'duration FLOAT', #Рассчитанная длительность задачи в днях. Тип float.
        'human_finish FLOAT', #Заданное время окончания задачи в днях от 01.01.2000. Тип float.
        'human_start FLOAT', #Заданное время начала задачи в днях от 01.01.2000. Тип float.
        'moderator_id INTEGER', #Идентификатор пользователя, изменившего задачу.
        'modified TIMESTAMP', #Время изменения задачи.
        'mtm TIMESTAMP', #Время модификации данных.
        'name TEXT', #Имя задачи.TIMESTAMP
        'offset FLOAT', #Рассчитанное время начала задачи в днях от 01.01.2000. Тип float.
        'parent_id INTEGER', #Идентификатор родительской задачи.
        'parent_url TEXT', #Полный путь до родителькой задачи. Пример: /Test project/Scene 1/
        'planned FLOAT', #Запланированное время на задачу в часах. Тип float.
        'priority INTEGER', #Приоритет задачи.
        'privilege INTEGER', #Права доступа текущего пользователя к задаче. Тип integer.
        'progress FLOAT', #Прогресс задачи. Тип float, от 0.0 до 100.0.
        'project_id INTEGER', #Идентификатор проекта задачи.
        'resource_approved FLOAT', #Принятое время материальных ресурсов по задаче с её подзадачами в часах. Тип float.
        'resource_declared FLOAT', #Заявленное время материальных ресурсов по задаче с её подзадачами в часах. Тип float.
        'resource_self_approved FLOAT', #Принятое время материальных ресурсов по задаче в минутах. Тип float.
        'resource_self_declared FLOAT', #Заявленное время материальных ресурсов по задаче в минутах. Тип float.
        'self_budget FLOAT', #Бюджет задачи.
        'self_spent FLOAT', #Затраты (сумма платежей) по задаче.
        'self_status INTEGER', #Собственный статус задачи. Статус установленный пользователем.
        'self_users_approved FLOAT', #Принятое время пользователей по задаче в минутах. Тип float.
        'self_users_declared FLOAT', #Заявленное время пользователей по задаче в минутах. Тип float.
        'spent FLOAT', #Затраты (сумма платежей) по задаче с её подзадачами. Тип float.
        'users_approved FLOAT', #Принятое время пользователей по задаче и её подзадачам в часах. Тип float.
        'users_declared FLOAT', #Заявленное время пользователей по задаче и её подзадачам в часах. Тип float.
    ],
    "Messages":[
        'id INTEGER PRIMARY KEY',
        'approved_time INTEGER', # Принятое время в минутах.
        'created TIMESTAMP', # Время создания сообщения.
        'creator_id INTEGER', # Идентификатор автора сообщения.
        'creator_name TEXT', # Имя автора сообщения.
        'moderator_id INTEGER', # Идентификатор пользователя, изменившего сообщение.
        'moderator_name TEXT', # Имя пользователя, изменившего сообщение.
        'mtm TIMESTAMP', # Время модификации данных.
        'pid INTEGER', # Идентификатор родительского сообщения.
        'status_id INTEGER', # Идентификатор статуса.
        'text TEXT', # Текст сообщения в формате HTML.
        'tid INTEGER', # Идентификатор задачи, к которой относится сообщение.
        'type INTEGER', # Тип сообщения. 
        'work_time INTEGER', # Рабочее время в минутах.
        'xmtm TIMESTAMP', # Реальное время модификации данных.
    ],
    "Attachments":[
        'id INTEGER PRIMARY KEY',
        'group_id INTEGER', # Идентификатор вложения.
        'comment TEXT', # Комментарий к файлу.
        'created TIMESTAMP', # Время создания.
        'del INTEGER', # Если значение этого поля не равно 0, значит вложение удалено.
        'event_id INTEGER', # Идентификатор сообщения.
        'file_name TEXT', # Идентификатор сообщения.
        'file_size INTEGER', # Размер файла вложения в байтах.
        'hash TEXT', # Хеш файла.
        'mtm TIMESTAMP', # Время модификации данных.
        'tag INTEGER', # Тип файла вложения.
    ],
    "Links":[
        'id INTEGER PRIMARY KEY',
        'del BOOL', # Если значение этого поля равно True, значит связь удалена.
        'dst INTEGER', # Задача, к которой идет связь.
        'mtm TIMESTAMP', # Время модификации данных.
        'src INTEGER', # Задача, от которой идет связь.
    ]
}
"""dict: таблицы бд на создание."""

LOG_TABLES_FIELDS={
    "Statuses": ("id","name","description"),
    "Activities": ("id","name"),
    "Users": ("id","login","full_name"),
    "Tasks": ("activity_name","parent_url","name"),
    "Messages": ("id", "tid", "text"),
    "Attachments": ("event_id", "group_id", "file_name", "file_size"),
    "Links":("id", "src", "dst"),
}
"""dict: словарь по именам таблиц, со списком полей, которые отображаются в процедуре :func:`backup_log_db`"""


def backup_log_db(table):
    """Чтение базы данных, с построчным логом полей. Читаемые поля берутся из :attr:`LOG_TABLES_FIELDS`
    
    Parameters
    -----------
    table : str
        имя таблицы
    """
    #(db path)
    db_path=path_utils.get_backup_db_path()
    if not db_path:
        return (False, "@Путь до файла базы данных бекапа не возможно обнаружить! | Убедитесь что проект активен и определена бекап директория.")

    b,r=db._get(db_path, table)
    if not b:
        return (b, r)
    logger.info(f"Чтение таблицы: '{table}'")
    for row in r:
        drow=dict(row)
        rstr=""
        for f in LOG_TABLES_FIELDS[table]:
            rstr=f"{rstr}, {f}:{drow[f]}"
        logger.info(rstr)
    return(True, "Ok!")


def _backup_write_string_to_db(db_path, table, kw, ob, log_key, test_where, update=True):
    """
    Parameters
    ----------
    db_path : str
        путь до базы данных
    table : str
        имя таблицы
    kw : dict
        словарь сохраняемых данных
    ob : list
        сохраняемый объект
    log_key : int
        значение ``db_types`` - для отображения в логах
    test_where : dict
        словарь по которому идентифицируется данный объект (в основном по ``id``)
    update : bool
        обновлять или нет

    Returns
    ---------
    None
    """
    #(create table)
    b,r=db._db_create_table(db_path, table, table_data=TABLES[table])
    if not b:
        raise Exception(f"in _db_create_table() '{table}' - {r}")
    #(test exists)
    b,r=db._get(db_path, table, where=test_where)
    if not b:
        logger.warning(f"{ob[log_key]} - {r}")
    else:
        if not r:
            #(insert)
            b,r=db._insert(db_path, table, **kw)
            if not b:
                logger.warning(f"{ob[log_key]} - {r}")
            else:
                logger.info(f"{ob[log_key]} - сохранено!")
        else:
            #(update)
            if update:
                b,r=db._update(db_path, table, test_where, kw)
                if not b:
                    logger.warning(f"{ob[log_key]} - {r}")
                else:
                    logger.info(f"{ob[log_key]} - обновлено!")
            else:
                logger.info(f"{ob[log_key]} - существует!")


def save_tasks(db_path):
    """Сохранение задач со всеми месседжами и вложениями. """

    logger.info("")
    logger.info("<<СОХРАНЕНИЕ ЗАДАЧ:>>")

    project_id=os.environ['CEREBRO_B3D_CURRENT_PROJECT_ID']
    # close_status='2927339757819282376'
    delta=datetime.timedelta(days=1)
    date=(datetime.date.today()+delta).strftime("%Y-%m-%d")

    # com="""select null::timestamp with time zone as mtm, * from "search_Tasks"(
    # '{%s}'::bigint[]
    # , ''::text
    # , 't.flags & 0000001 = 0
    # and t.cc_status != %s'::text
    # , 100
    # );"""% (project_id, close_status)

    com="""select null::timestamp with time zone as mtm, * from "search_Tasks"(
    '{%s}'::bigint[]
    , ''::text
    , 't.flags & 0000001 = 0
    and t.creationtime::date <= ''%s'''::text
    , 10000
    );"""% (project_id, date)

    tasks_ids=G.sess.execute(com)

    for task in tasks_ids:
        id=task[1]
        t=G.sess.task(id)
        logger.info('.'*50)
        logger.info(f"{t[dbtypes.TASK_DATA_ACTIVITY_NAME]} - {t[dbtypes.TASK_DATA_PARENT_URL]}{t[dbtypes.TASK_DATA_NAME]}")

        kw={
            'id':t[dbtypes.TASK_DATA_ID],
            'activity_id':t[dbtypes.TASK_DATA_ACTIVITY_ID],
            'activity_name':t[dbtypes.TASK_DATA_ACTIVITY_NAME],
            'allocated':t[dbtypes.TASK_DATA_ALLOCATED],
            'budget':t[dbtypes.TASK_DATA_BUDGET],
            'cc_status':t[dbtypes.TASK_DATA_CC_STATUS], #Рассчитанный статус задачи. Рассчитанный статус может отличатся от собственного, поскольку он может быть унаследован от контейнера. Рассчетный статус является актуальным статусом задачи.
            'created':t[dbtypes.TASK_DATA_CREATED], #Время создания задачи.
            'creator_id':t[dbtypes.TASK_DATA_CREATOR_ID], #Идентификатор автора задачи.
            'duration':t[dbtypes.TASK_DATA_DURATION], #Рассчитанная длительность задачи в днях. Тип float.
            'human_finish':t[dbtypes.TASK_DATA_HUMAN_FINISH], #Заданное время окончания задачи в днях от 01.01.2000. Тип float.
            'human_start':t[dbtypes.TASK_DATA_HUMAN_START], #Заданное время начала задачи в днях от 01.01.2000. Тип float.
            'moderator_id':t[dbtypes.TASK_DATA_MODERATOR_ID], #Идентификатор пользователя, изменившего задачу.
            'modified':t[dbtypes.TASK_DATA_MODIFIED], #Время изменения задачи.
            'mtm':t[dbtypes.TASK_DATA_MTM], #Время модификации данных.
            'name':t[dbtypes.TASK_DATA_NAME], #Имя задачи.TIMESTAMP
            'offset':t[dbtypes.TASK_DATA_OFFSET], #Рассчитанное время начала задачи в днях от 01.01.2000. Тип float.
            'parent_id':t[dbtypes.TASK_DATA_PARENT_ID], #Идентификатор родительской задачи.
            'parent_url':t[dbtypes.TASK_DATA_PARENT_URL], #Полный путь до родителькой задачи. Пример: /Test project/Scene 1/
            'planned':t[dbtypes.TASK_DATA_PLANNED], #Запланированное время на задачу в часах. Тип float.
            'priority':t[dbtypes.TASK_DATA_PRIORITY], #Приоритет задачи.
            'privilege':t[dbtypes.TASK_DATA_PRIVILEGE], #Права доступа текущего пользователя к задаче. Тип integer.
            'progress':t[dbtypes.TASK_DATA_PROGRESS], #Прогресс задачи. Тип float, от 0.0 до 100.0.
            'project_id':t[dbtypes.TASK_DATA_PROJECT_ID], #Идентификатор проекта задачи.
            'resource_approved':t[dbtypes.TASK_DATA_RESOURCE_APPROVED], #Принятое время материальных ресурсов по задаче с её подзадачами в часах. Тип float.
            'resource_declared':t[dbtypes.TASK_DATA_RESOURCE_DECLARED], #Заявленное время материальных ресурсов по задаче с её подзадачами в часах. Тип float.
            'resource_self_approved':t[dbtypes.TASK_DATA_RESOURCE_SELF_APPROVED], #Принятое время материальных ресурсов по задаче в минутах. Тип float.
            'resource_self_declared':t[dbtypes.TASK_DATA_RESOURCE_SELF_DECLARED], #Заявленное время материальных ресурсов по задаче в минутах. Тип float.
            'self_budget':t[dbtypes.TASK_DATA_SELF_BUDGET], #Бюджет задачи.
            'self_spent':t[dbtypes.TASK_DATA_SELF_SPENT], #Затраты (сумма платежей) по задаче.
            'self_status':t[dbtypes.TASK_DATA_SELF_STATUS], #Собственный статус задачи. Статус установленный пользователем.
            'self_users_approved':t[dbtypes.TASK_DATA_SELF_USERS_APPROVED], #Принятое время пользователей по задаче в минутах. Тип float.
            'self_users_declared':t[dbtypes.TASK_DATA_SELF_USERS_DECLARED], #Заявленное время пользователей по задаче в минутах. Тип float.
            'spent':t[dbtypes.TASK_DATA_SPENT], #Затраты (сумма платежей) по задаче с её подзадачами. Тип float.
            'users_approved':t[dbtypes.TASK_DATA_USERS_APPROVED], #Принятое время пользователей по задаче и её подзадачам в часах. Тип float.
            'users_declared':t[dbtypes.TASK_DATA_USERS_DECLARED],
        }
        _backup_write_string_to_db(db_path, "Tasks", kw, t, dbtypes.TASK_DATA_NAME, {"id":t[dbtypes.TASK_DATA_ID]})

        # TASK LINKS
        logger.info('--links--')
        for l in G.sess.task_links(t[dbtypes.TASK_DATA_ID]):
            kw={
                'id':l[dbtypes.TASK_LINK_ID],
                'del':l[dbtypes.TASK_LINK_DEL], # Если значение этого поля равно True, значит связь удалена.
                'dst':l[dbtypes.TASK_LINK_DST], # Задача, к которой идет связь.
                'mtm':l[dbtypes.TASK_LINK_MTM], # Время модификации данных.
                'src':l[dbtypes.TASK_LINK_SRC], # Задача, от которой идет связь.
            }
            _backup_write_string_to_db(db_path, "Links", kw, l, dbtypes.TASK_LINK_ID, {'id':l[dbtypes.TASK_LINK_ID]})

        # TASK MESSEGES
        logger.info('--messages--')
        for m in G.sess.task_messages(t[dbtypes.TASK_DATA_ID]):
            kw={
                'id':m[dbtypes.MESSAGE_DATA_ID],
                'approved_time':m[dbtypes.MESSAGE_DATA_APPROVED_TIME], # Принятое время в минутах.
                'created':m[dbtypes.MESSAGE_DATA_CREATED], # Время создания сообщения.
                'creator_id':m[dbtypes.MESSAGE_DATA_CREATOR_ID], # Идентификатор автора сообщения.
                'creator_name':m[dbtypes.MESSAGE_DATA_CREATOR_NAME], # Имя автора сообщения.
                'moderator_id':m[dbtypes.MESSAGE_DATA_MODERATOR_ID], # Идентификатор пользователя, изменившего сообщение.
                'moderator_name':m[dbtypes.MESSAGE_DATA_MODERATOR_NAME], # Имя пользователя, изменившего сообщение.
                'mtm':m[dbtypes.MESSAGE_DATA_MTM], # Время модификации данных.
                'pid':m[dbtypes.MESSAGE_DATA_PID], # Идентификатор родительского сообщения.
                'status_id':m[dbtypes.MESSAGE_DATA_STATUS_ID], # Идентификатор статуса.
                'text':m[dbtypes.MESSAGE_DATA_TEXT], # Текст сообщения в формате HTML.
                'tid':m[dbtypes.MESSAGE_DATA_TID], # Идентификатор задачи, к которой относится сообщение.
                'type':m[dbtypes.MESSAGE_DATA_TYPE], # Тип сообщения.
                'work_time':m[dbtypes.MESSAGE_DATA_WORK_TIME], # Рабочее время в минутах.
                'xmtm':m[dbtypes.MESSAGE_DATA_XMTM], # Реальное время модификации данных.
            }
            _backup_write_string_to_db(db_path, "Messages", kw, m, dbtypes.MESSAGE_DATA_ID, {'id':m[dbtypes.MESSAGE_DATA_ID]})

            # MESSAGE ATTACHMENTS
            logger.info('--attachments--')
            for a in G.sess.message_attachments(m[dbtypes.MESSAGE_DATA_ID]):
                if not a[dbtypes.ATTACHMENT_DATA_TAG] in ATTACHMENT_SAVED_TAGS or a[dbtypes.ATTACHMENT_DATA_DEL]!=0:
                    continue
                kw={
                    'id':a[dbtypes.ATTACHMENT_DATA_ID],
                    'group_id':a[dbtypes.ATTACHMENT_DATA_GROUP_ID], # Идентификатор вложения.
                    'comment':a[dbtypes.ATTACHMENT_DATA_COMMENT], # Комментарий к файлу.
                    'created':a[dbtypes.ATTACHMENT_DATA_CREATED], # Время создания.
                    'del':a[dbtypes.ATTACHMENT_DATA_DEL], # Если значение этого поля не равно 0, значит вложение удалено.
                    'event_id':a[dbtypes.ATTACHMENT_DATA_EVENT_ID], # Идентификатор сообщения.
                    'file_name':a[dbtypes.ATTACHMENT_DATA_FILE_NAME], # Идентификатор сообщения.
                    'file_size':a[dbtypes.ATTACHMENT_DATA_FILE_SIZE], # Размер файла вложения в байтах.
                    'hash':a[dbtypes.ATTACHMENT_DATA_HASH], # Хеш файла.
                    'mtm':a[dbtypes.ATTACHMENT_DATA_MTM], # Время модификации данных.
                    'tag':a[dbtypes.ATTACHMENT_DATA_TAG], # Тип файла вложения.
                }
                _backup_write_string_to_db(db_path, "Attachments", kw, a, dbtypes.ATTACHMENT_DATA_FILE_NAME, {'id':a[dbtypes.ATTACHMENT_DATA_ID]}, update=False)
                attachment_dir=path_utils.get_backup_attachment_dir(a[dbtypes.ATTACHMENT_DATA_ID], create=True)
                attachment_path=os.path.join(attachment_dir, str(a[dbtypes.ATTACHMENT_DATA_FILE_NAME]))
                if not os.path.exists(attachment_path):
                    b,r=srvconn.download_attachment(a)
                    if not b:
                        logger.warning(r)
                    else:
                        shutil.copyfile(r, attachment_path)
                        logger.info(f"download to: {attachment_path}")
                else:
                    logger.info(f"already exists: {attachment_path}")

    return (True, "Ok!")


def save_statuses(db_path):
    """Запись существующих в студии статусов. """
    logger.info("")
    logger.info("<<СОХРАНЕНИЕ СТАТУСОВ:>>")

    statuses=G.sess.statuses()
    for s in statuses:
        kw=dict(
            id=s[dbtypes.STATUS_DATA_ID],
            name=s[dbtypes.STATUS_DATA_NAME],
            description=s[dbtypes.STATUS_DATA_DESCRIPTION],
            color=s[dbtypes.STATUS_DATA_COLOR],
        )
        _backup_write_string_to_db(db_path, "Statuses", kw, s, dbtypes.STATUS_DATA_NAME, {"id":s[dbtypes.STATUS_DATA_ID]})

    return (True, "Ok!")


def save_activities(db_path):
    """Запись существующих видов деятельностей. """
    logger.info("")
    logger.info("<<СОХРАНЕНИЕ ВИДОВ ДЕЯТЕЛЬНОСТИ:>>")

    activities=G.sess.activities()
    for ob in activities:
        kw=dict(
            id=ob[dbtypes.ACTIVITY_DATA_ID],
            name=ob[dbtypes.ACTIVITY_DATA_NAME],
            color=ob[dbtypes.ACTIVITY_DATA_COLOR],
        )
        _backup_write_string_to_db(db_path, "Activities", kw, ob, dbtypes.ACTIVITY_DATA_NAME, {"id":ob[dbtypes.ACTIVITY_DATA_ID]})

    return (True, "Ok!")


def save_users(db_path):
    """Запись существующих юзеров. """
    logger.info("")
    logger.info("<<СОХРАНЕНИЕ ЮЗЕРОВ:>>")

    users=G.sess.users()
    for ob in users:
        kw=dict(
            id=ob[dbtypes.USER_DATA_ID],
            login=ob[dbtypes.USER_DATA_LOGIN],
            email=ob[dbtypes.USER_DATA_EMAIL],
            first_name=ob[dbtypes.USER_DATA_FIRST_NAME],
            last_name=ob[dbtypes.USER_DATA_LAST_NAME],
            full_name=ob[dbtypes.USER_DATA_FULL_NAME],
            phone=ob[dbtypes.USER_DATA_PHONE],
        )
        _backup_write_string_to_db(db_path, "Users", kw, ob, dbtypes.USER_DATA_LOGIN, {"id":ob[dbtypes.USER_DATA_ID]})

    return (True, "Ok!")


def run(backup_folder, project=None):
    """Main функция бекапа. Данные проекта берутся из переменных окружения.

    Таблицы:

    * Statuses - статусы.
    * ContainerTypes - виды деятельности.
    * Artists - артисты.
    * Containers - Контейнеры задачи. (назначения в атрибуте ``allocated``)
        * Messages - сообщения по задаче, со всем содержимым.
            * Files - файловые объекты мессаджей.
    * Links - связи между задачами.

    Properties
    -----------
    backup_folder : str
        Директория где сохраняется бекап.

    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)
    """
    logger.info(f"beckup {os.environ.get('CEREBRO_B3D_CURRENT_PROJECT_NAME')} to {backup_folder} start")

    #(db path)
    db_path=path_utils.get_backup_db_path()
    if not db_path:
        return (False, "Путь до файла базы данных бекапа не возможно обнаружить! | Убедитесь что проект активен и определена бекап директория.")

    save_statuses(db_path)
    save_activities(db_path)
    save_users(db_path)
    save_tasks(db_path)

    return (True, "BackUp is FINISH!")