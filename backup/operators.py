# -*- coding: utf-8 -*-

"""Бекап на локалку любого проекта по выбору. """

import os

import bpy

from .. import settings
from . import backup


class CEREBRO_log_backup_db(bpy.types.Operator):
    """
    bpy.ops.cerebro.log_backup_db('INVOKE_DEFAULT')
    """
    bl_idname = "cerebro.log_backup_db"
    bl_label = "Log backup db"

    table: bpy.props.StringProperty(name="Table")

    def execute(self, context):
        # (1)
        b,r=backup.backup_log_db(self.table)
        if b:
            self.report({'INFO'}, r)
        else:
            # bpy.ops.cerebro.message(message=r)
            self.report({'WARNING'}, r)
        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}


def set_backup_folder(self, context):
    r, projects_dir = settings.set_special_folder(self.cerebro_backup_folder, "backup_folder")
    if r:
        self.cerebro_backup_folder = projects_dir


class CEREBRO_backup_panel(bpy.types.Operator):
    """
    bpy.ops.cerebro.backup_panel('INVOKE_DEFAULT')
    """
    bl_idname = "cerebro.backup_panel"
    bl_label = "BackUp"

    def execute(self, context):
        # (1)
        b,r=backup.run(context.scene.cerebro_backup_folder)
        if b:
            self.report({'INFO'}, r)
        else:
            # bpy.ops.cerebro.message(message=r)
            self.report({'WARNING'}, r)
        return{'FINISHED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        row=col.row(align=True)
        row.label(text="Welcom to Backup:")
        if self.current_project_name:
            row.label(text=self.current_project_name)
        else:
            row.label(text="Проект не выбран!!!")
        col.prop(context.scene, "cerebro_backup_folder")

    def invoke(self, context, event):
        self.current_project_name=os.environ.get("CEREBRO_B3D_CURRENT_PROJECT_NAME")
        backup_folder = settings.get_special_folder("backup_folder")
        bpy.types.Scene.cerebro_backup_folder = bpy.props.StringProperty(name = 'Backup Folder', subtype='DIR_PATH', default=backup_folder, update = set_backup_folder)
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}
