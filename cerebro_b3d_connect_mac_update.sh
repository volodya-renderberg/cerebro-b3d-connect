#!/bin/bash

echo Start of Ftrack_b3d_connect update ...

read -p "Input version of blender[2.93]:" bversion

if ! [ -n "$bversion" ]; then 
    bversion=2.93
fi
echo version - ${bversion}

cd ~/Library

cd "Application Support/Blender/${bversion}/scripts/addons/cerebro-b3d-connect"

git pull origin master

echo update is complete.