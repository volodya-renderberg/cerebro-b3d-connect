@echo off
echo Start of installation Ftrack_b3d_connect ...

set /p bversion=Input version of blender[2.93]:

if "%bversion%" == "" (
set bversion=2.93
)

echo version - %bversion%

set addons_dir=%appdata%\Blender Foundation\Blender\%bversion%\scripts\addons
set modules_dir=%appdata%\Blender Foundation\Blender\%bversion%\scripts\modules

echo %addons_dir%
echo %modules_dir%

if not exist "%addons_dir%" (mkdir "%addons_dir%")
if not exist "%modules_dir%" (mkdir "%modules_dir%")

if not exist "%addons_dir%\cerebro-b3d-connect" (mkdir "%addons_dir%\cerebro-b3d-connect")
xcopy /s/e/z "%cd%" "%addons_dir%\cerebro-b3d-connect"

::cd "%addons_dir%"
::git clone https://gitlab.com/volodya-renderberg/cerebro-b3d-connect.git

cd "%addons_dir%\cerebro-b3d-connect\modules"
powershell -command "Expand-Archive pycerebro_modules_lin.zip ""%modules_dir%"""

echo installation is complete.

pause