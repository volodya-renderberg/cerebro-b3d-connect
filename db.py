"""Взаимодействия с таблицами базы данных.

В директории проекта создаётся sqlite таблица для записи версий загруженных с Cerebro.
В директории каждой задачи создаётся sqlite таблица учёта локальных рабочих версий.
"""

import sqlite3
import os
import datetime
import traceback

from pycerebro import dbtypes

try:
    import path_utils
except:
    from . import path_utils

TABLES={
"Version":[
    'local_version INTEGER PRIMARY KEY AUTOINCREMENT',
    'created TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
    'version_id INTEGER',
    'description TEXT',
],
"LoadedVersion":[
    'task_id INTEGER PRIMARY KEY',
    'version_id INTEGER NOT NULL',
    'created TIMESTAMP NOT NULL',
    'loaded TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
]}
"""Таблицы базы данных. Ключ - имя таблицы, значение - список команд создания. 

.. code-block:: python

    TABLES={
    "Version":[
        'local_version INTEGER PRIMARY KEY AUTOINCREMENT',
        'created TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
        'version_id INTEGER',
        'description TEXT',
    ],
    "LoadedVersion":[
        'task_id INTEGER PRIMARY KEY',
        'version_id INTEGER NOT NULL',
        'created TIMESTAMP NOT NULL',
        'loaded TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
    ]}   

"""

def _db_set(path, com, data_com=False) -> tuple:
    """Внесение изменений в базу данных. 

    Parameters
    ----------
    path : str
        Путь до файла базы данных Sqlite3.
    com : str
        Строка команды
    data_com : tuple
        Кортеж значений подставляемых в команду.

    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)
    """  
    try:
        # -- CONNECT  .db
        conn = sqlite3.connect(path, detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
        conn.row_factory = sqlite3.Row
        c = conn.cursor()
        if data_com:
            c.execute(com, data_com)
        elif com:
            c.execute(com)
        else:
            pass
    except Exception as e:
        print(f'{traceback.format_exc()}')
        try:
            conn.close()
        except:
            print(f'{traceback.format_exc()}')
            pass
        print('#'*3, 'Exception in _db_set()', e)
        print('#'*3, 'com:', com)
        print('#'*3, 'data_com:', data_com)
        return(False, 'Exception in _db_set(), please look the terminal!')
    
    conn.commit()
    conn.close()
    return(True, 'Ok!')

def _db_get(path, com, data_com=False) -> tuple:
    """Чтение строк из базы данных. 

    Parameters
    ----------
    path : str
        Путь до файла базы данных Sqlite3.
    com : str
        Строка команды
    data_com : tuple
        Кортеж значений подставляемых в команду.

    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)
    """
    try:
        # -- CONNECT  .db
        conn = sqlite3.connect(path, detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
        conn.row_factory = sqlite3.Row
        c = conn.cursor()
        if data_com:
            c.execute(com, data_com)
        else:
            c.execute(com)
        data=c.fetchall()
        conn.close()
        return(True, data)
        
    except Exception as e:
        print(f'{traceback.format_exc()}')
        try:
            conn.close()
        except:
            print(f'{traceback.format_exc()}')
            pass
        print('#'*3, 'Exception in _db_get()', e)
        print('#'*3, 'com:', com)
        print('#'*3, 'data_com:', data_com)
        return(False, 'Exception in _db_get(), please look the terminal!')

def _db_create_table(path, table, table_data=None) -> tuple:
    """Создание таблицы sqlite3 при отсутствии.

    Parameters
    ----------
    path : str
        Путь до файла базы данных sqlite.
    table : str
        Название таблицы, ключ из :attr:`TABLES`.
    table_data : list
        Список или кортеж строк команд создания таблицы. 
        При наличии этого параметра таблица :attr:`TABLES` не учитывается.

    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)
    """
    if not table_data:
        table_data=TABLES.get(table)
    if table_data:
        com = f'CREATE TABLE IF NOT EXISTS {table} ({", ".join(table_data)})'
        return _db_set(path, com)
    else:
        return(True, "Создание таблицы проигнорировано!")

def _insert(path, table, **kw) -> tuple:
    """Вставка строки в таблицу.

    Parameters
    ----------
    path : str
        Путь до файла базы данных sqlite.
    table : str
        Название таблицы, ключ из :attr:`TABLES`.
    kw : dict
        вставляемая строка, ключи - имена столбцов.

    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)
    """
    # test table
    b,r=_db_create_table(path, table)
    if not b:
        return(b,r)

    keys=[]
    val=[]
    values=[]

    for item in kw.items():
        keys.append(item[0])
        val.append("?")
        values.append(item[1])

    com = f'INSERT INTO {table} ({", ".join(keys)}) VALUES ({", ".join(val)})'
    return _db_set(path, com, data_com=values)

def _delete(path, table, where) -> tuple:
    """Удаление строки из таблицы.

    Parameters
    ----------
    path : str
        Путь до файла базы данных sqlite.
    table : str
        Название таблицы, ключ из :attr:`TABLES`.
    where : dict
        Условия поиска удаляемой строки. Каждая пара ключ-значение объединяются условием ``AND``.

    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)
    """
    # test table
    b,r=_db_create_table(path, table)
    if not b:
        return(b,r)

    where_data=[]
    com_values=[]

    for key in where:
        where_data.append(f'{key} = ?')
        com_values.append(where[key])

    com = f'DELETE FROM {table} WHERE {" AND ".join(where_data)}'
    return _db_set(path, com, data_com=com_values)

def _update(path, table, where, values) -> tuple:
    """Редактирование строки таблицы.

    Parameters
    ----------
    path : str
        Путь до файла базы данных sqlite.
    table : str
        Название таблицы, ключ из :attr:`TABLES`.
    where : dict
        Условия поиска изменяемой строки. Каждая пара ключ-значение объединяются условием ``AND``.
    values : dict
        Изменяемые данные.

    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)
    """
    # test table
    b,r=_db_create_table(path, table)
    if not b:
        return(b,r)

    set_data=[]
    where_data=[]
    com_values=[]

    for key in values:
        set_data.append(f'{key} = ?')
        com_values.append(values[key])
    for key in where:
        where_data.append(f'{key} = ?')
        com_values.append(where[key])

    com = f'UPDATE {table} SET {", ".join(set_data)} WHERE {" AND ".join(where_data)}'
    return _db_set(path, com, data_com=com_values)

def _get(path, table, where=False, sort=False, limit=False) -> tuple:
    """Поиск и чтение строк таблицы.

    Parameters
    ----------
    path : str
        Путь до файла базы данных sqlite.
    table : str
        Название таблицы, ключ из :attr:`TABLES`.
    where : dict optional
        Условия поиска изменяемой строки. Каждая пара ключ-значение объединяются условием ``AND``.
    sort : list, tuple optional
        список имён столбцов для сортировки, если перед именем столбца поставить ``-`` то будет обратная сортировка.
    limit : int optional
        количество отдаваемых строк.

    Returns
    -------
    tuple
        (True, string) или (False, comment)

    """
    # test table
    b,r=_db_create_table(path, table)
    if not b:
        return(b,r)

    com=f'SELECT * FROM {table}'
    com_data=[]

    if where:
        where_string=[]
        for key in where:
            where_string.append(f'{key} = ?')
            com_data.append(where[key])

        com=f'{com} WHERE {", ".join(where_string)}'

    if sort:
        sorting=[]
        for item in sort:
            if item.startswith('-'):
                sorting.append(f'{item[1:]} DESC')
            else:
                sorting.append(item)
        com = f'{com} ORDER BY {", ".join(sorting)}'

    if limit:
        com = f'{com} LIMIT {limit}'
    
    # print(com)
    # print(com_data)

    return _db_get(path, com, com_data)


def added_strings(path, table_name, strings, pk='id', rewrite=True):
    """Запись данных в базу данных, с перезаписью или пропуском существующих, проверка по совпадающему ``pk``.
    
    Parameters
    ----------
    path : str
        путь до файла базы данных ``sqlite3``.
    table_name : str
        имя таблицы базы данных.
    strings : list
        Список строк (словарей) для записи или перезаписи в таблицу.
    pk : str optional
        примари кей таблицы, по умолчанию ``id``.
    rewrite : bool
        Перезаписывать или нет существующие строки. По умолчанию ``True``.
    """ 
    for s in strings:
        pass

    return(True, "Ok!")        


def add_work_version(task, description: str) -> tuple:
    """Создание записи о новой work версии.

    Parameters
    ----------
    task : ftrack.task
        Динамический объект задачи.

    Returns
    -------
    tuple
        (True, num_of_new_version) или (False, comment)
    """
    if not description:
        return(False, "description is a required parameter")

    b, path = path_utils.get_versions_db_path(task)
    if not b:
        return (b, path)
    table="Version"

    # insert
    b,r = _insert(path, table, description=description)
    if not b:
        return (b, r)

    # get version
    b,r = _get(path, table, sort=['-local_version'], limit=1)
    if not b:
        return (b, r)
    else:
        return (True, r[0]['local_version'])

def del_work_version(task, version: int) -> tuple:
    """Удаление записи о work версии по номеру.

    Parameters
    ----------
    task : ftrack.task
        Динамический объект задачи.
    version : int
        Локальный номер удаляемой версии.

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    b, path = path_utils.get_versions_db_path(task)
    if not b:
        return (b, path)
    table="Version"

    return _delete(path, table, {"local_version": version})

def update_work_version(task, version: int, version_id: int) -> tuple:
    """Заполнение поля ``version_id``, после удачной выгрузки на ftrack.

    Parameters
    ----------
    task : list
        Динамический объект задачи.
    version : int
        Локальный номер редактируемой версии.
    version_id : int
        ``id`` версии загруженной на ftrack.

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    b, path = path_utils.get_versions_db_path(task)
    if not b:
        return (b, path)
    table="Version"

    return _update(path, table, {"local_version": version}, {"version_id": version_id})

def get_work_versioin(task, version: int) -> tuple:
    """Получение строки work версии по её номеру.

    Parameters
    ----------
    task : ftrack.task
        Динамический объект задачи.
    version : int
        Номер локальной версии.
    """
    b, path = path_utils.get_versions_db_path(task)
    if not b:
        return (b, path)
    table="Version"

    # get version
    b,r = _get(path, table, where={'local_version': version}, limit=1)
    if not b:
        return (b, r)
    else:
        return (True, r[0])

def get_all_work_versions(task) -> tuple:
    """Получение строк всех work версий.

    Parameters
    ----------
    task : list
        Динамический объект задачи.
    """
    b, path = path_utils.get_versions_db_path(task)
    if not b:
        return (b, path)
    table="Version"

    # get all versions
    return _get(path, table, sort=['local_version'])

def get_latest_work_version(task) -> tuple:
    """Получение строки последней work версии.

    Parameters
    ----------
    task : list
        Динамический объект задачи.
    """
    b, path = path_utils.get_versions_db_path(task)
    if not b:
        return (b, path)
    table="Version"

    # get version
    b,r = _get(path, table, sort=['-local_version'], limit=1)
    if not b:
        return (b, r)
    else:
        if r:
            return (True, r[0])
        else:
            return(True, None)

def get_new_work_version(task) -> tuple:
    """
    Возвращает номер для новой версии.

    Parameters
    ----------
    task : list
        Динамический объект задачи.
    """
    # -- определить номер последней локальной версии
    b,r = get_latest_work_version(task)
    if not b:
        return(False, r)
    if not r:
        version=1
    else:
        version=r["local_version"]+1
    return(True, version)

def add_loaded_version(version) -> tuple:
    """Создание записи о новой загруженной версии.

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    b, path = path_utils.get_project_db_path()
    if not b:
        return (b, path)
    table="LoadedVersion"

    # insert
    return _insert(
        path,
        table,
        task_id=version['message'][dbtypes.MESSAGE_DATA_TID],
        version_id=version['message'][dbtypes.MESSAGE_DATA_ID],
        created=version['message'][dbtypes.MESSAGE_DATA_CREATED]
        )
    # return _insert(path, table, task_id=version['task_id'], version_id=version['id'], created=version['date'].format())

def update_loaded_version(version) -> tuple:
    """Обновление записи о загруженной версии (id, created, loaded).

    .. note:: Проверяет на наличие записи, при отсутствии выполняет :func:`add_loaded_version`

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    # (1) TEST EXISTS
    b,r = exists_loaded_version(version)
    if not b:
        return(False, r)
    if r[0]=="missing":
        return add_loaded_version(version)
    
    # (2) UPDATE
    b, path = path_utils.get_project_db_path()
    if not b:
        return (b, path)
    table="LoadedVersion"

    values={
        "version_id": version['message'][dbtypes.MESSAGE_DATA_ID],
        "created": version['message'][dbtypes.MESSAGE_DATA_CREATED],
        # "created": version['date'].format(),
        "loaded": datetime.datetime.now()
        }
    return _update(path, table, {"task_id": version['message'][dbtypes.MESSAGE_DATA_TID]}, values)

def del_loaded_version(version) -> tuple:
    """Удаление записи о загруженной версии.

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    b, path = path_utils.get_project_db_path()
    if not b:
        return (b, path)
    table="LoadedVersion"

    return _delete(path, table, {"task_id": version['task_id']})

def exists_loaded_version(version) -> tuple:
    """Проверка наличия или свежести загруженной версии.

    Parameters
    ----------
    version : dict
        Словарь версии {"message": message, "attachment": attachment}

    Returns
    -------
    tuple
        (True, answer) или (False, comment)
        где answer(tuple) - имеет три варианта: ("missing",) ("latest",) ("old", (id, created))
    """
    b, path = path_utils.get_project_db_path()
    if not b:
        return (b, path)
    table="LoadedVersion"

    # get
    b,r = _get(path, table, where={"task_id": version['message'][dbtypes.MESSAGE_DATA_TID]}, limit=1)
    if not b:
        return (b, r)

    if not r:
        return (True, ("missing",))
    elif r[0]['version_id'] == version['message'][dbtypes.MESSAGE_DATA_ID]:
        return (True, ("latest",))
    else:
        return (True, ("old", (r[0]['version_id'], r[0]['created'])))

def get_latest_loaded_version(task) -> tuple:
    """Получение строки последней загруженной версии. 

    Parameters
    ----------
    task : ftrack.task
        Динамический объект задачи.
    """
    b, path = path_utils.get_project_db_path()
    if not b:
        return (b, path)
    table="LoadedVersion"

    # get version
    b,r = _get(path, table, where={"task_id": task[dbtypes.TASK_DATA_ID]}, sort=['-loaded'], limit=1)
    if not b:
        return (b, r)
    else:
        if r:
            return (True, r[0])
        else:
            return(True, None)