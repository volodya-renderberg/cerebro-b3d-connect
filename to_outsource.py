# -*- coding: utf-8 -*-

"""
Выгрузка шотов и компонент для работы на уатсорсе без плагина.
"""

import os
import logging
import traceback
import json
import shutil

import bpy

from pycerebro import dbtypes

try:
    import settings
    import working
    import tools
    import path_utils
    # import server_connect as srvconn
except:
    from . import settings
    from . import working
    from . import tools
    from . import path_utils
    from . import server_connect as srvconn

logger=logging.getLogger(__name__)

TO_OUTSOURCE_FOLDER_NAME="to_outsource_folder"

#PROCEDURES

def temp_proc(context):
    b,shot=working.get_selected_shot(context)
    # logger.info(shot[dbtypes.TASK_DATA_NAME])
    incoming_tasks=srvconn.get_direct_incoming_tasks(shot)
    for lid,task in incoming_tasks.items():
        logger.info(task[dbtypes.TASK_DATA_NAME])

# def export_all_shot_content(context, with_update_incoming=False):
#     """Выгрузка в директорию для аутсорса всего контента шота. """
#     #(get folder)
#     folder=context.scene.cerebro_to_outsource_folder
#     if not os.path.exists(folder):
#         return (False, "Директория для экспорта не определена!")

#     #(get shot)
#     b,shot=working.get_selected_shot(context)
#     # logger.info(shot[dbtypes.TASK_DATA_NAME])

#     #(get task list)
#     b, r=srvconn.get_downloadable_incoming_links(
#         task_id=shot[dbtypes.TASK_DATA_ID],
#         for_download=True,
#         get_components=with_update_incoming,
#         only_direct_links=True
#     )
#     if not b:
#         return(False, b)
#     #(-- update incoming)
#     if with_update_incoming:
#         for task, vers, byte, url, status in r[0]:
#             if vers and status in ('missing', 'old'):
#                 b,r=working.download_incoming_task_data_component(task, vers, url=url)
#                 if not b:
#                     logger.warning(r)
#                 else:
#                     logger.debug(r)
#             else:
#                 continue


def _get_shots_objects(shots_names: list):
    """Возвращает список объектов (ассетов) шотов по их именам. """
    shots_objects=list()
    for shot_name in shots_names:
        b,shot=srvconn.get_asset_by_name(shot_name)
        # logger.info(shot)
        if not b:
            logger.warning(f"{shot}")
        elif not shot:
            logger.warning(f"Шот по имени \"{shot_name}\" не обнаружен на сервере!")
        else:
            shots_objects.append(shot)
            logger.info(f"{shot_name} - {shot[dbtypes.TASK_DATA_ACTIVITY_NAME]}")
    return shots_objects


def _get_incoming_tasks(shots_objects: list):
    """Возвращает словарь по ``id`` объектов задач шотов, применимых для входящих связей. """
    def _get_icomings_dict(asset: list, incoming_tasks: dict):
        incomings=srvconn.get_direct_incoming_tasks(asset)
        for lid,t in incomings.items():
            logger.info(f"@@@ {t[dbtypes.TASK_DATA_ID]} - {t[dbtypes.TASK_DATA_ACTIVITY_NAME]} - {t[dbtypes.TASK_DATA_NAME]}")
            incoming_tasks[t[dbtypes.TASK_DATA_ID]]=t
            if t[dbtypes.TASK_DATA_ACTIVITY_NAME] in settings.COMPOSITE_ASSETS:
                logger.info(f"### {t[dbtypes.TASK_DATA_ID]} - {t[dbtypes.TASK_DATA_ACTIVITY_NAME]} - {t[dbtypes.TASK_DATA_NAME]}")
                _get_icomings_dict(t, incoming_tasks)
        return incoming_tasks
    
    def _get_input_tasks_ids(shot: list, input_tasks_ids: set):
        links=srvconn.get_incoming_links(shot[dbtypes.TASK_DATA_ID])
        for l in links:
            input_tasks_ids.add(l[dbtypes.TASK_LINK_SRC])

    def _fill_incoming_tasks(input_tasks_ids: list, incoming_tasks: dict):
        for tid in input_tasks_ids:
            if not tid in incoming_tasks:
                b,t=srvconn.get_task_by_id(tid)
                if b:
                    incoming_tasks[t[dbtypes.TASK_DATA_ID]]=t
                    logger.info(f"$$$ {t[dbtypes.TASK_DATA_ID]} - {t[dbtypes.TASK_DATA_ACTIVITY_NAME]} - {t[dbtypes.TASK_DATA_NAME]}")
    #
    incoming_tasks=dict()
    input_tasks_ids=set()
    #
    for shot in shots_objects:
        # incoming_tasks=_get_icomings_dict(shot, incoming_tasks)
        _get_input_tasks_ids(shot, input_tasks_ids)
    #
    _fill_incoming_tasks(input_tasks_ids, incoming_tasks)
    #
    for tid,t in incoming_tasks.items():
        if t[dbtypes.TASK_DATA_ACTIVITY_NAME] in settings.COMPOSITE_ASSETS:
            _get_input_tasks_ids(t, input_tasks_ids)
    #
    _fill_incoming_tasks(input_tasks_ids, incoming_tasks)
    
    return incoming_tasks

def _update_incoming(components):
    """ """
    for t, vers, byte, url, status in components:
        logger.info(f"{srvconn.parent_name(t)} - {t[dbtypes.TASK_DATA_NAME]} - {status} - {url}")
        if vers and status in ('missing', 'old'):
            b,r=working.download_incoming_task_data_component(t, vers, url=url)
            if not b:
                logger.warning(r)
            else:
                logger.info(r)


def _copy_assets(components, shot=False):
    """ if ``shot`` == True - то название директории будет по имени шота, а не ``id``"""
    for t, vers, byte, url, status in components:
        b,source_path=path_utils.get_task_folder_path(t, create=False)
        if not b:
            return (b,source_path)
        b,target_path=path_utils.get_task_folder_path_to_outsource(t, create=False, prject_folder_name_us_id=False, shot=shot)
        if not b:
            return (b,source_path)
        logger.info(f"FROM {source_path}")
        logger.info(f"TO {target_path}")
        if os.path.exists(source_path):
            shutil.copytree(source_path, target_path, dirs_exist_ok=True)
            versions_path=os.path.join(target_path, 'versions')
            if os.path.exists(versions_path):
                shutil.rmtree(versions_path)
            logger.info(f"COPIED")
        else:
            logger.info("NO FOUND")


def export_all_episode_content(context, with_update_incoming=False):
    """Выгрузка контента всей серии.
    
    Сбор списка происходит по всем связям всех шотов.
    """
    #(get incomings tasks)
    shots=working._get_all_shots(context)
    shots_objects=_get_shots_objects(shots)
    incoming_tasks=_get_incoming_tasks(shots_objects)
    
    #(get specifically incomings tasks)
    in_tasks=list()
    for t in list(incoming_tasks.values()):
        it=srvconn.get_task_of_asset(t)
        if it:
            in_tasks.append(it)
            logger.info(f"{t[dbtypes.TASK_DATA_NAME]} - {it[dbtypes.TASK_DATA_NAME]}")
        else:
            logger.warning(f"No found input task for: {t[dbtypes.TASK_DATA_NAME]}")

    #(get components)
    components, volume=srvconn._get_tasks_conponents(in_tasks, sess=G.sess)

    #(update incoming)
    if with_update_incoming:
        _update_incoming(components)
        # for t, vers, byte, url, status in components:
        #     logger.info(f"{srvconn.parent_name(t)} - {t[dbtypes.TASK_DATA_NAME]} - {status} - {url}")
        #     if vers and status in ('missing', 'old'):
        #         b,r=working.download_incoming_task_data_component(t, vers, url=url)
        #         if not b:
        #             logger.warning(r)
        #         else:
        #             logger.info(r)

    #(copy assets)
    _copy_assets(components)
    # for t, vers, byte, url, status in components:
    #     b,source_path=path_utils.get_task_folder_path(t, create=False)
    #     if not b:
    #         return (b,source_path)
    #     b,target_path=path_utils.get_task_folder_path_to_outsource(t, create=False)
    #     if not b:
    #         return (b,source_path)
    #     logger.info(f"FROM {source_path}")
    #     logger.info(f"TO {target_path}")
    #     if os.path.exists(source_path):
    #         shutil.copytree(source_path, target_path, dirs_exist_ok=True)
    #         versions_path=os.path.join(target_path, 'versions')
    #         if os.path.exists(versions_path):
    #             shutil.rmtree(versions_path)
    #         logger.info(f"COPIED")
    #     else:
    #         logger.info("NO FOUND")
    
    return (True, "Ok")

#OPERATORS

def set_to_outsource_folder(self, context):
    """ """
    r, path = settings.set_special_folder(self.cerebro_to_outsource_folder, TO_OUTSOURCE_FOLDER_NAME)
    if r:
        self.cerebro_to_outsource_folder = path

class CEREBRO_to_outsource_panel(bpy.types.Operator):
    """
    bpy.ops.cerebro.to_outsource_panel('INVOKE_DEFAULT')
    """
    bl_idname = "cerebro.to_outsource_panel"
    bl_label = "Export Content to Outsource"

    def execute(self, context):
        # (1)
        return{'FINISHED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        row=col.row(align=True)
        row.label(text="Выгрузка на аутсорс:")
        if self.current_project_name:
            row.label(text=self.current_project_name)
        else:
            row.label(text="Проект не выбран!!!")
        col.prop(context.scene, "cerebro_to_outsource_folder")
        col.operator("cerebro.upload_shots_content", text="Весь контент серии").action="all_content"
        col.operator("cerebro.upload_shots_content", text="Выборочный контент").action="choise_content"

    def invoke(self, context, event):
        self.current_project_name=os.environ.get("CEREBRO_B3D_CURRENT_PROJECT_NAME")
        to_outsource_folder = settings.get_special_folder(TO_OUTSOURCE_FOLDER_NAME)
        bpy.types.Scene.cerebro_to_outsource_folder = bpy.props.StringProperty(name = 'Folder', subtype='DIR_PATH', default=to_outsource_folder, update = set_to_outsource_folder)
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}


class CEREBRO_upload_shots_content(bpy.types.Operator):
    bl_idname = "cerebro.upload_shots_content"
    bl_label = "Upload Shots Content"

    action: bpy.props.StringProperty()
    with_update: bpy.props.BoolProperty(name="With Update Incoming", default=True)

    def execute(self, context):
        # (1)
        # task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        if self.action=="all_content":
            # b,r=export_all_shot_content(context, with_update_incoming=self.with_update)
            b,r=export_all_episode_content(context, with_update_incoming=self.with_update)
            if not b:
                self.report({'WARNING'}, r)
                return{'FINISHED'}
        elif self.action=="choise_content":
            bpy.ops.cerebro.upload_shots_choise_content('INVOKE_DEFAULT', with_update=self.with_update)
        self.report({'INFO'}, self.action)
        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}


class CEREBRO_upload_shots_choise_content(bpy.types.Operator):
    bl_idname="cerebro.upload_shots_choise_content"
    bl_label="Upload Choise Content"

    with_update: bpy.props.BoolProperty(name="With Update Incoming", default=True)

    def execute(self, context):
        return{'FINISHED'}

    def draw(self, context):
        layout=self.layout
        col=layout.column(align = True)
        col.prop(context.scene, "cerebro_filter_other")
        for id,t in self.incoming_tasks.items():
            target=f"{srvconn.parent_name(t)}{t[dbtypes.TASK_DATA_NAME]}"
            if context.scene.cerebro_filter_other and not tools.search(context.scene.cerebro_filter_other, target):
                continue
            row=col.row(align=True)
            row.label(text=t[dbtypes.TASK_DATA_NAME])
            op=row.operator("cerebro.upload_shots_choise_content_action")
            op.tid=str(id)
            op.with_update=self.with_update

    def invoke(self, context, event):
        #
        shots=working._get_all_shots(context)
        shots_objects=_get_shots_objects(shots)
        self.incoming_tasks=_get_incoming_tasks(shots_objects)
        #
        wm=context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}


class CEREBRO_upload_shots_choise_content_action(bpy.types.Operator):
    bl_idname="cerebro.upload_shots_choise_content_action"
    bl_label="Upload"

    tid: bpy.props.StringProperty()
    with_update: bpy.props.BoolProperty(name="With Update Incoming", default=True)

    def execute(self, context):
        #(folder test)
        if not os.environ.get(f"CEREBRO_B3D_SPECIAL_{TO_OUTSOURCE_FOLDER_NAME.upper()}"):
            self.report({'WARNING'}, f"Директория для экспорта не определена!")
            return{'FINISHED'}
        if not os.path.exists(os.environ.get(f"CEREBRO_B3D_SPECIAL_{TO_OUTSOURCE_FOLDER_NAME.upper()}")):
            self.report({'WARNING'}, f"Директория для экспорта не определена!")
            return{'FINISHED'}
        
        #(get task)
        b,task=srvconn.get_task_by_id(int(self.tid))
        if not b:
            self.report({'WARNING'}, task)
            return{'FINISHED'}
        
        #(get specifically incomings tasks)
        it=srvconn.get_task_of_asset(task)

        #(get components)
        components, volume=srvconn._get_tasks_conponents((it,))

        #(update incoming)
        if self.with_update:
            _update_incoming(components)
        
        #(copy assets)
        _copy_assets(components)
        
        self.report({'INFO'}, f"Uploaded - {self.tid} - {task[dbtypes.TASK_DATA_NAME]} - {self.with_update}")
        return{'FINISHED'}


class CEREBRO_upload_current_shot_to_outsource(bpy.types.Operator):
    bl_idname="cerebro.upload_current_shot_to_outsource"
    bl_label="Upload this Shot to Outsource (for Managers)"

    def execute(self, context):
        # (1)
        # bpy.ops.cerebro.upload_current_shot_to_outsource_action()
        return{'FINISHED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        row=col.row(align=True)
        row.label(text="Выгрузка на аутсорс:")
        if self.current_project_name:
            row.label(text=self.current_project_name)
        else:
            row.label(text="Проект не выбран!!!")
        col.prop(context.scene, "cerebro_to_outsource_folder")
        col.operator("cerebro.upload_current_shot_to_outsource_action")

    def invoke(self, context, event):
        self.current_project_name=os.environ.get("CEREBRO_B3D_CURRENT_PROJECT_NAME")
        to_outsource_folder = settings.get_special_folder(TO_OUTSOURCE_FOLDER_NAME)
        bpy.types.Scene.cerebro_to_outsource_folder = bpy.props.StringProperty(name = 'Folder', subtype='DIR_PATH', default=to_outsource_folder, update = set_to_outsource_folder)
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}
    
    
class CEREBRO_upload_current_shot_to_outsource_action(bpy.types.Operator):
    bl_idname="cerebro.upload_current_shot_to_outsource_action"
    bl_label="Upload This Shot"

    def execute(self, context):
        #(folder test)
        if not os.environ.get(f"CEREBRO_B3D_SPECIAL_{TO_OUTSOURCE_FOLDER_NAME.upper()}"):
            self.report({'WARNING'}, f"Директория для экспорта не определена!")
            return{'FINISHED'}
        if not os.path.exists(os.environ.get(f"CEREBRO_B3D_SPECIAL_{TO_OUTSOURCE_FOLDER_NAME.upper()}")):
            self.report({'WARNING'}, f"Директория для экспорта не определена!")
            return{'FINISHED'}
        #(copy assets)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        _copy_assets(((task, None, None, None, None),), shot=True)
        self.report({'INFO'}, f"Шот загружен")
        return{'FINISHED'}