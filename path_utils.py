# -*- coding: utf-8 -*-

"""Поиск путей задач, проектов, таблиц. """

import os
import json
import logging
import traceback
import tempfile
import shutil
from zipfile import ZipFile

from pycerebro import dbtypes

try:
    import settings
except:
    from . import settings


BACKUP_DB="backup_data.db"
"""str: Имя файла базы данных sqlite где хранятся все таблицы скачиваемого проекта. Расположение в папке проекта бекап директории."""

BACKUP_PATH_IDENTIFIER = 'name'
"""str: Ключ из ``task[link]`` значение которого используется для построения путей директорий при бекапе. """

BACKUP_FOLDER_DATA = 'data.json'
"""str: имя файла содержащего основные параметры директори.

.. code-block:: python

    {
        "type":'',
        "object_type":'',
        "name":'',
        "id":''
    }
"""

BACKUP_RECOVER_DATA="recover.json"
"""str: имя файла содержащего данные о загруженных задачах. 

Структура данных словарь: **ключ** - ``id`` *TypedContext* из ftrak, **значение** - ``id`` задачи созданной в серебре.
"""

BACKUP_MAKED_TASKS="maked_tasks.json"
"""str: имя файла содержащего данные о созданных задачах. 

Структура данных словарь: **ключ** - ``id`` *TypedContext* из ftrak, **значение** - ``id`` задачи созданной в серебре.
"""

BACKUP_VERSIONS_FOLDER='versions'
"""str: имя директории с ``beckup`` версиями задачи. """

PATH_IDENTIFIER = 'id'
"""str: Ключ из ``task[link]`` значение которого используется для построения путей директорий. """

PROJECT_DB = '.project_data.db'
"""Имя файла базы данных sqlite где хранятся данные загруженных версий задач ассетов. Расположение в папке проекта. """

VERSIONS_DB = '.versions_data.db'
"""str: Имя файла базы данных sqlite где хранятся данные локальных версий текущей задачи. Расположение в папке задачи. """

EXT='.blend'
"""str: Расширение для рабочих файлов по умолчанию. """

TEXTURES_FOLDER = "textures"
"""str: Название директории текстур задачи. """

RENDER_FOLDER="render_output"
"""str: Название директории куда сохраняется рендер секвенций. """

PREFIX_ANIMATIC='Animatic_'
"""str: Префикс видео файлов аниматика. """


def _task_id_from_path(path) -> str:
    """Возвращает **id** задачи (или None) по ``path`` - путь к файлу или папке, любой вложенности (макс глубина 20) в директории задачи. 

    * Например мувка аниматика или плейбласта.
    * Также вернёт **id** если путь сама директория задачи.

    Returns
    -------
    str
        **id** задачи
    """
    dir=settings.PROJECTS
    parts=list()
    i=0
    def cp(path, i):
        if i>20:
            return
        #(1)
        item=os.path.basename(path)
        if item==dir:
            if i>1:
                return parts[-2:][0]
            else:
                return
        parts.append(item)
        i+=1
        return cp(os.path.dirname(path), i)
        
    
    return cp(path, i)


def _create_path(path: str, create: bool) -> tuple:
    """Создание при необходимости директории до path. """
    if create and not os.path.exists(path):
        try:
            os.makedirs(path)
            return(True, path)
        except Exception as e:
            print(f'{traceback.format_exc()}')
            print(f'exception when creating a path: {path} - {e}')
            return (False, f"{e}")
    else:
        return(True, path)


def _parent_name(task):
    """Возвращает имя родительской задачи, из параметра ``url`` без запросов на сервер.

    Parameters
    ----------
    task : list
        Задача.

    Returns
    -------
    str
        Имя родительской задачи.
    """

    return task[dbtypes.TASK_DATA_PARENT_URL].split('/')[-2:][0]


def get_ext(task):
    return EXT


def get_task_folder_path(task, create=True) -> tuple:
    """Возвращает локальный путь до директории задачи, при необходимости создаёт.

    .. note:: путь создаётся по :ref:`CEREBRO_B3D_PROJECTS_DIR`/project_id/task_id

    Parameters
    ----------
    task : list
        Данная задача.
    create : bool, optional
        Если **True**, то при отсутствии директория будет создана.

    Returns
    -------
    tuple
        (True, path_to_task_folder) или (False, comment)

    """
    if not task:
        return(False, f"In get_task_folder_path() task={task}")
    projects_folder=os.environ.get('CEREBRO_B3D_PROJECTS_DIR')
    
    if not projects_folder or not os.path.exists(projects_folder):
        return (False, f"In get_task_folder_path() Projects Dir is not defined or does not exist: {projects_folder}")

    path=projects_folder
    # (var 1)
    # for item in task["link"]:
    #     path = os.path.join(path, item[PATH_IDENTIFIER])
    # (var 2)
    path=os.path.join(path, os.environ["CEREBRO_B3D_CURRENT_PROJECT_ID"])
    path=os.path.join(path, str(task[dbtypes.TASK_DATA_ID]))

    return _create_path(path, create)
    

def get_task_folder_path_to_outsource(task, create=True, prject_folder_name_us_id=True, shot=False) -> tuple:
    """Возвращает локальный путь до директории задачи, в папке выгрузки на аутсорс (при необходимости создаёт).

    .. note:: путь создаётся по ``TO_OUTSOURCE_FOLDER``/project_id/task_id

    Parameters
    ----------
    task : list
        Данная задача.
    create : bool, optional
        Если **True**, то при отсутствии директория будет создана.

    Returns
    -------
    tuple
        (True, path_to_task_folder) или (False, comment)

    """
    if not task:
        return(False, f"In get_task_folder_path() task={task}")
    to_outsource_folder = settings.get_special_folder("to_outsource_folder")
    
    if not to_outsource_folder or not os.path.exists(to_outsource_folder):
        return (False, f"In get_task_folder_path() Projects Dir is not defined or does not exist: {to_outsource_folder}")

    path=to_outsource_folder
    # (var 1)
    # for item in task["link"]:
    #     path = os.path.join(path, item[PATH_IDENTIFIER])
    # (var 2)
    if prject_folder_name_us_id:
        path=os.path.join(path, os.environ["CEREBRO_B3D_CURRENT_PROJECT_ID"])
    else:
        path=os.path.join(path, os.environ["CEREBRO_B3D_CURRENT_PROJECT_FULLNAME"])
    #
    if shot:
        path=os.path.join(path, task[dbtypes.TASK_DATA_PARENT_URL].split('/')[-2:][0])
    else:
        path=os.path.join(path, str(task[dbtypes.TASK_DATA_ID]))
    #
    return _create_path(path, create)


def get_internal_task_folder_path(task, name, create=True):
    """ 
    Путь до директории ``task_folder/name`` (папки в директории задачи). При необходимости создаёт.

    Parameters
    ----------
    task : list
        Данная задача.
    name : str
        имя директории внутри задачи.
    create : bool, optional
        Если **True**, то при отсутствии директория будет создана.

    Returns
    -------
    tuple
        (True, path_to_meta_folder) или (False, comment)

    """
    b,r=get_task_folder_path(task, create)
    if not b:
        return(b,r)

    path=os.path.join(r, name)

    return _create_path(path, create)


def get_project_folder_path(create=True) -> tuple:
    """Возвращает локальный путь до директории проекта, при необходимости создаёт.

    .. note:: Данные берутся из переменных окружения:  :ref:`CEREBRO_B3D_PROJECTS_DIR` и :ref:`CEREBRO_B3D_CURRENT_PROJECT_ID`

    Parameters
    ----------
    create : bool, optional
        Если **True**, то при отсутствии директория будет создана.

    Returns
    -------
    tuple
        (True, path) или (False, comment)

    """
    projects_folder=os.environ.get('CEREBRO_B3D_PROJECTS_DIR')

    if not projects_folder or not os.path.exists(projects_folder):
        return (False, f"Projects Dir is not defined or does not exist: {projects_folder}")

    path = os.path.join(projects_folder, os.environ['CEREBRO_B3D_CURRENT_PROJECT_ID'])

    return _create_path(path, create)

def get_folder_of_textures_path(task, create=True) -> tuple:
    """Возвращает путь до директории textures задачи, при необходимости создаёт. """
    b,r=get_task_folder_path(task)
    if not b:
        return(b,r)
    path = os.path.join(r, TEXTURES_FOLDER)
    return _create_path(path, create)

def get_project_db_path() -> tuple:
    """Путь до файла :attr:`PROJECT_DB` 

    .. note:: Данные берутся из переменных окружения:  :ref:`CEREBRO_B3D_PROJECTS_DIR` и :ref:`CEREBRO_B3D_CURRENT_PROJECT_ID`
    """

    b, project_path=get_project_folder_path()
    if not b:
        return(False, project_path)

    path = os.path.join(project_path, PROJECT_DB)
    return (True, path)

def get_versions_db_path(task) -> tuple:
    """Путь до файла :attr:`VERSIONS_DB` """

    b, r = get_task_folder_path(task)
    if not b:
        return(False, r)
    path = os.path.join(r, VERSIONS_DB)
    return (True, path)

def get_top_version_path_of_workfile(task) -> tuple:
    """Возвращает путь до локальной ``top`` версии рабочего файла.

    .. note:: Название файла - ``task[parent][name].ext``.
        
    Parameters
    ----------
    task : Task
        Данная задача.
    
    Returns
    -------
    tuple
        (True, path) или (False, comment)
    """
    b, r = get_task_folder_path(task)
    if not b:
        return(b, r)
    #() get ext
    ext=get_ext(task)
    path=os.path.join(r, f'{_parent_name(task)}{ext}')
    return(True, path)

def get_version_path_of_workfile(task, version) -> tuple:
    """Возвращает путь до локальной версии рабочего файла.

    .. note:: расположение в ``task_folder/versions/``, нейминг ``asset_name__v0000.ext``.
        
    Parameters
    ----------
    task : Task
        Данная задача.
    version : str, int
        Номер версии.

    Returns
    -------
    tuple
        (True, path) или (False, comment)
    """
    b, r = get_task_folder_path(task)
    if not b:
        return(b, r)
    #() get ext
    ext=get_ext(task)
    path=os.path.join(r, 'versions', f'{_parent_name(task)}_v{version}{ext}')
    return(True, path)


def _backup_update_folder_data(folder_path, update_data, file_name=False):
    """Дописывание или переписывание данных в data.json директории

    Parameters
    ----------
    folder_path : str
        Путь директории
    update_data : dict
        Новые значения

    Returns
    -------
    bool
        True в случае удачи, или None
    """
    if not file_name:
        file_name=BACKUP_FOLDER_DATA
    data_path=os.path.join(folder_path, file_name)
    logging.debug(data_path)
    if os.path.exists(data_path):
        logging.debug("exists")
        with open(data_path, "r") as f:
            logging.debug("open")
            data=json.load(f)

        logging.debug(data)
        data.update(update_data)

        with open(data_path, "w") as f:
            json.dump(data, f, indent=4, sort_keys=True)
        return True
    else:
        with open(data_path, "w") as f:
            json.dump(update_data, f, indent=4, sort_keys=True)
        return True


def _backup_get_folder_data(folder_path, file_name=False):
    """Чтение данных из data.json директории

    Parameters
    ----------
    folder_path : str
        Путь директории
    
    Returns
    -------
    dict или None
    """
    if not file_name:
        file_name=BACKUP_FOLDER_DATA
    data_path=os.path.join(folder_path, file_name)
    if os.path.exists(data_path):
        with open(data_path, "r") as f:
            data=json.load(f)
        return data
    else:
        return {}


def _pack_folder_to_zip(zip_name: str, folder: str, new_name=False):
    """Упаковка директории в зип архив (в темп директории).

    Parameters
    ----------
    zip_name : str
        имя создаваемого архива
    folder : str
        путь до пакуемой директории.
    new_name : str option
        как будет называться директория в самом архиве.

    Returns
    -------
    tuple
        (True, path_to_zip) или (False, comment)
    """
    zip_dir = tempfile.mkdtemp()

    if new_name:
        to_path=os.path.join(zip_dir, new_name)
    else:
        to_path = os.path.join(zip_dir, os.path.basename(folder))
    if os.path.exists(folder):
        shutil.copytree(folder, to_path)
    zip_path = os.path.join(zip_dir, zip_name)

    os.chdir(zip_dir)
    with ZipFile(zip_name, "w") as zf:
        folder=os.path.basename(to_path)
        for root, dirs, files in os.walk(folder):
            for file in files:
                if os.path.samefile(root, folder):
                    f_path=os.path.join(os.path.basename(folder), file)
                else:
                    f_path=os.path.join(os.path.basename(folder), os.path.relpath(root, start=folder), file)
                print(f'file: {f_path}')
                zf.write(f_path)
            for fld in dirs:
                if os.path.samefile(root, folder):
                    d_path=os.path.join(os.path.basename(folder), fld)
                else:
                    d_path=os.path.join(os.path.basename(folder), os.path.relpath(root, start=folder), fld)
                print(f'folder: {d_path}')
                zf.write(d_path)

    return(True, zip_path)


def get_backup_project_folder_path(create=True):
    """Возвращает путь до бекап директории проекта или None.

    Используются переменные окружения ``CEREBRO_B3D_SPECIAL_BACKUP_FOLDER``, ``CEREBRO_B3D_CURRENT_PROJECT_NAME``.
    
    Parameters
    ------------
    create : bool optional
        Создавать или нет директорию при отсутствии.

    Returns
    --------
    str
        Путь до бекап директории проекта или None
    """
    if not os.environ.get("CEREBRO_B3D_SPECIAL_BACKUP_FOLDER") or not os.environ.get("CEREBRO_B3D_CURRENT_PROJECT_NAME"):
        return None

    path=os.path.join(os.environ.get("CEREBRO_B3D_SPECIAL_BACKUP_FOLDER"), os.environ.get("CEREBRO_B3D_CURRENT_PROJECT_NAME"))

    if not os.path.exists(path) and create:
        os.makedirs(path)
        return path
    elif not os.path.exists(path) and not create:
        return None
    else:
        return path


def get_backup_db_path():
    """Возвращает путь до :attr:`BACKUP_DB` из бекап директории проекта или None. 
    
    Используются переменные окружения ``CEREBRO_B3D_SPECIAL_BACKUP_FOLDER``, ``CEREBRO_B3D_CURRENT_PROJECT_NAME``.

    Returns
    -------
    str
        Путь до файла базы данных или None.
    """
    project_path=get_backup_project_folder_path()
    if not project_path:
        return None

    path=os.path.join(project_path, BACKUP_DB)

    return path


def get_backup_attachment_dir(attachment_id, create=True):
    """Возвращает путь до директории Attachment задачи.
    
    Parameters
    ------------
    attachment_id : int
        id аттачмента.
    create : bool
        создавать директорию или нет, при отсутствии.

    Returns
    -------
    str
        путь до директории, или None.
    """
    attachment_id=str(attachment_id)

    project_path=get_backup_project_folder_path()
    if not project_path:
        return None
    
    path=os.path.join(project_path, 'attachments', attachment_id)
    
    b,r=_create_path(path, create)

    if b:
        return r
    else:
        return None