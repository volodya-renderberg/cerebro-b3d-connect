.. Cerebro b3d connect documentation master file, created by
   sphinx-quickstart on Mon Jun 28 19:06:33 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Cerebro b3d connect's documentation!
===============================================

.. toctree::
   :maxdepth: 2
   
   install_update_delete
   develop


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
