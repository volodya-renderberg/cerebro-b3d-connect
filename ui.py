# -*- coding: utf-8 -*-

import webbrowser
import os
import logging
import tempfile

import bpy

from pycerebro import dbtypes

from . import light_collections_managment as lcm
from . import settings
from . import path_utils
from . import working
from . import db
from . import server_connect as srvconn
from . import check
from . import hooks
from . import tools
from . import render_tools
from . import update
from . import recovery_location as rl
from .backup import operators
from . import to_outsource

logging.basicConfig(
    # filename=os.path.join(tempfile.gettempdir(),'ftrack_log'),
    # filemode='a',
    format='%(asctime)s,%(msecs)d %(name)s [%(levelname)s] %(message)s',
    datefmt='%H:%M:%S',
    # level=logging.INFO,
    level=logging.DEBUG
    )

logger=logging.getLogger(__name__)

CAUTION_TO_INPROGRESS=['Открытие приведёт','к смене статуса на "в работе"','всё равно продолжить?']
CAUTION_OF_LAG=['Локальная версия отстаёт',' от сервера на [%s] версий!']

class G():
    storage = None
    """list: объект storage, определяется в settings.get_storage()"""
    future_render_commit_num=None # номер будущего коммита рендера. / следует сбрасывать после использования.
    future_commit_num=None # номер будущего коммита задачи. / следует сбрасывать после использования.
    sess=None
    current_panel="task list"
    direct_incoming_tasks=dict()
    """list: прямой список входящих (словарь по id самой связи) задач какой либо задачи. результат функции :func:`working.get_direct_incoming_tasks`"""
    # (результат auth())
    activites=dict() # словарь имён видов деятельности по их id
    asset_models_of_project=dict() # словарь по id задач с типом AssetModel текущего проекта
    statuses=dict() # словарь имён статусов по id
    projects=tuple() # список для EnumProperty [(project_id, project_name, '')...]
    projects_dict=None # словарь объектов проектов, с ключами по str(project_id)
    projects_tags_dict=None # словарь тегов проекта, с ключами по str(project_id)
    #(результат fill_tasks_list())
    task_list=tuple()
    task_dict=None # словарь задач по id
    task_parent_dict=None # словарь родительских задач по id текущих.
    tasks_cache=dict() # словарь загружаемых в разное время задач, по их id.
    #
    versions_list=tuple()
    ftrack_versions_list=tuple()
    ftrack_versions_dict=None
    components=()
    sourcepath=None
    graphics_editors={}
    role='Working'
    check=None
    selected_task=None
    task_templates=()
    message=tuple()# tuple: кортеж строк для label
    lag=0 # int: отставание локальнойверсии от версии на сервере. заполняется в "cerebro.select_task"
    incoming_tasks=dict() #словарь входящих задач для текущей задачи.заполняется в cerebro.open_scene_from_incoming_task
    release={} #словарь последнего релиза.

    @classmethod
    def get_status_id(cls, status_name):
        for i in cls.statuses.items():
            if i[1]==status_name:
                return i[0]
        return None

__builtins__["G"]=G


# def search(search: str, target: str) -> bool:
#     """Поиск  ``search`` в ``target`` """
#     return all([s.lower() in target.lower() for s in search.split()])


def fill_projects(self, context):
    if G.projects:
        # print(G.projects)
        return G.projects


def change_project(self, context):
    fill_tasks_lists(self, context)
    bpy.ops.cerebro.switch_panels(action="to task list")


def run_sources_panel(self, context):
    bpy.ops.cerebro.sources_panel_action(from_cache=self.cerebro_source_panel)


def fill_tasks_lists(self, context):
    prj_id = context.scene.ftrack_projects
    prj=G.projects_dict[prj_id]
    # os.environ["CEREBRO_B3D_ROLE"]=context.scene.ftrack_role
    os.environ["CEREBRO_B3D_CURRENT_PROJECT_ID"]=prj_id
    os.environ["CEREBRO_B3D_CURRENT_PROJECT_NAME"]=prj[dbtypes.TASK_DATA_NAME]
    os.environ["CEREBRO_B3D_CURRENT_PROJECT_FULLNAME"]=prj[dbtypes.TASK_DATA_NAME].capitalize()
    if "fps" in G.projects_tags_dict[prj_id]:
        os.environ["CEREBRO_B3D_CURRENT_PROJECT_FPS"]=str(G.projects_tags_dict[prj_id]["fps"])
    else:
        os.environ["CEREBRO_B3D_CURRENT_PROJECT_FPS"]='24'
    if "width" in G.projects_tags_dict[prj_id]:
        os.environ["CEREBRO_B3D_CURRENT_PROJECT_WIDTH"]=str(G.projects_tags_dict[prj_id]["width"])
    else:
        os.environ["CEREBRO_B3D_CURRENT_PROJECT_WIDTH"]='1280'
    if "height" in G.projects_tags_dict[prj_id]:
        os.environ["CEREBRO_B3D_CURRENT_PROJECT_HEIGHT"]=str(G.projects_tags_dict[prj_id]["height"])
    else:
        os.environ["CEREBRO_B3D_CURRENT_PROJECT_HEIGHT"]='720'
    if "fstart" in G.projects_tags_dict[prj_id]:
        os.environ["CEREBRO_B3D_CURRENT_PROJECT_FSTART"]=str(G.projects_tags_dict[prj_id]["fstart"])
    else:
        os.environ["CEREBRO_B3D_CURRENT_PROJECT_FSTART"]="1000"
    if G.role=='Cheking':
        r,m=srvconn.get_tasks_list(check_list=True)
    else:
        r,m=srvconn.get_tasks_list(sess=G.sess, close_session=False)
    if r:
        G.task_list=m[0]
        G.task_dict=m[1]
        G.task_parent_dict=m[2]
        # print(G.task_list)
        # print(G.task_dict)
        # print(G.task_parent_dict)
    context.scene.ftrack_panel_task_list=True
    print("*** fill_tasks_lists() ***")

def change_role(self, context):
    os.environ["CEREBRO_B3D_ROLE"]=G.role=context.scene.ftrack_role
    fill_tasks_lists(self, context)

def read_graphics_editors():
    G.graphics_editors=settings.read_graphics_editors()

def set_projects_folder(self, context):
    r, projects_dir = settings.set_projects_folder(self.ftrack_current_projects_dir)
    if r:
        self.ftrack_current_projects_dir = projects_dir

def get_versions_list(task_id=False):
    b,r = srvconn.get_versions_list(task_id=int(task_id))
    if not b:
        G.ftrack_versions_list=tuple()
        G.ftrack_versions_dict=dict()
        return(b,r)
    else:
        G.ftrack_versions_list=r[0]
        G.ftrack_versions_dict=r[1]
        return(True, "Ok")


def get_task_templates():
    b,r=srvconn.get_task_templates()
    if b:
        G.task_templates=r
    else:
        print(f"{'7'*50} - {r}")


def fill_task_templates(self, context):
    if G.task_templates:
        params=list()
        for t in G.task_templates:
            params.append((t["name"],)*3)
        print(f"{'8'*50} - {params} - {G.task_templates}")
        return params
    return []


def lag_test_of_version(self, task):
    """Проверка отставания версии и заполнение G.lag натуральным числом величины отставания. """
    b,r=working.lag_test_of_version(task)
    if not b:
        self.report({'WARNING'}, str(r))
    else:
        G.lag=r
        self.report({'INFO'}, str(r))


def set_params():
    # filter/search
    bpy.types.Scene.ftrack_filter_gen = bpy.props.StringProperty(name = 'Filter', default='', update = None)
    bpy.types.Scene.ftrack_filter_source = bpy.props.StringProperty(name = 'Filter', default='', update = None)
    bpy.types.Scene.cerebro_filter_other = bpy.props.StringProperty(name = 'Filter', default='', update = None)
    # seting
    bpy.types.Scene.ftrack_setting_in_process = bpy.props.BoolProperty(name = 'Setting', default=False, update = None)
    projects_folder = settings.get_projects_folder()
    bpy.types.Scene.ftrack_current_projects_dir = bpy.props.StringProperty(name = 'Projects Dir', subtype='DIR_PATH', default=projects_folder, update = set_projects_folder)
    # auth
    bpy.types.Scene.ftrack_auth_in_process = bpy.props.BoolProperty(name = 'Auth', default=False, update = None)
    bpy.types.Scene.ftrack_auth_current_user = bpy.props.StringProperty(name = 'Auth user', default='No authorization', update = None)
    # bpy.types.Scene.ftrack_projects = bpy.props.EnumProperty(items=fill_projects, name = 'Projects', default=None, update = fill_tasks_lists)
    bpy.types.Scene.ftrack_projects = bpy.props.EnumProperty(items=fill_projects, name = 'Projects', default=None, update = change_project)
    bpy.types.Scene.ftrack_role = bpy.props.EnumProperty(items=[('Working',)*3,('Cheking',)*3], name = 'Role', default=None, update = change_role)
    # panels
    bpy.types.Scene.ftrack_panel_task_list = bpy.props.BoolProperty(name = 'Task list', default=False, update = None)
    bpy.types.Scene.ftrack_panel_selected_task = bpy.props.BoolProperty(name = 'Selected task', default=False, update = None)
    bpy.types.Scene.ftrack_panel_working_task = bpy.props.BoolProperty(name = 'Working task', default=False, update = None)
    bpy.types.Scene.ftrack_panel_versions_list = bpy.props.BoolProperty(name = 'Versions list', default=False, update = None)
    bpy.types.Scene.ftrack_panel_ftrack_versions_list = bpy.props.BoolProperty(name = 'Ftracl versions list', default=False, update = None)
    bpy.types.Scene.ftrack_panel_sources = bpy.props.BoolProperty(name = 'Source panel', default=False, update = None)
    # sources panel
    bpy.types.Scene.cerebro_source_panel = bpy.props.EnumProperty(
        items=[("mode", "-- select --", "mode"), ("cache", "From cache","cache"), ("reload","New read data","reload")],
        name="Sources Panel",
        default=None,
        update=run_sources_panel)
    # animatic tools
    bpy.types.Scene.cerebro_panel_animatic_tools_create_shots_method1 = bpy.props.BoolProperty(name = 'Create Shots \"Method 1\"', default=False, update = None)
    bpy.types.Scene.cerebro_panel_animatic_tools_create_shots_method2 = bpy.props.BoolProperty(name = 'Create Shots \"Method 2\"', default=False, update = None)
    # bpy.types.Scene.ftrack_task_templates = bpy.props.EnumProperty(items=fill_task_templates, name = 'Task templates', default=None, update = None)
    #
    auth_data=settings.read_auth_data()
    if auth_data:
        # bpy.types.Scene.ftrack_server_url = bpy.props.StringProperty(name = 'Server URL', default=auth_data["server_url"], update = None)
        bpy.types.Scene.ftrack_api_key = bpy.props.StringProperty(name = 'Password', default=auth_data["api_key"], update = None, subtype="PASSWORD")
        bpy.types.Scene.ftrack_api_user = bpy.props.StringProperty(name = 'Username', default=auth_data["api_user"], update = None)
    else:
        # bpy.types.Scene.ftrack_server_url = bpy.props.StringProperty(name = 'Server URL', default='https://mycompany.ftrackapp.com', update = None)
        bpy.types.Scene.ftrack_api_key = bpy.props.StringProperty(name = 'Password', default='', update = None)
        bpy.types.Scene.ftrack_api_user = bpy.props.StringProperty(name = 'Username', default='', update = None)

    read_graphics_editors()


class CEREBRO_light_panel(bpy.types.Panel):
    bl_idname = "CEREBRO_PT_light_panel"
    bl_label = 'Light (Cerebro):'
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = 'Light(Cerebro)'

    @classmethod
    def poll(self, context):
        if G.selected_task and G.selected_task[dbtypes.TASK_DATA_ACTIVITY_NAME] in ("rendering", "рендер", "layout"):
            return True
        else:
            return False

    def draw(self, context):
        layout = self.layout
        col = layout.column(align = True)
        col.label(text='All collections')
        col.operator("cerebro.light_local_save_collections")
        col.operator("cerebro.light_local_set_from_file_collections")
        col = layout.column(align = True)
        col.operator("cerebro.light_set_from_file_collections", text="Set from incoming Location").action="global"
        # for col_name in lcm.COLLECTIONS:
        #     col.label(text=f'{col_name}:')
        #     row = col.row(align = True)
        #     row.operator("cerebro.light_select_objects_of_collections").collection=col_name
        #     row.operator("cerebro.light_add_selected_objects_to_collections").collection=col_name
        #     row.operator("cerebro.light_remove_selected_objects_from_collections").collection=col_name
        # col.operator("cerebro.light_select_objects_of_collections", text="Select object no collections").collection="No"


class CEREBRO_main_panel(bpy.types.Panel):
    bl_idname = "CEREBRO_PT_main_panel"
    bl_label = 'Cerebro:'
    # bl_space_type = "VIEW_3D"
    # bl_region_type = "UI"
    bl_category = 'Cerebro'
    #bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column(align = True)
        
        row = col.row(align = True)
        row.label(text='')
        row.operator('cerebro.manual', icon='QUESTION')
        
        col.label(text=context.scene.ftrack_auth_current_user)


        ## SETTING
        if context.scene.ftrack_setting_in_process:
            col.label(text='PROJECTS FOLDER:')
            col.prop(context.scene, "ftrack_current_projects_dir")
            # row = col.row(align = True)

            col.label(text='UPDATE:')
            col.operator("cerebro.update", text="Проверить наличие обновлений.")
            
            col.label(text='GRAPHICS EDITORS:')
            if G.graphics_editors:
                for name in G.graphics_editors:
                    row = col.row(align = True)
                    row.label(text=name)
                    row.label(text=G.graphics_editors[name])
                    row.operator("cerebro.del_graphics_editors").alias=name
            col.operator("cerebro.add_graphics_editors")

            col = layout.column(align = True)
            col.operator("cerebro.setting_start", text='Cancel').action='cancel'
        ## AUTH
        elif context.scene.ftrack_auth_in_process:
            col = layout.column(align = True)
            # col.prop(context.scene, "ftrack_server_url")
            col.prop(context.scene, "ftrack_api_user")
            col.prop(context.scene, "ftrack_api_key")
            row = col.row(align = True)
            row.operator("cerebro.authorization", text='Cancel').action='cancel'
            row.operator("cerebro.authorization").action='auth'
        else:
            row = col.row(align = True)
            row.operator("cerebro.auth_start")
            row.operator("cerebro.setting_start").action='start'
        
        ## PROJECTS LIST
        layout = self.layout
        col = layout.column(align = True)
        # col.separator_spacer()
        col.separator(factor=4.0)
        if G.projects:
            col.prop(context.scene, "ftrack_projects")

        ## TASK LIST
        if context.scene.ftrack_panel_task_list:
            col = layout.column(align = True)
            col.prop(context.scene, "ftrack_role")

            row=col.row(align = True)
            row.label(text='TASKS:')
            row.operator("cerebro.reload_tasks_list")
            col.prop(context.scene, "ftrack_filter_gen")
            if G.task_list:
                for task in G.task_list:
                    try:
                        # conditions=(
                        #     context.scene.ftrack_filter_gen.lower() in srvconn.parent_name(task).lower(),
                        #     context.scene.ftrack_filter_gen.lower() in task[dbtypes.TASK_DATA_NAME].lower(),
                        #     context.scene.ftrack_filter_gen.lower() in G.statuses[str(task[dbtypes.TASK_DATA_CC_STATUS])].lower()
                        #     )
                        # if context.scene.ftrack_filter_gen and not any(conditions):
                        #     continue
                        target=f"{srvconn.parent_name(task)}{task[dbtypes.TASK_DATA_NAME]}{G.statuses[str(task[dbtypes.TASK_DATA_CC_STATUS])]}"
                        if context.scene.ftrack_filter_gen and not tools.search(context.scene.ftrack_filter_gen, target):
                            continue
                        else:
                            row = col.row(align = True)
                            row.label(text=f"[{srvconn.parent_name(task)}] {task[dbtypes.TASK_DATA_NAME]}")
                            row.label(text=G.statuses[str(task[dbtypes.TASK_DATA_CC_STATUS])])
                            row.operator("cerebro.select_task").task_id=str(task[dbtypes.TASK_DATA_ID])
                    except:
                        pass

        ## SELECTED TASK
        if context.scene.ftrack_panel_selected_task:
            col = layout.column(align = True)
            G.selected_task=G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
            col.label(text=f'SELECTED TASK: [{os.environ["CEREBRO_B3D_CURRENT_ASSET_NAME"]}] '
                f'{G.selected_task[dbtypes.TASK_DATA_NAME]}')

            #(WEB)
            # col = layout.column(align = True)
            b=col.operator("cerebro.open_last_version_by_webbrowser", text="WEB чат по задаче.")
            b.shot_name=""
            b.task_id=''

            #(From Cerebro:)
            col.label(text="From Cerebro:")
            col.operator("cerebro.download_version").action="start"
            # if os.environ["CEREBRO_B3D_CURRENT_ASSET_TYPE"] in settings.MONTAGE_OBJECT_TYPES:
            #     pass
            # else:
            #     col.operator("cerebro.update_incoming")
            #     # col.operator("cerebro.sources_panel")
            #     # col.prop(context.scene, "cerebro_source_panel")
            #     col.label(text="Source panel:")
            #     row=col.row()
            #     row.operator("cerebro.sources_panel_action", text="From cache").from_cache="cache"
            #     row.operator("cerebro.sources_panel_action", text="New read data").from_cache="reload"
            #
            col.operator("cerebro.update_incoming")
            # col.label(text="Source panel:")
            row=col.row(align=True)
            row.label(text="Source panel:")
            row.operator("cerebro.sources_panel_action", text="From cache").from_cache="cache"
            row.operator("cerebro.sources_panel_action", text="New read").from_cache="reload"
            #
            # # col = layout.column(align = True)
            # b=col.operator("cerebro.open_last_version_by_webbrowser")
            # b.shot_name=""
            # b.task_id=''

            #(Local:)
            col = layout.column(align = True)
            col.label(text="Способы открытия сцены:")
            if G.role=='Working':
                if os.environ["CEREBRO_B3D_CURRENT_ASSET_TYPE"] in ("Episode", "Location", "Shot") or G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]][dbtypes.TASK_DATA_ACTIVITY_NAME] in ("animation",):
                    row=col.row(align=True)
                    open_op=row.operator("cerebro.open_scene_from_incoming_task")
                    open_op.dialog=True
                    open_op.build=False
                    build_op=row.operator("cerebro.open_scene_from_incoming_task", text="Build from incoming")
                    build_op.dialog=True
                    build_op.build=True
                else:
                    col.operator("cerebro.open_scene_from_incoming_task").dialog=True
                col.operator("cerebro.current_scene_to_work")
                if os.environ["CEREBRO_B3D_CURRENT_ASSET_TYPE"] in ("Episode", "Location", "Shot") or G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]][dbtypes.TASK_DATA_ACTIVITY_NAME] in ("animation",):
                    row=col.row(align=True)
                    row.operator("cerebro.open_work_top_version")
                    row.operator("cerebro.build_scene")
                    row=col.row(align=True)
                    row.operator("cerebro.open_work_version").action="start"
                    row.operator("cerebro.build_scene_from_version")
                else:
                    col.operator("cerebro.open_work_top_version")
                    col.operator("cerebro.open_work_version").action="start"
            else:
                col.operator("cerebro.open_work_top_version", text="Look")
                col.operator("cerebro.open_work_version", text="Look version").action="start"
            col.operator("cerebro.open_file_browser")

            col = layout.column(align = True)
            col.operator("cerebro.switch_panels").action="to task list"

        ## WORKING OF TASK
        if context.scene.ftrack_panel_working_task:
            layout = self.layout
            сol = layout.column(align = True)
            # col.label(text=f'{G.role.upper()} OF TASK: [{G.task_parent_dict[str(G.selected_task[dbtypes.TASK_DATA_ID])][dbtypes.TASK_DATA_NAME]}] '
            col.label(text=f'{G.role.upper()} OF TASK: [{srvconn.parent_name(G.selected_task)}] '
                f'{G.selected_task[dbtypes.TASK_DATA_NAME]}')
            #

            #(WEB)
            # col = layout.column(align = True)
            b=col.operator("cerebro.open_last_version_by_webbrowser", text="WEB чат по задаче.")
            b.shot_name=""
            b.task_id=''

            col.label(text="From Cerebro:")
            col.operator("cerebro.download_version").action="start"
            # col.operator("cerebro.update_incoming")
            # col.operator("cerebro.sources_panel")
            # col.prop(context.scene, "cerebro_source_panel")
            # col.label(text="Source panel:")
            row=col.row(align=True)
            row.label(text="Source panel:")
            row.operator("cerebro.sources_panel_action", text="From cache").from_cache="cache"
            row.operator("cerebro.sources_panel_action", text="New read data").from_cache="reload"
            #
            # # col=layout.column(align=True)
            # b=col.operator("cerebro.open_last_version_by_webbrowser")
            # b.shot_name=""
            # b.task_id=''

            if G.role=='Working':
                #(Sources:)
                сol = layout.column(align = True)
                col.label(text="Fix scene:")
                # if G.task_parent_dict[str(G.selected_task[dbtypes.TASK_DATA_ID])][dbtypes.TASK_DATA_ACTIVITY_NAME] in settings.MONTAGE_OBJECT_TYPES:
                if os.environ["CEREBRO_B3D_CURRENT_ASSET_TYPE"] in settings.MONTAGE_OBJECT_TYPES:
                    # col.operator("cerebro.download_animatic")
                    pass
                else:
                    # col.operator("cerebro.update_incoming")
                    # col.operator("cerebro.sources_panel")
                    col.operator("cerebro.recover_paths")
                    row=col.row()
                    row.operator("cerebro.recover_save_position_of_linked_objects")
                    row.operator("cerebro.recover_set_position_of_linked_objects")
            #
            if G.role=='Working':
                col = layout.column(align = True)
                col.label(text="Local:")
                col.operator("cerebro.open_work_version").action="start"
                col.operator("cerebro.open_file_browser")

                # #(NOTES)
                # col.label(text="Notes:")
                # b1=col.operator("cerebro.open_last_version_by_webbrowser")
                # # b1.prefix="Commit"
                # b1.shot_name=""

                #(Textures:)
                # if G.task_parent_dict[str(G.selected_task[dbtypes.TASK_DATA_ID])][dbtypes.TASK_DATA_ACTIVITY_NAME] in settings.MONTAGE_OBJECT_TYPES:
                if os.environ["CEREBRO_B3D_CURRENT_ASSET_TYPE"] in settings.MONTAGE_OBJECT_TYPES:
                    pass
                else:
                    col.label(text="Textures:")
                    col.operator("cerebro.collecting_textures")
                    col.operator("cerebro.open_graphics_editor")

                #(Animation Tools:)
                if os.environ["CEREBRO_B3D_CURRENT_ASSET_TYPE"] in settings.ANIMATION_OBJECT_TYPES or G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]][dbtypes.TASK_DATA_ACTIVITY_NAME] in settings.ANIMATION_TYPES:
                    col = layout.column(align = True)
                    col.label(text="Animation Tools:")
                    # col.operator("cerebro.pack_links", text="Append selected link").action="selected"
                    # col.operator("cerebro.refresh_proxy", text="Refresh proxy")
                    col.operator("cerebro.download_animatic_to_shot")
                    col.operator("cerebro.download_animation_from_incoming").dialog=True
                    row=col.row(align=True)
                    row.label(text="Child Of:")
                    row.operator("b3dtools.child_of_on_off", text="ON").action="ON"
                    row.operator("b3dtools.child_of_on_off", text="OFF").action="OFF"
                    col.operator("cerebro.upload_current_shot_to_outsource")

                #(Render Tools)
                if os.environ["CEREBRO_B3D_CURRENT_ASSET_TYPE"] in settings.ANIMATION_OBJECT_TYPES or G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]][dbtypes.TASK_DATA_ACTIVITY_NAME] in settings.RENDERING_TYPES:
                    col = layout.column(align = True)
                    col.label(text="Rendering Tools:")
                    row=col.row(align = True)
                    row.operator("cerebro.render_evee", text="Clear folder").action="CLEAN_FOLDER"
                    row.operator("cerebro.render_evee", text="Settings").action="SETTINGS"
                    col.operator("cerebro.set_shot_clipping")
                    col.operator("cerebro.link_world_from_location")
                    col.operator("cerebro.objects_to_render_collection")
                    # row.operator("cerebro.push_render")
                    # row.operator("cerebro.render_evee", text="Render").action="FINAL_RENDER"

                #(local animatic tools)
                if os.environ["CEREBRO_B3D_CURRENT_ASSET_TYPE"]=="Shot" and os.environ.get("CEREBRO_B3D_CURRENT_TASK_ID"):
                    if G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]][dbtypes.TASK_DATA_ACTIVITY_NAME]=="animatic":
                        col = layout.column(align = True)
                        col.label(text="Animatic Tools:")
                        col.operator("cerebro.set_shot_timing")

                #(Animatic Tools:)
                if os.environ["CEREBRO_B3D_CURRENT_ASSET_TYPE"] in settings.MONTAGE_OBJECT_TYPES:
                    # layout = self.layout
                    box=layout.box()
                    box.label(text="Animatic Tools:")
                    # box.operator("cerebro.download_animatic")
                    box.prop(context.scene, "cerebro_panel_animatic_tools_create_shots_method1")
                    box.prop(context.scene, "cerebro_panel_animatic_tools_create_shots_method2")

                    #(-- create shots method 1)
                    if context.scene.cerebro_panel_animatic_tools_create_shots_method1:
                        steps=box.box()
                        steps.label(text="Create Shots \"Method 1\":")
                        steps.operator("cerebro.download_animatic")
                        row = steps.row(align = False)
                        row.label(text="step 1:")
                        row.operator("cerebro.rename_animatic_markers", text="Rename markers to Sequence").action="sequence"
                        row = steps.row(align = False)
                        row.label(text="step 2:")
                        row.operator("cerebro.rename_animatic_markers", text="Rename markers to Shot").action="shot"
                        row = steps.row(align = False)
                        row.label(text="step 3:")
                        row.operator("cerebro.create_shots_from_markers", text="Create Shots").method="method 1"

                    #(-- create shots method 2)
                    if context.scene.cerebro_panel_animatic_tools_create_shots_method2:
                        steps=box.box()
                        steps.label(text="Create Shots \"Method 2\":")
                        steps.operator("cerebro.download_scene_to_animatic")
                        row = steps.row(align = False)
                        row.label(text="step 1:")
                        row.operator("cerebro.rename_animatic_markers", text="Rename markers to Sequence").action="sequence2"
                        row = steps.row(align = False)
                        row.label(text="step 2:")
                        row.operator("cerebro.rename_animatic_markers", text="Rename markers to Shot").action="shot"
                        row = steps.row(align = False)
                        row.label(text="step 3:")
                        row.operator("cerebro.create_shots_from_markers", text="Create Shots").method="method 2"

                    #
                    edit_shots=box.box()
                    edit_shots.label(text="Export animatic data:")
                    col=edit_shots.column(align = False)
                    col.operator("cerebro.animatic_export_shots_data_to_csv").timeformat="frame"
                    col.operator("cerebro.animatic_export_shots_data_to_csv", text="Export shots timing to .csv (00:00:00:00)").timeformat="time"
                    col.operator("cerebro.to_outsource_panel")
                    # col.operator("cerebro.upload_shots_choise_content")
                    #
                    edit_shots=box.box()
                    edit_shots.label(text="Edit Shots:")
                    col=edit_shots.column(align = False)
                    col.operator("cerebro.re_create_selected_shots")
                    col.operator("cerebro.download_shot_animatic_to_episode")
                    #
                    review=box.box()
                    review.label(text="Review:")
                    col=review.column(align = False)
                    row = col.row(align = False)
                    row.label(text="Select shot sequences:")
                    row.operator("cerebro.select_shot_sequences_by_task_status", text="Choice status")
                    col.operator("cerebro.download_review")
                    #
                    col=box.column(align = False)
                    col.operator("cerebro.timing_from_selected_shots")
                    #
                    check_box=box.box()
                    shot_name=None
                    if bpy.context.selected_sequences:
                        if bpy.context.selected_sequences[0].name.startswith("Shot"):
                            shot_name=bpy.context.selected_sequences[0].name.split('.')[0]
                    check_box.label(text=f'Checking shot: {shot_name}')
                    col=check_box.column(align = False)
                    if shot_name:
                        b=col.operator("cerebro.open_last_version_by_webbrowser", text="Open this Shot by WEB")
                        # b.prefix="Review"
                        b.shot_name=shot_name
                        b.task_id=''

                        col.operator("cerebro.remove_incoming_links_from_selected_shot_panel", text="Edit shot content")

                        col.label(text="Change status:")
                        b1=col.operator("cerebro.change_status_of_selected_shot", text='На переработку')
                        b1.status_name="на переработку"
                        b1.shot_name=shot_name
                        b2=col.operator("cerebro.change_status_of_selected_shot", text='Принято. Режиссер')
                        b2.status_name="Принято. Режиссер"
                        b2.shot_name=shot_name
                        b3=col.operator("cerebro.change_status_of_selected_shot", text='Выполнена')
                        b3.status_name="выполнена"
                        b3.shot_name=shot_name
                        # b4=col.operator("cerebro.change_status_of_selected_shot", text='to "Approved"')
                        # b4.status_name="Approved"
                        # b4.shot_name=shot_name
                    
                #(To Cerebro:)
                col = layout.column(align = True)
                col.label(text="To Cerebro:")
                col.operator("cerebro.check_scene")
                col.operator("cerebro.commit", text="Commit")
                col.operator("cerebro.change_status").status_name="на утверждение"

                # if G.selected_task["parent"]["object_type"]["name"] in settings.ANIMATION_OBJECT_TYPES:
                col = layout.column(align = True)
                col.label(text="Playblast:")
                box=col.box()
                box.prop(context.scene.render, "stamp_font_size")
                box.prop(context.scene.render, "stamp_foreground")
                box.prop(context.scene.render, "stamp_background")
                col.operator("cerebro.playblast")
                col.operator("cerebro.playblast_to_review")
            else:
                col = layout.column(align = True)
                col.label(text="Local:")
                col.operator("cerebro.open_work_version", text="Look version").action="start"
                col.operator("cerebro.check_scene")
                col.operator("cerebro.collecting_textures")
                col.operator("cerebro.open_file_browser")

                col = layout.column(align = True)
                col.label(text="To Cerebro:")
                col.operator("cerebro.commit", text="Commit")

                col = layout.column(align = True)
                col.label(text="Playblast:")
                box=col.box()
                box.prop(context.scene.render, "stamp_font_size")
                box.prop(context.scene.render, "stamp_foreground")
                box.prop(context.scene.render, "stamp_background")
                col.operator("cerebro.playblast")
                col.operator("cerebro.playblast_to_review")

                # col.label(text="Notes:")
                # b1=col.operator("cerebro.open_last_version_by_webbrowser")
                # # b1.prefix="Commit"
                # b1.shot_name=""
                
                #(Animation Tools:)
                if os.environ["CEREBRO_B3D_CURRENT_ASSET_TYPE"] in settings.ANIMATION_OBJECT_TYPES:
                    col = layout.column(align = True)
                    col.label(text="Animation Tools:")
                    # col.operator("cerebro.pack_links", text="Append selected link").action="selected"
                    col.operator("cerebro.refresh_proxy", text="Refresh proxy")
                    col.operator("cerebro.download_animatic_to_shot")
                
                col.label(text="Change status to:")
                col.operator("cerebro.change_status", text='На переработку').status_name="на переработку"
                col.operator("cerebro.change_status", text='Принято. Лид').status_name="Принято. Лид"
                col.operator("cerebro.change_status", text='Принято. Режиссер').status_name="Принято. Режиссер"
                col.operator("cerebro.change_status", text='Выполнена').status_name="выполнена"
            
            col = layout.column(align = True)
            col.operator("cerebro.switch_panels").action="to task list"

        ## SOURCE PANEL
        if context.scene.ftrack_panel_sources:
            layout = self.layout
            col = layout.column(align = True)
            col.label(text="Sources:")
            col.prop(context.scene, "ftrack_filter_source")
            for task, vers, byte, url, status in G.components[0]:
                # conditions=(
                #     context.scene.ftrack_filter_source.lower() in srvconn.parent_name(task).lower(),
                #     context.scene.ftrack_filter_source.lower() in task[dbtypes.TASK_DATA_NAME].lower(),
                #     )
                target=f"{srvconn.parent_name(task)}{task[dbtypes.TASK_DATA_NAME]}{status}"
                if not tools.search(context.scene.ftrack_filter_source, target):
                    continue
                else:
                    row = col.row(align = True)
                    row.label(text=srvconn.parent_name(task))
                    # row.label(text=task[dbtypes.TASK_DATA_NAME])
                    # row.label(text=status)
                    row.label(text=f"{task[dbtypes.TASK_DATA_NAME]} [{status}]")
                    if status=='missing':
                        row.label(text='')
                        row.operator("cerebro.reload_incoming_version").task_id=str(task[dbtypes.TASK_DATA_ID])
                        row.operator("cerebro.props_info", icon="QUESTION", text='').task_id=str(task[dbtypes.TASK_DATA_ID])
                    else:
                        row.operator("cerebro.link_of_source", text="Add").task_id=str(task[dbtypes.TASK_DATA_ID])
                        row.operator("cerebro.reload_incoming_version").task_id=str(task[dbtypes.TASK_DATA_ID])
                        row.operator("cerebro.props_info", icon="QUESTION", text='').task_id=str(task[dbtypes.TASK_DATA_ID])
            col.operator("cerebro.switch_panels").action=f"to {G.current_panel}"

        ## LIST OF LOCAL VERSIONS
        if context.scene.ftrack_panel_versions_list:
            layout = self.layout
            col = layout.column(align = True)
            for version in G.versions_list:
                row = col.row(align = True)
                row.label(text=str(version['local_version']))
                # row.label(text=str(version['version_id']))
                row.label(text=str(version['created']))
                row.label(text=version['description'])
                row.operator("cerebro.open_work_version", text="Open").action=str(version['local_version'])
                row.operator("cerebro.message", text="", icon="QUESTION").message=f"Commit: {version['description']}"
            col.operator("cerebro.switch_panels").action=f"to {G.current_panel}"

        ## LIST OF GLOBAL VERSIONS
        if context.scene.ftrack_panel_ftrack_versions_list:
            col = layout.column(align = True)
            col.label(text=f'DOWNLOAD VERSION of [{srvconn.parent_name(G.selected_task)}] '
                f'{G.selected_task[dbtypes.TASK_DATA_NAME]}')
            for i,v in enumerate(G.ftrack_versions_list):
                try:
                    row = col.row(align = True)
                    if not "_v" in v['attachment'][dbtypes.ATTACHMENT_DATA_COMMENT]:
                        row.label(text=f"{v['attachment'][dbtypes.ATTACHMENT_DATA_COMMENT]}_v{i+1}")
                    else:
                        row.label(text=v['attachment'][dbtypes.ATTACHMENT_DATA_COMMENT])
                    row.label(text=f"[{v['message'][dbtypes.MESSAGE_DATA_CREATOR_NAME]}]: {v['message'][dbtypes.MESSAGE_DATA_TEXT]}")
                    row.label(text=str(v['message'][dbtypes.MESSAGE_DATA_CREATED].strftime("%Y-%m-%d %H:%M:%S")))
                    row.operator("cerebro.download_version", text='download').action=str(v['message'][dbtypes.MESSAGE_DATA_ID])
                    row.operator("cerebro.message", text="", icon="QUESTION").message=f"Commit: {v['message'][dbtypes.MESSAGE_DATA_TEXT]}"
                except Exception as e:
                    print(f'{traceback.format_exc()}')
                    # print(e)
            col.operator("cerebro.switch_panels").action=f"to {G.current_panel}"

class CEREBRO_VIEW_3D_panel(CEREBRO_main_panel, bpy.types.Panel):
    bl_idname = "CEREBRO_PT_VIEW_3D_panel"
    bl_label = 'Cerebro:'
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = 'Cerebro'

class CEREBRO_SEQUENCE_EDITOR_panel(CEREBRO_main_panel, bpy.types.Panel):
    bl_idname = "CEREBRO_PT_SEQUENCE_EDITOR_panel"
    bl_label = 'Cerebro:'
    bl_space_type = "SEQUENCE_EDITOR"
    bl_region_type = "UI"
    bl_category = 'Cerebro'

class CEREBRO_auth_start(bpy.types.Operator):
    bl_idname = "cerebro.auth_start"
    bl_label = "Authorization"

    def execute(self, context):
        context.scene.ftrack_auth_in_process=True
        return {'FINISHED'}


class CEREBRO_setting_start(bpy.types.Operator):
    bl_idname = "cerebro.setting_start"
    bl_label = "Setting"

    action: bpy.props.StringProperty(name="Action")

    def execute(self, context):
        if self.action=='cancel':
            context.scene.ftrack_setting_in_process=False
        else:
            context.scene.ftrack_setting_in_process=True
        return {'FINISHED'}

class CEREBRO_authorization(bpy.types.Operator):
    bl_idname = "cerebro.authorization"
    bl_label = "Auth"

    action: bpy.props.StringProperty(name="Action")

    def execute(self, context):
        if self.action=='cancel':
            context.scene.ftrack_auth_in_process=False
            return {'FINISHED'}
        r,m=auth(context)
        if not r:
            self.report({'WARNING'}, m)
        else:
            self.report({'INFO'}, m)
            bpy.ops.cerebro.update('INVOKE_DEFAULT')
        return {'FINISHED'}


class CEREBRO_update(bpy.types.Operator):
    bl_idname = "cerebro.update"
    bl_label = "Upodate"

    action: bpy.props.StringProperty(name="Action")

    def execute(self, context):
        b,r=update.update(G.release)
        if not b:
            self.report({'WARNING'}, r)
        else:
            m=f"Установлена версия {G.release['tag_name']}|Чтобы применить обновления перезапустите Блендер!"
            bpy.ops.cerebro.message("INVOKE_DEFAULT", message=m)
            self.report({'INFO'}, m)
        return {'FINISHED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        col.label(text=f"Доступно обновление плагина: ({G.release['tag_name']}). Обновить?")

    def invoke(self, context, event):
        b,r=update.lag_test()
        if b:
            print(f"[INFO]: существует более свежая версия плагина {r['tag_name']}.")
            G.release=r
            wm = context.window_manager
            return wm.invoke_props_dialog(self)
        else:
            self.report({'INFO'}, "Обновления отсутствуют.")
            return{'FINISHED'}


class CEREBRO_manual(bpy.types.Operator):
    bl_idname = "cerebro.manual"
    bl_label = "Manual"
    
    def execute(self, context):
        url='https://cerebro-b3b-connect-manual.readthedocs.io/en/latest'
        # url=f'file:///{os.environ["CEREBRO_DOCS_PATH"]}'
        webbrowser.open_new_tab(url)
        return{'FINISHED'}


class CEREBRO_api(bpy.types.Operator):
    bl_idname = "cerebro.api"
    bl_label = "Python api"
    
    def execute(self, context):
        # url='https://ftrack-b3d-connect-manual.readthedocs.io/en/latest'
        url=f'file:///{os.environ["CEREBRO_DOCS_PATH"]}'
        webbrowser.open_new_tab(url)
        return{'FINISHED'}


class CEREBRO_set_folder(bpy.types.Operator):
    bl_idname = "cerebro.set_folder"
    bl_label = "Set Folder"
    
    directory:  bpy.props.StringProperty(subtype="DIR_PATH")
        
    def execute(self, context):
        result = settings.set_projects_folder(self.directory)
        if not result[0]:
            self.report({'WARNING'}, result[1])
        else:   
            self.report({'INFO'}, (f'set Projects folder: {result[1]}'))
        return{'FINISHED'}
    
    def invoke(self, context, event):
        wm = context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

class CEREBRO_select_task(bpy.types.Operator):
    bl_idname = "cerebro.select_task"
    bl_label = "select"

    task_id:  bpy.props.StringProperty(name="task_id")
    
    def execute(self, context):
        # (1)
        task=G.task_dict[self.task_id]
        os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]=self.task_id
        os.environ["CEREBRO_B3D_CURRENT_ASSET_NAME"]=srvconn.parent_name(task)
        os.environ["CEREBRO_B3D_CURRENT_ASSET_TYPE"]=srvconn.get_asset_type_of_task(task)
        os.environ["CEREBRO_B3D_CURRENT_ASSET_ID"]=str(task[dbtypes.TASK_DATA_PARENT_ID])
        if os.environ["CEREBRO_B3D_CURRENT_ASSET_TYPE"]=="Shot":
            tags=srvconn.get_task_tags(srvconn.get_parent_task(task)[dbtypes.TASK_DATA_ID])
            if 'fend' in tags and 'fstart' in tags:
                os.environ["CEREBRO_B3D_SCENE_FEND"]=str(float(os.environ["CEREBRO_B3D_CURRENT_PROJECT_FSTART"]) + float(tags['fend']) - float(tags['fstart']))
            else:
                os.environ["CEREBRO_B3D_SCENE_FEND"]=str(float(os.environ["CEREBRO_B3D_CURRENT_PROJECT_FSTART"]) + 100)
            if 'clip_start' in tags:
                os.environ["CEREBRO_B3D_CLIP_START"]=str(tags['clip_start'])
            if 'clip_end' in tags:
                os.environ["CEREBRO_B3D_CLIP_END"]=str(tags['clip_end'])
        lag_test_of_version(self, task)
        self.report({'INFO'}, self.task_id)
        # (2)
        bpy.ops.cerebro.switch_panels(action="to selected")
        return{'FINISHED'}

class CEREBRO_switch_panels(bpy.types.Operator):
    bl_idname = "cerebro.switch_panels"
    bl_label = "Close"

    action: bpy.props.StringProperty(name="action")

    def execute(self, context):
        self.report({'INFO'}, self.action)
        # (1)
        if self.action=="to task list":
            context.scene.ftrack_panel_sources=False
            context.scene.ftrack_panel_ftrack_versions_list=False
            context.scene.ftrack_panel_selected_task=False
            context.scene.ftrack_panel_working_task=False
            context.scene.ftrack_panel_versions_list=False
            context.scene.ftrack_panel_task_list=True
            G.current_panel="task list"
            # fill_tasks_lists(self, context)
            srvconn.timer_stop()
            context.scene.ftrack_filter_gen=os.environ.get("CEREBRO_B3D_CURRENT_ASSET_NAME", "")
        elif self.action=="to versions":
            context.scene.ftrack_panel_task_list=False
            context.scene.ftrack_panel_sources=False
            context.scene.ftrack_panel_ftrack_versions_list=False
            context.scene.ftrack_panel_working_task=False
            context.scene.ftrack_panel_selected_task=False
            context.scene.ftrack_panel_versions_list=True
        elif self.action=="to ftrack versions":
            context.scene.ftrack_panel_task_list=False
            context.scene.ftrack_panel_sources=False
            context.scene.ftrack_panel_working_task=False
            context.scene.ftrack_panel_selected_task=False
            context.scene.ftrack_panel_ftrack_versions_list=True
        elif self.action=="to working":
            context.scene.ftrack_panel_task_list=False
            context.scene.ftrack_panel_sources=False
            context.scene.ftrack_panel_ftrack_versions_list=False
            context.scene.ftrack_panel_selected_task=False
            context.scene.ftrack_panel_versions_list=False
            context.scene.ftrack_panel_working_task=True
            G.current_panel="working"
        elif self.action=="to selected":
            context.scene.ftrack_panel_sources=False
            context.scene.ftrack_panel_versions_list=False
            context.scene.ftrack_panel_ftrack_versions_list=False
            context.scene.ftrack_panel_task_list=False
            context.scene.ftrack_panel_selected_task=True
            G.current_panel="selected"
        elif self.action=="to sources":
            context.scene.ftrack_panel_versions_list=False
            context.scene.ftrack_panel_ftrack_versions_list=False
            context.scene.ftrack_panel_task_list=False
            context.scene.ftrack_panel_selected_task=False
            context.scene.ftrack_panel_working_task=False
            context.scene.ftrack_panel_sources=True
        return{'FINISHED'}

class CEREBRO_current_scene_to_work(bpy.types.Operator):
    bl_idname = "cerebro.current_scene_to_work"
    bl_label = "Current scene to work"

    def execute(self, context):
        # (0)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        # (1)
        b,r = working.check_file_structure(task)
        if not b:
            self.report({'WARNING'}, r)
            return{'FINISHED'}
        # (2)
        b,r=working.save_current_scene_as_top_version(task)
        if not b:
            self.report({'WARNING'}, r)
            return{'FINISHED'}
        # (3)
        bpy.ops.cerebro.switch_panels(action="to working")
        # (end)
        self.report({'INFO'}, "Current scene to work")
        return{'FINISHED'}


class CEREBRO_open_scene_from_incoming_task(bpy.types.Operator):
    bl_idname = "cerebro.open_scene_from_incoming_task"
    bl_label = "Open from incoming"

    dialog: bpy.props.BoolProperty(default=False)
    task_id: bpy.props.StringProperty(default='')
    build: bpy.props.BoolProperty(default=False)

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]

        # (2)
        row=G.incoming_tasks.get(self.task_id)
        if row:
            incoming_task=row['task']
        else:
            self.report({'INFO'}, "No incoming task!")
            return{'FINISHED'}
        if self.build:
            b,r=working.build_scene(context, task, True, from_incoming=incoming_task)
        else:
            b,r=working.open_scene_from_incoming_task(task, incoming_task)
        if not b:
            # bpy.ops.cerebro.message('INVOKE_DEFAULT', message=f"[WARNING]: | Открытие не удалось!")
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
            
        # (3)
        if not self.build:
            bpy.ops.cerebro.switch_panels(action="to working")
        return{'FINISHED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        col.label(text="Incoming tasks:")
        for task_id in G.incoming_tasks:
            row = col.row(align = True)
            row.label(text=G.incoming_tasks[task_id]['task'][dbtypes.TASK_DATA_NAME])
            row.label(text=G.incoming_tasks[task_id]['status'])
            if self.build:
                op=row.operator('cerebro.open_scene_from_incoming_task', text="Build")
            else:
                op=row.operator('cerebro.open_scene_from_incoming_task', text="Open")
            op.task_id=task_id
            op.dialog=False
            op.build=self.build

    def invoke(self, context, event):
        if self.dialog:
            b,r=srvconn.get_incoming_tasks(G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]])
            if not b:
                self.report({'WARNING'}, r)
                return{'FINISHED'}
            G.incoming_tasks=r
            wm = context.window_manager
            return wm.invoke_props_dialog(self)
        else:
            return self.execute(context)


class CEREBRO_build_scene(bpy.types.Operator):
    bl_idname = "cerebro.build_scene"
    bl_label = "Build"

    link: bpy.props.BoolProperty(default=True)
    path: bpy.props.StringProperty()

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]

        b,r=working.build_scene(context, task, self.link, source_path=self.path)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, "Scene is built.")
        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)


class CEREBRO_build_scene_from_version(bpy.types.Operator):
    bl_idname = "cerebro.build_scene_from_version"
    bl_label = "Build from version"

    def execute(self, context):

        return{'FINISHED'}

    def draw(self, context):
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        layout = self.layout
        col = layout.column()
        for version in G.versions_list:
            row = col.row(align = True)
            row.label(text=str(version['local_version']))
            row.label(text=str(version['created']))
            row.label(text=version['description'])
            b,vpath = path_utils.get_version_path_of_workfile(task, version["local_version"])
            if b:
                op=row.operator("cerebro.build_scene", text="Build")
                op.path=vpath
                op.link=True

    def invoke(self, context, event):
        # (1)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        b,r = db.get_all_work_versions(task)
        if not b:
            self.report({'WARNING'}, r)
            return{'FINISHED'}
        G.versions_list=r

        # (2)
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

        
class CEREBRO_open_work_top_version(bpy.types.Operator):
    bl_idname = "cerebro.open_work_top_version"
    bl_label = "Open"

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        # (2)
        if G.role=="Working":
            b,r=working.open_task(task)
        else:
            b,r=working.open_task(task, look=True)
        if not b:
            self.report({'WARNING'}, r)
            return{'FINISHED'}
        # (3)
        bpy.ops.cerebro.switch_panels(action="to working")
        # (end)
        self.report({'INFO'}, "Open")
        return{'FINISHED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        for line in G.message:
            col.label(text=line)

    def invoke(self, context, event):
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        G.message=list()
        dialog=False
        if G.lag:
            for item in CAUTION_OF_LAG:
                try:
                    G.message.append(item % G.lag)
                except:
                    G.message.append(item)
            dialog=True
        if G.statuses[str(task[dbtypes.TASK_DATA_CC_STATUS])] in srvconn.CHEKING_STATUSES and G.role=="Working":
            G.message.append('-------')
            G.message=G.message+CAUTION_TO_INPROGRESS
            dialog=True

        if dialog:
            wm = context.window_manager
            return wm.invoke_props_dialog(self)
        else:
            return self.execute(context)
        
class CEREBRO_open_work_version(bpy.types.Operator):
    bl_idname = "cerebro.open_work_version"
    bl_label = "Open version"

    action: bpy.props.StringProperty(name="Action")

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        # (2)
        if self.action=='start':
            # (1)
            b,r = db.get_all_work_versions(task)
            if not b:
                self.report({'WARNING'}, r)
                return{'FINISHED'}
            G.versions_list=r
            # (2)
            bpy.ops.cerebro.switch_panels(action="to versions")
            # (end)
            return{'FINISHED'}
        else:
            # (1)
            if G.role=="Working":
                b,r = working.open_version(task, int(self.action))
            else:
                b,r = working.open_version(task, int(self.action), look=True)
            if not b:
                self.report({'WARNING'}, r)
                # return{'FINISHED'}
            # (2)
            bpy.ops.cerebro.switch_panels(action="to working")
            # (end)
            self.report({'INFO'}, f"open version {self.action}")
            return{'FINISHED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        for line in G.message:
            col.label(text=line)

    def invoke(self, context, event):
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        G.message=list()
        dialog=False
        if self.action!='start' and G.lag:
            for item in CAUTION_OF_LAG:
                try:
                    G.message.append(item % G.lag)
                except:
                    G.message.append(item)
            dialog=True
        if self.action!='start' and task[dbtypes.TASK_DATA_CC_STATUS] in srvconn.CHEKING_STATUSES and G.role=="Working":
            G.message.append('-------')
            G.message=G.message+CAUTION_TO_INPROGRESS
            dialog=True

        if dialog:
            wm = context.window_manager
            return wm.invoke_props_dialog(self)
        else:
            return self.execute(context)

class CEREBRO_commit(bpy.types.Operator):
    bl_idname = "cerebro.commit"
    bl_label = "Commit"

    to_review: bpy.props.BoolProperty(name="To Review", default=False)
    description: bpy.props.StringProperty(name="Description")
    push: bpy.props.BoolProperty(name="Push to Cerebro", default=True)

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        
        #(2)
        if self.to_review:
            bpy.ops.cerebro.check_scene('INVOKE_DEFAULT')
            if not G.check[0]:
                return{'FINISHED'}
        #(3)
        if self.to_review:
            status_name='на утверждение'
        else:
            status_name='в работе'
        #(4)
        if not self.description:
            self.report({'WARNING'}, "Description is a required parameter!")
            return{'FINISHED'}
        #(5)
        V=''
        if self.push:
            srvconn._get_future_commit_num(task)
            if G.future_commit_num:
                V=f" (v{G.future_commit_num}) "
                
        b,v=working.commit(context, task, f"{V}{self.description} [{bpy.app.version_string}]", status_name, push=self.push)
        if not b:
            self.report({'WARNING'}, v)
            return{'FINISHED'}
        #(6)
        b,r=srvconn.task_status_to(task, status_name)
        if not b:
            self.report({'WARNING'}, v)
            return{'FINISHED'}
        self.report({'INFO'}, f"Create version: {v}")
        return{'FINISHED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        # col.label(text="Commit")
        col.prop(self, "to_review")
        col.prop(self, "description")
        if G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]][dbtypes.TASK_DATA_ACTIVITY_NAME] in ("rigging", "layout"):
            col.prop(self, "push")
        elif os.environ["CEREBRO_B3D_CURRENT_ASSET_TYPE"]=="Location":
            col.prop(self, "push")

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        # wm = context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

class CEREBRO_download_version(bpy.types.Operator):
    bl_idname = "cerebro.download_version"
    bl_label = "Download Version"

    action: bpy.props.StringProperty(name="Action")
    message: bpy.props.StringProperty(name="Message", default='')

    def execute(self, context):
        # (1)
        if self.action=="start":
            b,r = get_versions_list()
            if not b:
                self.report({'WARNING'}, r)
                return{'FINISHED'}
            bpy.ops.cerebro.switch_panels(action="to ftrack versions")
        else:
            # (1)
            task=G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
            b,r=working.download_version(task, G.ftrack_versions_dict[self.action])
            if not b:
                self.report({'WARNING'}, r)
                return{'FINISHED'}
            self.report({'INFO'}, r)
            lag_test_of_version(self, task) #(!!!)

            #(2 message)
            v=G.ftrack_versions_dict[self.action]
            # if not "_v" in v['attachment'][dbtypes.ATTACHMENT_DATA_COMMENT]:
            #     m=f"{v['attachment'][dbtypes.ATTACHMENT_DATA_COMMENT]}_v{i+1}"
            # else:
            #     m=v['attachment'][dbtypes.ATTACHMENT_DATA_COMMENT]
            m=v['message'][dbtypes.MESSAGE_DATA_TEXT]
            bpy.ops.cerebro.message('INVOKE_DEFAULT', message=f"Загружена версия: {m}")

        # (end)
        return{'FINISHED'}


    def draw(self, context):
        layout = self.layout
        col = layout.column()
        for m in self.message.split('|'):
            col.label(text=m)


    def invoke(self, context, event):
        if self.action=="start":
            return self.execute(context)
        else:
            task=G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
            b,r=db.get_all_work_versions(task)
            if not b:
                self.report({'WARNING'}, r)
                return{'FINISHED'}
            if r[-1:] and str(r[-1:][0]["version_id"])==self.action:
                self.message=f"Скачиваемая версия совпадает | с последней локальной. | Всё равно скачать?"
                wm = context.window_manager.invoke_props_dialog(self)
                return {'RUNNING_MODAL'}
            else:
                local_num=None
                for v in r:
                    if str(v["version_id"])==self.action:
                        local_num=v["local_version"]
                if local_num:
                    self.message=f"Скачиваемая версия совпадает | c {local_num}-ой локальной. | Всё равно скачать?"
                    wm = context.window_manager.invoke_props_dialog(self)
                    return {'RUNNING_MODAL'}
                else:
                    return self.execute(context)


class CEREBRO_update_incoming(bpy.types.Operator):
    bl_idname = "cerebro.update_incoming"
    bl_label = "Update incoming"

    def execute(self, context):
        # (1)
        b,r=srvconn.get_downloadable_incoming_links(for_download=True)
        if not b:
            self.report({'WARNING'}, r)
            return{'FINISHED'}
        G.components=r
        # return{'FINISHED'}
        bpy.ops.cerebro.download_incoming()
        self.report({'INFO'}, "Loading is complete!")
        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}

class CEREBRO_download_incoming(bpy.types.Operator):
    bl_idname = "cerebro.download_incoming"
    bl_label = "Download?"

    info_data: bpy.props.StringProperty(name="Download data", default="qwerty")

    def execute(self, context):
        for task, vers, byte, url, status in G.components[0]:
            if vers and status in ('missing', 'old'):
                b,r=working.download_incoming_task_data_component(task, vers, url=url)
                if not b:
                    self.report({'WARNING'}, r)
                else:
                    self.report({'INFO'}, r)
            else:
                continue
        # self.report({'INFO'}, "Loading is complete!")
        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}

class CEREBRO_collecting_textures(bpy.types.Operator):
    bl_idname = "cerebro.collecting_textures"
    bl_label = "Collect textures"

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        b,r=working.collect_textures(task)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, "Textures collected")
        return{'FINISHED'}


class CEREBRO_sources_panel(bpy.types.Operator):
    bl_idname = "cerebro.sources_panel"
    bl_label = "Sources panel"

    def execute(self, context):
        # (1)
        return{'FINISHED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        if G.components:
            col.operator("cerebro.sources_panel_action", text="From cache").from_cache="cache"
        col.operator("cerebro.sources_panel_action", text="New read data").from_cache="reload"

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)


class CEREBRO_sources_panel_action(bpy.types.Operator):
    bl_idname = "cerebro.sources_panel_action"
    bl_label = "Sources panel action"

    from_cache: bpy.props.StringProperty(default='')

    def execute(self, context):
        # (1)
        if self.from_cache=="reload":
            b,r=srvconn.get_downloadable_incoming_links()
            if not b:
                self.report({'WARNING'}, r)
                return{'FINISHED'}
            G.components=r
        elif self.from_cache=="cache":
            if not G.components:
                return{'FINISHED'}
        else:
            return{'FINISHED'}

        # (2)
        bpy.ops.cerebro.switch_panels(action="to sources")
        self.report({'INFO'}, "The sources panel is open")
        return{'FINISHED'}

class CEREBRO_link_of_source(bpy.types.Operator):
    bl_idname = "cerebro.link_of_source"
    bl_label = "Incoming data"

    task_id: bpy.props.StringProperty(name="Task ID")
    # filepath: bpy.props.StringProperty(subtype="FILEPATH")

    def execute(self, context):
        #(1)
        self.report({'INFO'},G.sourcepath)
        return{'FINISHED'}

    def invoke(self, context, event):
        bpy.types.Scene.source_collection_filter = bpy.props.StringProperty(name = 'Filter', default='', update = None)
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def draw(self, context):
        #
        for item in G.components[0]:
            if str(item[0][dbtypes.TASK_DATA_ID])==self.task_id:
                b,(r,path)=working.get_collections_of_source(item[0])
                if b:
                    G.sourcepath=path
                else:
                    r=[]
                break
        #
        layout = self.layout
        col = layout.column()
        col.label(text="List of collections:")
        col.prop(context.scene, "source_collection_filter")
        for name in r:
            if context.scene.source_collection_filter and not tools.search(context.scene.source_collection_filter, name):
                continue
            row = col.row(align = True)
            row.label(text=name)
            op=row.operator("cerebro.link", text="Owerride")
            op.collection=name
            op.link=True
            op.task_id=self.task_id
            op=row.operator("cerebro.link", text="Append")
            op.collection=name
            op.link=False
            op.task_id=self.task_id

class CEREBRO_link(bpy.types.Operator):
    bl_idname = "cerebro.link"
    bl_label = "Link"

    link: bpy.props.BoolProperty(name="Link", default=True)
    owerride: bpy.props.BoolProperty(name="Owerride", default=True)
    collection: bpy.props.StringProperty(name="Collection")
    task_id: bpy.props.StringProperty(name="Task ID")

    def execute(self, context):
        # (1)
        b,c=working.link_collection(context, G.sourcepath, self.collection, self.link, task_id=self.task_id)
        if not b:
            self.report({'INFO'}, c)
            return{'FINISHED'}
        if self.link and self.owerride:
            b,r=working.owerride_collection(context, c)
            if not b:
                self.report({'INFO'}, r)
                return{'FINISHED'}
        self.report({'INFO'}, f"Linked {self.collection}")
        return{'FINISHED'}

class CEREBRO_reload_incoming_version(bpy.types.Operator):
    bl_idname = "cerebro.reload_incoming_version"
    bl_label = "Reload version"

    task_id: bpy.props.StringProperty(name="Task ID")
    
    def execute(self, context):
        #(1)
        self.report({'INFO'}, "Ok!")
        return{'FINISHED'}

    def invoke(self, context, event):
        get_versions_list(self.task_id)
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        col.label(text="List of versions:")
        for version in G.ftrack_versions_list:
            row = col.row(align = True)
            row.label(text=version["message"][dbtypes.MESSAGE_DATA_CREATOR_NAME])
            row.label(text=version['message'][dbtypes.MESSAGE_DATA_TEXT])
            row.label(text=str(version['message'][dbtypes.MESSAGE_DATA_CREATED].strftime("%Y-%m-%d %H:%M:%S")))
            op=row.operator("cerebro.reload_incoming_version_action")
            op.task_id=self.task_id
            op.version_id=str(version["message"][dbtypes.MESSAGE_DATA_ID])

class CEREBRO_reload_incoming_version_action(bpy.types.Operator):
    bl_idname = "cerebro.reload_incoming_version_action"
    bl_label = "Download"

    task_id: bpy.props.StringProperty(name="Task ID")
    version_id: bpy.props.StringProperty(name="Version ID")

    def execute(self, context):
        #(1)
        b,r=working.download_incoming_task_data_component(self.task_id, G.ftrack_versions_dict[self.version_id])
        self.report({'INFO'}, f"task: {self.task_id} version:{self.version_id}")
        return{'FINISHED'}

class CEREBRO_add_graphics_editors(bpy.types.Operator):
    bl_idname = "cerebro.add_graphics_editors"
    bl_label = "Add graphics editor"

    alias: bpy.props.StringProperty(name="Name")
    filepath: bpy.props.StringProperty(subtype="FILE_PATH")

    def execute(self, context):
        # (1)
        if not self.alias:
            self.alias=os.path.basename(self.filepath).capitalize()
        if self.filepath:
            settings.add_graphics_editors(self.alias, self.filepath)
            read_graphics_editors()

        self.report({'INFO'}, f"Added: {self.alias} - {self.filepath}")
        return{'FINISHED'}

    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

class CEREBRO_del_graphics_editors(bpy.types.Operator):
    bl_idname = "cerebro.del_graphics_editors"
    bl_label = "Delete"

    alias: bpy.props.StringProperty()
    filepath: bpy.props.StringProperty(subtype="FILE_PATH")

    def execute(self, context):
        # (1)
        if self.alias:
            settings.del_graphics_editors(self.alias)
            read_graphics_editors()

        self.report({'INFO'}, f"Deleted: {self.alias}")
        return{'FINISHED'}

class CEREBRO_open_graphics_editor(bpy.types.Operator):
    bl_idname = "cerebro.open_graphics_editor"
    bl_label = "Edit textures"

    app_item = [('-select application-',)*3]
    if not G.graphics_editors:
        read_graphics_editors()
    if G.graphics_editors:
        for key in G.graphics_editors:
            app_item.append((key,)*3)

    alias: bpy.props.EnumProperty(name = 'Open by:', items = tuple(app_item))
    filepath: bpy.props.StringProperty(subtype="FILE_PATH")

    def execute(self, context):
        # (1)
        if self.alias != "-select application-":
            working.open_images(G.graphics_editors[self.alias], self.filepath)

        self.report({'INFO'}, f"Opening: {self.alias}")
        return{'FINISHED'}

    def invoke(self, context, event):
        task=G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        b, path = path_utils.get_folder_of_textures_path(task)
        if b:
            self.filepath = path + '/'
        # else:
        #     self.filepath = "//"
        wm = context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

class CEREBRO_open_file_browser(bpy.types.Operator):
    bl_idname = "cerebro.open_file_browser"
    bl_label = "Open task folder"

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        b,r=working.open_task_folder_in_filebrowser(task)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}


class CEREBRO_props_info(bpy.types.Operator):
    bl_idname = "cerebro.props_info"
    bl_label = "Info"

    task_id: bpy.props.StringProperty()
    
    def draw(self, context):
        layout = self.layout
        col = layout.column()
        col.operator("cerebro.open_file_browser_by_id", text="Open Folder").task_id=self.task_id
        op=col.operator("cerebro.open_last_version_by_webbrowser", text="Open WEB")
        op.task_id=self.task_id
        op.shot_name=''

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def execute(self, context):
        return{'FINISHED'}
            
        
class CEREBRO_open_file_browser_by_id(bpy.types.Operator):
    bl_idname = "cerebro.open_file_browser_by_id"
    bl_label = "Open task folder"

    task_id: bpy.props.StringProperty()
    
    def execute(self, context):
        # (1)
        task = G.task_dict[self.task_id]
        b,r=working.open_task_folder_in_filebrowser(task)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}


class CEREBRO_select_object(bpy.types.Operator):
    bl_idname = "cerebro.select_object"
    bl_label = "Select"

    ob_string: bpy.props.StringProperty() # from ob.__repr__()

    def execute(self, context):
        #(0)
        def select_other(ob):
            self.report({'INFO'}, "*** WRONG OBJECT ***")
            self.report({'INFO'}, ob.name)
            self.report({'INFO'}, self.ob_string)
            self.report({'INFO'}, "Look the Info panel!")
        #(1)
        bpy.ops.object.select_all(action='DESELECT')
        #(2)
        ob=eval(self.ob_string)
        if ob.name in bpy.data.objects:
            try:
                ob.select_set(True)
                context.view_layer.objects.active = ob
                self.report({'INFO'}, "*** WRONG OBJECT ***")
                self.report({'INFO'}, self.ob_string)
                self.report({'INFO'}, f"selected: {ob.name}")
            except:
                select_other(ob)
        elif hasattr(ob, "type") and ob.type=="IMAGE":
            self.report({'INFO'}, "*** Uncollected IMAGE ***")
            self.report({'INFO'}, ob.name)
            self.report({'INFO'}, f"filepath: {ob.filepath_from_user()}")
            self.report({'INFO'}, "Look the Info panel!")
        else:
            select_other(ob)
        
        return{'FINISHED'}

class CEREBRO_check_scene(bpy.types.Operator):
    bl_idname = "cerebro.check_scene"
    bl_label = "Check"

    def execute(self, context):
        if G.check and G.check[0]:
            self.report({'INFO'}, "All right!")
        else:
            for key in G.check[1]:
                if not G.check[1][key]:
                    continue
                self.report({'INFO'}, f'{key}:')
                if isinstance(G.check[1][key], list):
                    for item in G.check[1][key]:
                        self.report({'INFO'}, item)
                else:
                    self.report({'INFO'}, str(G.check[1][key]))
        return{'FINISHED'}

    def invoke(self, context, event):
        #(1)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        #(2)
        b,r=check.check(task, context)
        G.check=(b,r)
        if not b:
            wm = context.window_manager
            return wm.invoke_props_dialog(self)
        else:
            return self.execute(context)

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        # col.label(text="Check list:")
        for key in G.check[1]:
            if not G.check[1][key]:
                continue
            col.label(text=f'{key}:')

            if isinstance(G.check[1][key], list):
                for item in G.check[1][key]:
                    row = col.row(align = True)
                    row.label(text=item)
                    
                    row.operator("cerebro.select_object").ob_string=item
            else:            
                col.label(text=str(G.check[1][key]))

class CEREBRO_change_status(bpy.types.Operator):
    bl_idname = "cerebro.change_status"
    bl_label = "Change status to \"на утверждение\""

    status_name: bpy.props.StringProperty()

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        b,r=srvconn.task_status_to(task, self.status_name)
        if not b:
            self.report({'WARNING'}, r)
            return{'FINISHED'}
        self.report({'INFO'}, f"status changed to {self.status_name}")
        return{'FINISHED'}


class CEREBRO_change_status_of_selected_shot(bpy.types.Operator):
    bl_idname = "cerebro.change_status_of_selected_shot"
    bl_label = "Change review status"

    status_name: bpy.props.StringProperty()
    shot_name: bpy.props.StringProperty()

    def execute(self, context):
        b,r=working.change_status_of_selected_shot(self.shot_name, self.status_name)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, f"status changed to {self.status_name}")
        return{'FINISHED'}

class CEREBRO_pack_links(bpy.types.Operator):
    bl_idname = "cerebro.pack_links"
    bl_label = "Pack Links"

    action: bpy.props.StringProperty()

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        # (selection)
        if self.action=="selected":
            b,r=working.append_selected_link(context, context.object)
            if not b:
                self.report({'WARNING'}, r)
            else:
                self.report({'INFO'}, r)

        # self.report({'INFO'}, "pack_links")
        return{'FINISHED'}

class CEREBRO_refresh_proxy(bpy.types.Operator):
    bl_idname = "cerebro.refresh_proxy"
    bl_label = "Refresh proxy"

    def execute(self, context):
        b,r=tools.refresh_proxy(context)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)

        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        # wm = context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}


class CEREBRO_download_animatic_to_shot(bpy.types.Operator):
    bl_idname = "cerebro.download_animatic_to_shot"
    bl_label = "Download animatic"

    # version: bpy.props.StringProperty(name='Version')

    def execute(self, context):
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        b,r=working.download_animatic_to_shot(context, task)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)

        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        # wm = context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}


class CEREBRO_playblast(bpy.types.Operator):
    bl_idname = "cerebro.playblast"
    bl_label = "Local playblast"

    def execute(self, context):
        # (1)
        hooks._set_render_setting_for_playblast()
        bpy.ops.render.opengl(animation=True)
        self.report({'INFO'}, "playblast made")
        return{'FINISHED'}


class CEREBRO_playblast_to_review(bpy.types.Operator):
    bl_idname = "cerebro.playblast_to_review"
    bl_label = "Playblast to version"

    to_review: bpy.props.BoolProperty(name="Status to \"на утверждение\"", default=True)
    use_last: bpy.props.BoolProperty(name="Use last playblast", default=False)
    description: bpy.props.StringProperty(name="Description")
    commit: bpy.props.BoolProperty(name="Make commit", default=False)

    def execute(self, context):
        #(0) playblast
        if not self.use_last:
            if os.environ["CEREBRO_B3D_CURRENT_ASSET_TYPE"] in settings.MONTAGE_OBJECT_TYPES:
                hooks._set_render_setting_for_playblast()
            else:
                hooks._set_render_setting_for_playblast(fstart='ENV', fend='ENV')
            bpy.ops.render.opengl(animation=True)
        #(1)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        #(2) new status
        if self.to_review:
            status_name="на утверждение"
        else:
            status_name="в работе"
        #(3) to review
        path=bpy.path.abspath(bpy.context.scene.render.filepath)
        if not os.path.exists(path):
            self.report({'WARNING'}, "Video file not found!")
            return{'FINISHED'}
        b,v=working.playblast_to_review(task, self.description, path, status_name)
        if not b:
            self.report({'WARNING'}, v)
            return{'FINISHED'}
        #(4) change status
        # if task['status']['name'] != status_name:
        b,r=srvconn.task_status_to(task, status_name)
        if not b:
            self.report({'WARNING'}, r)
            return{'FINISHED'}
        self.report({'INFO'}, f"Playblast to version: {v}")
        if self.commit:
            bpy.ops.cerebro.commit(description=self.description, to_review=self.to_review)
        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        # wm = context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}


class CEREBRO_download_scene_to_animatic(bpy.types.Operator):
    bl_idname = "cerebro.download_scene_to_animatic"
    bl_label = "Download scene"

    def execute(self, context):
        return{'FINISHED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        # col.label(text="Выбрать сцену:")
        for s in self.scenes:
            row=col.row()
            row.label(text=s.name)
            row.operator("cerebro.download_scene_to_animatic_action").source_scene=s.name

    def invoke(self, context, event):
        self.scenes=list()
        for s in bpy.data.scenes:
            if s==context.scene:
                continue
            self.scenes.append(s)
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}


class CEREBRO_download_scene_to_animatic_action(bpy.types.Operator):
    bl_idname = "cerebro.download_scene_to_animatic_action"
    bl_label = "Download Scene"

    source_scene: bpy.props.StringProperty(default='')

    def execute(self, context):
        b,r=working.download_scene_to_animatic(context, bpy.data.scenes[self.source_scene])
        self.report({'INFO'}, self.source_scene)
        return {'FINISHED'}


class CEREBRO_download_animatic(bpy.types.Operator):
    bl_idname = "cerebro.download_animatic"
    bl_label = "Download animatic"

    filepath: bpy.props.StringProperty(subtype="FILE_PATH")

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        b,r=working.download_animatic(context, task, self.filepath)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}

    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}


class CEREBRO_rename_animatic_markers(bpy.types.Operator):
    bl_idname = "cerebro.rename_animatic_markers"
    bl_label = "Rename markers"

    action: bpy.props.StringProperty()

    def execute(self, context):
        # (1)
        if self.action=='sequence':
            b,r=working.rename_sequences_markers(context)
        elif self.action=='sequence2':
            b,r=working.rename_sequences_markers(context, method=2)
        elif self.action=='shot':
            b,r=working.rename_shots_markers(context)
        else:
            self.report({'WARNING'}, 'Unknown action!')
            return{'FINISHED'}
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}


class CEREBRO_create_shots_from_markers(bpy.types.Operator):
    bl_idname = "cerebro.create_shots_from_markers"
    bl_label = "Create Shots from markers"

    method: bpy.props.StringProperty(name="Method", default="method 1")
    step: bpy.props.IntProperty(name='Num of shots', default=10)
    template: bpy.props.EnumProperty(items=fill_task_templates, name = 'Task templates', default=None, update = None)

    def execute(self, context):
        # (1)
        if self.method=="method 1":
            b,r=working.create_shots_from_markers(context, step=self.step, template=self.template)
        else:
            #(method 2)
            b,r=working.create_shots_from_markers(context, step=self.step, template=self.template, method=2)
        if not b:
            self.report({'WARNING'}, f"create_shots_from_markers: \"{r}\"")
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}

    def invoke(self, context, event):
        get_task_templates()
        # wm = context.window_manager
        # return wm.invoke_props_dialog(self)
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}

    # def draw(self, context):
    #     layout = self.layout
    #     col = layout.column()
    #     col.label(text="List of versions:")


class CEREBRO_re_create_selected_shots(bpy.types.Operator):
    bl_idname = "cerebro.re_create_selected_shots"
    bl_label = "Re-create selected Animatic"

    def execute(self, context):
        # (1)
        b,r=working.re_create_selected_shots(context)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, str(r))
        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}


class CEREBRO_download_shot_animatic_to_episode(bpy.types.Operator):
    bl_idname = "cerebro.download_shot_animatic_to_episode"
    bl_label = "Refresh selected Animatic"

    def execute(self, context):
        # (1)
        b,r=working.download_shot_animatic_to_episode(context)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, str(r))
        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}


class CEREBRO_download_review(bpy.types.Operator):
    bl_idname = "cerebro.download_review"
    bl_label = "Download last Review"

    def execute(self, context):
        # (1)
        b,r=working.download_review(context)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, str(r))
        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}


class CEREBRO_select_shot_sequences_by_task_status(bpy.types.Operator):
    bl_idname = "cerebro.select_shot_sequences_by_task_status"
    bl_label = "Select shot sequences by task status"

    # status: bpy.props.StringProperty(name="Task status", default="Pending Review")
    statuses=[
        ("в работе",)*3,
        ("на утверждение",)*3,
        ("на переработку",)*3,
        ("выполнена",)*3,
    ]
    status: bpy.props.EnumProperty(name = 'Task status', items = statuses)

    def execute(self, context):
        # (1)
        b,r=working.select_shot_sequences_by_task_status(context, self.status)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, f"Selected shot sequences by status {self.status}")
        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}


class CEREBRO_light_local_save_collections(bpy.types.Operator):
    bl_idname = "cerebro.light_local_save_collections"
    bl_label = "Save to local text file"

    def execute(self, context):
        # (1)
        b,r=lcm.save_collections_children(lcm.COLLECTIONS)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}

    def invoke(self, context, event):
        get_task_templates()
        # wm = context.window_manager
        # return wm.invoke_props_dialog(self)
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}


class CEREBRO_light_local_set_from_file_collections(bpy.types.Operator):
    bl_idname = "cerebro.light_local_set_from_file_collections"
    bl_label = "Set from local text file"

    def execute(self, context):
        # (1)
        b,r=lcm.set_collections_children(context)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}


class CEREBRO_light_set_from_file(bpy.types.Operator):
    bl_idname = "cerebro.light_set_from_file_collections"
    bl_label = "Set from local text file"

    action: bpy.props.StringProperty()

    def execute(self, context):
        # (1)
        if self.action=='local':
            b,r=lcm.set_collections_children(context)
            if not b:
                self.report({'WARNING'}, r)
            else:
                self.report({'INFO'}, "Ok!")
        else:
            b,r=srvconn.get_downloadable_incoming_links(for_download=True)
            if not b:
                self.report({'WARNING'}, r)
            else:
                components=r[0]
                for item in components:
                    # if item[0]["parent"]["type"]["name"]=="Location":
                    activity_name=srvconn.get_asset_type_of_task(item[0])
                    if activity_name=="Location":
                        b,r=lcm.set_collections_children(context, incoming_task=item[0])
                        if not b:
                            self.report({'WARNING'}, r)
                        else:
                            self.report({'INFO'}, f'Set data from - {activity_name} - {srvconn.parent_name(item[0])} - {item[4]}')
                self.report({'INFO'}, "Ok!")
        return{'FINISHED'}


class CEREBRO_light_select_objects_of_collections(bpy.types.Operator):
    bl_idname = "cerebro.light_select_objects_of_collections"
    bl_label = "Select objects"

    collection: bpy.props.StringProperty()

    def execute(self, context):
        # (1)
        if self.collection=="No":
            lcm.select_no_collections_objects()
        else:
            lcm.select_objects_of_collection(self.collection)
        # if not b:
        #     self.report({'WARNING'}, r)
        # else:
        #     self.report({'INFO'}, r)
        return{'FINISHED'}


class CEREBRO_light_add_selected_objects_to_collections(bpy.types.Operator):
    bl_idname = "cerebro.light_add_selected_objects_to_collections"
    bl_label = "Add selected"

    collection: bpy.props.StringProperty()

    def execute(self, context):
        # (1)
        b,r=lcm.add_selected_objects_to_collection(context, self.collection)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}


class CEREBRO_light_remove_selected_objects_from_collections(bpy.types.Operator):
    bl_idname = "cerebro.light_remove_selected_objects_from_collections"
    bl_label = "Remove selected"

    collection: bpy.props.StringProperty()

    def execute(self, context):
        # (1)
        b,r=lcm.remove_selected_objects_from_collection(context, self.collection)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}


class CEREBRO_light_set_from_incoming_task(bpy.types.Operator):
    bl_idname = "cerebro.light_set_from_incoming_task"
    bl_label = "Set from incoming task"

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        b,r=srvconn.get_incoming_tasks(task)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}


class CEREBRO_open_last_version_by_webbrowser(bpy.types.Operator):
    bl_idname = "cerebro.open_last_version_by_webbrowser"
    bl_label = "Open by WEB"

    # prefix: bpy.props.StringProperty(default="Review")
    shot_name: bpy.props.StringProperty(default="")
    task_id: bpy.props.StringProperty(default="")

    def execute(self, context):
        # (1)
        if self.shot_name:
            b,r=working.open_last_version_by_webbrowser(asset_name=self.shot_name)
        elif self.task_id:
            b,r=working.open_last_version_by_webbrowser(task=self.task_id)
        else:
            b,r=working.open_last_version_by_webbrowser()
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}


class CEREBRO_timing_from_selected_shots(bpy.types.Operator):
    bl_idname = "cerebro.timing_from_selected_shots"
    bl_label = "Timing from selected shots"

    def execute(self, context):
        # (1)
        b,r=working.timing_from_selected_shots(context)

        self.report({'INFO'}, "Ok!")
        return{'FINISHED'}


class CEREBRO_recovery(bpy.types.Operator):
    bl_idname = "cerebro.recovery"
    bl_label = "Recovery"

    path: bpy.props.StringProperty()
    limit: bpy.props.IntProperty(default=0)

    def execute(self, context):
        # (1)
        b,r=working.recovery(self.path, self.limit)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}


class CEREBRO_recovery_assigned(bpy.types.Operator):
    bl_idname = "cerebro.recovery_assign"
    bl_label = "Assign"

    path: bpy.props.StringProperty()
    
    def execute(self, context):
        # (1)
        b,r=working.recovery_assign(path=self.path)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}


class CEREBRO_recovery_links(bpy.types.Operator):
    bl_idname = "cerebro.recovery_links"
    bl_label = "Links"

    path: bpy.props.StringProperty()
    
    def execute(self, context):
        # (1)
        b,r=working.recovery_links(path=self.path)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}


class CEREBRO_recover_paths(bpy.types.Operator):
    bl_idname = "cerebro.recover_paths"
    bl_label = "Recover paths"

    filepath: bpy.props.StringProperty(subtype="FILE_PATH")

    def execute(self, context):
        # (1)
        b,r=working.recover_paths(self.filepath)
        self.report({'INFO'}, "Ok")
        return{'FINISHED'}

    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}


class CEREBRO_recover_save_position_of_linked_objects(bpy.types.Operator):
    bl_idname = "cerebro.recover_save_position_of_linked_objects"
    bl_label = "Save position of linked objects"

    def execute(self, context):
        # (1)
        b,r=rl.save_data(context)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}


class CEREBRO_recover_set_position_of_linked_objects(bpy.types.Operator):
    bl_idname = "cerebro.recover_set_position_of_linked_objects"
    bl_label = "Set position"

    def execute(self, context):
        # (1)
        b,r=rl.set_data(context)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}


class CEREBRO_reload_tasks_list(bpy.types.Operator):
    bl_idname = "cerebro.reload_tasks_list"
    bl_label = "Reload"

    def execute(self, context):
        # (1)
        fill_tasks_lists(self, context)
        self.report({'INFO'}, "template_operator")
        return{'FINISHED'}


class CEREBRO_assets_migrate(bpy.types.Operator):
    """
    bpy.ops.cerebro.assets_migrate('INVOKE_DEFAULT')
    """
    bl_idname = "cerebro.assets_migrate"
    bl_label = "Миграция ассетов."

    def execute(self, context):
        # (1)
        b,r=working.assets_migrate(context.scene.source_folder, context.scene.target_folder)
        self.report({'INFO'}, "assets_migrate")
        return{'FINISHED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        box=col.box()
        box.label(text=f"Source:")
        c=box.column(align=True)
        c.prop(context.scene, "ftrack_projects")
        c.prop(context.scene, "source_folder")
        box=col.box()
        box.label(text=f"Target:")
        c=box.column(align=True)
        # c.prop(context.scene, "target_project")
        c.prop(context.scene, "target_folder")


    def invoke(self, context, event):
        # bpy.types.Scene.source_project = bpy.props.EnumProperty(items=fill_projects, name = 'Project', default=None, update = None)
        # bpy.types.Scene.target_project = bpy.props.EnumProperty(items=fill_projects, name = 'Project', default=None, update = None)
        bpy.types.Scene.source_folder = bpy.props.StringProperty(name = 'Folder (id)', default='', update = None)
        bpy.types.Scene.target_folder = bpy.props.StringProperty(name = 'Folder (id)', default='', update = None)
        wm = context.window_manager
        return wm.invoke_props_dialog(self)


def commit_zip_execute(self, context):
    logger.debug("commit_zip_execute")
    bpy.ops.cerebro.commit_zip()


class CEREBRO_commit_zip(bpy.types.Operator):
    """
    bpy.ops.cerebro.commit_zip('INVOKE_DEFAULT')
    """
    bl_idname="cerebro.commit_zip"
    bl_label="Commit task_data.zip"
    # bl_options = {'REGISTER','UNDO'}

    def execute(self, context):
        # (1) tests
        path=bpy.path.abspath(context.scene.task_data_path)
        if not context.scene.description_to_commit_zip:
            self.report({"WARNING"}, "Нужен Description!")
            bpy.ops.cerebro.message('INVOKE_DEFAULT', message="[WARNING]: Нужен Description!")
            return{'FINISHED'}
        if not os.path.exists(path):
            self.report({"WARNING"}, f"Файл не выбран! {path}")
            bpy.ops.cerebro.message('INVOKE_DEFAULT', message=f"[WARNING]: Файл не выбран! {path}")
            return{'FINISHED'}
        if not os.path.basename(path)==f"{srvconn.TASK_DATA_COMPONENT}{srvconn.TASK_DATA_EXT}":
            self.report({"WARNING"}, f"Выбран не {srvconn.TASK_DATA_COMPONENT}{srvconn.TASK_DATA_EXT}!")
            bpy.ops.cerebro.message('INVOKE_DEFAULT', message=f"Выбран не {srvconn.TASK_DATA_COMPONENT}{srvconn.TASK_DATA_EXT}!")
            return{'FINISHED'}

        # (2) upload
        task=G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        b,r=srvconn.push_task_data_to_cerebro(task, path, context.scene.description_to_commit_zip)
        if not b:
            self.report({'WARNING'}, r)
            bpy.ops.cerebro.message('INVOKE_DEFAULT', message=f"[WARNING]: {r}")
            return{'FINISHED'}

        # (fin)
        m=f"commit: {path} descrittion: {context.scene.description_to_commit_zip}"
        self.report({'INFO'}, m)
        bpy.ops.cerebro.message('INVOKE_DEFAULT', message=m)
        return{'FINISHED'}

    def draw(self, context):
        layout=self.layout
        col=layout.column()
        col.prop(context.scene, "description_to_commit_zip")
        col.prop(context.scene, "task_data_path")

    def invoke(self, context, event):
        bpy.types.Scene.description_to_commit_zip = bpy.props.StringProperty(name='Description', default='', update=None)
        bpy.types.Scene.task_data_path=bpy.props.StringProperty(name='task_data.zip', subtype='FILE_PATH', update=commit_zip_execute)
        wm=context.window_manager
        return wm.invoke_props_dialog(self)


class CEREBRO_message(bpy.types.Operator):
    bl_idname = "cerebro.message"
    bl_label = "Message"

    message: bpy.props.StringProperty(default="None")

    def execute(self, context):
        return{'FINISHED'}

    def draw(self, context):
        layout=self.layout
        col=layout.column()
        for item in self.message.split("|"):
            col.label(text=item)

    def invoke(self, context, event):
        wm=context.window_manager
        return wm.invoke_props_dialog(self)


class CEREBRO_mass_links(bpy.types.Operator):
    """
    bpy.ops.cerebro.mass_links("INVOKE_DEFAULT")
    """
    bl_idname = "cerebro.mass_links"
    bl_label = "Mass Links"

    def execute(self, context):
        # (1)
        out_id=int(context.scene.mass_links_output_id)
        in_id=int(context.scene.mass_links_input_id)
        
        b,r=working.mass_links(out_id, in_id)
        if not b:
            bpy.ops.cerebro.message('INVOKE_DEFAULT', message=f"WARNING! {r}")
            self.report({'WARNING'}, f"{r}")
        else:
            bpy.ops.cerebro.message('INVOKE_DEFAULT', message=f"connected: {out_id}->{in_id}")
        
        self.report({'INFO'}, "mass_links end")
        return{'FINISHED'}

    def draw(self, context):
        layout=self.layout
        col=layout.column()
        col.prop(context.scene, "mass_links_output_id")
        col.prop(context.scene, "mass_links_input_id")

    def invoke(self, context, event):
        bpy.types.Scene.mass_links_output_id = bpy.props.StringProperty(name='Output task id', default='', update=None)
        bpy.types.Scene.mass_links_input_id = bpy.props.StringProperty(name='Input folder id', default='', update=None)
        wm=context.window_manager
        return wm.invoke_props_dialog(self)


class CEREBRO_remove_mass_links(bpy.types.Operator):
    """
    bpy.ops.cerebro.remove_mass_links("INVOKE_DEFAULT")
    """
    bl_idname = "cerebro.remove_mass_links"
    bl_label = "Remove Mass Links"

    def execute(self, context):
        # (1)
        f_id=int(context.scene.mass_links_folder_id)
        
        b,r=working.remove_mass_links(f_id)
        if not b:
            bpy.ops.cerebro.message('INVOKE_DEFAULT', message=f"WARNING! {r}")
            self.report({'WARNING'}, f"{r}")
        else:
            bpy.ops.cerebro.message('INVOKE_DEFAULT', message=f"connected: {out_id}->{in_id}")
        
        self.report({'INFO'}, "mass_links end")
        return{'FINISHED'}

    def draw(self, context):
        layout=self.layout
        col=layout.column()
        col.prop(context.scene, "mass_links_folder_id")

    def invoke(self, context, event):
        bpy.types.Scene.mass_links_folder_id = bpy.props.StringProperty(name='Folder id', default='', update=None)
        wm=context.window_manager
        return wm.invoke_props_dialog(self)


class CEREBRO_remove_incoming_links_from_this_task_panel(bpy.types.Operator):
    """
    bpy.ops.cerebro.remove_incoming_links_from_this_task_panel("INVOKE_DEFAULT")
    bpy.ops.cerebro.remove_incoming_links_from_this_task_panel("INVOKE_DEFAULT", mode="asset")
    """
    bl_idname = "cerebro.remove_incoming_links_from_this_task_panel"
    bl_label = "Incoming links of this task"

    mode: bpy.props.StringProperty(default="task")

    def execute(self, context):
        # (1)
                
        self.report({'INFO'}, "Ok!")
        return{'FINISHED'}

    def draw(self, context):
        layout=self.layout
        col=layout.column()
        col.label(text="Incoming tasks:")
        col.prop(context.scene, "ftrack_filter_source")
        for lid,task in G.direct_incoming_tasks.items():
            if not tools.search(context.scene.ftrack_filter_source, task[dbtypes.TASK_DATA_NAME]):
                continue
            print(task[dbtypes.TASK_DATA_NAME])
            row = col.row(align = True)
            row.label(text=task[dbtypes.TASK_DATA_NAME])
            row.operator("cerebro.remove_incoming_links_from_selected_shot_action").lid=str(lid)

    def invoke(self, context, event):
        if self.mode=="mode":
            task=G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        else:
            b,task=srvconn.get_task_by_id(int(os.environ["CEREBRO_B3D_CURRENT_ASSET_ID"]))
            # logger.info(shot)
            if not b:
                self.report({'WARNING'}, task)
                return{'FINISHED'}
        G.direct_incoming_tasks=srvconn.get_direct_incoming_tasks(task, r=False)
        logger.info(G.direct_incoming_tasks)
        wm=context.window_manager
        return wm.invoke_props_dialog(self)


class CEREBRO_remove_incoming_links_from_selected_shot_panel(bpy.types.Operator):
    """
    bpy.ops.cerebro.remove_incoming_links_from_selected_shot_panel("INVOKE_DEFAULT")
    """
    bl_idname = "cerebro.remove_incoming_links_from_selected_shot_panel"
    bl_label = "Incoming links of selected Shot"

    def execute(self, context):
        # (1)
                
        self.report({'INFO'}, "Ok!")
        return{'FINISHED'}

    def draw(self, context):
        layout=self.layout
        col=layout.column()
        col.label(text=f"[{self.shot_name}] Incoming tasks:")
        for lid,task in G.direct_incoming_tasks.items():
            print(task[dbtypes.TASK_DATA_NAME])
            row = col.row(align = True)
            row.label(text=task[dbtypes.TASK_DATA_NAME])
            row.operator("cerebro.remove_incoming_links_from_selected_shot_action").lid=str(lid)

        col.operator("cerebro.add_asset_models_of_project").shot_id=str(self.shot_id)

    def invoke(self, context, event):
        b,shot=working.get_selected_shot(context)
        self.shot_name=shot[dbtypes.TASK_DATA_NAME]
        self.shot_id=shot[dbtypes.TASK_DATA_ID]
        # logger.info(shot)
        if not b:
            self.report({'WARNING'}, shot)
            return{'FINISHED'}
        G.direct_incoming_tasks=srvconn.get_direct_incoming_tasks(shot)
        logger.info(G.direct_incoming_tasks)
        wm=context.window_manager
        return wm.invoke_props_dialog(self)


class CEREBRO_remove_incoming_links_from_selected_shot_action(bpy.types.Operator):
    """
    action
    """
    bl_idname = "cerebro.remove_incoming_links_from_selected_shot_action"
    bl_label = "del"

    lid: bpy.props.StringProperty()

    def execute(self, context):
        # (1)
        lid=int(self.lid)

        b,r=srvconn.drop_link_tasks(lid)
        if b:
            G.direct_incoming_tasks.pop(lid)
            self.report({'INFO'}, f"Remove {lid}")
        else:
            self.report({'WARNING'}, f"{r}")
        return{'FINISHED'}

    def invoke(self, context, event):
        wm=context.window_manager
        return wm.invoke_props_dialog(self)


class CEREBRO_copy_incoming_links(bpy.types.Operator):
    """
    bpy.ops.cerebro.copy_incoming_links("INVOKE_DEFAULT")
    """
    bl_idname = "cerebro.copy_incoming_links"
    bl_label = "Copy Incoming Links"

    def execute(self, context):
        # (1)
        source_id=int(context.scene.copy_incoming_source_id)
        target_id=int(context.scene.copy_incoming_target_id)
        
        b,r=working.copy_incoming_links(source_id, target_id)
        if not b:
            bpy.ops.cerebro.message('INVOKE_DEFAULT', message=f"WARNING! {r}")
            self.report({'WARNING'}, f"{r}")
        else:
            bpy.ops.cerebro.message('INVOKE_DEFAULT', message=f"Copy links: from {source_id} to {target_id}")
        
        self.report({'INFO'}, "Copy links end")
        return{'FINISHED'}

    def draw(self, context):
        layout=self.layout
        col=layout.column()
        col.prop(context.scene, "copy_incoming_source_id")
        col.prop(context.scene, "copy_incoming_target_id")

    def invoke(self, context, event):
        bpy.types.Scene.copy_incoming_source_id = bpy.props.StringProperty(name='Source Task ID', default='', update=None)
        bpy.types.Scene.copy_incoming_target_id = bpy.props.StringProperty(name='Target Task ID', default='', update=None)
        wm=context.window_manager
        return wm.invoke_props_dialog(self)


class CEREBRO_render_evee(bpy.types.Operator):
    bl_idname = "cerebro.render_evee"
    bl_label = "Render Eevee"

    action: bpy.props.StringProperty()

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        if self.action=="TEST_RENDER":
            render_tools.render_evee(context, task, test=True)
        elif self.action=="FINAL_RENDER":
            render_tools.render_evee(context, task, test=False)
        elif self.action=="CLEAN_FOLDER":
            render_tools.clean_render_folder(task)
            self.report({'INFO'}, "Folder Cleaned")
            return{'FINISHED'}
        elif self.action=="SETTINGS":
            render_tools.settings(context, task)
            self.report({'INFO'}, "The render settings are set")
            return{'FINISHED'}
        
        #(fin)
        self.report({'INFO'}, "Render Eevee")
        return{'FINISHED'}


class CEREBRO_push_render(bpy.types.Operator):
    bl_idname = "cerebro.push_render"
    bl_label = "Push render"

    to_review: bpy.props.BoolProperty(name="Status to \"на утверждение\"", default=True)
    description: bpy.props.StringProperty()
    commit: bpy.props.BoolProperty(name="Make commit", default=True)

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]

        # #(2) change status
        # if self.to_review:
        #     b,r=srvconn.task_status_to(task, "на утверждение")
        #     if not b:
        #         self.report({'WARNING'}, r)
        #         return{'FINISHED'}

        #(3) push render
        srvconn._get_future_render_commit_num(task)
        b,r=working.push_render(context, task, self.description)
        if not b:
            self.report({'WARNING'}, r)
            return{'FINISHED'}
        else:
            self.report({'INFO'}, r)

        #(4) commit
        # if self.commit:
        #     bpy.ops.cerebro.commit(description=self.description, to_review=self.to_review)
        
        self.report({'INFO'}, "Push render")
        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}


class CEREBRO_set_shot_timing(bpy.types.Operator):
    bl_idname = "cerebro.set_shot_timing"
    bl_label = "Set Timing of shot (to cerebro)"

    def execute(self, context):
        # (1)
        working.set_shot_timing(context)
        self.report({'INFO'}, f"Тайминг шота {os.environ['CEREBRO_B3D_CURRENT_ASSET_NAME']} записан в Cerebro.")
        return{'FINISHED'}


class CEREBRO_set_shot_clipping(bpy.types.Operator):
    bl_idname = "cerebro.set_shot_clipping"
    bl_label = "Save clipping of Shot Camera (to cerebro)"

    def execute(self, context):
        # (1)
        working.set_shot_clipping(context)
        self.report({'INFO'}, f"\"clip_start\", \"clip_end\" шота {os.environ['CEREBRO_B3D_CURRENT_ASSET_NAME']} записаны в Cerebro.")
        return{'FINISHED'}


class CEREBRO_add_asset_models_of_project(bpy.types.Operator):
    bl_idname = "cerebro.add_asset_models_of_project"
    bl_label = "Add Assets"

    shot_id: bpy.props.StringProperty()

    def execute(self, context):
        # (1)
        # b,r=srvconn.add_asset_models_of_project()
        # if b:
        #     self.report({'INFO'}, self.shot_id)
        # else:
        #     self.report({'WARNING'}, r)
        return{'FINISHED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column(align = True)
        col.prop(context.scene, "cerebro_assets_list_filter")
        for t in self.tasks:
            if context.scene.cerebro_assets_list_filter and not tools.search(context.scene.cerebro_assets_list_filter, t[dbtypes.TASK_DATA_NAME]):
                continue
            row=col.row(align=True)
            row.label(text=t[dbtypes.TASK_DATA_NAME])
            b=row.operator("cerebro.add_asset_models_of_project_action")
            b.shot_id=self.shot_id
            b.asset_id=str(t[dbtypes.TASK_DATA_ID])

    def invoke(self, context, event):
        bpy.types.Scene.cerebro_assets_list_filter=bpy.props.StringProperty(name="filter")
        b,r=srvconn.add_asset_models_of_project()
        if b:
            self.tasks=r
            for t in r:
                G.asset_models_of_project[str(t[dbtypes.TASK_DATA_ID])]=t
        else:
            self.tasks=[]
            G.asset_models_of_project=dict()
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}


class CEREBRO_add_asset_models_of_project_action(bpy.types.Operator):
    bl_idname = "cerebro.add_asset_models_of_project_action"
    bl_label = "Add"

    shot_id: bpy.props.StringProperty()
    asset_id: bpy.props.StringProperty()

    def execute(self, context):
        try:
            lid=srvconn.set_link_tasks(int(self.asset_id), int(self.shot_id))
            G.direct_incoming_tasks[lid]=G.asset_models_of_project[self.asset_id]
        except:
            print(f'{traceback.format_exc()}')
            self.report({'WARNING'}, "Look the terminal!")
        return{'FINISHED'}
        

class CEREBRO_animatic_export_shots_data_to_csv(bpy.types.Operator):
    bl_idname = "cerebro.animatic_export_shots_data_to_csv"
    bl_label = "Export shots timing to .csv (frame)"

    timeformat: bpy.props.StringProperty(default="frame")

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        b,r=working.animatic_export_shots_data_to_csv(context, task, timeformat=self.timeformat)
        if b:
            self.report({'INFO'}, r)
        else:
            self.report({'WARNING'}, r)
        return{'FINISHED'}
        return{'FINISHED'}
        
        
class CEREBRO_link_world_from_location(bpy.types.Operator):
    """Линкует активный World из локации.  """
    bl_idname = "cerebro.link_world_from_location"
    bl_label = "Link World from Location"

    def draw(self, context):
        layout = self.layout
        col = layout.column(align = True)
        for id,t in  G.direct_incoming_tasks.items():
            if t[dbtypes.TASK_DATA_ACTIVITY_NAME]=="Location":
                # logger.info(id)
                # logger.info(t[dbtypes.TASK_DATA_ID])
                row=col.row(align=True)
                row.label(text=f"{t[dbtypes.TASK_DATA_ACTIVITY_NAME]} - {t[dbtypes.TASK_DATA_NAME]}")
                row.operator("cerebro.link_world_from_location_action").link_id=str(id)

    def execute(self, context):
        # (1)
        self.report({'INFO'}, "Ok!")
        return{'FINISHED'}

    def invoke(self, context, event):
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        p=srvconn.get_parent_task(task)
        G.direct_incoming_tasks=srvconn.get_direct_incoming_tasks(p)
        # logger.info(G.direct_incoming_tasks)
        wm=context.window_manager
        return wm.invoke_props_dialog(self)
        
        
class CEREBRO_link_world_from_location_action(bpy.types.Operator):
    bl_idname = "cerebro.link_world_from_location_action"
    bl_label = "Link World"

    link_id: bpy.props.StringProperty()

    def execute(self, context):
        # (1)
        location=G.direct_incoming_tasks[int(self.link_id)]
        # logger.info(location)
        b,r=working.link_worlds_from_location(context, location)
        if b:
            self.report({'INFO'}, "Ok!")
        else:
            self.report({'WARNING'}, f"{r}")
        
        return{'FINISHED'}
        
        
class CEREBRO_download_animation_from_incoming(bpy.types.Operator):
    bl_idname = "cerebro.download_animation_from_incoming"
    bl_label = "Import animation from incoming task"

    dialog: bpy.props.BoolProperty(default=False)
    task_id: bpy.props.StringProperty(default='')
    
    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]

        # (2)
        row=G.incoming_tasks.get(self.task_id)
        if row:
            incoming_task=row['task']
        else:
            self.report({'INFO'}, "No incoming task!")
            return{'FINISHED'}
        b,r=working.animation_from_incoming(context, task, incoming_task)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        col.label(text="Incoming tasks:")
        for task_id in G.incoming_tasks:
            row = col.row(align = True)
            row.label(text=G.incoming_tasks[task_id]['task'][dbtypes.TASK_DATA_NAME])
            row.label(text=G.incoming_tasks[task_id]['status'])
            op=row.operator('cerebro.download_animation_from_incoming', text="Import animation")
            op.task_id=task_id
            op.dialog=False

    def invoke(self, context, event):
        if self.dialog:
            b,r=srvconn.get_incoming_tasks(G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]])
            if not b:
                self.report({'WARNING'}, r)
                return{'FINISHED'}
            G.incoming_tasks=r
            wm = context.window_manager
            return wm.invoke_props_dialog(self)
        else:
            return self.execute(context)
        
        
class CEREBRO_test_content(bpy.types.Operator):
    """
    bpy.ops.cerebro.test_content("INVOKE_DEFAULT")
    """
    bl_idname = "cerebro.test_content"
    bl_label = "Test Content"

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        self.report({'INFO'}, "Ok!")
        return{'FINISHED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        col.label(text="Content:")
        for k,item in self.data.items():
            row = col.row(align = True)
            row.label(text=k)
            row.label(text=f"{item[0]}|{item[1]}|{item[2]}")

    def invoke(self, context, event):
        self.data=dict()
        worked=list()
        in_tasks=dict()
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        b,asset=srvconn.get_task_by_id(task[dbtypes.TASK_DATA_PARENT_ID])
        incoming_tasks=srvconn.get_direct_incoming_tasks(asset, r=True)
        # logger.info(incoming_tasks)
        for t in incoming_tasks:
            in_tasks[incoming_tasks[t][dbtypes.TASK_DATA_NAME]]=incoming_tasks[t]
        for l in bpy.data.libraries:
            in_scene=True
            asset_name=os.path.basename(l.filepath).split(".")[0]
            exists_file=os.path.exists(bpy.path.abspath(l.filepath))
            if asset_name in in_tasks:
                worked.append(asset_name)
                link=True
            self.data[asset_name]=(in_scene, link, exists_file)
            logger.info(asset_name)

        for asset_name,t in in_tasks.items():
            if asset_name in worked:
                continue
            in_scene=False
            link=True
            b,path=path_utils.get_top_version_path_of_workfile(t)
            if not b:
                continue
            else:
                exists_file=os.path.exists(path)
            self.data[asset_name]=(in_scene, link, exists_file) 
            logger.info(asset_name)           

        wm = context.window_manager
        return wm.invoke_props_dialog(self)
        
        
### TEMPLATE
class CEREBRO_template_operator(bpy.types.Operator):
    bl_idname = "cerebro.template_operator"
    bl_label = "Text"

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]]
        self.report({'INFO'}, "Template operator!")
        return{'FINISHED'}
        
        
def register():
    # bpy.utils.register_class(CEREBRO_main_panel)
    bpy.utils.register_class(CEREBRO_VIEW_3D_panel)
    bpy.utils.register_class(CEREBRO_SEQUENCE_EDITOR_panel)
    bpy.utils.register_class(CEREBRO_auth_start)
    bpy.utils.register_class(CEREBRO_setting_start)
    bpy.utils.register_class(CEREBRO_authorization)
    bpy.utils.register_class(CEREBRO_update)
    bpy.utils.register_class(CEREBRO_manual)
    bpy.utils.register_class(CEREBRO_api)
    bpy.utils.register_class(CEREBRO_set_folder)
    bpy.utils.register_class(CEREBRO_select_task)
    bpy.utils.register_class(CEREBRO_current_scene_to_work)
    bpy.utils.register_class(CEREBRO_open_scene_from_incoming_task)
    bpy.utils.register_class(CEREBRO_switch_panels)
    bpy.utils.register_class(CEREBRO_open_work_top_version)
    bpy.utils.register_class(CEREBRO_open_work_version)
    bpy.utils.register_class(CEREBRO_commit)
    bpy.utils.register_class(CEREBRO_download_version)
    bpy.utils.register_class(CEREBRO_update_incoming)
    bpy.utils.register_class(CEREBRO_download_incoming)
    bpy.utils.register_class(CEREBRO_collecting_textures)
    bpy.utils.register_class(CEREBRO_sources_panel)
    bpy.utils.register_class(CEREBRO_sources_panel_action)
    bpy.utils.register_class(CEREBRO_link_of_source)
    bpy.utils.register_class(CEREBRO_link)
    bpy.utils.register_class(CEREBRO_add_graphics_editors)
    bpy.utils.register_class(CEREBRO_del_graphics_editors)
    bpy.utils.register_class(CEREBRO_open_graphics_editor)
    bpy.utils.register_class(CEREBRO_open_file_browser)
    bpy.utils.register_class(CEREBRO_open_file_browser_by_id)
    bpy.utils.register_class(CEREBRO_reload_incoming_version)
    bpy.utils.register_class(CEREBRO_reload_incoming_version_action)
    bpy.utils.register_class(CEREBRO_select_object)
    bpy.utils.register_class(CEREBRO_check_scene)
    bpy.utils.register_class(CEREBRO_change_status)
    bpy.utils.register_class(CEREBRO_change_status_of_selected_shot)
    bpy.utils.register_class(CEREBRO_pack_links)
    bpy.utils.register_class(CEREBRO_playblast)
    bpy.utils.register_class(CEREBRO_playblast_to_review)
    bpy.utils.register_class(CEREBRO_refresh_proxy)
    bpy.utils.register_class(CEREBRO_download_animatic)
    bpy.utils.register_class(CEREBRO_download_animatic_to_shot)
    bpy.utils.register_class(CEREBRO_rename_animatic_markers)
    bpy.utils.register_class(CEREBRO_create_shots_from_markers)
    bpy.utils.register_class(CEREBRO_re_create_selected_shots)
    bpy.utils.register_class(CEREBRO_download_shot_animatic_to_episode)
    bpy.utils.register_class(CEREBRO_download_review)
    bpy.utils.register_class(CEREBRO_select_shot_sequences_by_task_status)
    bpy.utils.register_class(CEREBRO_light_panel)
    bpy.utils.register_class(CEREBRO_light_local_save_collections)
    bpy.utils.register_class(CEREBRO_light_local_set_from_file_collections)
    bpy.utils.register_class(CEREBRO_light_select_objects_of_collections)
    bpy.utils.register_class(CEREBRO_light_add_selected_objects_to_collections)
    bpy.utils.register_class(CEREBRO_light_remove_selected_objects_from_collections)
    bpy.utils.register_class(CEREBRO_light_set_from_incoming_task)
    bpy.utils.register_class(CEREBRO_open_last_version_by_webbrowser)
    bpy.utils.register_class(CEREBRO_timing_from_selected_shots)
    bpy.utils.register_class(CEREBRO_recovery)
    bpy.utils.register_class(CEREBRO_recovery_assigned)
    bpy.utils.register_class(CEREBRO_recovery_links)
    bpy.utils.register_class(CEREBRO_recover_paths)
    bpy.utils.register_class(CEREBRO_reload_tasks_list)
    bpy.utils.register_class(CEREBRO_recover_save_position_of_linked_objects)
    bpy.utils.register_class(CEREBRO_recover_set_position_of_linked_objects)
    bpy.utils.register_class(CEREBRO_light_set_from_file)
    bpy.utils.register_class(CEREBRO_assets_migrate)
    bpy.utils.register_class(CEREBRO_commit_zip)
    bpy.utils.register_class(CEREBRO_message)
    bpy.utils.register_class(CEREBRO_mass_links)
    bpy.utils.register_class(CEREBRO_build_scene)
    bpy.utils.register_class(CEREBRO_build_scene_from_version)
    bpy.utils.register_class(CEREBRO_render_evee)
    bpy.utils.register_class(CEREBRO_push_render)
    bpy.utils.register_class(CEREBRO_copy_incoming_links)
    bpy.utils.register_class(CEREBRO_remove_incoming_links_from_selected_shot_panel)
    bpy.utils.register_class(CEREBRO_remove_incoming_links_from_this_task_panel)
    bpy.utils.register_class(CEREBRO_remove_mass_links)
    bpy.utils.register_class(CEREBRO_remove_incoming_links_from_selected_shot_action)
    bpy.utils.register_class(CEREBRO_download_scene_to_animatic)
    bpy.utils.register_class(CEREBRO_download_scene_to_animatic_action)
    bpy.utils.register_class(CEREBRO_set_shot_timing)
    bpy.utils.register_class(CEREBRO_add_asset_models_of_project)
    bpy.utils.register_class(CEREBRO_add_asset_models_of_project_action)
    bpy.utils.register_class(operators.CEREBRO_backup_panel)
    bpy.utils.register_class(operators.CEREBRO_log_backup_db)
    bpy.utils.register_class(CEREBRO_set_shot_clipping)
    bpy.utils.register_class(CEREBRO_props_info)
    bpy.utils.register_class(CEREBRO_animatic_export_shots_data_to_csv)
    bpy.utils.register_class(to_outsource.CEREBRO_upload_shots_content)
    bpy.utils.register_class(to_outsource.CEREBRO_to_outsource_panel)
    bpy.utils.register_class(to_outsource.CEREBRO_upload_shots_choise_content)
    bpy.utils.register_class(to_outsource.CEREBRO_upload_shots_choise_content_action)
    bpy.utils.register_class(to_outsource.CEREBRO_upload_current_shot_to_outsource)
    bpy.utils.register_class(to_outsource.CEREBRO_upload_current_shot_to_outsource_action)
    bpy.utils.register_class(CEREBRO_link_world_from_location)
    bpy.utils.register_class(CEREBRO_link_world_from_location_action)
    bpy.utils.register_class(CEREBRO_download_animation_from_incoming)
    bpy.utils.register_class(tools.B3DTOOLS_child_of_on_off)
    bpy.utils.register_class(render_tools.CEREBRO_objects_to_render_collection)
    bpy.utils.register_class(CEREBRO_test_content)
    set_params()

def unregister():
    # bpy.utils.unregister_class(CEREBRO_main_panel)
    bpy.utils.unregister_class(CEREBRO_VIEW_3D_panel)
    bpy.utils.unregister_class(CEREBRO_SEQUENCE_EDITOR_panel)
    bpy.utils.unregister_class(CEREBRO_auth_start)
    bpy.utils.unregister_class(CEREBRO_setting_start)
    bpy.utils.unregister_class(CEREBRO_authorization)
    bpy.utils.unregister_class(CEREBRO_update)
    bpy.utils.unregister_class(CEREBRO_manual)
    bpy.utils.unregister_class(CEREBRO_api)
    bpy.utils.unregister_class(CEREBRO_set_folder)
    bpy.utils.unregister_class(CEREBRO_select_task)
    bpy.utils.unregister_class(CEREBRO_current_scene_to_work)
    bpy.utils.unregister_class(CEREBRO_open_scene_from_incoming_task)
    bpy.utils.unregister_class(CEREBRO_switch_panels)
    bpy.utils.unregister_class(CEREBRO_open_work_top_version)
    bpy.utils.unregister_class(CEREBRO_open_work_version)
    bpy.utils.unregister_class(CEREBRO_commit)
    bpy.utils.unregister_class(CEREBRO_download_version)
    bpy.utils.unregister_class(CEREBRO_update_incoming)
    bpy.utils.unregister_class(CEREBRO_download_incoming)
    bpy.utils.unregister_class(CEREBRO_collecting_textures)
    bpy.utils.unregister_class(CEREBRO_sources_panel)
    bpy.utils.unregister_class(CEREBRO_sources_panel_action)
    bpy.utils.unregister_class(CEREBRO_link_of_source)
    bpy.utils.unregister_class(CEREBRO_link)
    bpy.utils.unregister_class(CEREBRO_add_graphics_editors)
    bpy.utils.unregister_class(CEREBRO_del_graphics_editors)
    bpy.utils.unregister_class(CEREBRO_open_graphics_editor)
    bpy.utils.unregister_class(CEREBRO_open_file_browser)
    bpy.utils.unregister_class(CEREBRO_open_file_browser_by_id)
    bpy.utils.unregister_class(CEREBRO_reload_incoming_version)
    bpy.utils.unregister_class(CEREBRO_reload_incoming_version_action)
    bpy.utils.unregister_class(CEREBRO_select_object)
    bpy.utils.unregister_class(CEREBRO_check_scene)
    bpy.utils.unregister_class(CEREBRO_change_status)
    bpy.utils.unregister_class(CEREBRO_change_status_of_selected_shot)
    bpy.utils.unregister_class(CEREBRO_pack_links)
    bpy.utils.unregister_class(CEREBRO_playblast)
    bpy.utils.unregister_class(CEREBRO_playblast_to_review)
    bpy.utils.unregister_class(CEREBRO_refresh_proxy)
    bpy.utils.unregister_class(CEREBRO_download_animatic)
    bpy.utils.unregister_class(CEREBRO_download_animatic_to_shot)
    bpy.utils.unregister_class(CEREBRO_rename_animatic_markers)
    bpy.utils.unregister_class(CEREBRO_create_shots_from_markers)
    bpy.utils.unregister_class(CEREBRO_re_create_selected_shots)
    bpy.utils.unregister_class(CEREBRO_download_shot_animatic_to_episode)
    bpy.utils.unregister_class(CEREBRO_download_review)
    bpy.utils.unregister_class(CEREBRO_select_shot_sequences_by_task_status)
    bpy.utils.unregister_class(CEREBRO_light_panel)
    bpy.utils.unregister_class(CEREBRO_light_local_save_collections)
    bpy.utils.unregister_class(CEREBRO_light_local_set_from_file_collections)
    bpy.utils.unregister_class(CEREBRO_light_select_objects_of_collections)
    bpy.utils.unregister_class(CEREBRO_light_add_selected_objects_to_collections)
    bpy.utils.unregister_class(CEREBRO_light_remove_selected_objects_from_collections)
    bpy.utils.unregister_class(CEREBRO_light_set_from_incoming_task)
    bpy.utils.unregister_class(CEREBRO_open_last_version_by_webbrowser)
    bpy.utils.unregister_class(CEREBRO_timing_from_selected_shots)
    bpy.utils.unregister_class(CEREBRO_recovery)
    bpy.utils.unregister_class(CEREBRO_recovery_assigned)
    bpy.utils.unregister_class(CEREBRO_recovery_links)
    bpy.utils.unregister_class(CEREBRO_recover_paths)
    bpy.utils.unregister_class(CEREBRO_reload_tasks_list)
    bpy.utils.unregister_class(CEREBRO_recover_save_position_of_linked_objects)
    bpy.utils.unregister_class(CEREBRO_recover_set_position_of_linked_objects)
    bpy.utils.unregister_class(CEREBRO_light_set_from_file)
    bpy.utils.unregister_class(CEREBRO_assets_migrate)
    bpy.utils.unregister_class(CEREBRO_commit_zip)
    bpy.utils.unregister_class(CEREBRO_message)
    bpy.utils.unregister_class(CEREBRO_mass_links)
    bpy.utils.unregister_class(CEREBRO_build_scene)
    bpy.utils.unregister_class(CEREBRO_build_scene_from_version)
    bpy.utils.unregister_class(CEREBRO_render_evee)
    bpy.utils.unregister_class(CEREBRO_push_render)
    bpy.utils.unregister_class(CEREBRO_copy_incoming_links)
    bpy.utils.unregister_class(CEREBRO_remove_incoming_links_from_selected_shot_panel)
    bpy.utils.unregister_class(CEREBRO_remove_incoming_links_from_this_task_panel)
    bpy.utils.unregister_class(CEREBRO_remove_mass_links)
    bpy.utils.unregister_class(CEREBRO_remove_incoming_links_from_selected_shot_action)
    bpy.utils.unregister_class(CEREBRO_download_scene_to_animatic)
    bpy.utils.unregister_class(CEREBRO_download_scene_to_animatic_action)
    bpy.utils.unregister_class(CEREBRO_set_shot_timing)
    bpy.utils.unregister_class(CEREBRO_add_asset_models_of_project)
    bpy.utils.unregister_class(CEREBRO_add_asset_models_of_project_action)
    bpy.utils.unregister_class(operators.CEREBRO_backup_panel)
    bpy.utils.unregister_class(operators.CEREBRO_log_backup_db)
    bpy.utils.unregister_class(CEREBRO_set_shot_clipping)
    bpy.utils.unregister_class(CEREBRO_props_info)
    bpy.utils.unregister_class(CEREBRO_animatic_export_shots_data_to_csv)
    bpy.utils.unregister_class(to_outsource.CEREBRO_upload_shots_content)
    bpy.utils.unregister_class(to_outsource.CEREBRO_to_outsource_panel)
    bpy.utils.unregister_class(to_outsource.CEREBRO_upload_shots_choise_content)
    bpy.utils.unregister_class(to_outsource.CEREBRO_upload_shots_choise_content_action)
    bpy.utils.unregister_class(to_outsource.CEREBRO_upload_current_shot_to_outsource)
    bpy.utils.unregister_class(to_outsource.CEREBRO_upload_current_shot_to_outsource_action)
    bpy.utils.unregister_class(CEREBRO_link_world_from_location)
    bpy.utils.unregister_class(CEREBRO_link_world_from_location_action)
    bpy.utils.unregister_class(CEREBRO_download_animation_from_incoming)
    bpy.utils.unregister_class(tools.B3DTOOLS_child_of_on_off)
    bpy.utils.unregister_class(render_tools.CEREBRO_objects_to_render_collection)
    bpy.utils.unregister_class(CEREBRO_test_content)
    srvconn.timer_stop()

def auth(context=bpy.context):
    b,m=srvconn.auth(
        # context.scene.ftrack_server_url,
        context.scene.ftrack_api_user,
        context.scene.ftrack_api_key,
        )
    context.scene.ftrack_auth_in_process=False
    if b:
        G.sess=m
        os.environ["CEREBRO_B3D_AUTH_USER"]=context.scene.ftrack_auth_current_user=context.scene.ftrack_api_user
        # get projects
        b,r=srvconn.get_active_projects_list(sess=m)
        if b:
            G.projects=r[0]
            G.projects_dict=r[1]
            G.projects_tags_dict=r[2]
            G.statuses=r[3]
            G.activites=r[4]
            print(G.activites)
            # for key in G.projects_dict.keys():
            #     print(G.projects_dict[key]["custom_attributes"]["fps"])

        return(True, 'Successful Authentication')
    else:
        os.environ["CEREBRO_B3D_AUTH_USER"]=context.scene.ftrack_auth_current_user='No authorization'
        return(False, m)