#
#
#
#

import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__),"modules"))

# os.environ["CEREBRO_EVENT_PLUGIN_PATH"]=os.path.join(os.path.dirname(__file__), "plugins")
os.environ["CEREBRO_DOCS_PATH"]=os.path.join(os.path.dirname(__file__), "html", "index.html")
os.environ["CEREBRO_ROOT_PATH"]=os.path.dirname(__file__)

bl_info = {
    "name": "Cerebro b3d connect",
    "description": "Cerebro tools for Blender.",
    "author": "Volodya Renderberg",
    "version": (1, 6, 4),
    "blender": (3, 3, 0),
    "location": "View3d tools panel",
    "warning": "", # used for warning icon and text in addons panel
    "doc_url":'https://cerebro-b3b-connect-manual.readthedocs.io/en/latest',
    "category": "Pipeline"}

if "bpy" in locals():
    import importlib
    importlib.reload(ui)
else:
    from . import ui

import bpy


##### REGISTER #####

def register():
    ui.register()

def unregister():
    ui.unregister()

if __name__ == "__main__":
    register()

