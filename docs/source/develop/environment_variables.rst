.. _environment-variables-page:

Environment Variables
=====================


.. _CEREBRO_ROOT_PATH:

CEREBRO_ROOT_PATH
-----------------

* Путь до директория плагина.


.. _CEREBRO_DOCS_PATH:

CEREBRO_DOCS_PATH
-----------------

* Путь до ``index.html`` технической документации.


.. _CEREBRO_B3D_ROLE:

CEREBRO_B3D_ROLE
----------------

* Роль юзера: ``Working`` - исполнитель или ``Cheking`` - проверяющий.


.. _CEREBRO_B3D_PROJECTS_DIR:

CEREBRO_B3D_PROJECTS_DIR
------------------------


* Путь до текущей директрии для размещения проектов, определяемая в :func:`settings.set_projects_folder`


.. _CEREBRO_B3D_CLIP_START:

CEREBRO_B3D_CLIP_START
-----------------------

* Параметр ``clip_start`` для активной камеры шота. Определяется только для тех задач у которых тип родителя ``Shot``.


.. _CEREBRO_B3D_CLIP_END:

CEREBRO_B3D_CLIP_END
----------------------

* Параметр ``clip_end`` для активной камеры шота. Определяется только для тех задач у которых тип родителя ``Shot``.


.. _CEREBRO_B3D_CURRENT_PROJECT_ID:

CEREBRO_B3D_CURRENT_PROJECT_ID
------------------------------

* ``id`` текущего проекта.


.. _CEREBRO_B3D_CURRENT_PROJECT_NAME:

CEREBRO_B3D_CURRENT_PROJECT_NAME
--------------------------------

* ``name`` текущего проекта.


.. _CEREBRO_B3D_CURRENT_PROJECT_FULLNAME:

CEREBRO_B3D_CURRENT_PROJECT_FULLNAME
------------------------------------

* ``full_name`` текущего проекта.


.. _CEREBRO_B3D_CURRENT_PROJECT_FPS:

CEREBRO_B3D_CURRENT_PROJECT_FPS
-------------------------------

* ``fps`` текущего проекта.


.. _CEREBRO_B3D_CURRENT_PROJECT_WIDTH:

CEREBRO_B3D_CURRENT_PROJECT_WIDTH
---------------------------------

* ``width`` ширина в пикселах для разрешения текущего проекта.


.. _CEREBRO_B3D_CURRENT_PROJECT_HEIGHT:

CEREBRO_B3D_CURRENT_PROJECT_HEIGHT
----------------------------------

* ``height`` высота в пикселах для разрешения текущего проекта.


.. _CEREBRO_B3D_CURRENT_PROJECT_FSTART:

CEREBRO_B3D_CURRENT_PROJECT_FSTART
----------------------------------

* Кадр с которого будет начинаться анимация во всех шотах. По умолчанию 1000.


.. _CEREBRO_B3D_CURRENT_TASK_ID:

CEREBRO_B3D_CURRENT_TASK_ID
---------------------------

* ``id`` текущей задачи.


.. _CEREBRO_B3D_CURRENT_ASSET_NAME:

CEREBRO_B3D_CURRENT_ASSET_NAME
------------------------------

* ``name`` имя ассета текущей задачи.


.. _CEREBRO_B3D_CURRENT_ASSET_TYPE:

CEREBRO_B3D_CURRENT_ASSET_TYPE
------------------------------

* ``object_type.name`` имя типа ассета текущей задачи.


.. _CEREBRO_B3D_CURRENT_ASSET_ID:

CEREBRO_B3D_CURRENT_ASSET_ID
----------------------------

* ``id`` ассета текущей задачи.


.. _CEREBRO_B3D_AUTH_USER:

CEREBRO_B3D_AUTH_USER
---------------------

* ``username`` авторизованного пользователя.


.. _CEREBRO_B3D_SCENE_FEND:

CEREBRO_B3D_SCENE_FEND
----------------------

* финальный кадр шота, считается от нуля. Определяется только для тех задач у которых тип родителя ``Shot``.


.. _CEREBRO_B3D_SHOTS_STATUSES:

CEREBRO_B3D_SHOTS_STATUSES
--------------------------

* *json* словарь с ключами: ``time`` - время записи в isoformat(), ``data`` - словарь статусов по именам шотов. Это статусы задач последних версий ревью шотов. Заполняется в :func:`working.select_shot_sequences_by_task_status`