Installation
============

Contents:

.. toctree::
   :maxdepth: 1

   install_windows
   install_linux
   install_macos