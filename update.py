# -*- coding: utf-8 -*-

"""Обновление плагина из ``git`` репозитория.

Константы определяющие репозиторий для обновления:

:attr:`ENGINE`

:attr:`REPO`

:attr:`OWNER`

:attr:`PROJECT_ID`

:attr:`GITLAB_DOMAIN`


Константы которые редактируются для использования данного модуля в других плагинах:

:attr:`ROOT_PATH`

:attr:`DATA_FOLDER`
"""

import requests
import os
import json
import datetime
import tempfile
import shutil
# import platform
import ssl
import logging
from urllib.request import urlretrieve
from zipfile import ZipFile

import bpy

logger = logging.getLogger(__name__)

ROOT_PATH=os.environ.get("CEREBRO_ROOT_PATH")
"""str: Директория плагина.

Назначается из переменной окружения, которая задаётся в __init__.py плагина

.. code-block:: python

    os.environ["PLAGIN_NAME_ROOT_PATH"]=os.path.dirname(__file__)
"""

DATA_FOLDER=".cerebro_b3d_connect"
"""str: Имя директории настроект плагина, которая располагается в домашней папки системы. """

VERSION_DATA_FILE=".last_version"
"""str: Имя файла с записью версии последнего обновления. """

ENGINE="gitlab"
"""str: Поддерживаются варианты - **github**, **gitlab** """

REPO="git-test"
"""str: Имя репозитория (github). """

OWNER="volodya-renderberg"
"""str: Никнейм владельца репозитория (github). """

# PROJECT_ID=34370532
PROJECT_ID=3
"""int: ``id`` проекта (gitlab). """

# USER_ID=5082521

GITHUB_URL=f"https://api.github.com/repos/{OWNER}/{REPO}/releases/latest"
"""str: Url последнего релиза на ``github`` """

# GITLAB_DOMAIN="https://gitlab.com"
GITLAB_DOMAIN="https://gitlab.propellers.pro"
"""str: домен сайта гитлаб. """

ACCESS_TOKEN="NdiWV8saurQwetQTD2Lb"
"""str: токен доступа. """

def _get_gitlab_url():
    if ACCESS_TOKEN:
        return f"{GITLAB_DOMAIN}/api/v4/projects/{PROJECT_ID}/releases/?access_token={ACCESS_TOKEN}"
    else:
        return f"{GITLAB_DOMAIN}/api/v4/projects/{PROJECT_ID}/releases"

GITLAB_URL=_get_gitlab_url()
"""str: Url последнего релиза на ``gitlab`` """


def _clean_dir():
    """Удаление временных каталогов в директории /addons/ """
    addons=os.path.dirname(ROOT_PATH)
    for item in os.listdir(addons):
        if item.startswith('~'):
            del_path=os.path.join(addons, item)
            if os.path.exists(del_path) and os.path.isdir(del_path):
                try:
                    shutil.rmtree(del_path)
                except:
                 pass


def _get_data_folder(create=True) -> str:
    """Возвращает путь до директории где хранятся настройки, при отсутствии создаёт.

    Parameters
    ----------
    create : bool, optional
        Если ``True`` то создаст при отсутствии.

    Returns
    -------
    str
        path.
    """
    folder_path=os.path.join(os.path.expanduser('~'), DATA_FOLDER)
    if not os.path.exists(folder_path):
        if create:
            try:
                os.makedirs(folder_path)
            except Exception as e:
                print(f'exception1 from _get_data_folder() - {e}')
                return None
        else:
            return None
    return folder_path


def _write_release_data(release: dict):
    """Запись в *json* файл словаря релиза, полученного в :func:`_get_latest_release`.

    Returns
    -------
    None
    """
    #(datetime)
    release["released_at"]=datetime.datetime.isoformat(release["released_at"])

    #(version)
    bv='.'.join([str(i) for i in bpy.app.version[:2]])
    path=os.path.join(_get_data_folder(), VERSION_DATA_FILE)

    #(write)
    #(-- get data)
    if os.path.exists(path):
        with open(path, 'r', encoding='utf-8') as f:
            try:
                data=json.load(f)
            except:
                data=dict()
    else:
        data=dict()
    data[bv]=release
    #(-- write)
    with open(path, 'w', encoding='utf-8') as f:
        json.dump(data, f)
    


def _read_release_data():
    """Чтение из *json* фала словаря релиза последнего обновления, записанного в :func:`_write_release_data`.

    Returns
    -------
    dict None
        {"tag_name":str, "num":float, "released_at":datetime, "zip_url":str}
    """
    bv='.'.join([str(i) for i in bpy.app.version[:2]])
    path = os.path.join(_get_data_folder(), VERSION_DATA_FILE)
    if os.path.exists(path):
        with open(path, 'r', encoding='utf-8') as f:
            try:
                data=json.load(f)
                #(release)
                release=data.get(bv, None)
                #(datetime)
                if release:
                    release["released_at"]=datetime.datetime.fromisoformat(release["released_at"])
            except:
                release=None
    else:
        release=None
    return release


def _download_zip(url):
    """Скачивание zip файла в tmp директорию.

    Returns
    -------
    str
        download_path
    """
    #(download)
    download_dir = tempfile.mkdtemp()
    dowload_path=os.path.join(download_dir, "release.zip")
    ssl._create_default_https_context = ssl._create_unverified_context
    urlretrieve(url, dowload_path)
        
    return dowload_path


def _unpack_zip(zip_file, target_dir):
    """Распаковывание содержимого архива в директорию. 

    Parameters
    ----------
    zip_file : str
        Путь до архива
    target_dir : str
        Путь до папки назначения

    Return
    ------
    str
        Путь к распакованной директории.
    """
    with ZipFile(zip_file) as myzip:
        members=myzip.namelist()
        # print(members)
        myzip.extractall(path=target_dir, members=members)

    return os.path.join(target_dir, members[0])


def _get_latest_release():
    """Возвращает словарь последнего релиза или поднимает Exception(). 

    Returns
    -------
    dict
        {"tag_name":str, "num":float, "released_at":datetime, "zip_url":str}
    """
    if ENGINE=="gitlab":
        r=requests.get(GITLAB_URL)
        if not r.ok:
            logger.warning(GITLAB_URL)
            raise Exception(r.text)
        release=dict()
        data=r.json()[0]
        #(tag_name)
        release["tag_name"]=data["tag_name"]
        #(released_at)
        if data["released_at"].endswith("Z"):
            time_string=data["released_at"][:-1]
            release["released_at"]=datetime.datetime.fromisoformat(time_string)
        else:
            release["released_at"]=datetime.datetime.fromisoformat(data["released_at"])
        #(num)
        release["num"]=float(''.join(i for i in data["tag_name"] if not i.isalpha()))
        #(zip_url)
        for item in data["assets"]["sources"]:
            if item["format"]=="zip":
                release["zip_url"]=item["url"]
        #(end)
        return release
    elif ENGINE=="github":
        r=requests.get(GITHUB_URL)
        if not r.ok:
            raise Exception(r.text)
        release=dict()
        data=r.json()
        #(tag_name)
        release["tag_name"]=data["tag_name"]
        #(released_at)
        if data["published_at"].endswith("Z"):
            time_string=data["published_at"][:-1]
            release["released_at"]=datetime.datetime.fromisoformat(time_string)
        else:
            release["released_at"]=datetime.datetime.fromisoformat(data["published_at"])
        #(num)
        release["num"]=float(''.join(i for i in data["tag_name"] if not i.isalpha()))
        #(zip_url)
        release["zip_url"]=data["zipball_url"]                

        #(end)
        # return r.json()
        return release


def lag_test():
    """Проверка свежести существующей версии.

    Returns
    -------
    tuple
        (bool, release) ``bool`` - свежесть (*True* - устаревшая), ``release`` - славарь получаемый в :func:`_get_latest_release`.
    """
    _clean_dir()

    #(get_latest_release)
    release=_get_latest_release()

    #(read_release_data)
    c_release=_read_release_data()

    #(сверка)
    # if not c_release or c_release["released_at"]<release["released_at"]:
    if not c_release or c_release["num"]<release["num"]:
        return (True, release)
    else:
        return (False, release)


def update(release: dict):
    """Загрузка обновления плагина.

    Parameters
    ----------
    release : dict
        Словарь релиза получаемый в :func:`_get_latest_release`.

    Returns
    -------
    None
    """
    # _write_release_data(release)
    # return(True, "Ok!")

    #(download zip)
    dowload_path=_download_zip(release["zip_url"])

    #(unpuck zip)
    plugins_dir=os.path.dirname(ROOT_PATH)
    new_dir=_unpack_zip(dowload_path, plugins_dir)
    #(-- rename old)
    dir_name=os.path.basename(ROOT_PATH)
    rn_name=f"~{dir_name}"
    rn_path=os.path.join(plugins_dir, rn_name)
    shutil.move(ROOT_PATH, rn_path)
    #(-- rename new)
    shutil.move(new_dir, ROOT_PATH)
    #(-- remove old)
    _clean_dir()
    # if platform.system().lower()!='windows':
    #     shutil.rmtree(rn_path)

    #(write_release_data)
    _write_release_data(release)

    return(True, "Ok!")