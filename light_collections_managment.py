"""
Операции с коллекциями для настройки освещения в сценах "WithoutBake" и "Bake".
"""
import os
import json

import bpy

try:
    import path_utils
    import hooks
except:
    from . import path_utils
    from . import hooks

OBJECT_TYPES=['MESH']
"""list: типы объектов, которые добавляются в коллекции Bake и WithoutBake. """

LIGHT_TYPES=['LIGHT_PROBE', 'LIGHT']
"""list: типы объектов, которые добавляются в коллекции освещения. """

COLLECTIONS=["WithoutBake", "Bake", "LightProbes"]
"""list: общий перечень коллекций. """

SAVING_WORLD_ATTRIBUTES=[
"use_nodes",
]
"""list: список сохраняемых атрибутов bpy.context.scene.world"""

SAVING_RENDER_ATTRIBUTES=[
"filter_size",
"film_transparent",
"use_overwrite",
"use_placeholder",
"resolution_percentage",
"use_compositing",
"use_sequencer",
]
"""list: список сохраняемых атрибутов bpy.context.scene.render"""

SAVING_RENDER_IMAGE_SETTINGS_ATTRIBUTES=[
"file_format",
]
"""list: список сохраняемых атрибутов bpy.context.scene.render.image_settings"""

SAVING_EEVEE_ATTRIBUTES=[
"use_gtao",
"gtao_distance",
"gtao_factor",
"gtao_quality",
"use_gtao_bent_normals",
"use_gtao_bounce",
'use_bloom',
"bloom_threshold",
"bloom_knee",
"bloom_radius",
"bloom_color",
"bloom_intensity",
"bloom_clamp",
"shadow_cube_size",
"shadow_cascade_size",
"use_shadow_high_bitdepth",
"use_soft_shadows",
"light_threshold",
"use_ssr",
"use_ssr_refraction",
"use_ssr_halfres",
"ssr_quality",
"ssr_max_roughness",
"ssr_thickness",
"ssr_border_fade",
"ssr_firefly_fac",
"volumetric_start",
"volumetric_end",
"volumetric_tile_size",
"volumetric_samples",
"volumetric_sample_distribution",
"use_volumetric_lights",
"volumetric_light_clamp",
"use_volumetric_shadows",
"volumetric_shadow_samples",
"gi_auto_bake",
"gi_diffuse_bounces",
"gi_cubemap_resolution",
"gi_visibility_resolution",
"gi_irradiance_smoothing",
"gi_glossy_clamp",
"gi_filter_quality",
"gi_cubemap_display_size",
"gi_irradiance_display_size",
"use_overscan",
]
"""list: список сохраняемых атрибутов bpy.context.scene.eevee"""

RENDER_LAYER_ATTRIBUTES=[
"use_pass_combined",
"use_pass_z",
"use_pass_mist",
"use_pass_normal",
"use_pass_diffuse_direct",
"use_pass_diffuse_color",
"use_pass_glossy_direct",
"use_pass_glossy_color",
"use_pass_emit",
"use_pass_environment",
"use_pass_shadow",
"use_pass_ambient_occlusion",
"use_pass_cryptomatte_object",
"use_pass_cryptomatte_material",
"use_pass_cryptomatte_asset",
"pass_cryptomatte_depth",
"use_pass_cryptomatte_accurate",
]
"""list: список сохраняемых атрибутов bpy.context.scene.view_layers["RenderLayer"]"""

SCENE_ATTRIBUTES=[
"use_pass_volume_direct",
"use_pass_bloom",
]
"""list: список сохраняемых атрибутов bpy.context.scene.view_layers['RenderLayer'].eevee"""

MIST_ATTRIBUTES=[
"start",
"depth",
"falloff",
]
"""list: список сохраняемых атрибутов bpy.context.scene.world.mist_settings """

def get_local_path(to_write=True, task=False) -> tuple:
    """
    Возвращает путь до файла '../meta/light_collections.json' или ошибку в случае отсутствия.

    Parameters
    ----------
    to_write : bool, optional
        Если True - речь о записи и проверяется только наличие дирректории, если False - то проверяется наличие файла.
    task : ftrack.task
        Если передавать будет искать meta в этой задаче.
    """
    #(1)
    if task:
        b,folder=path_utils.get_task_folder_path(task, create=False)
        if not b:
            return(b,folder)
    else:
        folder=os.path.dirname(bpy.data.filepath)
    path = os.path.join(folder, 'meta', 'light_collections.json')
    #(2)
    if to_write:
        if not os.path.exists(os.path.dirname(path)):
            return (False, "meta path not found!")
        else:
            return (True, path)
    else:
        if not os.path.exists(path):
            return (False, f"\"{path}\" path not found!")
        else:
            return (True, path)

def save_collections_children(collections: list, path=False) -> tuple:
    """
    Запись в json файл объектов с типами из :attr:`OBJECT_TYPES`

    Parameters
    ----------
    collections : list
        Список колекций над которыми производится действие.
    path : bool, optional
        Путь для записи, если False - значит путь для записи в данный ассет.

    Returns
    -------
    tuple
        (True, "Data saved!") или (False, comment)
    """
    childrens=dict()
    for name in collections:
        childrens[name]=list()
        if 'light' in name.lower():
            type_list=LIGHT_TYPES
        else:
            type_list=OBJECT_TYPES
        if not name in bpy.data.collections:
            print(f"info: no collection with \"{name}\" name found")
            continue
        for ob in bpy.data.collections[name].all_objects:
            if ob.type in type_list:
                childrens[name].append(ob.name)
    #                print(f"{name} - {ob.name} - {ob.type}")
    #(ATTRIBUTES)
    attributes=childrens["attributes"]=dict()
    attributes['eevee']=dict()
    for attr_name in SAVING_EEVEE_ATTRIBUTES:
        if "color" in attr_name:
            attributes['eevee'][attr_name]=tuple(getattr(bpy.context.scene.eevee, attr_name))
        else:
            attributes['eevee'][attr_name]=getattr(bpy.context.scene.eevee, attr_name)
    #(--RENDER)
    attributes['render']=dict()
    for attr_name in SAVING_RENDER_ATTRIBUTES:
        attributes['render'][attr_name]=getattr(bpy.context.scene.render, attr_name)
    #(--WORLD)
    attributes['world']=dict()
    for attr_name in SAVING_WORLD_ATTRIBUTES:
        attributes['world'][attr_name]=getattr(bpy.context.scene.world, attr_name)
    attributes['world_default_values']=list()
    for w_name in bpy.data.worlds.keys():
        if "world" in w_name.lower():
            attributes['world_default_values'].append(tuple(bpy.data.worlds[w_name].node_tree.nodes['Background'].inputs[0].default_value))
            attributes['world_default_values'].append(bpy.data.worlds[w_name].node_tree.nodes['Background'].inputs[1].default_value)
            break
    #()
    if "RenderLayer" in bpy.context.scene.view_layers.keys():
        layer_name="RenderLayer"
    elif "View Layer" in bpy.context.scene.view_layers.keys():
        layer_name="View Layer"
    else:
        pass
    #()
    attributes['render_layer']=dict()
    for attr_name in RENDER_LAYER_ATTRIBUTES:
        attributes['render_layer'][attr_name]=getattr(bpy.context.scene.view_layers[layer_name], attr_name)
    #(SCENE_ATTRIBUTES)
    attributes['render_layer_eevee']=dict()
    for attr_name in SCENE_ATTRIBUTES:
        attributes['render_layer_eevee'][attr_name]=getattr(bpy.context.scene.view_layers[layer_name].eevee, attr_name)
    #(RENDER SETTINGS)
    attributes['render_settings']=dict()
    for attr_name in SAVING_RENDER_IMAGE_SETTINGS_ATTRIBUTES:
        attributes['render_settings'][attr_name]=getattr(bpy.context.scene.render.image_settings, attr_name)
    #(MIST)
    attributes['mist']=dict()
    for attr_name in MIST_ATTRIBUTES:
        attributes['mist'][attr_name]=getattr(bpy.context.scene.world.mist_settings, attr_name)
    #(LIGHT PROBES)
    light_probes=childrens["light_probes"]=dict()
    for ob in bpy.data.objects:
        if ob.type=="LIGHT_PROBE":
            if ob.data.visibility_collection:
                light_probes[ob.name]=ob.data.visibility_collection.name
            else:
                light_probes[ob.name]=None

    print(attributes)
    
    if not path:
        b,path=get_local_path()
        if not b:
            return(b,path)
    with open(path, 'w') as f:
        json.dump(childrens, f, sort_keys=True, indent=4)

    return(True, "Data saved!")
        
        
def set_collections_children(context, incoming_task=False):
    """
    * Линкование объектов в коллекции по спискам из текстового файла.
    * Назначение значений параметров из того же файла.

    Parameters
    ----------
    context : blender.context
        context
    incoming_task : ftrack.task optoional
        Если False - будет поиск в meta текущей задачи, иначе в переданой.

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    if incoming_task:
        b,path=get_local_path(to_write=False, task=incoming_task)
    else:
        b,path=get_local_path(to_write=False)
    if not b:
        return(b,path)
    with open(path, 'r') as f:
        data=json.load(f)
    # print(data.keys())
    # #(COLLECTIONS)
    # for name in COLLECTIONS:
    #     if not name in bpy.data.collections:
    #         collection=bpy.data.collections.new(name)
    #         context.scene.collection.children.link(collection)
    #     else:
    #         collection=bpy.data.collections[name]
    #     if 'light' in name.lower():
    #         type_list=LIGHT_TYPES
    #     else:
    #         type_list=OBJECT_TYPES
    #     for ob_name in data[name]:
    #         if not ob_name in collection.all_objects:
    #             if ob_name in bpy.data.objects:
    #                 ob=bpy.data.objects[ob_name]
    #             else:
    #                 print(f"WARNING: not found object \"{ob_name}\" for collection \"{name}\"!")
    #                 continue
    #             if ob.type in type_list:
    #                 collection.objects.link(ob)
    #             else:
    #                 print(f"WARNING: wrong type \"{ob.type}\" of object \"{ob_name}\"!")
    #(RENDER SETTINGS)
    hooks._set_render_setting_for_playblast(fstart="ENV", fend="ENV", filepath="")
    #(PARAMETERS)
    attributes=data["attributes"]
    if "RenderLayer" in bpy.context.scene.view_layers.keys():
        layer_name="RenderLayer"
    elif "View Layer" in bpy.context.scene.view_layers.keys():
        layer_name="View Layer"

    for key in attributes.keys():
        if key=="world_default_values":
            for i,v in enumerate(attributes['world_default_values']):
                bpy.data.worlds["World"].node_tree.nodes['Background'].inputs[i].default_value=v
        elif key=="render_layer":
            for attr_name in attributes[key]:
                setattr(bpy.context.scene.view_layers[layer_name], attr_name, attributes[key][attr_name])
        elif key=="render_layer_eevee":
            for attr_name in attributes[key]:
                setattr(bpy.context.scene.view_layers[layer_name].eevee, attr_name, attributes[key][attr_name])
        elif key=="render_settings":
            for attr_name in attributes[key]:
                setattr(bpy.context.scene.render.image_settings, attr_name, attributes[key][attr_name])
        elif key=="mist":
            for attr_name in attributes[key]:
                setattr(bpy.context.scene.world.mist_settings, attr_name, attributes[key][attr_name])
        else:
            for attr_name in attributes[key]:
                eval(f"setattr(bpy.context.scene.{key}, '{attr_name}', attributes['{key}']['{attr_name}'])")
    #(LIGHT PROBES)
    if "light_probes" in data:
        for probe_name, collection_name in data["light_probes"].items():
            try:
                if collection_name:
                    bpy.data.objects[probe_name].data.visibility_collection=bpy.data.collections[collection_name]
                else:
                    bpy.data.objects[probe_name].data.visibility_collection=None
            except Exception as e:
                print(f"WARNING: {e}")


    return(True, "Ok!")
                    

def select_objects_of_collection(name):
    """
    Выделение объектов с типом из :attr:`OBJECT_TYPES`, которые входят в коллекцию "name".

    Parameters
    ----------
    name : str
         Имя коллекции

    Returns
    -------
    None
    """
    collection=bpy.data.collections[name]
    for ob in bpy.data.objects:
        ob.select_set(False)
    for ob in collection.all_objects:
        if ob.type in OBJECT_TYPES or ob.type in LIGHT_TYPES:
            ob.select_set(True)
            
            
def select_no_collections_objects():
    """
    Выделение всез объектов с типом из :attr:`OBJECT_TYPES`, которые не входят ни в одну из коллекций :attr:`COLLECTIONS`.

    Returns
    -------
    None
    """
    colls=list()
    for col_name in COLLECTIONS:
        if col_name in bpy.data.collections:
            colls.append(bpy.data.collections[col_name])
        else:
            collection=bpy.data.collections.new(col_name)
            bpy.context.scene.collection.children.link(collection)
            colls.append(collection)
    #
    for ob in bpy.data.objects:
        ob.select_set(False)
    for ob in bpy.data.objects:
        if ob.type in OBJECT_TYPES or ob.type in LIGHT_TYPES:
            any_list=list()
            for col in colls:
                any_list.append(ob.name in col.all_objects)
            if not any(any_list):
                try:
                    ob.select_set(True)
                except Exception as e:
                    print(f"WARNING: {e} {ob.name}")
        else:
            print(f"WARNING: wrong type \"{ob.type}\" of object \"{ob.name}\"!")


def add_selected_objects_to_collection(context, collection: str) -> tuple:
    """
    Добавление выделенных объектов в коллекцию

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    if collection in bpy.data.collections:
        col=bpy.data.collections[collection]
    else:
        col=bpy.data.collections.new(collection)
        bpy.context.scene.collection.children.link(col)

    if not context.selected_objects:
        return(False, "No objects selected!")

    if 'light' in collection.lower():
        type_list=LIGHT_TYPES
    else:
        type_list=OBJECT_TYPES
    for ob in context.selected_objects:
        if ob.type in type_list:
            col.objects.link(ob)
        elif ob.type=="ARMATURE":
            for ch in ob.children:
                if ch.type in type_list:
                    col.objects.link(ch)

    return (True, "Ok!")


def remove_selected_objects_from_collection(context, collection: str) -> tuple:
    """
    Удаление выделенных объектов из коллекции.

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    if collection in bpy.data.collections:
        col=bpy.data.collections[collection]
    else:
        return(False, f"{collection} collection does not exist!")

    if 'light' in collection.lower():
        type_list=LIGHT_TYPES
    else:
        type_list=OBJECT_TYPES
    for ob in context.selected_objects:
        if ob.type in type_list and ob.name in col.all_objects:
            col.objects.unlink(ob)
        elif ob.type=="ARMATURE":
            for ch in ob.children:
                if ch.type in type_list and ch.name in col.all_objects:
                    col.objects.unlink(ch)

    return (True, "Ok!")