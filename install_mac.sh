#!/bin/bash

echo Start of installation Ftrack_b3d_connect ...

read -p "Input version of blender[2.93]:" bversion

if ! [ -n "$bversion" ]; then 
    bversion=2.93
fi
echo version - ${bversion}

cd ~/Library

addons_dir="Application Support/Blender/${bversion}/scripts/addons"
modules_dir="Application Support/Blender/${bversion}/scripts/modules"
plugin_dir="${addons_dir}/cerebro-b3d-connect"

echo $addons_dir
echo $modules_dir

if [[ ! -f "${addons_dir}" ]]; then
    mkdir -p "${addons_dir}"
fi

if [[ ! -f "${modules_dir}" ]]; then
    mkdir -p "${modules_dir}"
fi

if [[ ! -f "${plugin_dir}" ]]; then
    mkdir -p "${plugin_dir}"
fi

cp -r * "${plugin_dir}"

cd "${plugin_dir}/modules"

unzip -d ../../../modules pycerebro_modules_lin.zip

echo installation is complete.