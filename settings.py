# -*- coding: utf-8 -*-

"""Настройка рабочей среды пользователя."""

import os
import shutil
import json
import datetime
import traceback
import logging

import bpy

from pycerebro import dbtypes

logger=logging.getLogger(__name__)

TO_RENDER_COLLECTION="Props_to_Render"
"""str : Имя коллекции, в которую дублируются коллекции объектов, участвующих в редере. """

# UNIVERSEID=11018 # testdrive
UNIVERSEID=11035 # пропеллерс
"""int: id вселенной

.. code-block:: python

    G.sess.execute(f'''select "getUniverseID_byName"(\'{имя вселенной}\'::character varying)''')[0][0]
"""


NEW_STORAGE_DOMEN = "propellers.cerebrohq.com"
"""str: Новый домен хранилища"""


STORAGE=[datetime.datetime(2022, 3, 9, 15, 2, 49, 977509, tzinfo=datetime.timezone(datetime.timedelta(seconds=10800), '+03:00')), 6, 2927339757819249390, 'storage2.cerebrohq.com', 'testdrive remote storage', 11018, 53687091200, 1, 45431, 4080, None, None, 1934099]
"""list: хранилище используемое для хранения данных.

Список хранилищ:

.. code-block:: python

    G.sess.execute('select * from "siteList"();')
"""


TEMPLATE_PROJECT_ID=2927339757819514711
"""int: id проекта где хранятся шаблоны задач для ассетов.

* Имя шаблона - имя родительской задачи шаблона.
* Все родительские задачи шаблонов лежат сразу под верхней задачей проекта.
"""


CUSTOM_ATTRIBUTES={
    "fps":2,
    "fstart":0,
    "fend":0,
    "width":0,
    "height":0,
}
"""dict: кастомные атрибуты, ключ имя, значение ``TAG_DATA_TYPE`` """


OBJECT_TYPES={
    "AssetModel": -678365,
    "Episode": -678365,
    "Folder": -465124,
    "Storyboard": -678365,
    "Sequence": -678365,
    "Shot": -678365,
    "Location": -678365,
    "animatic": -4386592,
    "animation": -9652134,
    "compositing": -5282980,
    "concept": -6009531,
    "fx": -11476030,
    "layout": -4887920,
    "modeling": -11430916,
    "montage": -6197570,
    "music": -6197570,
    "previz": -6197570,
    "rendering": -5592576,
    "rigging": -8465631,
    "script": -6197570,
    "shading": -7856988,
    "sound mix": -6197570,
    "texturing": -6197570,
    "tracking": -6197570,
    "voice": -6197570,
}
"""dict: типы видов деятельности и сущностей. """


ASSET_TYPES=[
    "Episode",
    "Storyboard",
    "Sequence",
    "Shot",
    "Location",
    "AssetModel",
]
"""list: типы видов деятельности, которые являются ассетами, итд. """


COMPOSITE_ASSETS=[
    'Location'
]
"""list : ассеты, для которых надо делать рекурсивную проверку при сборе списка входящих связей. """


COMMIT_DATA_FILE="commit_data.json"
"""str: имя файла с данными анимационного шота. """


COMMIT_DATA_TEXT="commit_data"
"""str: имя текстового дата блока с данными последнего коммита сцены.  """


CONTAINER_TYPES= ASSET_TYPES + ["Folder"]
"""list: типы видов деятельности, которые являются контейнерами, папками, ассетами, итд. """


MONTAGE_OBJECT_TYPES=(
    "Episode",
    "Sequence",
)
"""tuple: типы ассетов которые предполагают работу над всем фильмом на монтажном столе. """


RENDERING_TYPES=(
    "rendering",
    "рендер",
    'anim_render'
)
"""tuple: типы видов деятельности рендера."""


ANIMATION_TYPES=(
    'animation',
    'anim_render'
)
"""tuple: типы видов деятельности анимации."""


ANIMATION_OBJECT_TYPES=(
    "Shot",
)
"""tuple: типы ассетов которые предполагают работу над анимацией внутри шота."""


ANIMATORS_COLLECTION="Excipients"
"""str: имя коллекции в шоте, в которой аниматоры содержат дополнительный прикладной контент. Эта коллеция импортируется в сцену при билде. """


FROM_FTRACK_STATUSES={
    "On Hold": "на паузе",
    "Omitted": "закрыта",
    "New": None,
    "Not started": "готова к работе",
    "Open": "в работе",
    "WIP": None,
    "Rejected": "на переработку",
    "Duplicate": None,
    "In progress": "в работе",
    "Pending Review": "на утверждение",
    "Reopened": "в работе",
    "Needs attention": "на утверждение",
    "Normal": "готова к работе",
    "Proposed final": "на утверждение",
    "Approved": "выполнена",
    "Superseded": "закрыта",
    "Closed": "закрыта",
    "Completed": "выполнена",
    "Resolved": "выполнена",
    "Client approved": "на утверждение",
}
"""dict: соответсвие статусов ftrack и cerebro."""


DATA_FOLDER='.cerebro_b3d_connect'
"""str: Имя директории хранения локальных настроек пользователя расположение в ~/ """

TIMER_DATA_FILE='timer_data.json'
"""str: Имя файла где хранится отметка времени старта таймера (``time``) и *id* задачи (``task``). """

AUTH_FILE='auth_data.json'
"""srt: Имя файла где хранятся параметры для аутентификации пользователя. """

SETTING_FILE='setting.json'
"""str: Имя файла где храниться словарь общих настроек. Ключи словаря:

    * ``projects_folder`` - путь до :attr:`settings.PROJECTS`
"""

PROJECTS='Cerebro_Projects_Main'
"""str: Название директории где распологаются проекты. """

BACKUP_FOLDER='Propellers_Backup'
"""str: Название директории для бекапа проектов. """

BACKUP_BD_FILE='.cerebro_project_backup_data.db'
"""str : название файла базы данных sqlite3 с бекап данными проекта. """

RECOVERY_DATA="recovery_data.json"
"""str: файл где хранятся данные о положении объектов сцены, их анимационных экшенах... итд.

Расположение файла в ``meta`` директории задачи.
"""

RENDER_ZIP_NAME="render_sequence.zip"
"""str: имя архива с рендером секвенции. """

def _clean_dir(directory):
    """Удаление временных файлов и каталогов в директории """
    if not os.path.isdir(directory):
        return
    logger.debug(f'_clean_dir({directory})')
    for item in os.listdir(directory):
        if item.startswith('~'):
            del_path=os.path.join(directory, item)
            if os.path.exists(del_path) and os.path.isdir(del_path):
                try:
                    shutil.rmtree(del_path)
                except:
                    print(f'{traceback.format_exc()}')
            elif os.path.exists(del_path) and os.path.isfile(del_path):
                try:
                    os.remove(del_path)
                except:
                    print(f'{traceback.format_exc()}')


def _rename_file_to_tmp(path):
    """Переименование файла во временный, добавлением префикса `~` """
    logger.debug(f'_rename_file_to_tmp({path})')
    #(-- rename old)
    file_name=os.path.basename(path)
    rn_name=f"~{file_name}"
    rn_path=os.path.join(os.path.dirname(path), rn_name)
    shutil.move(path, rn_path)


def _get_data_folder(create=True) -> str:
    """Возвращает путь до директории где хранятся настройки, при отсутствии создаёт.

    Parameters
    ----------
    create : bool, optional
        Если ``True`` то создаст при отсутствии.

    Returns
    -------
    str
        path.
    """
    folder_path=os.path.join(os.path.expanduser('~'), DATA_FOLDER)
    if not os.path.exists(folder_path):
        if create:
            try:
                os.makedirs(folder_path)
            except Exception as e:
                print(f'exception1 from _get_data_folder() - {e}')
                return None
        else:
            return None
    return folder_path

def _write_setting_data(**kw) -> None:
    """Записывает данные в файл, по умолчанию в :attr:`settings.SETTING_FILE`

    Parameters
    ----------
    kw : dict
        Именованные аргументы, запишет все значения в словарь по ключам-(именам аргументов).
        Значение ключа ``file_name`` будет использовано для имени файла записи.
        Значение ключа ``file_path`` будет использовано для полного пути к файлу.

    Returns
    -------
    None
    """
    #(file_name)
    k="file_name"
    if k in kw.keys():
        file_name=kw.pop(k)
    else:
        file_name=SETTING_FILE
    #(file_path)
    k="file_path"
    if k in kw.keys():
        file_path=kw.pop(k)
    else:
        file_path=None

    #(path to file)
    if file_path is None:
        path = os.path.join(_get_data_folder(), file_name)
    else:
        path=file_path
    # get data
    if os.path.exists(path):
        with open(path, 'r', encoding='utf-8') as f:
            data=json.load(f)
    else:
        data=dict()
    # append data
    for key in kw:
        data[key]=kw[key]
    # write data
    try:
        with open(path, 'w', encoding='utf-8') as f:
            json.dump(data, f, sort_keys=True, indent=4)
    except Exception as e:
        print(f'{traceback.format_exc()}')

def _read_setting_data(key=False, file_name=SETTING_FILE, file_path=False, clean=True):
    """Читает данные из файла, по умолчанию из :attr:`settings.SETTING_FILE`

    Parameters
    ----------
    key : str, optional
        *   Если передать ключ - то вернёт только значение по этому ключю.
        *   Если не передавать, то будет возвращён весь словарь.
    file_name : str optional
        имя файла для чтения, по умолчанию :attr:`settings.SETTING_FILE`
    file_path : str optional
        полный путь до файла, ``file_name`` в этом случае не учитывается.
    clean : bool optional
        Делать очистку или нет

    Returns
    -------
    dict, optional
    """
    # clean tmp files
    if clean:
        _clean_dir(_get_data_folder())
    # path to file
    if file_path:
        path=file_path
    else:
        path = os.path.join(_get_data_folder(), file_name)
    # get data
    rename_json_to_tmp=False
    if os.path.exists(path):
        with open(path, 'r', encoding='utf-8') as f:
            try:
                data=json.load(f)
            except json.decoder.JSONDecodeError:
                rename_json_to_tmp=True
                logger.error(f'{traceback.format_exc()}')
                data=dict()
            except Exception:
                print(f'{traceback.format_exc()}')
                data=dict()
    else:
        data=dict()

    if rename_json_to_tmp:
        _rename_file_to_tmp(path)

    # return data
    if key:
        return data.get(key)
    else:
        return data


def get_storage():
    """
    Возвращает объект `Storage`
    """
    if G.storage:
        return G.storage
    storages = G.sess.execute('select * from "siteList"();')
    for s in storages:
        if s[3] == NEW_STORAGE_DOMEN:
            G.storage = s
            return s
    G.storage = storages[0]
    return storages[0]


def get_projects_folder() -> str:
    """Возвращает директорию для размещения проектов, чтение из :attr:`settings.SETTING_FILE` """
    path = _read_setting_data(key="projects_folder")
    try:
        if os.path.exists(path):
            os.environ['CEREBRO_B3D_PROJECTS_DIR']=path
            return path
        else:
            # os.environ['CEREBRO_B3D_PROJECTS_DIR']=None
            return 'Undefined'
    except Exception as e:
        print(f'exception from get_projects_folder() - {e}')
        # os.environ['CEREBRO_B3D_PROJECTS_DIR']=None
        return 'Undefined'


def set_projects_folder(path: str) -> tuple:
    """Содание директории для размещения проектов, запись пути в :attr:`settings.SETTING_FILE`. 

    Parameters
    ----------
    path : str
        Путь директории где будет создана :attr:`settings.PROJECTS`.

    Returns
    -------
    tuple
        (True, path - путь до созданной :attr:`settings.PROJECTS`) или (False, comment)
    """
    if not os.path.exists(path):
        return (False, f'the path does not exist: {path}')
    # --
    # print(f'basename ({path}) - {os.path.basename(clean_final_slash(path))}')
    if os.path.basename(clean_final_slash(path))==PROJECTS:
        projects_path=path
    else:
        projects_path = os.path.join(path, PROJECTS)
    # print(f'projects_path - {projects_path}')
    # --
    try:
        if not os.path.exists(projects_path):
            os.makedirs(projects_path)
        _write_setting_data(projects_folder=projects_path)
        os.environ['CEREBRO_B3D_PROJECTS_DIR']=projects_path
        return (True, projects_path)
    except Exception as e:
        print(f'exception from set_projects_folder() - {e}')
        return (False, f"{e}")


def get_special_folder(key) -> str:
    """возвращает каку-либо спец директорию по ключу
    
    * чтение из :attr:`settings.SETTING_FILE`
    * заполняет переменную окружения CEREBRO_B3D_SPECIAL_[key.upper]
    
    Parameters
    ----------
    key : str
        Ключ словаря в файле :attr:`settings.SETTING_FILE`

    Returns
    -------
    str
        **path** или 'Undefined'
    """
    path = _read_setting_data(key=key)
    try:
        if os.path.exists(path):
            os.environ[f'CEREBRO_B3D_SPECIAL_{key.upper()}']=path
            return path
        else:
            return 'Undefined'
    except Exception as e:
        print(f'exception from get_special_folder() - {e}')
        return 'Undefined'


def set_special_folder(path: str, key: str) -> tuple:
    """Содание директории для размещения бекапа проектов, запись пути в :attr:`settings.SETTING_FILE`.

    * заполняет переменную окружения CEREBRO_B3D_SPECIAL_[key.upper]
    
    Parameters
    ----------
    path : str
        Путь директории где будет создана спец директория ``key.upper()``.
    key : str
        Ключ словаря в файле :attr:`settings.SETTING_FILE`

    Returns
    -------
    tuple
        (True, path - путь до созданной спец директории) или (False, comment)
    """
    if not os.path.exists(path):
        return (False, f'the path does not exist: {path}')
    # --
    # print(f'basename ({path}) - {os.path.basename(clean_final_slash(path))}')
    if os.path.basename(clean_final_slash(path))==key.upper():
        projects_path=path
    else:
        projects_path = os.path.join(path, key.upper())
    # print(f'projects_path - {projects_path}')
    # --
    try:
        if not os.path.exists(projects_path):
            os.makedirs(projects_path)
        _write_setting_data(**{key:projects_path})
        os.environ[f'CEREBRO_B3D_SPECIAL_{key.upper()}']=projects_path
        return (True, projects_path)
    except Exception as e:
        print(f'exception from set_special_folder() - {e}')
        return (False, f"{e}")


def read_auth_data() -> dict:
    """Чтение параметров для аутентификации из файла :attr:`settings.AUTH_FILE`

    Returns
    -------
    dict
        {'api_user': '', 'api_key': '', 'server_url': ''}
    """

    # exists AUTH_FILE
    data=dict()
    file_path=os.path.join(_get_data_folder(), AUTH_FILE)
    if os.path.exists(file_path):
        try:
            with open(file_path, 'r', encoding='utf-8') as f:
                data=json.load(f)
        except:
            pass
    return data

def write_auth_data(user: str, key: str) -> None:
    """Запись параметров для аутентификации в файл :attr:`settings.AUTH_FILE`

    Parameters
    ----------
    user : str
        api_user
    key : str
        password

    Returns
    -------
    None
    """
    # write data
    file_path=os.path.join(_get_data_folder(), AUTH_FILE)
    data=dict(api_key=key, api_user=user)
    try:
        with open(file_path, 'w', encoding='utf-8') as f:
            json.dump(data, f)
    except Exception as e:
        print(f'exception2 from write_auth_data() - {e}')

def add_graphics_editors(name: str, path: str) -> None:
    """Добавление графического редактора в список (пути до экзешника или команды терминала).

    Parameters
    ----------
    name : str
        псевдоним для данного редактора.
    path : str
        путь до экзешника или команда терминала запускающая софт.

    Returns
    -------
    None
    """
    r=_read_setting_data(key="graphics_editors")
    if r:
        r[name]=path
    else:
        r={name:path}

    return _write_setting_data(graphics_editors = r)

def del_graphics_editors(name: str) -> None:
    """Удаление графического редактора из списка (пути до экзешника или команды терминала).

    Parameters
    ----------
    name : str
        псевдоним для данного редактора.

    Returns
    -------
    None
    """
    r=_read_setting_data(key="graphics_editors")
    if r and (name in r):
        del r[name]
    
    return _write_setting_data(graphics_editors = r)

def read_graphics_editors() -> dict:
    """Чтение словаря графических редакторов (пути до экзешников или команды терминала)

    Returns
    -------
    dict
        {data} или {empty}
    """
    return _read_setting_data(key="graphics_editors")


def write_commit_data_to_text_data_block(context, task, description):
    """Запись метадаты коммита в текстовый дата блок сцены :attr:`COMMIT_DATA_TEXT`. 
    
    Записываются данные:

    * version
    * description
    * artist
    * commited
    * project
    * asset
    * task

    Returns
    --------
    None
    """
    #(data)
    data=dict(
        version=G.future_commit_num,
        description=description,
        artist=os.environ.get("CEREBRO_B3D_AUTH_USER", "No auth user"),
        commited=datetime.datetime.now().strftime("%B %d, %Y"),
        project=os.environ.get("CEREBRO_B3D_CURRENT_PROJECT_NAME", None),
        asset=os.environ.get("CEREBRO_B3D_CURRENT_ASSET_NAME", None),
        task=G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]][dbtypes.TASK_DATA_NAME],
    )
    #(text)
    text_name=COMMIT_DATA_TEXT
    if not text_name in bpy.data.texts:
        text=bpy.data.texts.new(text_name)
    else:
        text=bpy.data.texts[text_name]
    text.clear()
    #(write)
    text.write(json.dumps(data, sort_keys=True, indent=4))


#### Utilites

def clean_final_slash(path):
    while path.endswith('/'):
        path=path[:-1]
    return path