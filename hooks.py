# -*- coding: utf-8 -*-

"""Хуки связанные с событиями open, commit, ... итд. """

import os
import logging
import datetime
import json

import bpy

from pycerebro import dbtypes

try:
    import server_connect as srvconn
    import path_utils
    import settings
    import tools
except:
    from . import server_connect as srvconn
    from . import path_utils
    from . import settings
    from . import tools

logger=logging.getLogger(__name__)

FILM_OBJECTS=(
    "Episode",
    "Sequence"
)


ANIMATION_TYPES=(
    "layout",
    "animation",
    "fx",
    "compositing",
    "storyboard",
    "montage",
    "animatic",
    "music",
    "voice",
    "sound_mix",
    "blocking",
    "rendering",
)


def _set_render_setting_for_playblast(fstart=None, fend=None, filepath=None):
    """Выставляет настройки для плейбласта. 

    Parameters
    ----------
    fstart : str, float, int, optional
        Значение для bpy.context.scene.fstart:

        * None - устанавливаться не будет.
        * float, int - будет установлено это значение.
        * 'ENV' - значение будет взято из переменной окружения :ref:`CEREBRO_B3D_CURRENT_PROJECT_FSTART`

    fend : str, float, int, optional
        Значение для bpy.context.scene.fend:

        * None - устанавливаться не будет.
        * float, int - будет установлено это значение.
        * 'ENV' - значение будет взято из переменной окружения :ref:`CEREBRO_B3D_SCENE_FEND` + стартовое значение.

    filepath : str, optional

        * Путь сохранения видеофайла, параметр для *bpy.context.scene.render.filepath*
        * Если не передавать значение будет взято из :ref:`CEREBRO_B3D_CURRENT_ASSET_NAME` + ".mp4"

    Returns
    -------
    None
    """
    try:
        if fstart is not None:
            if isinstance(fstart, int) or isinstance(fstart, int):
                bpy.context.scene.frame_start=int(fstart)
            elif fend=="ENV":
                bpy.context.scene.frame_start=int(float(os.environ.get('CEREBRO_B3D_CURRENT_PROJECT_FSTART')))
        if fend is not None:
            if isinstance(fend, int) or isinstance(fend, float):
                bpy.context.scene.frame_end=int(fend)
            elif fend=="ENV":
                bpy.context.scene.frame_end=int(float(os.environ.get('CEREBRO_B3D_SCENE_FEND')))
        #
        bpy.context.scene.render.resolution_x=int(float(os.environ["CEREBRO_B3D_CURRENT_PROJECT_WIDTH"]))
        bpy.context.scene.render.resolution_y=int(float(os.environ["CEREBRO_B3D_CURRENT_PROJECT_HEIGHT"]))
        bpy.context.scene.render.fps=int(float(os.environ["CEREBRO_B3D_CURRENT_PROJECT_FPS"]))
        bpy.context.scene.render.fps_base=1
        if not filepath:
            bpy.context.scene.render.filepath=f'//{os.environ["CEREBRO_B3D_CURRENT_ASSET_NAME"]}.mp4'
        else:
            bpy.context.scene.render.filepath=filepath
        bpy.context.scene.render.image_settings.file_format="FFMPEG"
        bpy.context.scene.render.ffmpeg.format="MPEG4"
        bpy.context.scene.render.ffmpeg.codec="H264"
        bpy.context.scene.render.ffmpeg.constant_rate_factor="VERYLOW"
        bpy.context.scene.render.ffmpeg.ffmpeg_preset="GOOD"
        bpy.context.scene.render.ffmpeg.gopsize=15
        bpy.context.scene.render.ffmpeg.use_max_b_frames=False
        bpy.context.scene.render.ffmpeg.audio_codec="MP3"
        bpy.context.scene.render.ffmpeg.audio_channels="STEREO"
        bpy.context.scene.render.ffmpeg.audio_mixrate=48000
        bpy.context.scene.render.ffmpeg.audio_bitrate=192
        bpy.context.scene.render.ffmpeg.audio_volume=1
        bpy.context.scene.render.image_settings.color_mode="RGB"
        bpy.context.scene.render.use_render_cache=False
        bpy.context.scene.render.use_file_extension=True
        #(METADATA)
        bpy.context.scene.render.metadata_input="SCENE"
        bpy.context.scene.render.use_stamp=True
        # bpy.context.scene.render.stamp_font_size=48
        bpy.context.scene.render.use_stamp_date=True
        bpy.context.scene.render.use_stamp_time=False
        bpy.context.scene.render.use_stamp_render_time=False
        bpy.context.scene.render.use_stamp_frame=True
        bpy.context.scene.render.use_stamp_frame_range=False
        bpy.context.scene.render.use_stamp_memory=False
        bpy.context.scene.render.use_stamp_hostname=False
        bpy.context.scene.render.use_stamp_camera=False
        bpy.context.scene.render.use_stamp_lens=True
        bpy.context.scene.render.use_stamp_scene=False
        bpy.context.scene.render.use_stamp_marker=False
        bpy.context.scene.render.use_stamp_filename=False
        # bpy.context.scene.render.use_stamp_sequencer_strip=True
        bpy.context.scene.render.use_stamp_labels=False
        bpy.context.scene.render.use_stamp_note = True
        if G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]][dbtypes.TASK_DATA_ACTIVITY_NAME]=='animatic':
            bpy.context.scene.render.use_stamp_sequencer_strip=True
            bpy.context.scene.render.stamp_note_text = G.task_dict[os.environ["CEREBRO_B3D_CURRENT_TASK_ID"]][dbtypes.TASK_DATA_ACTIVITY_NAME]
        else:
            bpy.context.scene.render.use_stamp_sequencer_strip=False
            bpy.context.scene.render.stamp_note_text = f"{os.environ['CEREBRO_B3D_CURRENT_ASSET_NAME']}-{G.task_dict[os.environ['CEREBRO_B3D_CURRENT_TASK_ID']][dbtypes.TASK_DATA_ACTIVITY_NAME]}"


    except Exception as e:
        print(f"{'#'*50}\n{e}")
    else:
        print(f'{"#"*50} _set_render_setting_for_playblast()')


def _save_commit_data(context, task):
    """Запись в ``meta``

    * позиции всех арматур
    * имена экшенов на арматурах
    * ROTATE MODE всех костей
    * все ``Child OF`` онстрейны
    * все ``Follow Path`` констрейны

    Parameters
    ----------
    context : bpy.context
        context

    Returns
    -------
    None
    """
    data=dict()
    overide_doubles=list()
    #(ANIMATORS_COLLECTION)
    animators_collection=None
    if settings.ANIMATORS_COLLECTION in bpy.data.collections:
        animators_collection=bpy.data.collections[settings.ANIMATORS_COLLECTION]
    logger.info(f"animators_collection: {animators_collection}")
    #()
    for ob in context.scene.objects:
        if ob.type=="ARMATURE" and ob.override_library:
            if ob.name.split(".")[0]!=ob.name:
                if not ob.name in overide_doubles:
                    overide_doubles.append(ob.name)
        if ob.type=="ARMATURE" or (ob.type=="CAMERA" and context.scene.camera==ob) or (animators_collection!=None and ob.name in animators_collection.objects):
            logger.info(ob.name)
            #(BONE DATA)
            bd=dict()
            ch_offs=dict()
            follow_paths=dict()
            if ob.type=="ARMATURE":
                for b in ob.pose.bones:
                    bd[b.name]=b.rotation_mode
                    if b.constraints:
                        ch_offs[b.name]=dict()
                        follow_paths[b.name]=dict()
                        for c in b.constraints:
                            if c.type=="CHILD_OF" and c.target and c.target.name!=ob.name:
                                c_data=dict()
                                if c.target:
                                    c_data['target']=c.target.name
                                else:
                                    c_data['target']=c.target
                                c_data['subtarget']=c.subtarget
                                c_data['use_location_x']=c.use_location_x
                                c_data['use_location_y']=c.use_location_y
                                c_data['use_location_z']=c.use_location_z
                                c_data['use_rotation_x']=c.use_rotation_x
                                c_data['use_rotation_y']=c.use_rotation_y
                                c_data['use_rotation_z']=c.use_rotation_z
                                c_data['use_scale_x']=c.use_scale_x
                                c_data['use_scale_y']=c.use_scale_y
                                c_data['use_scale_z']=c.use_scale_z
                                ch_offs[b.name][c.name]=c_data
                            elif c.type=="FOLLOW_PATH":
                                c_data=dict()
                                if c.target:
                                    c_data['target']=c.target.name
                                else:
                                    c_data['target']=c.target
                                c_data['forward_axis']=c.forward_axis
                                c_data['up_axis']=c.up_axis
                                c_data['use_fixed_location']=c.use_fixed_location
                                c_data['use_curve_radius']=c.use_curve_radius
                                c_data['use_curve_follow']=c.use_curve_follow
                                c_data['influence']=c.influence
                                follow_paths[b.name][c.name]=c_data
                                
            #(OTHER)
            rm=ob.rotation_mode
            if rm=="QUATERNION":
                r=ob.rotation_quaternion[:]
            else:
                r=ob.rotation_euler[:]
            t=ob.location[:]
            s=ob.scale[:]
            if ob.animation_data and ob.animation_data.action:
                a=ob.animation_data.action.name
            else:
                a=None
            if ob.type=="CAMERA":
                data["Shot_camera"]=(rm,t,r,s,a, ob.data.lens, ob.data.lens_unit)
            elif ob.type=="ARMATURE":
                data[ob.name]=(rm,t,r,s,a,bd,ch_offs,follow_paths)
            else:
                data[ob.name]=(rm,t,r,s,a)
            #(doubles)
            data["overide_doubles"]=overide_doubles

    b,meta=path_utils.get_internal_task_folder_path(task, "meta")
    if not b:
        raise Exception(meta)
    data["file_path"]=os.path.join(meta, settings.COMMIT_DATA_FILE)
    settings._write_setting_data(**data)


def _c_driver(context, rig, bone, constraint):
    data_path=f'pose.bones["{bone.name}"].constraints["{constraint.name}"].offset_factor'
    
    if rig.animation_data:
        fc=rig.animation_data.drivers.find(data_path)
        if fc:
            print(fc)
            fc.driver.expression=fc.driver.expression
            return
    f_curve=rig.driver_add(data_path)
    drv=f_curve.driver
    drv.type="SCRIPTED"
    drv.use_self=False
    drv.expression='path+offset'
    
    var=drv.variables.new()
    var.name='path'
    var.type="SINGLE_PROP"
    targ=var.targets[0]
    targ.id=rig
    targ.data_path=f'pose.bones["CTRL_root"]["path"]'
    
    var=drv.variables.new()
    var.name='offset'
    var.type="SINGLE_PROP"
    targ=var.targets[0]
    targ.id=rig
    targ.data_path=f'pose.bones["CTRL_root"]["offset"]'


def _rename_world_data_block_of_location(context, task):
    """Переименование ``World`` дата блока локации от названия ассета если он имеет дефолтовое название. """
    try:
        p=srvconn.get_parent_task(task)
        if p[dbtypes.TASK_DATA_ACTIVITY_NAME] in ("Location",):
            if context.scene.world.name=='World':
                context.scene.world.name=f"{os.environ.get('CEREBRO_B3D_CURRENT_ASSET_NAME')}_World"
    except:
        pass


def fix_follow_path_driver(context):
    """Восстановление драйверов для tutu ригов. """
    for ob in bpy.data.objects:
        if ob.type=="ARMATURE":
            root_names=["CTRL_root", "root"]
            for n in root_names:
                if n in ob.pose.bones:
                    root=ob.pose.bones[n]
                    c=None
                    for con in root.constraints:
                        if con.type=="FOLLOW_PATH":
                            c=con
                    if c and "path" in root and "offset" in root:
                        _c_driver(context, ob, root, c)


def post_open(task, look):
    """
    Действия после открытия рабочей сцены.

    Parameters
    ----------
    task : task
        Текущая задача.
    look : bool
        Если look=True то статус задачи меняться не будет.

    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)
    """
    #(fix ui)
    bpy.context.scene.ftrack_auth_current_user = os.environ["CEREBRO_B3D_AUTH_USER"]
    bpy.context.scene.ftrack_projects=os.environ["CEREBRO_B3D_CURRENT_PROJECT_ID"]
    if os.environ.get("CEREBRO_B3D_ROLE"):
        bpy.context.scene.ftrack_role=os.environ.get("CEREBRO_B3D_ROLE")

    #(render settings)
    # if task["type"]["name"] in ANIMATION_TYPES:
    if G.task_dict[str(task[dbtypes.TASK_DATA_ID])][dbtypes.TASK_DATA_ACTIVITY_NAME] in ANIMATION_TYPES:
        _set_render_setting_for_playblast()

    #(fix tutu rigs)
    fix_follow_path_driver(bpy.context)

    #(ANIMATORS_COLLECTION)
    tools.make_excipients_collection(bpy.context)

    #(chasnge status)
    if look:
        return(True, "Ok!")
    else:
        return srvconn.task_status_to_in_progress(task)

    #(обнуление начальных данных)
    G.future_render_commit_num=None
    G.future_commit_num=None
    G.components=tuple()


def pre_commit(context, task, description, status_name):
    """ """
    #(cleaning fields)
    context.scene.ftrack_api_key=""
    context.scene.ftrack_api_user=""
    #(relative paths)
    bpy.ops.file.make_paths_relative()
    #(rename world)
    _rename_world_data_block_of_location(context, task)
    #(text data block)
    settings.write_commit_data_to_text_data_block(context, task, description)
    #(saving commit data)
    _save_commit_data(context, task)
    # Clean Up
    bpy.data.orphans_purge(do_recursive=True)
    G.future_commit_num=None
    return(True, "Ok!")