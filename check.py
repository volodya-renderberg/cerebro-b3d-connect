# -*- coding: utf-8 -*-

"""Проверки."""

import os

import bpy

from pycerebro import dbtypes

try:
    import path_utils
    import server_connect as srvconn
except:
    from . import path_utils
    from . import server_connect as srvconn

CHECK_SHADING_STATUSES=(
    "modeling",
    "shading",
    "rigging",
    "layout"
)

def _path_test(textures_folder, tested_path) -> bool:
    if tested_path in ('', '/'):
        return False
    elif not os.path.exists(tested_path):
        return False
    elif os.path.samefile(textures_folder, tested_path):
        return True
    else:
       return _path_test(textures_folder, os.path.dirname(tested_path))

def _ascii_test(name) -> bool:
    return all(tuple(map(lambda x: True if ord(x)<128 else False, name)))

def check(task, context) -> tuple:
    """Проверки в соответствии с типом задачи. """
    ##
    all_right=True
    uncollected_textures=[] # текстуры не собранные в textures
    no_ascii=[] # объекты содержащие не ascii символы в именовании
    rig_collection=False
    geo_collection=False # наличие коллекций имена которых начинаются с имени ассета.
    ##
    test_data={
    "Текстуры не в \"textures\" задачи":uncollected_textures,
    "Латиница в именах": no_ascii,
    f"Коллекция {srvconn.parent_name(task)}_rig": rig_collection,
    f"Коллекция {srvconn.parent_name(task)}_geo": geo_collection,
    }
    print("*** Check file ***")

    #("AssetModel", "Location", "Shot")
    asset_type=srvconn.get_asset_type_of_task(task)
    if asset_type in ("AssetModel", "Location", "Shot"):
        #(uncollected_textures)
        print("* Текстуры не в \"textures\" задачи:")
        if task[dbtypes.TASK_DATA_ACTIVITY_NAME] in CHECK_SHADING_STATUSES:
            b, textures = path_utils.get_folder_of_textures_path(task)
            if not b:
                return(b, textures)

            for img in bpy.data.images:
                # if img.packed_file or img.library:
                #     continue
                # elif img.name=="Render Result":
                #     continue
                if any((
                    img.packed_file,
                    img.library,
                    img.name=="Render Result"
                )):
                    continue
                else:
                    b = _path_test(textures, img.filepath_from_user())
                    if not b:
                        uncollected_textures.append(img.__repr__())
                        all_right=False
                        print(f"\t* {img.name}: {img.filepath}")

        #(ascii)
        print("* Латиница в именах:")
        testing_collections=(bpy.data.objects, bpy.data.meshes, bpy.data.collections, bpy.data.armatures, bpy.data.materials)
        for collection in testing_collections:
            for ob in collection:
                if not _ascii_test(ob.name):
                    ob_string=ob.__repr__()
                    no_ascii.append(ob_string)
                    all_right=False
                    print(f"\t* {ob_string}")

        #(exists_asset_collections)
        if task[dbtypes.TASK_DATA_ACTIVITY_NAME] in ("rigging",) or asset_type=="Location":
            for collection in bpy.data.collections:
                if collection.name==f"{srvconn.parent_name(task)}_rig":
                    rig_collection=True
                if collection.name==f"{srvconn.parent_name(task)}_geo":
                    geo_collection=True

            if not rig_collection:
                test_data[f"Коллекция {srvconn.parent_name(task)}_rig"]="Не найдена"
                all_right=False
            else:
                test_data[f"Коллекция {srvconn.parent_name(task)}_rig"]=False

            if not geo_collection:
                test_data[f"Коллекция {srvconn.parent_name(task)}_geo"]="Не найдена"
                all_right=False
            else:
                test_data[f"Коллекция {srvconn.parent_name(task)}_geo"]=False


        if task[dbtypes.TASK_DATA_ACTIVITY_NAME] in ("modeling", "shading", "texturing"):
            for collection in bpy.data.collections:
                if collection.name==f"{srvconn.parent_name(task)}_geo":
                    geo_collection=True

            if not geo_collection:
                test_data[f"Коллекция {srvconn.parent_name(task)}_geo"]="Не найдена"
                all_right=False
            else:
                test_data[f"Коллекция {srvconn.parent_name(task)}_geo"]=False

        print(f"* All check - {all_right}\n")

    return(all_right, test_data)